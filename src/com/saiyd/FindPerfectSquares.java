package com.saiyd;

public class FindPerfectSquares {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		for(int i=1; i < 101; i++) {
			int square = i * i;
			if(square <= 100)
				System.out.println(square);
		}
	}

}
