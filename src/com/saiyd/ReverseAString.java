package com.saiyd;

import java.util.Stack;

public class ReverseAString {
	public static String reverse(String input) {
		System.out.println("input = " + input);
		StringBuffer output = new StringBuffer();
		for(int i = input.length() -1; i > -1; i--) {
			output.append(input.charAt(i));
		}
		System.out.println("output = " + output.toString());
		return output.toString();
	}
	
	public static String reverseUsingStack(String input) {
		System.out.println("input = " + input);
		
		Stack<Character> outputStack = new Stack<Character>();
		for(int i = 0; i < input.length(); i++) {
			outputStack.push(input.charAt(i));
		}
		
		StringBuffer output = new StringBuffer();

		while(!outputStack.empty()) {
			output.append(outputStack.pop());
		}
		System.out.println("output = " + output.toString());
		return output.toString();
	}
	
	public static String reverseWords(StringBuffer input) {
		System.out.println("input = " + input);
		input = new StringBuffer(reverse(input.toString()));
		
		// search for all spaces then reverse the words
		int sIndex = 0;
		int eIndex = 0;
		while(sIndex < input.length() && eIndex > -1) {
			eIndex = input.indexOf(" ", sIndex);
			if(eIndex > 0) {
				eIndex--;
			} else {
				eIndex = input.length() - 1;
			}
			reverseSubString(sIndex, eIndex, input);
			
			sIndex = eIndex + 2;
		}
		
		System.out.println("output = " + input.toString());
		return input.toString();
	}
	
	public static void reverseSubString(int sIndex, int eIndex, StringBuffer input) {
		if(sIndex < eIndex && sIndex > -1 && eIndex < input.length()) {
			while(sIndex < eIndex) {
				Character c = input.charAt(sIndex);
				input.setCharAt(sIndex, input.charAt(eIndex));
				input.setCharAt(eIndex, c);
				
				sIndex++;
				eIndex--;
			}
		}
		System.out.println(input.toString());
	}
	
	public static void main(String[] args) {
		String s1= "saiyd likes beans";
		String s2= "The walking dead is a scary show";
		
		//s1 = reverse(s1);

		//s1 = reverseUsingStack(s1);
		
		reverseWords(new StringBuffer(s2));
	}
}
