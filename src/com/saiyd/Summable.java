package com.saiyd;

public interface Summable<T> {
	T sum(T other);
}
