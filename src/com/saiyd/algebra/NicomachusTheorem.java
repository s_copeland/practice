package com.saiyd.algebra;

import java.util.ArrayList;
import java.util.List;

/*
 * http://mathworld.wolfram.com/NicomachussTheorem.html
 * 
 * The nth cubic number n^3 is a sum of n consecutive odd numbers, for example
 * 
 * 1^3	=	1	
 * 2^3	=	3+5	
 * 3^3	=	7+9+11	
 * 4^3	=	13+15+17+19,
 * 	
 * etc. This identity follows from
 * 
 * sigma_(i=1 to n) of [n(n-1)-1+2i] = n^3. 	
 * 
 * It also follows from this fact that
 *
 * In number theory, the sum of the first n cubes is the square of the nth triangular number.
 *
 *  sigma_(k=1 to n) of k^3 = (sigma_(k=1 to n) of k)^2 
 */
public class NicomachusTheorem {

	/*
	 * 
	 */
	public List<Integer> findLargestSequenceOfOddNumsThatSumTo(int nThCube) {
		List<Integer> ret = new ArrayList<Integer>();
		int cur = 0;
		
		for(int i = 1; i <= nThCube; i++) {
			cur = nThCube * (nThCube - 1) -1 + (2 * i);
			ret.add(cur);
		}
		
		return ret;
	}
	
	public static void main(String[] args) {
		NicomachusTheorem nt = new NicomachusTheorem();
		for(int i = 1; i <= 10; i++) {
			System.out.println(nt.findLargestSequenceOfOddNumsThatSumTo(i));
		}
	}
}
