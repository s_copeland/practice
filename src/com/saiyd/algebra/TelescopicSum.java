package com.saiyd.algebra;

/*
 * A sum in which subsequent terms cancel each other, leaving only initial and final terms.
 * 
 * http://mathworld.wolfram.com/TelescopingSum.html
 * 
 * S = SUM from i=1 to n-1 OF (ai - ai+1)
 *   = (a1 - a2) + (a2 -a3) + (a3 -a4) + ... + (an-1 - an)
 *   = (a1 -an) 
 */
public class TelescopicSum {

}
