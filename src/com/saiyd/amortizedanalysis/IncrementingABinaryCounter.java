package com.saiyd.amortizedanalysis;

import java.util.BitSet;

/*
 * A k-bit binary counter that counts upward from 0.
 * We use an array A[] of bits. The lowest order bit is
 * at index 0 and the highest order bit at size -1.
 * 
 * The value of the binary counter is derives as
 * 
 * x = Sigma(from i=0 to k-1) of (A[i} *2^i)
 * 
 * Example of a binary counter incremented 16 times
 * 
 * The arrow shows the bits that flip to achieve the next value.
 * 
 * Counter						Total
 * value						 cost
 * 	0		0 0 0 0 0 0 0 0		0
 * 			              ^
 *  1		0 0 0 0 0 0 0 1		1
 *  		            ^ ^
 *  2		0 0 0 0 0 0 1 0		3
 *  		              ^
 *  3		0 0 0 0 0 0 1 1		4
 *  		          ^ ^ ^
 *  4		0 0 0 0 0 1 0 0		7
 *  		              ^
 *  5		0 0 0 0 0 1 0 1		8
 *  		            ^ ^
 *  6		0 0 0 0 0 1 1 0		10
 *  		              ^
 *  7		0 0 0 0 0 1 1 1		11
 *  		        ^ ^ ^ ^
 *  8		0 0 0 0 1 0 0 0		15
 *  		              ^
 *  9		0 0 0 0 1 0 0 1		16
 *  		            ^ ^
 *  10		0 0 0 0 1 0 1 0		18
 *  		              ^
 *  11		0 0 0 0 1 0 1 1		19
 *  		          ^ ^ ^
 *  12		0 0 0 0 1 1 0 0		22
 *  		              ^
 *  13		0 0 0 0 1 1 0 1		23
 *  		            ^ ^
 *  14		0 0 0 0 1 1 1 0		25
 *  		              ^
 *  15		0 0 0 0 1 1 1 1		26
 *  		      ^ ^ ^ ^ ^
 *  16		0 0 0 1 0 0 0 0		31
 *  		              ^
 */
public class IncrementingABinaryCounter {

	/*
	 * At the start of each iteration of the while loop, we wish to add
	 * a 1 into position i. If input[i] = 1, then adding 1 flips the bit
	 * to 0 in position i and yields a carry of 1, to be added into position
	 * i+1 on the next iteration of the loop. Otherwise, the loop ends
	 * and then, if i < size, we know that input[i] = 0, so that adding
	 * a 1 into position i is taken care of by the if statement.
	 * 
	 * The cost of each increment operation is liner in the number of
	 * bits flipped.
	 * 
	 * A cursory analysis yields a bound that is correct but not tight.
	 * A single execution of increment takes time theta(k) in the worst
	 * case, in which array input contains all 1's. Thus, a sequence of n
	 * increment operations on an initially zero counter takes time O(n*k)
	 * in the worst case.
	 */
	public void increment(BitSet input) {
		int i = 0;
		while(i < input.size() && input.get(i)) {
			input.set(i, false);
			i++;
		}
		
		if(i < input.size())
			input.set(i, true);
	}

	public static void main(String[] args) {
		IncrementingABinaryCounter c = new IncrementingABinaryCounter();
		BitSet b = new BitSet();
		print(b);
		for(int i = 0; i < 16; i++)
		{
			c.increment(b);
			print(b);
		}
	}

	private static void print(BitSet b)
	{
		StringBuilder s = new StringBuilder();
		for( int i = b.size() -1; i > -1;  i--)
		{
			s.append(b.get( i ) == true ? 1: 0 );
		}

		System.out.println( s );
	}
	/*
	 * Aggregate Analysis
	 * 
	 * We can tighten our analysis to yield a worst-case cost of O(n) for a
	 * sequence of n increment's by observing that not all bits flip each time
	 * increment is called. As shown above, input[0] does flip each time increment
	 * is called. The next highest order bit, input[1] flips only every other time:
	 * a sequence of n increment operations on an initially zero counter causes 
	 * input[1] to flip floor(n/2) times. Similarly bit input[2] flips only every
	 * fourth time, or floor(n/4) times in a sequence of n increments.
	 * 
	 * In general, for i = 0,1 ... floor(lg n), bit input[i] flips floor(n/(2^i)) times
	 * in a sequence of n increment operations on an initially zero counter.
	 * For i > floor(lg n), bit input[i] never flips at all. The total number of flips
	 * in the sequence is thus
	 * 
	 * sigma(from i=0 to floor(lg n)) of ( floor(n/(2^i)) )
	 * 	<
	 * n*sigma(from i=0 to positive infinity) of ( 1/(2^i) )
	 * 
	 * The right side of the equations is equal to 2n
	 * 
	 * The worst case time for a sequence of n increment operations on an initially zero
	 * counter is therefore O(n). The average cost of each operation, and
	 * therefore the amortized cost per operation, is O(n)/n = O(1)
	 */
	
	/* Accounting method
	 * 
	 * For amortized analysis, let us charge an amortized cost of 2 dollars to set a bit to
	 * 1. When a bit is set, we use 1 dollar (out of the 2 dollars charged) to pay for
	 * the actual setting of the bit, and we place the other dollar on the bit as credit to
	 * be used later when we flip the bit back to 0. At any point in time, every 1 in the 
	 * counter has a dollar of credit on it, and thus we needn't charge anything to reset a
	 * bit to 0; we just pay for the reset with the dollar bill on the bit.
	 * 
	 * The amortized cost of increment can now be determined. The cost of resetting the bits
	 * within the while loop is paid for by the dollars on the bits that are reset. At most
	 * one bit is set, by the if statement, and therefore the amortized cost of an increment
	 * operation is at most 2 dollars. The number of 1's in the counter is never negative, and
	 * thus the amount of credit is always nonnegative. Thus, for n increment operations, the 
	 * total amortized cost is O(n), which bounds the total actual cost.
	 */
	
	/*
	 * Potential method
	 * 
	 * We define the potential of the counter after the ith increment operation to be bi, the
	 * number of 1's in the counter after the ith operations.
	 * 
	 * Let us compute the amortized cost of an increment operation. Suppose that the ith
	 * increment operation resets ti bits. The actual cost of the operation is therefore
	 * at most ti + 1, since in addition to resetting ti bits, it sets at most one bit to 1.
	 * If bi = 0, then the ith operation resets all k bits, and so 
	 * 
	 * 		bi-1 = ti = k. If bi > 0,
	 * 
	 * then 
	 * 		bi = bi-1 - ti + 1
	 * 
	 * In either case,
	 * 
	 * 		bi <= bi-1 - ti + 1 
	 * 
	 * and the potential difference is
	 * 
	 * 		Phi(Di) - Phi(Di-1) = (bi-1 - ti + 1) - bi-1
	 * 						    = 1 - ti
	 * 
	 * The amortized cost is therefore
	 * 
	 * 		ai = ci + Phi(Di) - Phi(Di-1)
	 * 		   <= (ti + 1) + (1 - ti)
	 * 		   = 2
	 * 
	 * If the counter starts at zero, then Phi(D0) = 0. Since Phi(Di) >= 0 for all i, the total
	 * amortized cost of a sequence of n increment operations is an upper bound on the total
	 * actual cost, and so the worst-case cost of n increment operations is O(n).
	 * 
	 * The potential method gives us an easy way to analyze the counter even when it does not
	 * start at zero. There are initially b0 1's, and after n increment operations there are
	 * bn 1's, where 0 <= b0, bn <= k (k is the number of bits in the counter) We can rewrite
	 * 
	 * 		(sigma(from i=1 to n) of (ai)) = (sigma(from i=1 to n) of (ci) + Phi(Dn) - Phi(D0)
	 * 
	 * as
	 * 		(sigma(from i=1 to n) of (ci)) = (sigma(from i=1 to n) of (ai) - Phi(Dn) + Phi(D0)
	 * 
	 * We have ai <= 2 for all 1 <= i <= n. Since Phi(D0) = b0 and Phi(Dn) = bn, the total
	 * actual cost of n increment operations is
	 * 
	 * 		(sigma(from i=1 to n) of (ci)) = (sigma(from i=1 to n) of (2) - bn + b0
	 * 									   = 2n - bn + b0
	 * 
	 * Note in particular that since b0 <= k, as long as k = O(n), the total actual cost is
	 * O(n). In other words, if we execute at least n = Omega(k) increment operations, the
	 * total actual cost is O(n), no matter what initial value the counter contains.
	 */
}
