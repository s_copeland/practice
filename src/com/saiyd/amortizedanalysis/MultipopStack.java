package com.saiyd.amortizedanalysis;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class MultipopStack <T> extends Stack<T> {
	/*
	 * push -> O(1)
	 * pop -> O(1)
	 * multipop -> O(n)
	 * 		The number of iterations of the while loop is the number min(numberToPop,stackSize)
	 * 
	 * The worst-case cost of a multipop operation in 
	 * the sequence is O(n), since the stack size is at 
	 * most n. The worst-case time of any stack operation
	 * is therefore O(n), and hence a sequence of n 
	 * operations costs O(n^2), since we may have O(n)
	 * multipop operations costing O(n) each. Although
	 * this analysis is correct, the O(n^2) result, obtained 
	 * by considering the worst-case cost of each operation
	 * individually, is not tight.
	 * 
	 * Using aggregate analysis, we can obtain a better
	 * upper bound that considers the entire sequence of n
	 * operations. In fact, although a single multipop operation
	 *  
	 */
	public List<T> multipop(int numberToPop) {
		List<T> ret = new ArrayList<T>();
		while(!this.isEmpty()) {
			ret.add(this.pop());
		}
		return ret;
	}
	/* Aggregate Analysis
	 * 
	 * In fact, although a single multipop operation can be expensive, any sequence of N
	 * push, pop, and multipop operations on an initially empty stack can cost at most O(n).
	 * Why?
	 * 
	 * Each object can be popped at most once for each time it is pushed. Therefore, the
	 * number of times that pop can be called on a nonempty stack, including calls within
	 * multipop, is at most the number of push operations, which is at most n.
	 * 
	 * For any value of n, any sequence of n push, pop, multipop operations takes a total of
	 * O(n) time.
	 * 
	 * The average cost of an operation is O(n)/n = O(1). In aggregate analysis we assign the
	 * amortized cost of each operation to be the average cost. Therefore all three stack 
	 * operations have an amortized cost of O(1).
	 * 
	 */
	
	/* Accounting method
	 * 
	 * push = 1
	 * pop = 1
	 * multipop = min(numberToPop, size)
	 * 
	 * Lets assume the folowing cost for the operations instead of the original.
	 * 
	 * push = 2
	 * pop = 0
	 * multipop = 0
	 * 
	 * We shall now show that we can pay for any sequence of stack operations by charging
	 * the amortized cost above. Suppose we use a dollar bill to represent each unit of cost.
	 * We start with an empty stack. Recall the analogy of plates in a cafeteria for a stack
	 * data structure. When we push a plate on the stack, we use 1 dollar to pay the actual
	 * cost of the push and are left with a credit of 1 dollar(out of the 2 dollars charged),
	 * which we put on top of the plate. At any point in time, every plate on the stack has
	 * a dollar of credit on it.
	 * 
	 * The dollar stored on the plate is prepayment for the cost of popping from the stack.
	 * When we execute a pop operation, charge the operation nothing and pay its actual cost
	 * using the credit stored in the stack. To pop a plate, we take the dollar of credit
	 * off the plate and use it to pay the actual cost of the operation. Thus, by charging
	 * the push operation a little bit more, we needn't charge the pop operation anything.
	 * 
	 * Moreover, we needn't charge the multipop operations anything either. To pop the
	 * first plate, we take the dollar of credit off the plate and use it to pay the actual
	 * cost of a pop operation. To pop a second plate, we again have a dollar of credit on
	 * the plate to pay for the pop operation, and so on. Thus, we have always charged 
	 * enough up front to pay for multipop operations. We have ensured the amount of credit
	 * is always nonnegative. Thus for any sequence of n push, pop, multipop oerations, the
	 * total amortized cost is an upper bound on the total actual cost. Since the total
	 * amortized cost is O(n), so it the total actual cost.
	 */
	
	/*
	 * Potential method
	 * 
	 * We define the potential function Phi on a stack to be the number of objects in the stack.
	 * For an empty stack Phi(D0) = 0. Since the number of objects in the stack is never
	 * negative, the stack Di that results after the ith operation has nonnegative potential,
	 * and thus
	 * 
	 * 		Phi(Di) >= 0
	 * 			     = Phi(D0)
	 * 
	 * The total amortized cost of n operations with respect to Phi therefore represents an
	 * upper bound on the actual cost.
	 * 
	 * Let us now compute the amortized costs of the various stack operations. If the ith
	 * operation on a stack containing s objects is a push operation, then the potential difference
	 * is
	 * 		Phi(Di) - Phi(Di-1) = (s + 1) - s
	 * 						    = 1
	 * 
	 * The amortized cost of the ith operation is defined as
	 * 
	 * 		ai = ci + Phi(Di) - Phi(Di-1)
	 * 
	 * So, the amortized cost of this push operation is
	 * 
	 * 		ai = 1 + 1
	 * 		ai = 2
	 * 
	 * Suppose that the ith operation on the stack is multipop(s,k) and that j = min(s, k) objects
	 * are popped off the stack. The actual cost of the operation is j, and the potential difference
	 * is
	 * 		Phi(Di) - Phi(Di-1) = -j
	 * 
	 * Thus, the amortized cost of the multipop operation is 
	 * 
	 * 		ai = ci + Phi(Di) - Phi(Di-1)
	 * 		   = j - j
	 * 		   = 0
	 * 
	 * Similarly, the amortized cost of an ordinary pop operation is 0. The amortized cost of each
	 * of the three operations is O(1), and thus the total amortized cost of a sequence of n
	 * operations is O(n). 
	 */
}
