package com.saiyd.amortizedanalysis;
/*
 * Amortized anaylsis techniques
 * 
 * 1. Aggregate Analysis
 * 		We determine an upper bound T(n) on the total cost of a sequence of
 * 		n operations.
 * 
 * 		The average cost per operation is then T(n)/n.
 * 
 * 		We take the average cost as the amortized cost of each operation,
 * 		so that all operations have the same amortized cost.
 * 
 * 2. Accounting method
 * 		We determine an amortized cost of each operation. When there is
 * 		more than one type of operation, each type of operation may
 * 		have a different amortized cost.
 * 
 * 		This method overcharges some operations early in the sequence, storing
 * 		the overcharge as "prepaid credit" on specific objects in the data structure.
 * 		The credit is used later in the sequence to pay for operations that
 * 		are charged less than they actually cost.
 * 
 * 		One can view the amortized cost of an operation as being split between its
 * 		actual cost and credit that is either deposited or used up.
 * 
 *  	We must choose the amortized cost of operations carefully. If we want
 *  	analysis with amortized costs to show that in the worst case the average
 *  	cost per operation is small, the total amortized cost of a sequence of
 *  	operations must be an upper bound on the total actual cost of the sequence.
 *  	Moreover, as in aggregate analysis, this relationship must hold for all
 *  	sequences of operations. If we denote the actual cost of the ith by ci
 *  	and the amortized cost of the ith operation by ai, we require
 *  
 *  		(sigma(from i=1 to n) of (ai)) >=  (sigma(from i=1 to n) of (ci))
 *  
 *  	for all sequences of n operations. The total credit stored in the data
 *  	structure is the difference between the total amortized cost and th total 
 *  	actual cost.
 *  
 *  	If the total credit were ever allowed to become negative(the result of
 *  	undercharging early operations with the promise of repaying the account later
 *  	on), then the total amortized cost incurred at that time would be below the
 *  	total actual costs incurred; for the sequence of operations up to that time,
 *  	the total amortized cost would not be an upper bound on the total actual cost.
 *  	Thus, we must take care that the total credit in the data structure never
 *  	becomes negative.
 * 		
 * 
 * 3. Potential method
 * 		Like the accounting method, we determine the amortized cost of each
 * 		operation and may overcharge operations early on to compensate for 
 * 		undercharges later.
 * 
 * 		The potential method maintains the credit as the "potential energy"
 * 		of the data structure as a whole instead of associating the credit
 * 		with individual objects within the data structure.
 * 
 * 		We start with an initial data structure D0 on which n operations are performed.
 * 		For each i = 1,2,...,n we let ci be the actual cost of the ith operation and
 * 		Di be the data structure that results after applying the ith operations to
 * 		data structure Di-1. 
 * 
 * 		A potential function Phi maps each data structure Di to a real number Phi(Di),
 * 		which is the potential associated with data structure Di.
 * 
 * 		The amortized cost, ai, of the ith operation with respect to potential to
 * 		potential function Phi is defined by
 * 
 * 			ai = ci + Phi(Di) - Phi(Di-1)
 * 
 * 		The amortized cost of each operation is therefore its actual cost plus the
 * 		increase in potential due to the operation. By the previous equation, the total
 * 		amortized cost of the n operations is
 * 
 * 			(sigma(from i=1 to n) of (ai)) = (sigma(from i=1 to n) of (ci + Phi(Di) - Phi(Di-1))
 * 
 * 		The Phi terms represent a telescoping series and can be reduced to Phi(Dn) - Phi(D0)
 * 
 * 			(sigma(from i=1 to n) of (ai)) = (sigma(from i=1 to n) of (ci) + Phi(Dn) - Phi(D0)
 * 
 * 		If we can define a potential function Phi so that Phi(Dn) >= Phi(D0), then the total
 * 		amortized cost is an upper bound on the total actual cost. In practice, we do not always
 * 		know how many operations might be performed. Therefore, if we require that
 * 		Phi(Di) >= Phi(D0) for all i, then we guarantee, as in the accounting method, that we pay
 * 		in advance.It is often convenient to define Phi(D0) to be 0 and then to show that 
 * 		Phi(Di) >= 0 for all i.
 * 
 * 		Intuitively, if the potential difference Phi(Di) - Phi(Di-1) of the ith operation is
 * 		positive, then the amortized cost ai represents an overcharge of the ith operation, and
 * 		the potential of the data structure increases. if the potential difference is negative,
 * 		then the amortized cost represents an undercharge to the ith operation, and the actual
 * 		cost of the operation is paid by the decrease in the potential.
 * 
 */
public class TypesOfAnalysis {

}
