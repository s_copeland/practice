package com.saiyd.bits;

import java.util.Random;

public class AbsoluteValueWithoutBranching {

	public int abs(int input) {
		/*
		 * Since negative numbers are stored in 2’s complement form, to get the 
		 * absolute value of a negative number we have to toggle bits of the 
		 * number and add 1 to the result.
		 */
		int mask = input >> 31; // create a mask
		// if input is negative, the mask will be all 1s
		// if input if positive, the mask will be all 0s
		
		int result = input + mask;
		result ^= mask;
		return result;
	}
	public static void main(String[] args) {
		Random rand = new Random();
		AbsoluteValueWithoutBranching abs = new AbsoluteValueWithoutBranching();

		for(int i = 0; i < 10; i++) {
			int randInt = rand.nextInt();
			System.out.println(" abs(" + randInt + ") = " + abs.abs(randInt));
		}
	}

}
