package com.saiyd.bits;

import java.util.Random;

public class ConditionallyNegateWithoutBranching {
	public int negate(int input, boolean flag) {
		input = (input ^ -Boolean.compare(flag, false)) + Boolean.compare(flag, false);
		return input;
	}
	
	public static void main(String[] args) {
		Random rand = new Random();
		ConditionallyNegateWithoutBranching c = new ConditionallyNegateWithoutBranching();
		
		for(int i = 0; i <= 16; i++) {
			test(c, i, true);
			test(c, i, false);
		}
		

	}

	private static void test(ConditionallyNegateWithoutBranching c, int input, boolean flag) {
		System.out.println("negate(" + input + ", " + flag + ")");
		System.out.println(input);
		Util.print32Bit(input);
		int result = c.negate(input, flag);
		Util.print32Bit(result);
		System.out.println(result);
		System.out.println();
	}
}
