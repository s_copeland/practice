package com.saiyd.bits;

import java.util.Random;

public class ConditionallySetOrClearABitWithoutBranching {
	public int setBit(int input, int mask, boolean flag) {
		input ^= (-Boolean.compare(flag, false) ^ input) & mask; 
		return input;
	}
	
	public static void main(String[] args) {
		Random rand = new Random();
		ConditionallySetOrClearABitWithoutBranching c = new ConditionallySetOrClearABitWithoutBranching();
		int mask = 0x8;
		for(int i = 0; i <= 16; i++) {
			test(c, i, mask, true);
			test(c, i, mask, false);
		}
		

	}

	private static void test(ConditionallySetOrClearABitWithoutBranching c, int input, int mask, boolean flag) {
		System.out.println("setBit(" + input + ", " + mask + ", " + flag + ")");
		Util.print32Bit(input);
		int result = c.setBit(input, mask, flag);
		Util.print32Bit(result);
		System.out.println();
	}
}
