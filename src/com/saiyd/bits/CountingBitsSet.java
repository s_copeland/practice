package com.saiyd.bits;

import java.util.Random;

public class CountingBitsSet {

	public static int count(int input) {
		// count the number of bits set in input
		int c = 0; // c accumulates the total bits set in input
				
		// handle negative numbers
		if((input >>> 31) > 0) {
			c++; // count this 1
			// clear the MSB
			input ^= 0x80000000;
		}
		
		for (;input > 0; c++)
		{
		  input &= input - 1; // clear the least significant bit set
		}
		return c;
	}
	
	public static void main(String[] args) {
		Random rand = new Random();
		CountingBitsSet c = new CountingBitsSet();
		
		for(int i = 0; i < 10; i++) {
			int randInt1 = rand.nextInt(32);
			randInt1 = -randInt1;
			int result = c.count(randInt1);
			
			System.out.println("count(" + randInt1 + ") = " + result);
			Util.print32Bit(randInt1);
			Util.print32Bit(result);
			System.out.println();
		}
	}
}
