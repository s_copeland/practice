package com.saiyd.bits;

import java.util.Random;

public class DetermineIfAPowerOfTwo {

	public boolean isAPowOfTwo(int input) {
		if(input == 0 || input == 1)
			return false;
		return (input & (input - 1)) == 0;
	}
	
	public static void main(String[] args) {
		Random rand = new Random();
		DetermineIfAPowerOfTwo powOf2 = new DetermineIfAPowerOfTwo();
		
		for(int i = 0; i <= 16; i++) {
			test(powOf2, i);
		}
		
		for(int i = 0; i < 10; i++) {
			int randInt = rand.nextInt();
			test(powOf2, randInt);
		}
	}

	private static void test(DetermineIfAPowerOfTwo powOf2, int t) {
		System.out.println(" isAPowOfTwo(" + t + ") = " + powOf2.isAPowOfTwo(t));
	}

}
