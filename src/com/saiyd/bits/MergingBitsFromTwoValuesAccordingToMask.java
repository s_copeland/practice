package com.saiyd.bits;

import java.util.Random;

public class MergingBitsFromTwoValuesAccordingToMask {

	public int merge(int a, int b, int mask) {
		return a ^ ((a ^ b) & mask); 
	}
	
	public static void main(String[] args) {
		Random rand = new Random();
		MergingBitsFromTwoValuesAccordingToMask m = new MergingBitsFromTwoValuesAccordingToMask();

		int mask = 0x0000FFFF;
		
		for(int i = 0; i < 5; i++) {
			int randInt1 = rand.nextInt();
			int randInt2 = rand.nextInt();
			int result = m.merge(randInt1, randInt2, mask);
			
			System.out.println("min(" + randInt1 + "," + randInt2 + ") = " + result);
			Util.print32Bit(randInt1);
			Util.print32Bit(randInt2);
			Util.print32Bit(mask);
			System.out.println("---------------------------------");
			Util.print32Bit(result);
			System.out.println();
		}
	}
}
