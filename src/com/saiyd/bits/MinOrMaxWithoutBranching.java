package com.saiyd.bits;

import java.util.Random;

public class MinOrMaxWithoutBranching {

	public int min(int x, int y) {
		// I believe that Boolean.compare() does subtraction internally, so there should be no branching
		return y ^ ((x ^ y) & -(Boolean.compare((x < y), false))); // min(x, y)
	}
	public int max(int x, int y) {
		// I believe that Boolean.compare() does subtraction internally, so there should be no branching
		return y ^ ((x ^ y) & -(Boolean.compare((x > y), false))); // min(x, y)
	}
	public static void main(String[] args) {
		Random rand = new Random();
		MinOrMaxWithoutBranching minOrMax = new MinOrMaxWithoutBranching();

		for(int i = 0; i < 10; i++) {
			int randInt1 = rand.nextInt();
			int randInt2 = rand.nextInt();
			System.out.println(" min(" + randInt1 + "," + randInt2 + ") = " + minOrMax.min(randInt1, randInt2));
		}
	}

}
