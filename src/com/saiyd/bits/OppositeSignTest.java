package com.saiyd.bits;

import java.util.Random;

public class OppositeSignTest {

	public boolean areSignsOpposite(int x, int y) {
		// the MSB of x is XORed with the MSB of y
		// if they are the same then the MSB of the output is zero
		// otherwise it is one, which makes the result negative
		return ((x ^ y) < 0);
	}
	public static void main(String[] args) {
		Random rand = new Random();
		OppositeSignTest ost = new OppositeSignTest();

		for(int i = 0; i < 10; i++) {
			int randInt1 = rand.nextInt();
			int randInt2 = rand.nextInt();
			System.out.println(" areSignsOpposite(" + randInt1 + "," + randInt2 + ") = " + ost.areSignsOpposite(randInt1, randInt2));
		}
	}

}
