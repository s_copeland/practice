package com.saiyd.bits;

/*
 * http://en.wikipedia.org/wiki/Double-precision_floating-point_format
 */
public class ParseDouble {
	private static final int EXPONENT_BIAS = 1023;
	private static final long signBitMask = 0x8000000000000000L;
	private static final long exponentMask = 0x7ff0000000000000L;
	private static final long significandMask = 0x000fffffffffffffL;
	private static final long assumedOneInSignificandMask = 0x0010000000000000L;
	private boolean negative;
	private long exponent;
	private long significand;

	public ParseDouble(double input) {
		long l = Double.doubleToRawLongBits(input);

		long signBit = Math.abs((l & signBitMask) >> 63);
		negative = (signBit == 1);
		exponent = (l & exponentMask) >> 52;
		exponent -= EXPONENT_BIAS;
		significand = l & significandMask;
		// turn on the assumed 1 bit
		significand |= assumedOneInSignificandMask;
	}

	@Override
	public String toString() {
		return "ParseDouble [negative=" + negative + ", exponent=" + exponent
				+ ", significand=" + significand + "]";
	}

	public static void main(String[] args) {
		double d = 1.0d / -3.0d;
		ParseDouble pd = new ParseDouble(d);
		System.out.println(pd);
		
		d = 1.0d / 3.0d;
		pd = new ParseDouble(d);
		System.out.println(pd);
		
		d = 1.0d / 1.0d;
		pd = new ParseDouble(d);
		System.out.println(pd);
	}

}
