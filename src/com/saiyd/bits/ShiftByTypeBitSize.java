package com.saiyd.bits;

public class ShiftByTypeBitSize {

	public static void main(String[] args) {
		/*
		 * Shifting an int by 32, does nothing
		 */
		System.out.println("Shifting an int by 32");
		int i = 1;
		System.out.println(i);
		i = i << 32;
		System.out.println(i);
		
		System.out.println("Shifting an int by 31");
		i = 1;
		System.out.println(i);
		i = i << 31;
		System.out.println(i);
		
		System.out.println("---------------------");
		System.out.println();
		
		
		/*
		 * Shifting an long by 64, does nothing
		 */
		System.out.println("Shifting a long by 64");
		long j = 1;
		System.out.println(j);
		j = j << 64;
		System.out.println(j);
		
		System.out.println("Shifting a long by 63");
		j = 1;
		System.out.println(j);
		j = j << 63;
		System.out.println(j);
	}

}
