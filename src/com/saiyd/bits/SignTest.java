package com.saiyd.bits;

import java.util.Random;

public class SignTest {
	public boolean isNegative(int value) {
		// unsigned shift, push in zeros from the left
		return (value >>> 31) > 0;
	}
	public static void main(String[] args) {
		Random rand = new Random();
		SignTest st = new SignTest();

		for(int i = 0; i < 10; i++) {
			int randInt = rand.nextInt();
			System.out.println(" isNegative(" + randInt + ") = " + st.isNegative(randInt));
		}
	}

}
