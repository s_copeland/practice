package com.saiyd.bits;

public class Util {
	public static void print32Bit(int i) {
		System.out.println(String.format("%32s", Integer.toBinaryString(i)).replace(' ', '0'));
	}
	public static void print64Bit(long i) {
		System.out.println(String.format("%64s", Long.toBinaryString(i)).replace(' ', '0'));
	}
}
