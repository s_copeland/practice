package com.saiyd.computationalcomplexity;

/*
 * NP-hard problems do not have to be elements of the complexity class NP, despite 
 * having NP as the prefix of their class name. The NP-naming system has some deeper 
 * sense, because the NP family is defined in relation to the class NP and the naming 
 * conventions in the Computational Complexity Theory:
 * 
 * NP
 * 
 * 	Class of computational problems for which a given solution can be verified as a 
 * 	solution in polynomial time by a deterministic Turing machine.
 * 
 * NP-hard
 * 
 * 	Class of problems which are at least as hard as the hardest problems in NP. 
 * 	Problems in NP-hard do not have to be elements of NP, indeed, they may not even 
 * 	be decidable problems.
 * 
 * NP-complete
 * 
 * 	Class of problems which contains the hardest problems in NP. Each element of 
 * 	NP-complete has to be an element of NP.
 * 
 * NP-easy
 * 
 * 	At most as hard as NP, but not necessarily in NP, since they may not be decision 
 * 	problems.
 * 
 * NP-equivalent
 * 
 * 	Exactly as difficult as the hardest problems in NP, but not necessarily in NP.
 */
public class Terms {

}
