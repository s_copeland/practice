package com.saiyd.computationalgeometry;

import java.util.Arrays;

public class ConvexHull {
	static class Point implements Comparable<Point> {
		final int x, y;
		
		public Point(int x, int y) {
			this.x = x;
			this.y = y;
		}
		@Override
		public int compareTo(Point o) {
			if(this.x == o.x) {
				return this.y - o.y;
			} else {
				return this.x - o.x;
			}
		}
		
		public String toString() {
			return "(" + x + "," + y + ")";
		}
	}
	
	private long cross(Point o, Point a, Point b) {
		return (a.x - o.x) * (b.y - o.y) - (a.y - o.y) * (b.x - o.x);
	}
	
	public Point[] convexHull(Point[] points) {
		if(points != null && points.length > 1) {
			int n = points.length;
			int k = 0;
			Point[]  hull = new Point[2* n];
			
			Arrays.sort(points);
			
			// build lower hull
			for(int i = 0; i < n; ++i) {
				while(k >= 2 && cross(hull[k - 2], hull[k - 1], points[i]) <= 0)
					k--;
				
				hull[k++] = points[i];
			}
			
			// build upper hull
			for(int i = n - 2, t = k + 1; i >= 0; i--) {
				while(k >= t && cross(hull[k - 2], hull[k - 1], points[i]) <= 0)
					k--;
				hull[k++] = points[i];
			}
			
			if(k > 1)
				hull = Arrays.copyOfRange(hull, 0, k - 1); // remove non-hull vertices after k; remove k - 1 which is a duplicate
			
			return hull;
		} else if(points != null && points.length <= 1) {
			return points;
		} else {
			return null;
		}
	}
	public static void main(String[] args) {
		Point[] input = new Point[4];
		input[0] = new Point(0,10);
		input[1] = new Point(10,0);
		input[2] = new Point(3,4);
		input[3] = new Point(0,0);
		
		ConvexHull cv = new ConvexHull();
		
		Point[] output = cv.convexHull(input);
		
		System.out.println(Arrays.toString(output));

	}

}
