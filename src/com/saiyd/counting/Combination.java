package com.saiyd.counting;

import java.math.BigInteger;

public class Combination {

	/*
	 * Calculate the number of unordered permutations ex: {a,b,c} is equivalent to {b,a,c}
	 * of size X from Y elements. Where each element cannot be repeated ( once it is picked
	 * you can only pick from those that remain)
	 * 
	 * Binomial coefficient
	 * n choose r = n! / ( (n - r)! * r!)
	 */
	public long evaluateWithOutRepetition( int n, int r) {
		BigInteger retVal = new BigInteger("0");
		
		if(r <= n) {
			Factorial factorial = new Factorial();
			BigInteger nFact = factorial.calculateBig(n);
			int nMinusR = n - r;
			BigInteger nMinusRFact = factorial.calculateBig(nMinusR);
			BigInteger rFact = factorial.calculateBig(r);
			
			retVal = nFact.divide((nMinusRFact.multiply(rFact)));
		}
		
		return retVal.longValue();
	}
	
	/*
	 * Calculate the number of unordered permutations ex: {a,b,c} is equivalent to {b,a,c}
	 * of size X from Y elements. Where each element can be repeated
	 * 
	 * {a,a,b} - a was chosen twice
	 * {a,b,b} - b was chosen twice
	 * 
	 * Examples of multisets, multiset of prime divisors of n
	 *   120 has the prime factorization of 2^3 * 3 * 5  or {2,2,2,3,5}
	 * 
	 * Binomial coefficient
	 * n choose r = n! / ( (n - r)! * r!)
	 * 
	 * where n = n+r-1   and r = r  or r =  n-1
	 * 
	 * gives
	 * 
	 * (n + r -1) choose r
	 *   or
	 * (n + r -1) choose n -1
	 */
	public long evaluateWithRepetition( int n, int r) {
		BigInteger retVal = new BigInteger("0");
		
		n = n + r -1;
		
		if(r <= n) {
			Factorial factorial = new Factorial();
			BigInteger nFact = factorial.calculateBig(n);
			int nMinusR = n - r;
			BigInteger nMinusRFact = factorial.calculateBig(nMinusR);
			BigInteger rFact = factorial.calculateBig(r);
			
			retVal = nFact.divide((nMinusRFact.multiply(rFact)));
		}
		
		return retVal.longValue();
	}
	
	/*
	 * SPECIAL NOTES
	 * 
	 * There are (n + r) choose r, strings containing r ones and n zeros
	 * 
	 * There are (n + 1) choose r, strings consisting of r ones and n zeros such that no two ones are adjacent
	 */
	
	public static void main(String[] args) {
		int xSize = 3;
		int ySize = 5;
		
		Combination comb = new Combination();
		/*System.out.println(comb.evaluateWithOutRepetition(xSize, ySize));
		System.out.println(comb.evaluateWithOutRepetition(2, 1));
		System.out.println(comb.evaluateWithOutRepetition(10, 4));
		System.out.println(comb.evaluateWithOutRepetition(14, 3));
		System.out.println();
		System.out.println(comb.evaluateWithOutRepetition(14, 2));
		System.out.println(comb.evaluateWithOutRepetition(3, 1));
		System.out.println(comb.evaluateWithOutRepetition(11, 2));
		System.out.println(comb.evaluateWithOutRepetition(3, 2));
		System.out.println(comb.evaluateWithOutRepetition(8, 2));
		System.out.println(comb.evaluateWithOutRepetition(3, 2));
		System.out.println(comb.evaluateWithOutRepetition(9, 3));
		System.out.println(comb.evaluateWithOutRepetition(5, 2));
				*/
		/*System.out.println(comb.evaluateWithOutRepetition(15, 3));
		System.out.println(comb.evaluateWithOutRepetition(15, 4));
		System.out.println(comb.evaluateWithOutRepetition(15, 5));
		System.out.println(comb.evaluateWithOutRepetition(15, 3));
		*/
		System.out.println(comb.evaluateWithOutRepetition(3, 2));
	}
}
