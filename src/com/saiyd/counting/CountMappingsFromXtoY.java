package com.saiyd.counting;

import java.util.ArrayList;
import java.util.Collection;

public class CountMappingsFromXtoY {
	/*
	 * The number of mappings from domain X to codomain Y is
	 * Y to the X power
	 * 
	 * Mappings
	 *  Injective - a one to one mapping from domain to codomain
	 *  Surjective - onto, eery element of the codomain is mapped to
	 *               by at least one element of the domain
	 *  Bijective - one to one and onto, every element of the codomain
	 *              is mapped to by exactly one element of he domain.
	 *              The function is injective and surjective
	 */
	public static long evaluate(Collection x, Collection y) {
		long retVal = 0;
		if(x != null && y != null) {
			double ySize = y.size();
			double xSize = x.size();
			double value = Math.pow(ySize, xSize);
			retVal = (long) value;
		}
		return retVal;
	}
	
	public static void main(String[] args) {
		int xSize = 3;
		int ySize = 5;
		
		Collection<Integer> setX = new ArrayList<Integer>();
		for(int i=0; i < xSize; i++) {
			setX.add(i);
		}
		
		Collection<Integer> setY = new ArrayList<Integer>();
		for(int i=0; i < ySize; i++) {
			setY.add(i);
		}
		
		System.out.println(evaluate(setX,setY));
	}
}
