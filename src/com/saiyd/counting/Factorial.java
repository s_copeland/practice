package com.saiyd.counting;

import java.math.BigInteger;

public class Factorial {
    //static final double ROOT2PI = 2.506628274631;
    static double ROOT2PI = Math.sqrt(2*Math.PI);
	public long calculate(long n) {
		long ret = 1;
		for(long i = 1; i <= n; i++) {
			ret = ret * i;
		}
		return ret;
	}
	
	public BigInteger calculateBig(long n) {
		BigInteger ret = new BigInteger("1");
		for(long i = 1; i <= n; i++) {
			BigInteger bigInti = new BigInteger(Long.toString(i));
			ret = ret.multiply(bigInti);
		}
		return ret;
	}
	
    /**
     * This function returns Stirling's approximation to (n!).
     * 
     * @param  n, the integer to find the factorial of.
     * @return  double
     */
    public double stirling(int n) {
        double f = 1.0;
        /*if(n <= 10) {
            // calculate the actual factorial
            for(int j = 1; j <= n; j++) f*=j;
            return f;
        }*/

        return (ROOT2PI*Math.pow((float)n,(float)n+0.5)*Math.exp(-(double)n));
    }
    
    double stirlingEnhanced(int n) {
        double f = 1.0;
        /*if(n <= 10) {
            // calculate the actual factorial
            for(int j = 1; j <= n; j++) f*=j;
            return f;
        }*/

        double additionalTerm = 1d + (1d/(12d * n));
        return additionalTerm * (ROOT2PI * Math.pow((float)n,(float)n+0.5)*Math.exp(-(double)n));
    }

	
	public static void main(String[] args) {
		int value = 3;
		long start = System.nanoTime();
		Factorial fact = new Factorial();
		
		System.out.println(fact.calculate(value));
		long end = System.nanoTime();
		System.out.println("factorialLoop runtime = " + (end - start));
		
		for(int i=0; i < 3; i++) {
			start = System.nanoTime();
			System.out.println(fact.stirling(value));
			end = System.nanoTime();
			System.out.println("stirling runtime = " + (end - start));
		}
		
		for(int i=0; i < 3; i++) {
			start = System.nanoTime();
			System.out.println(fact.stirlingEnhanced(value));
			end = System.nanoTime();
			System.out.println("stirlingEnhanced runtime = " + (end - start));
		}
		
	}
}
