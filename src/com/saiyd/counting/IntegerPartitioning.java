package com.saiyd.counting;

public class IntegerPartitioning {
	/*
	 * The partition of an integer is a way of writing it as a sum of positive integers. 
	 * For example, the partitions of the number 5 are:
	 * 
	 * 5
	 * 4+1
	 * 3+2
	 * 3 + 1 + 1
	 * 2+2+1
	 * 2+1+1+1
	 * 1+1+1+1+1
	 * 
	 * 
	 * http://en.wikipedia.org/wiki/Partition_(number_theory)
	 * 
	 * http://www.algorithmist.com/index.php/Coin_Change
	 */
	// http://www.programminglogic.com/integer-partition-algorithm/
	private static int table[][] = new int [100][100];
	
	public int partition(int sum, int largestNumber){
	    if (largestNumber==0)
	        return 0;
	    if (sum==0)
	        return 1;
	    if (sum<0)
	        return 0;
	
	    /*
	     * Notice that this algorithm re-computes solution to the 
	     * same sub-problems many times (i.e., we have overlapping sub-problems). 
	     * As usual, therefore, we can use dynamic programming to make it more 
	     * efficient.
	     */
	    if (table[sum][largestNumber]!=0)
	        return table[sum][largestNumber];
	
	    table[sum][largestNumber] = partition(sum,largestNumber-1) + partition(sum-largestNumber,largestNumber);
	    return table[sum][largestNumber];
	
	}
	
	public static void main(String[] args) {
		IntegerPartitioning ip = new IntegerPartitioning();
		
		System.out.println("14 with no part greater than 7 = " + ip.partition(14, 7));
		int[][] t = table;
		System.out.println("14 with no part greater than 3 = " + ip.partition(14, 3));
	}
}
