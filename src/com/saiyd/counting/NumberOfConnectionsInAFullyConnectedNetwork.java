package com.saiyd.counting;

import com.saiyd.numbertheory.PascalsTriangle;

public class NumberOfConnectionsInAFullyConnectedNetwork {
	/*
	 * # connections = (n(n-1)) / 2
	 */
	public long calculate(long numOfNodes) {
		return ( numOfNodes *(numOfNodes -1)) / 2;
	}
	
	public long calcByPasalsTriangle(long numOfNodes) {
		if(numOfNodes > PascalsTriangle.MAX_ROW) {
			return calculate(numOfNodes);
		} else {
			return PascalsTriangle.TABLE[(int)numOfNodes][2];
		}
	}
	
	public static void main(String[] args) {
		NumberOfConnectionsInAFullyConnectedNetwork nObj = new NumberOfConnectionsInAFullyConnectedNetwork();
		
		
		for(int i=0; i < 20; i++) {
			System.out.println("i = " + i + " " + nObj.calculate(i));
		}
	}
}
