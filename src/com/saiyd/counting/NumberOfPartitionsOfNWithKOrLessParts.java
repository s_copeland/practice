package com.saiyd.counting;

import java.util.Stack;

/*
 * http://en.wikipedia.org/wiki/Partition_(number_theory)
 * 
 * The number of partitions of n in which each part is less than or equal to m is equal to the number of partitions of n into m or fewer parts.
 * 
 */
public class NumberOfPartitionsOfNWithKOrLessParts {
	
	/*
	 * This is almost the same ast integer partitioning...
	 * 
	 * Should it be exactly the same?
	 * 
	 */
	public long calculate(long numOfObjects, long maxNumberOfPartitions) {
		if(maxNumberOfPartitions == 0)
			return 0;
		if(numOfObjects == 1)
			return 1;
		//if(maxNumberOfPartitions >= numOfObjects)
			
		return calculate(numOfObjects, maxNumberOfPartitions -1) + calculate(numOfObjects - maxNumberOfPartitions, maxNumberOfPartitions);
	}
	

	long table[][] = new long [100][100];
	
	public long partition(int sum, int largestNumber){
	    if (largestNumber==0)
	        return 0;
	    if (sum==0)
	        return 1;
	    if (sum<0)
	        return 0;
	
	    if (table[sum][largestNumber]!=0)
	        return table[sum][largestNumber];
	
	    table[sum][largestNumber]=partition(sum,largestNumber-1)
	    + partition(sum-largestNumber,largestNumber);
	    return table[sum][largestNumber];
	
	}
	/*
	public long calculate2(long numOfObjects, long maxNumberOfPartitions) {
		if(maxNumberOfPartitions == 0)
			return 0;
		if(numOfObjects == 1)
			return 1;
		//if(maxNumberOfPartitions >= numOfObjects)
		Stack<StackFrame> stack = new Stack<StackFrame>();
		StackFrame curStackFrame = new StackFrame();
		curStackFrame.objects = numOfObjects;
		curStackFrame.parts = maxNumberOfPartitions;
		
		long curRetVal = 0;
		
		while(true) {
			// clear the retVal 
			curRetVal = -1;
			
			if(curStackFrame.parts == 0) {
				curRetVal = 0;
			} else if(curStackFrame.objects == 1) {
				curRetVal = 1;
			} else {
				
			}
			
			if(curRetVal != -1 && curStackFrame.leftFuncAnswer == -1) {
				curStackFrame.leftFuncAnswer = curRetVal;
				// do the right side if not done
				if(curStackFrame.rightFuncAnswer == -1) {
					
				}
			}
		}
			
		return calculate(numOfObjects, maxNumberOfPartitions -1) + calculate(numOfObjects - maxNumberOfPartitions, maxNumberOfPartitions);
	}
	*/
	class StackFrame {
		long objects;
		long parts;
		long leftFuncAnswer = -1;
		long rightFuncAnswer = -1;
	}
	
	public static void main(String[] args) {
		NumberOfPartitionsOfNWithKOrLessParts nop = new NumberOfPartitionsOfNWithKOrLessParts();
		
		System.out.println(nop.calculate(1, 0));
		System.out.println(nop.partition(14, 7));
	}
}
