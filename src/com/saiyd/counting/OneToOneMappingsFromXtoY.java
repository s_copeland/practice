package com.saiyd.counting;

import java.util.ArrayList;
import java.util.Collection;

public class OneToOneMappingsFromXtoY {
	/*
	 * Count the number of one-to-one mappings
	 * from set X to set Y
	 * 
	 * Counted by declining products
	 * Ex: If |S| = 3 and |T| = 5, then there are 5 * 4 * 3 mappings
	 * 
	 * This is the same as a 3-permutation of 5 elements
	 */
	public static long evaluate(Collection x, Collection y) {
		long retVal = 0;
		if(x != null && y != null && x.size() <= y.size()) {
			int xSize = x.size();
			int ySize = y.size();
			int numberOfChoicesForCurElement = 1;
			
			for(int i = 0; i < xSize; i++) {
				numberOfChoicesForCurElement *= ySize;
				ySize--;
			}
			
			retVal = numberOfChoicesForCurElement;
		}
		return retVal;
	}
	
	/*
	 * Use factorials instead to calculate a x-permutation of
	 * y elements
	 * 
	 * P(n,r) = n!/(n - r!)
	 */
	public static long evaluate2(Collection y, Collection x) {
		Permutation perm = new Permutation();
		return perm.evaluateXSizedPermOfYWithOutRepetition(y.size(), x.size());
	}
	
	public static void main(String[] args) {
		int xSize = 3;
		int ySize = 5;
		
		Collection<Integer> setX = new ArrayList<Integer>();
		for(int i=0; i < xSize; i++) {
			setX.add(i);
		}
		
		Collection<Integer> setY = new ArrayList<Integer>();
		for(int i=0; i < ySize; i++) {
			setY.add(i);
		}
		
		System.out.println(evaluate(setY,setX));
		System.out.println(evaluate2(setY,setX));
	}
}
