package com.saiyd.counting;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/*
 * If a set has n elements and if
 * n1 + n2 + n3 +... + nk = n
 * 
 * then there are
 * 
 * (n!) / (n1! * n2! * n3! * ... nk!)
 * 
 * also written as, n choose n1 n2 n3 ... nk
 * 
 * ordered partitions
 * 
 * or 
 * (n choose n1) * (n - n1 choose n2) * ... (n - n1 - ... nk-1 choose nk) 
 * 
 * Counts the number of ordered partitions of a set of objects. The order of the 
 * items in each partition does not matter, but the order of the partitions does.
 * 
 * Also calculates the coefficient of multinomials of the form
 * (a1 + a2 + ... ak)^n
 * The coefficient is multiplied by term of the form 
 * a1^n1 * a2^n2 * ... ak^nk
 * 
 * 
 * Also calculates the permutations of multisets. Where a set of n objects
 * is partitioned into subsets of k different types with n1, n2, n3, ... nk
 * 
 * Ex: in an alphabet {a,b,c} the number of words having length 10 using
 * 4 a's, 3 b's, 2 c's
 * 
 * members, such that n = n1 + n2 + n3 + ... nk
 * 
 * http://en.wikipedia.org/wiki/Multinomial_theorem
 */
public class OrderedPartitions {
	public long calculate(int numOfObj, Collection<Long> partitions) {
		Factorial fact = new Factorial();
		
		BigInteger nFact = fact.calculateBig(numOfObj);
		BigInteger divisor = new BigInteger("1");
		for(Long partition : partitions) {
			BigInteger partFact = fact.calculateBig(partition);
			divisor = divisor.multiply(partFact);
		}
		
		BigInteger answer = nFact.divide(divisor);
		return answer.longValue();
	}
	
	public static void main(String[] args) {
		OrderedPartitions op = new OrderedPartitions();
		List<Long> al = new ArrayList<Long>();
		al.add(3l);
		al.add(2l);
		al.add(2l);
		System.out.println(op.calculate(8, al));
		
		al = new ArrayList<Long>();
		al.add(4l);
		al.add(3l);
		al.add(3l);
		System.out.println(op.calculate(10, al));
	}
}
