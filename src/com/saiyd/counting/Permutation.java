package com.saiyd.counting;

import java.util.Collection;

public class Permutation {
	/*
	 * Use factorials to calculate a x-permutation of
	 * y elements
	 * 
	 * P(n,r) = n!/(n - r!)
	 * 
	 * for 1<=r<=n TODO Fix this code!!!
	 */
	public long evaluateXSizedPermOfYWithOutRepetition(int n, int r) {
		long retVal = 0;
		if(r <= n) {		
			Factorial factorial = new Factorial();
			
			long nFact = factorial.calculate(n);
			int nMinusR = n - r;
			long nMinusRFact = factorial.calculate(nMinusR);
			retVal = nFact / nMinusRFact;
		}
		return retVal;
	}
	
	/*
	 * n to the r power
	 */
	public long evaluateXSizedPermOfYWithRepetition(int n, int r) {
		return (long) Math.pow(n, r);
	}
}
