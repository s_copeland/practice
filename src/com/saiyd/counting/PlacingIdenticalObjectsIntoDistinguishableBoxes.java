package com.saiyd.counting;

import com.saiyd.numbertheory.PascalsTriangle;


public class PlacingIdenticalObjectsIntoDistinguishableBoxes {
	/*
	 * n is the number of identical objects
	 * k is the number of distinguishable boxes
	 * 
	 * (n + k -1) choose (k -1)
	 *    or
	 * (n + k - 1) choose (n)
	 * 
	 * You can think of numbers as boxes with identical balls
	 * 123 = a box with one ball, a box with 2 balls and a box with 3 balls
	 * 
	 * 00142 = two boxes with 0 balls, one box with 1 ball, one box with 4 balls and one box with 2 balls
	 * 
	 * How many numbers from 1 to 10000 have the property that the sum of their digits is 7. This is the
	 * same as the number of ways to place 7 balls into five boxes. We use 5 boxes because 100000 is excluded because it does not sum to 7
	 * 
	 * 
	 * TAKING identical objects from distinguishable boxes
	 * 
	 * How many ways can 10 coins (being a coin is the identical property) from pennies, nickels, dimes, and quaters
	 * (each class of coin is a different box)
	 * 
	 * Each selection is an ordered tuple who's sum is 10
	 * Ex: {1,2,3,4} = 10  
	 * 		one penny, 2 nickels, 3 dimes, 4 quaters
	 * 
	 * 
	 * 
	 */
	public long calculate(long numIdenticalObjects, long numDistinguishableBoxes) {
		long retVal = 0;
		long n = numIdenticalObjects + numDistinguishableBoxes -1;
		
		if(n >= numIdenticalObjects) {
			Factorial fact = new Factorial();
			
			
			long nFact = fact.calculate(n);
			long nMinusRFact = fact.calculate(n - numIdenticalObjects);
			long rFact = fact.calculate(numIdenticalObjects);
			retVal = nFact / (nMinusRFact * rFact);
		}
		return retVal;
	}
	
	
	public static void main(String[] args) {
		PlacingIdenticalObjectsIntoDistinguishableBoxes p = new PlacingIdenticalObjectsIntoDistinguishableBoxes();
		
		System.out.println("5 objects 4 boxes = " + p.calculate(5, 4));
		System.out.println("14 objects 3 boxes = " + p.calculate(14, 3));
		
	}
}
