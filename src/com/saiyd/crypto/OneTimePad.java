package com.saiyd.crypto;

import java.security.SecureRandom;

public class OneTimePad {

	private byte[] otp;

	public byte[] encrypt(byte[] input) {
		SecureRandom sr = new SecureRandom();
		otp = new byte[input.length];
		sr.nextBytes(otp); // fill with random bytes
		
		for(int i = 0; i < input.length; i++) {
			input[i] ^= otp[i];
		}
		return input;
	}
	
	public byte[] decrypt(byte[] input, byte[] pad) {
		for(int i = 0; i < input.length; i++) {
			input[i] ^= pad[i];
		}
		return input;
	}
	
	public byte[] getOtp() {
		return otp;
	}

	public static void main(String[] args) {
		OneTimePad otp = new OneTimePad();
		String s = "Hello World";
		byte[] plainText = s.getBytes();
		System.out.println("Plain text = " + new String(plainText));
		byte[] cipherText = otp.encrypt(plainText);
		System.out.println("Cipher text = " + new String(cipherText));
		byte[] decryptedText = otp.decrypt(cipherText, otp.getOtp());
		System.out.println("Decrypted text = " + new String(decryptedText));
	}

}
