package com.saiyd.datastructures;

import static com.saiyd.datastructures.LowercaseVocabularyTrie.ALPHABET;

import com.saiyd.datastructures.LowercaseVocabularyTrie.Node;

public class AnagramLowercaseVocabularyTrie extends LowercaseVocabularyTrie {
	public <T> boolean insert(String word, T value) {
		Node node = root;
		Node child = null;
		
		int wLen = word.length();
		int i = 0;
		for(; i < wLen; i++) {
			int index = ALPHABET.indexOf(word.charAt(i));
			child = node.children[index];
			
			if(child == null) {
				// add missing char as a child
				child = new Node();
				child.value = value;
				child.alphaIndex = index;
				// indicate this is a word and not a prefix
				if(i == (wLen -1))
					child.word = true;
				
				node.children[index] = child;
			}
			
			node = child; // keep looking for chars not in the trie
		}
		
		size++;
		return true;
	}
}
