package com.saiyd.datastructures;

/*
 * An Implicit Binary Tree
 * 
 * Our first implementation of a (priority) Queue is based on a technique that is over 
 * four hundred years old. Eytzinger's method allows us to represent a complete binary 
 * tree as an array by laying out the nodes of the tree in breadth-first order 
 * 
 * In this way, the root is stored at position 0, the root's left child is stored at 
 * position 1, the root's right child at position 2, the left child of the left child 
 * of the root is stored at position 3, and so on. 
 * 
 */
public class BinaryHeap <T extends Comparable<T>> {
	public static enum Type { MAX, MIN };
	
	private Type heapType;
	
	public BinaryHeap(Type type) {
		heapType = type;
	}
	
	public void buildHeap(T[] itemsArray, int numItemsToHeapify) {
		if(itemsArray == null)
			throw new IllegalArgumentException("The items array cannot be null");
		
		int heapifyCount = (int) Math.floor((double) (itemsArray.length / 2));
		for(int i = heapifyCount; i > 0; i--) {
			if(heapType == Type.MAX) {
				maxHeapify(itemsArray, numItemsToHeapify, i - 1);
			} else if(heapType == Type.MIN) {
				minHeapify(itemsArray, numItemsToHeapify, i - 1);
			}
		}
	}
	
	public void heapify(T[] heapArray, int numItemsToHeapify, int rootIndex) {
		if(heapType == Type.MAX) {
			maxHeapify(heapArray, numItemsToHeapify, rootIndex);
		} else if(heapType == Type.MIN) {
			minHeapify(heapArray, numItemsToHeapify, rootIndex);
		}		
	}
	/*
	 * Restores the heap property 
	 * 
	 *  the above algorithm to correctly re-heapify the array, 
	 *  the node at index i and its two direct children must 
	 *  violate the heap property. 
	 *  If they do not, the algorithm will fall through with no change to the array. 
	 *  
	 * assumes that binary trees rooted at
	 * left(i) and right(i) are heaps
	 */
	private void maxHeapify(T[] heapArray, int numItemsToHeapify, int rootIndex) {
		int leftChildIndex = left(rootIndex);
		int rightChildIndex = right(rootIndex);
		int largestIndex = rootIndex;
		
		if(leftChildIndex <= numItemsToHeapify && heapArray[leftChildIndex].compareTo(heapArray[largestIndex]) == 1)
			largestIndex = leftChildIndex;
		
		if(rightChildIndex <= numItemsToHeapify && heapArray[rightChildIndex].compareTo(heapArray[largestIndex]) == 1)
			largestIndex = rightChildIndex;
		
		if(largestIndex != rootIndex) {
			T temp = heapArray[rootIndex];
			heapArray[rootIndex] = heapArray[largestIndex];
			heapArray[largestIndex] = temp;
			maxHeapify(heapArray, numItemsToHeapify, largestIndex);
		}
	}
	
	private void minHeapify(T[] heapArray, int numItemsToHeapify, int rootIndex) {
		int leftChildIndex = left(rootIndex);
		int rightChildIndex = right(rootIndex);
		int smallestIndex = rootIndex;
		
		if(leftChildIndex <= numItemsToHeapify && heapArray[leftChildIndex].compareTo(heapArray[smallestIndex]) == -1)
			smallestIndex = leftChildIndex;
		
		if(rightChildIndex <= numItemsToHeapify && heapArray[rightChildIndex].compareTo(heapArray[smallestIndex]) == -1)
			smallestIndex = rightChildIndex;
		
		if(smallestIndex != rootIndex) {
			T temp = heapArray[rootIndex];
			heapArray[rootIndex] = heapArray[smallestIndex];
			heapArray[smallestIndex] = temp;
			minHeapify(heapArray, numItemsToHeapify, smallestIndex);
		}
	}
	
	/*
	 * Returns the index of the parent
	 * of the specified node
	 */
	private int parent(int index) {
		return (index -1) >> 1; //zero base, if the array started at 1, then: index >> 1
	}
	
	/*
	 * Returns the index of the left
	 * child of the specified node
	 */
	private int left(int index) {
		return (index << 1) + 1; // zero base
	}
	
	/*
	 * Returns the index of the right
	 * child of the specified node
	 */
	private int right(int index) {
		return (index << 1) + 2; // zero base
	}
}
