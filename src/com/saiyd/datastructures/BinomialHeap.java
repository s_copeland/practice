package com.saiyd.datastructures;

import static com.saiyd.datastructures.BinomialTree.Node;

import java.util.Stack;

/*
 * A binomial heap H is a set of binomial trees that satisfies the following
 * binomial heap properties. (These properties are for min heap, max is the opposite)
 * 
 * 		1.Each binomial tree in H obeys the (min/max)-heap property: the key of a 
 * 		  node is greater than or equal to the key of its parent. We say that each
 *        such tree is (min/max)-heap-ordered.
 *      
 *      2. For any nonnegative integer k, there is at most one binomial tree in H
 *         whose root has degree k.
 *         
 *         The second property implies that an n-node binomial heap H consists of
 *         at most floor(lg n) + 1 binomial trees.
 *         
 *         To see why, observe that the binary representation of n has floor(lg n) + 1
 *         bits. Therefore a binomial tree Bi appears in H if and only if bit Bi = 1.
 * 
 * 
 * 
 * A mergeable heap that supports the follwoing 5 operations.
 * 
 * 	makeHeap() - creates and returns a new heap containing no elements
 * 	insert(x) - inserts node x, whose key field has already been filled in
 * 	minimum() - returns a pointer to the node in the heap whose key is minimum
 * 	extractMin() - deletes the node from the heap who's keys is minimum, returns a pointer to that node
 * 	union(h) - creates and returns a new heap that contains all the nodes of
 * 			   this heap and heap h
 * 	
 * Extra opeartions
 * 	
 * 	decreaseKey(x, k) - assigns to node x within this heap the new key value
 * 						k, which is assumed to be no greater than its current key value
 * 	delete(x) - deletes node x from the heap
 * 
 * Normal binary heaps perform the union operation poorly, Theta(n) in the worst case.
 * 
 * 
 * 
 * Worst Case Heap Running Time Comparisons
 * 						BinaryHeap		BinomialHeap	FibonacciHeap (amortized)
 * makeHeap()			Theta(1)		Theta(1)		Theta(1)
 * insert(x)			Theta(lg n)		O(lg n)			Theta(1)
 * minimum()			Theta(1)		O(lg n)			Theta(1)
 * extractMin() 		Theta(lg n)		Theta(lg n)		O(lg n)
 * union(h) 			Theta(n)		O(lg n)			Theta(1) 	
 * decreaseKey(x, k)	Theta(lg n)		Theta(lg n)		Theta(1)
 * delete(x)			Theta(lg n)		Theta(lg n)		O(lg n)
 * 
 * Binary heaps, Binomial heaps, and Fibonacci heaps are all inefficient in their
 * support of the search operation.
 */
public class BinomialHeap <T extends Comparable> {
	/* The roots of binomial trees in a binomial heap are organized in a linked list,
	 * which we refer to as the root list. The degrees of the roots strictly
	 * increase as we traverse the root list.
	 * 
	 * By the 2nd binomial-heap property, in an n-node binomial heap the degrees of
	 * roots are a subset of {0,1,... floor(lg n)}
	 */
	private Node<T> rootList;
	private final T max;
	private final T min;
	
	public BinomialHeap(T min, T max) {
		this.min = min;
		this.max = max;
	}
	
	/*
	 * This implementation assumes there are no keys with value
	 * of max.
	 * 
	 * Since a binomial haep is min/max heap ordered, the key
	 * must reside in a root node. This procedure checks all roots,
	 * which number at most floor(lg n) + 1
	 */
	public Node<T> minimumNode() {
		Node<T> y = null;
		Node<T> x = rootList;
		T curMin = max;
		while(x != null) {
			if(x.data.compareTo(curMin) < 0) {
				curMin = x.data;
				y = x;
			}
			x = x.rightSibling;
		}
		return y;
	}
	
	public T minimumValue() {
		Node<T> n = minimumNode();
		if(n != null)
			return n.data;
		return null;
	}
	
	/*
	 * Takes O(1) time. It works because the left-child, right-sibling
	 * representation of each binomial tree matches the ordering property
	 * of the tree: in a Bk tree, the leftmost child of the root is the 
	 * root of a Bk-1 tree.
	 */
	private void link(Node<T> y, Node<T> z) {
		y.parent = z;
		y.rightSibling = z.leftChild;
		z.leftChild = y;
		z.degree++;
	}
	
	/*
	 * This procedure destroys h1 and h2 in the process.
	 */
	public BinomialHeap<T> union(BinomialHeap<T> h1, BinomialHeap<T> h2) {
		BinomialHeap<T> h = new BinomialHeap<T>(min, max);
		h.rootList = merge(h1, h2);
		h1 = null;
		h2 = null;
		
		if(h.rootList == null)
			return h;
		
		Node<T> prevX = null;
		Node<T> x = h.rootList;
		Node<T> nextX = x.rightSibling;
		
		while(nextX != null) {
			if( (x.degree != nextX.degree) || (nextX.rightSibling != null && nextX.rightSibling.degree == x.degree) ){
				prevX = x;
				x = nextX;
			} else {
				if(x.data.compareTo(nextX.data) <= 0) {
					x.rightSibling = nextX.rightSibling;
					link(nextX, x);
				} else {
					if(prevX == null) {
						h.rootList = nextX;
					} else {
						prevX.rightSibling = nextX;
					}
					link(x, nextX);
					x = nextX;
				}
			}
			
			nextX = x.rightSibling;
		}
		
		return h;
	}

	/*
	 * Merges the root list of h1 and h2 into a single linked list
	 * that is sorted monotonically increasing order of the root's degree. 
	 * The merge is similar to merge in mergesort.
	 */
	private Node<T> merge(BinomialHeap h1, BinomialHeap h2) {
		Node<T> retRootListNode = null;
		Node<T> curRootListNode = null;
		
		if(h1 == null && h2 != null) {
			retRootListNode = h2.rootList;
		} else if(h1 != null && h2 == null) {
			retRootListNode = h1.rootList;
		} else if(h1 != null || h2 != null) {
			Node<T> nextNode = null;
			Node<T> h1Ptr = h1.rootList;
			Node<T> h2Ptr = h2.rootList;
			while(h1Ptr != null || h2Ptr != null) {
				if((h1Ptr != null && h2Ptr != null)) {
					if(h1Ptr.degree < h2Ptr.degree) {
						nextNode = h1Ptr;
						h1Ptr = h1Ptr.rightSibling;
					} else {
						nextNode = h2Ptr;
						h2Ptr = h2Ptr.rightSibling;
					}
				} else if(h1Ptr != null && h2Ptr == null) {
					nextNode = h1Ptr;
					h1Ptr = h1Ptr.rightSibling;
				} else if(h1Ptr == null && h2Ptr != null) {
					nextNode = h2Ptr;
					h2Ptr = h2Ptr.rightSibling;
				}
				
				if(retRootListNode == null) {
					curRootListNode = nextNode;
					retRootListNode = nextNode;
				} else {
					curRootListNode.rightSibling = nextNode;
					curRootListNode = nextNode;
				}
			}
		}
		
		return retRootListNode;
	}
	
	/*
	 * This procedure assumes that x has been already allocated and 
	 * the data has been set
	 */
	public void insert(Node<T> x) {
		BinomialHeap<T> h = new BinomialHeap<T>(min, max);
		x.parent = null;
		x.leftChild = null;
		x.rightSibling = null;
		x.degree = 0;
		h.rootList = x;
		BinomialHeap<T> j = union(this, h);
		this.rootList = j.rootList;
	}
	
	public void insert(T data) {
		Node<T> x = new Node<T>();
		x.data = data;
		insert(x);
	}
	
	public Node<T> extractMinNode() {
		if(rootList == null)
			return null;
		
		 // 1. find the root x with the min key in the root list and remove x from the root list
		Node<T> minRoot = rootList;
		Node<T> prevRoot = null;
		Node<T> curRoot = rootList;
		while(curRoot != null) {
			if(curRoot.rightSibling != null && curRoot.data.compareTo(curRoot.rightSibling.data) > 0) {
				minRoot = curRoot.rightSibling;
				prevRoot = curRoot;
			}
			
			curRoot = curRoot.rightSibling;
		}
		// unlink the minRoot node from the root list
		if(prevRoot != null)
			prevRoot.rightSibling = minRoot.rightSibling;
		if(rootList == minRoot)
			rootList = minRoot.rightSibling;
		if(minRoot != null)
			minRoot.rightSibling = null;
		
		 // 2. H' = make-binomial-heap()
		BinomialHeap<T> h = new BinomialHeap<T>(min, max);
		
		 // 3. reverse the order of the linked list of x's children and set the root list of H'
		//     to point to the head of the resulting list
		Stack<Node<T>> stack = new Stack<Node<T>>();
		Node<T> curChild = minRoot.leftChild;
		Node<T> temp = null;
		while(curChild != null) {
			temp = curChild;
			curChild = curChild.rightSibling;
			// unlink
			temp.rightSibling = null;
			// unlink the top node of the minRoot tree from its children
			temp.parent = null;
			stack.push(temp);
		}
		Node<T> firstChild = null;
		// reverse
		while(!stack.isEmpty()) {
			if(firstChild == null) {
				firstChild = stack.pop();
				curChild = firstChild;
			} else {
				curChild.rightSibling = stack.pop();
				curChild = curChild.rightSibling;
			}
		}
		h.rootList = firstChild;
		minRoot.leftChild = null;
		 
		// 4. H = union(H, H')
		BinomialHeap<T> hUnion = union(this, h);
		this.rootList = hUnion.rootList;
		
		 // 5. return x
		return minRoot;
	}
	
	public T extractMinValue() {
		Node<T> node = extractMinNode();
		if(node != null)
			return node.data;
		return null;
	}
	
	public void decreaseKey(Node<T> x, T data) {
		//if(data.compareTo(x.data) > 0)
		//	throw new IllegalArgumentException();
		x.data = data;
		Node<T> y = x;
		Node<T> z = y.parent;
		
		while(z != null && y.data.compareTo(z.data) < 0) {
			T temp = y.data;
			y.data = z.data;
			z.data = temp;
			y = z;
			z = y.parent;
		}
	}
	
	public void delete(Node<T> x) {
		decreaseKey(x, min);
		extractMinNode();
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("{");
		if(rootList != null)
			sb.append(rootList.toString());
		sb.append("}");
		return sb.toString();
	}
	
	public String printRootListDegrees() {
		StringBuilder sb = new StringBuilder();
		sb.append("Root list degrees {");
		
		Node<T> node = rootList;
		while(node != null) {
			sb.append(node.degree);
			node = node.rightSibling;
			if(node != null)
				sb.append(",");
		}

		sb.append("}");
		return sb.toString();
	}
	
	public static void main(String[] args) {
		BinomialHeap<Integer> bHeap = new BinomialHeap<Integer>(Integer.MIN_VALUE, Integer.MAX_VALUE);
		insertValues(bHeap);
		System.out.println(bHeap.printRootListDegrees());
		extractAll(bHeap);
		insertValues(bHeap);
		printMin(bHeap);
	}

	private static void extractAll(BinomialHeap<Integer> bHeap) {
		System.out.println(bHeap.toString());
		System.out.println(bHeap.extractMinValue());
		System.out.println(bHeap.toString());
		System.out.println(bHeap.extractMinValue());
		System.out.println(bHeap.toString());
		System.out.println(bHeap.extractMinValue());
		System.out.println(bHeap.toString());
		System.out.println(bHeap.extractMinValue());
		System.out.println(bHeap.toString());
		System.out.println(bHeap.extractMinValue());
		System.out.println(bHeap.toString());
		System.out.println(bHeap.extractMinValue());
		System.out.println(bHeap.toString());
	}

	private static void insertValues(BinomialHeap<Integer> bHeap) {
		bHeap.insert(10);
		bHeap.insert(1);
		bHeap.insert(6);
		bHeap.insert(12);
		bHeap.insert(25);
		bHeap.insert(18);
	}
	
	private static void printMin(BinomialHeap<Integer> bHeap) {
		System.out.println("------- Test decreaseKey ------");
		System.out.println(bHeap.toString());
		System.out.println("Min = " + bHeap.minimumNode().data);
		System.out.println("Inserting 100");
		Node<Integer> ONE_HUNDRED = new Node<Integer>();
		ONE_HUNDRED.data = 100;
		bHeap.insert(ONE_HUNDRED);
		System.out.println(bHeap.toString());
		System.out.println("Decreasing key 100 to 2");
		bHeap.decreaseKey(ONE_HUNDRED, 2);

		System.out.println(bHeap.toString());
		System.out.println("Min = " + bHeap.minimumNode().data);
		System.out.println("Decreasing key 2 to 0");
		bHeap.decreaseKey(ONE_HUNDRED, 0);
		System.out.println(bHeap.toString());
		System.out.println("------- Test decreaseKey ------");
		bHeap.extractMinValue();
		System.out.println(bHeap.toString());
		System.out.println(bHeap.extractMinValue());
		System.out.println(bHeap.toString());
		System.out.println(bHeap.extractMinValue());
		System.out.println(bHeap.toString());
		System.out.println(bHeap.extractMinValue());
		System.out.println(bHeap.toString());
		System.out.println(bHeap.extractMinValue());
		System.out.println(bHeap.toString());
		System.out.println(bHeap.extractMinValue());
		System.out.println(bHeap.toString());
		System.out.println(bHeap.extractMinValue());
		System.out.println(bHeap.toString());
		System.out.println("------- Test decreaseKey ------");
	}
}
