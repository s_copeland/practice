package com.saiyd.datastructures;

/*
 * A binomial tree Bk is an ordered tree defined recursively. The binomial
 * tree B0 consists of a single node. The binomial tree Bk consists of two
 * binomial trees Bk-1 that are linked together: the root of one is the
 * leftmost child of the root of the other.
 * 
 * Properties of binomial trees
 * 		1. there are 2^k nodes
 * 		2. The height of the tree is k
 * 		3. There are exactly (k choose i) nodes at depth i for i = 0,1,...k
 * 		4. The root has degree k, which is greater than that of any other
 * 		   node; moreover if the children of the root are numbered from left
 * 		   to right by k - 1, k - 2, ..., 0, child i is the root of a subtree
 * 		   Bi
 * 
 * The maximum degree of any node in a n-node binomial tree is lg n.
 * 
 * The term "binomial tree" comes from property 3
 * 
 * B0  O
 * 
 * B1  O
 * 	   |
 *     O
 * 
 * B2     O
 *      / |
 *     O  O
 * 	   |
 *     O
 *             
 * B3       _ _ O
 *         / /  |
 *        O  O  O
 *      / |  |
 *     O  O  O
 * 	   |
 *     O
 */
public class BinomialTree {
	public static class Node<T extends Comparable> {
		T data;
		Node parent;
		Node leftChild;
		/*
		 * If this node is a root then the this
		 * points to the next sibling in the root list
		 */
		Node rightSibling;
		int degree;
		
		public T getData() {
			return data;
		}

		public void setData(T data) {
			this.data = data;
		}

		public Node getParent() {
			return parent;
		}

		public void setParent(Node parent) {
			this.parent = parent;
		}

		public Node getLeftChild() {
			return leftChild;
		}

		public void setLeftChild(Node leftChild) {
			this.leftChild = leftChild;
		}

		public Node getRightSibling() {
			return rightSibling;
		}

		public void setRightSibling(Node rightSibling) {
			this.rightSibling = rightSibling;
		}

		public int getDegree() {
			return degree;
		}

		public void setDegree(int degree) {
			this.degree = degree;
		}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			if(data != null)
				sb.append(data.toString());
			if(leftChild != null) {
				sb.append(",");
				sb.append(leftChild.toString());
			}
			
			if(rightSibling != null) {
				sb.append(",");
				sb.append(rightSibling.toString());
			}
			
			return sb.toString();
		}
	}
}
