package com.saiyd.datastructures;

import java.util.ArrayList;
import java.util.Arrays;

/*
 * B-trees are balanced search trees designed to work well on magnetic disk or
 * other direct-access secondary storage devices. They are similar to red-black
 * trees, but are better at minimizing disk I/O operations.
 * 
 * B-trees differ from red-black trees in that B-tree nodes may have many
 * children, from a handful to thousands.
 * 
 * Every n node B-tree has height O(lg n), although the height of a B-tree can
 * be considerably less than that of a red-black tree.
 * 
 * The data associated with a B-tree key can be stored in the same node as the 
 * key or be a reference to some other storage location.
 * 
 * B+-tree
 * 		A variant of a B-tree that stores all satellite information in the leaves
 * 		and stores only keys and child pointers in the internal nodes, thus
 * 		maximizing the branching factor of the internal nodes.
 * 
 * Definition of B-trees
 * 
 * 		A B-tree T is a rooted tree having the following properties
 * 			1. Every node x has the following fields
 * 				a. x.key_count, the number of keys currently stored in node x
 * 				b. x.key[], stores keys in nondecreasing order
 * 				c. x.isLeaf, a boolean value that is true if x is a leaf and false
 * 				   if x is an internal node
 * 
 * 			2. Each internal node x also contains x._key_count + 1 pointers to child
 * 			   nodes. Leaf nodes have no children, so their child node pointers are
 * 			   undefined.
 * 
 * 			3. The keys in x.key[] separate the ranges of keys stored in each subtree.
 * 			   If ki is any key stored in the subtree referenced by x.key[j] then
 * 
 * 					k1 <= x.key[j]
 * 
 * 			4. All leaves have the same depth, which is the tree's height h
 * 
 * 			5. There are lower and upper bounds on the number of keys a node can
 * 			   contain. These bounds can be expressed in terms of a fixed integer
 * 			   t >= 2 called the minimum degree of the B-tree.
 * 				a. Every node other than the root must have at least t - 1 keys.
 * 				   Every internal node other than the root thus has at least t
 * 				   children. If the tree is nonempty, the root must have at least
 * 				   one key.
 * 				b. Every node can contain at most 2*t - 1 keys. Therefore, an internal
 * 				   node can have at most 2*t children. We say that a node is full if
 * 				   it contains exactly 2*t - 1 keys.
 * 
 * 		The simplest B-tree occurs when t = 2. Every internal node then has either
 * 		2, 3, or 4 children, and we have a 2-3-4 tree. In practice, however, much
 * 		larger values of t are typically used.
 * 
 * Height of a B-tree
 * 		The number of disk accesses required for most operations on a B-tree is
 * 		proportional to the height of the B-tree. 
 * 
 * 		If n >= 1, then for any n-key B-tree T of height h and minimum degree t >= 2
 * 			h <= log base t( (n + 1)/2 )
 */
public class Btree {
	private static final int MINIMUM_DEGREE = 2;
	
	public static final class Node {
		int keyCount;
		Comparable[] key = new Comparable[2 * MINIMUM_DEGREE - 1];
		Node[] child = new Node[2 * MINIMUM_DEGREE];
		boolean isLeaf;
		
		public String toString() {
			/*ArrayList<Comparable> al = new ArrayList<Comparable>();
			// we use <= because we have n keys and n + 1 children
			for(int i = 0; i <= keyCount; i++) {
				al.add(key[i]);
			}
			return al.toString();*/
			return Arrays.toString(key);
		}
	}
	
	private Node root = create();
	
	public Comparable search(Comparable key) {
		return searchInternal(root, key);
	}

	private Comparable searchInternal(Node node, Comparable key) {
		int i = 0;
		while(i < node.keyCount && key.compareTo(node.key[i]) > 0) {
			i++;
		}
		if(i < node.keyCount && key.compareTo(node.key[i]) == 0)
			return node.key[i];
		if(node.isLeaf) {
			return null;
		} else {
			return searchInternal(node.child[i], key);
		}
	}
	
	private Node create() {
		Node node = new Node();
		node.isLeaf = true;
		node.keyCount = 0;
		// write this node to disk
		return node;
	}
	
	public void insert(Comparable key) {
		Node r = root;
		if(r.keyCount == 2 * MINIMUM_DEGREE - 1) {
			Node s = new Node();
			root = s;
			s.isLeaf = false;
			s.keyCount = 0;
			s.child[0] = r;
			splitChild(s, 0, r);
			insertNonFull(s, key);
		} else {
			insertNonFull(r, key);
		}
	}
	
	private void splitChild(Node x, int index, Node y) {
		Node z = new Node();
		z.isLeaf = y.isLeaf;
		// we expect to copy this many nodes into z
		z.keyCount = MINIMUM_DEGREE - 1;
		
		/*
		 * Copy the top half of the keys in y
		 * into z
		 */
		for(int j = 0; j < MINIMUM_DEGREE - 1; j++) {
			z.key[j] = y.key[j + MINIMUM_DEGREE];
			y.key[j + MINIMUM_DEGREE] = null;
		}
		
		/*
		 * Copy the top half of the children in y
		 * into z
		 */
		if(!y.isLeaf) {
			for(int j = 0; j < MINIMUM_DEGREE; j++) {
				z.child[j] = y.child[j + MINIMUM_DEGREE];
			}
		}
		
		// node y's key count will be this number after we
		// move the median node up to y's parent
		y.keyCount = MINIMUM_DEGREE -1;
		
		/*
		 * Slide the children of node x up to make room
		 * for z
		 */
		for(int j = x.keyCount +1 ; j >= (index + 1); j--) { // this might be off by one
			x.child[j+1] = x.child[j];
		}
		// set z as child of x
		x.child[index + 1] = z;
		
		/*
		 * Slide the keys of node x up to make room for
		 * z's key
		 */
		for(int j = x.keyCount; j >= index; j--) {
			x.key[j+1] = x.key[j];
		}
		// move the median key from y into node
		x.key[index] = y.key[MINIMUM_DEGREE - 1];
		y.key[MINIMUM_DEGREE - 1] = null;
		
		x.keyCount = x.keyCount + 1;
		// write node y to disk
		// write node z to disk
		// write node node to disk
	}
	
	public void insertNonFull(Node x, Comparable key) {
		int i = x.keyCount -1; // because we are zero based
		
		if(x.isLeaf) {
			while(i >= 0 && key.compareTo(x.key[i]) < 0) {
				x.key[i+1] = x.key[i];
				i--;
			}
			
			x.key[i+1] = key;
			x.keyCount = x.keyCount + 1;
			// write node x to disk
		} else {
			while(i >= 0 && key.compareTo(x.key[i]) < 0) {
				i--;
			}
			i = i + 1;
			
			// read child i of x from disk
			
			if(x.child[i].keyCount == 2 * MINIMUM_DEGREE - 1) {
				splitChild(x, i, x.child[i]);
				if(key.compareTo(x.key[i]) > 0)
					i = i + 1;
			}
			
			insertNonFull(x.child[i], key);
		}
	}
	
	public static void main(String[] args) {
		Btree bt = new Btree();
		
		bt.insert(new Character('A'));
		bt.insert(new Character('D'));
		bt.insert(new Character('F'));
		bt.insert(new Character('H'));
		bt.insert(new Character('L'));
		bt.insert(new Character('N'));
		bt.insert(new Character('P'));
		
		System.out.println(bt.search('A'));
		System.out.println(bt.search('D'));
		System.out.println(bt.search('F'));
		System.out.println(bt.search('H'));
		System.out.println(bt.search('L'));
		System.out.println(bt.search('N'));
		System.out.println(bt.search('P'));

	}
}
