package com.saiyd.datastructures;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;


public class IndexedPriorityQueue <T extends Comparable<T>> implements Iterable<T> {
	public enum Type { MAX, MIN };
	private Type heapType;
	private List<Integer> heapList = new ArrayList<Integer>();
	
	private Map<Integer,Integer> qIndex; // Map of indexNumber to position in the heap array
	private Map<Integer, T> keys;
	private int saiyd;
	
	public IndexedPriorityQueue(Type type) {
		//heapList.addAll(items);
		heapList = new ArrayList<Integer>();
		qIndex = new HashMap<Integer, Integer>();
		keys = new HashMap<Integer,T>();
		heapType = type;
		//buildHeap();
	}
	
	/*public void buildHeap() {
		int N = heapList.size() - 1;
		int heapifyCount = (int) Math.floor((double) (N / 2));
		for(int i = heapifyCount; i >= 0; i--) {
			bubbleDown(i);
		}
	}*/
	
	private boolean isLessThan(int index1, int index2) {
		return keys.get(heapList.get(index1)).compareTo(keys.get(heapList.get(index2))) < 0;
	}
	
	private boolean isGreaterThan(int index1, int index2) {
		//System.out.println(keys);
		return keys.get(heapList.get(index1)).compareTo(keys.get(heapList.get(index2))) > 0;
	}
	
	/*
	 * Returns the index of the parent
	 * of the specified node
	 */
	private int parent(int index) {
		return (index -1) >> 1; //zero base, if the array started at 1, then: index >> 1
	}
	
	/*
	 * Returns the index of the left
	 * child of the specified node
	 */
	private int left(int index) {
		return (index << 1) + 1; // zero base
	}
	
	/*
	 * Returns the index of the right
	 * child of the specified node
	 */
	private int right(int index) {
		return (index << 1) + 2; // zero base
	}
	
	private void exchange(int index1, int index2) {
		/*
		 *  swap items in the binary heap
		 *  
		 *  which is a heap of indexes
		 */
		Integer temp = heapList.get(index1);
		heapList.set(index1, heapList.get(index2));
		heapList.set(index2, temp);
		

		qIndex.put(heapList.get(index1), index1);
		qIndex.put(heapList.get(index2), index2);
	}
	
	protected void bubbleDown(int index) {
		if(heapType == Type.MAX) {
			maxPQBubbleDown(index);
		} else if(heapType == Type.MIN) {
			minPQBubbleDown(index);
		}		
	}
	
	private void maxPQBubbleDown(int index) { // swim
		int heapEnd = heapList.size() -1;
		
		while(left(index) <= heapEnd) {
			// set to the left child of index
			int childIndex = left(index); 
			// compare left child with right child
			if(childIndex < heapEnd && isLessThan(childIndex, childIndex + 1))
				childIndex++; // set to the right child of index
			
			// childIndex is now set to the child with greater value
			
			if(!isLessThan(index, childIndex))
				break;
			exchange(index, childIndex);
			index = childIndex;
		}
	}
	
	private void minPQBubbleDown(int index) { // swim
		int heapEnd = heapList.size() -1;
		
		while(left(index) <= heapEnd) {
			// set to the left child of index
			int childIndex = left(index); 
			int rightChildIndex = childIndex + 1;
			
			// compare left child with right child
			if(childIndex < heapEnd && rightChildIndex <= heapEnd  && isGreaterThan(childIndex, rightChildIndex))
				childIndex++; // set to the right child of index
			
			// childIndex is now set to the child with lesser value
			
			if(!isGreaterThan(index, childIndex))
				break;
			exchange(index, childIndex); // move index down to child's spot
			index = childIndex;
		}
	}
	
	private void bubbleUp(int index) {
		if(heapType == Type.MAX) {
			maxPQBubbleUp(index);
		} else if(heapType == Type.MIN) {
			minPQBubbleUp(index);
		}
	}
	
	private void maxPQBubbleUp(int index) {
		while(index > 0 && isLessThan(parent(index), index)) {
			exchange(index, parent(index));
			index = parent(index);
		}
	}
	
	private void minPQBubbleUp(int index) {
		while(index > 0 && isGreaterThan(parent(index), index)) {
			exchange(index, parent(index));
			index = parent(index);
		}
	}

	public boolean contains(int index) {
		boolean ret = false;
		if(qIndex.get(index) != null)
			ret = true;
		return ret;
	}
	
	public boolean isEmpty() {
		return heapList.size() < 1;
	}
	
	public void insert(int index, T key) {
		if(index < 0)
			throw new IllegalArgumentException("Index is out of bounds");
		if(contains(index))
			throw new IllegalArgumentException("The index already exists in the priority queue");
		/*
		 * increase the heap array by 1
		 * set the new empty element to some marker for invalid values
		 * call increase(indexOfNewLastElement, key)
		 */
		int nextIndexNumber = heapList.size();
		//saiyd++;
		qIndex.put(index, nextIndexNumber);
		
		heapList.add(index);
		
		keys.put(index, key);
		
		if(heapList.size() > 1)
			bubbleUp(nextIndexNumber);
	}
	
	public int peekFrontIndex() {
		if(heapList.size() > 0)
			return heapList.get(0);
		return -2;
	}
	
	public int getFrontIndex() {
		int ret = peekFrontIndex();
		getFrontKey();
		return ret;
	}
	
	public T peekFrontKey() {
		//if(keys.size() > 0 && peekFrontIndex() > -1)
		int index = peekFrontIndex();
		return keys.get(index);
		//return null;
	}
	
	public T getFrontKey() {
		if(heapList.size() < 1)
			return null;
		
		int origFrontIndex = peekFrontIndex();
		T retVal = peekFrontKey();
		int end = heapList.size() -1;
		
		exchange(0, heapList.size() -1);
		
		/* we need to remove the item that was
		 * just swapped to the end of the heap
		 * to ensure that it does not get moved
		 * from the end of the heap during
		 * bubbleDown
		 * 
		 * This is because bubbleDown uses
		 * heapList.size() -1 to represend the end
		 */
		
		heapList.remove(end);
		end = heapList.size() -1; // update end
		
		bubbleDown(0);
		

		
		qIndex.remove(origFrontIndex); 
		keys.remove(origFrontIndex); // not sure if i should do this
		
		// clear the key associated with the old front index
		//qIndex.put(qIndex.get(origFrontIndex), null); 
		
		//keys.put(origFrontIndex, null); // not sure if i should do this
		
		return retVal;
		/*
		 * if(heapSize < 1)
		 *    throw some error
		 * max = heapArray[0];
		 * heapArray[0] = heapArray[heapArray.length -1];
		 * shrink the heap array by 1 element
		 * maxHeapify(heapArray, indexZero)
		 * return max
		 */
	}
	
	public T getKey(int index) {
		if(index >= keys.size() || index < 0)
			throw new IllegalArgumentException("The index is out of bounds");
		if(!contains(index))
			throw new NoSuchElementException();
		return keys.get(index);
	}
	
	/*
	 * Change the key associated with the index to the
	 * specified value
	 */
	public void changeKey(int index, T key) {
		if(index < 0 || index >= heapList.size())
			throw new IndexOutOfBoundsException();
		if(!contains(index))
			throw new NoSuchElementException();
		
		keys.put(index, key);
		bubbleUp(qIndex.get(index));
		bubbleDown(qIndex.get(index));
	}
	
	/*
	 * decrease the key associated with index i to the specified value
	 */
	public void decreaseKey(int index, T key) {
		if(!contains(index))
			throw new NoSuchElementException();
		
		if(keys.get(index).compareTo(key) <= 0)
			throw new IllegalArgumentException("Calling decreaseKey with an argument that would not decrease the key");
		
		keys.put(index, key);
		bubbleUp(qIndex.get(index));
	}
	
	/*
	 * increase the key associated with index i to the specified value
	 */
	public void increaseKey(int index, T key) {
		if(!contains(index))
			throw new NoSuchElementException();
		
		if(keys.get(index).compareTo(key) >= 0)
			throw new IllegalArgumentException("Calling increaseKey with an argument that would not increase the key");
		
		keys.put(index, key);
		bubbleDown(qIndex.get(index));
	}
	
	public void deleteKey(int index) {
		if(index < 0 || index >= heapList.size())
			throw new IndexOutOfBoundsException();
		if(!contains(index))
			throw new NoSuchElementException();
		
		int curIndex = qIndex.get(index);
		exchange(curIndex, heapList.size() - 1);
		
		bubbleUp(curIndex);
		bubbleDown(curIndex);
		keys.remove(keys.get(index));
		qIndex.remove(qIndex.get(index));
	}
	
	@Override
	public Iterator<T> iterator() {
		return this.new IndexedPriorityQueueIterator();
	}
	
	class IndexedPriorityQueueIterator implements Iterator<T> {

		@Override
		public boolean hasNext() {
			return heapList.size() > 0;
		}

		@Override
		public T next() {
			return getFrontKey();
		}

		@Override
		public void remove() {
			// not implemented
		}
		
	}
	
	public static void main(String[] args) {
		SecureRandom sr = new SecureRandom();
		
		IndexedPriorityQueue<Integer> ipq = new IndexedPriorityQueue<Integer>(IndexedPriorityQueue.Type.MIN);
		ArrayList<Integer> arrayList = new ArrayList<Integer>(10);
		for(int i = 0; i < 10; i++) {
			int next = sr.nextInt(20);
			arrayList.add(next);
			ipq.insert(i, next);
		}
		
		//ipq.increaseKey(4, 70);
		ArrayList<Integer> origArrayList = new ArrayList<Integer>(arrayList);
		System.out.println(origArrayList);
		
		
		for(Integer i : ipq) {
			System.out.print(i);
			System.out.print(" ");
		}
		System.out.println();
	}


}
