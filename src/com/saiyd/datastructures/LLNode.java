package com.saiyd.datastructures;

public class LLNode <T> {
	private T data;
	private LLNode<T> next;
	private LLNode<T> prev;
	
	public LLNode(T data) {
		this.data = data;
	}
	
	public T getData() {
		return data;
	}
	
	public void setData(T data) {
		this.data = data;
	}
	
	public LLNode<T> getNext() {
		return next;
	}
	
	public void setNext(LLNode<T> next) {
		this.next = next;
	}
	
	public LLNode<T> getPrev() {
		return prev;
	}
	
	public void setPrev(LLNode<T> prev) {
		this.prev = prev;
	}
}
