package com.saiyd.datastructures;

/*
 * The scheme for representing a binary tree can be extended to
 * any class of trees in which the number of children of each node
 * is at most some constant k. 
 * 
 * It has the advantage of using only O(n) space (vs having each node
 * with an array of children) for any n-node rooted tree.
 * 
 * Instead of having a pointer to each of its children, however,
 * each node x has only two pointers
 *     1. left-child: points to the leftmost child of he node
 *     2. right-sibling: points to the sibling of the node immediately to the right
 */
public class LeftChildRightSiblingTree {
	public static enum NodeColor {
		WHITE, BLACK;
	}
	
	public static class Node<T> {
		T data;
		Node parent;
		Node leftChild;
		Node rightSibling;
		NodeColor color = NodeColor.WHITE;
		
		public T getData() {
			return data;
		}
		public void setData(T data) {
			this.data = data;
		}
		public Node getParent() {
			return parent;
		}
		public void setParent(Node parent) {
			this.parent = parent;
		}
		public Node getLeftChild() {
			return leftChild;
		}
		public void setLeftChild(Node leftChild) {
			this.leftChild = leftChild;
		}
		public Node getRightSibling() {
			return rightSibling;
		}
		public void setRightSibling(Node rightSibling) {
			this.rightSibling = rightSibling;
		}
		public NodeColor getColor() {
			return color;
		}
		public void setColor(NodeColor color) {
			this.color = color;
		}
		@Override
		public String toString() {
			return "Node [data=" + data + "]";
		}
		
	}
	
	public static void main(String[] args) {
		// create an example tree for the debugger
		Node<Integer> root = new Node<Integer>();
		Node<Integer> someNode = new Node<Integer>();
		
		root.leftChild = someNode;
		someNode.parent = root;
		someNode.leftChild = new Node<Integer>();
		someNode.leftChild.parent = someNode;
		
		// add a bunch of siblings
		someNode.rightSibling = new Node<Integer>();
		someNode.rightSibling.parent = root;
		someNode.rightSibling.rightSibling = new Node<Integer>();
		someNode.rightSibling.rightSibling.parent = root;
		someNode.rightSibling.rightSibling.rightSibling = new Node<Integer>();
		someNode.rightSibling.rightSibling.rightSibling.parent = root;
		
		// check with a debugger here
		int nothing = 5;
	}
}
