package com.saiyd.datastructures;

public class LinkedList <T> {
	/*
	 * mHead could also be a sentinel. By this we
	 * mean it could point to the first element with its next pointer
	 * and the tail with its prev pointer The list would then
	 * be circular and doubly linked
	 */
	private LLNode<T> mHead;
	
	public int getSize() {
		int count = 0;
		LLNode<T> tail = mHead;
		while(tail != null) {
			tail = tail.getNext();
			count++;
		}
		return count;
	}
	
	public void addToBack(T data) {
		if(mHead == null) {
			mHead = new LLNode<T>(data);
			//System.out.println("addToBack: new head " + data);
		} else {
			// find the end of the linked list
			LLNode<T> tail = findTail();
			//System.out.println("addToBack: add to tail " + data);
			tail.setNext(new LLNode<T>(data));
		}
	}

	private LLNode<T> findTail() {
		LLNode<T> tail = mHead;
		while(tail.getNext() != null) {
			tail = tail.getNext();
		}
		return tail;
	}
	
	public void addToFront(T data) {
		if(mHead == null) {
			mHead = new LLNode<T>(data);
		} else {
			// make a new head
			LLNode<T> newHead = new LLNode<T>(data);
			newHead.setNext(mHead);
			mHead = newHead;
		}
	}
	
	public T getData(int index) {
		T ret = null;
		int curIndex = 0;
		LLNode<T> curNode = null;
		if(mHead != null && index >= 0) {
			curNode = mHead;
			while(curNode != null) {
				if(curIndex == index) {
					ret = curNode.getData();
					break;
				}
				curNode = curNode.getNext();
				curIndex++;
			}
		}
		return ret;
	}
	
	private LLNode<T> getNode(int index) throws IndexOutOfBoundsException {
		if(index < 0)
			throw new IndexOutOfBoundsException();
		if(index >= getSize())
			throw new IndexOutOfBoundsException();
		
		// TODO throw an exception if the index is too big
		
		int curIndex = 0;
		LLNode<T> curNode = null;
		if(mHead != null && index >= 0) {
			curNode = mHead;
			while(curNode != null) {
				if(curIndex == index) {
					break;
				}
				curNode = curNode.getNext();
				curIndex++;
			}
		}
		return curNode;		
	}
	
	public T removeData(int index) throws IndexOutOfBoundsException {
		T ret = null;
		if(index == 0 && mHead != null) {
			ret = mHead.getData();
			LLNode<T> temp = mHead;
			mHead = mHead.getNext();
			temp.setNext(null);
		} else {
			// find the preceeding node, so we can link to the node following the one we are removing
			int prevNodeIndex = index - 1;
			LLNode<T> prevNode = getNode(prevNodeIndex);
			LLNode<T> node = getNode(index);
			if(prevNode != null && node != null) {
				prevNode.setNext(node.getNext());
				node.setNext(null);
				ret = node.getData();
			}
		}

		return ret;
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		LLNode<T> curNode = mHead;
		if(curNode != null) {
			sb.append(curNode.getData());
			//System.out.println("toString: head " + curNode.getData());
			
			curNode = curNode.getNext();
			
			while(curNode != null) {
				sb.append(", ");
				sb.append(curNode.getData());
				//System.out.println("toString: " + curNode.getData());
				curNode = curNode.getNext();
			}
		}
		
		return sb.toString();
	}
	
	public static void main(String[] args) {
		LinkedList<Integer> ll = new LinkedList<Integer>();
		ll.addToBack(1);
		ll.addToBack(2);
		ll.addToBack(3);
		ll.addToBack(4);
		System.out.println(ll.toString());
		
		LinkedList<Integer> ll2 = new LinkedList<Integer>();
		ll2.addToFront(1);
		ll2.addToFront(2);
		ll2.addToFront(3);
		ll2.addToFront(4);
		System.out.println(ll2.toString());
	}
}
