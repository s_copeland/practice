package com.saiyd.datastructures;

import java.util.Stack;

/*
 * http://www.geeksforgeeks.org/trie-delete/
 * 
 * Looking up data in a trie is faster in the worst case, O(m) time 
 * (where m is the length of a search string)
 * 
 * A trie can provide an alphabetical ordering of the entries by key.
 * 
 * In computer science, a trie, also called digital tree and 
 * sometimes radix tree or prefix tree (as they can be searched 
 * by prefixes), is an ordered tree data structure that is used 
 * to store a dynamic set or associative array where the keys 
 * are usually strings. Unlike a binary search tree, no node in 
 * the tree stores the key associated with that node; instead, 
 * its position in the tree defines the key with which it is 
 * associated. All the descendants of a node have a common 
 * prefix of the string associated with that node, and the root 
 * is associated with the empty string. Values are normally not 
 * associated with every node, only with leaves and some inner 
 * nodes that correspond to keys of interest. For the 
 * space-optimized presentation of prefix tree, see compact 
 * prefix tree.
 * 
 * 
 * Trie is an efficient information retrieval data structure. 
 * Using trie, search complexities can be brought to optimal 
 * limit (key length). If we store keys in binary search tree, 
 * a well balanced BST will need time proportional to M * log N, 
 * where M is maximum string length and N is number of keys 
 * in tree. Using trie, we can search the key in O(M) time. 
 * However the penalty is on trie storage requirements
 * 
 * Though tries are most commonly keyed by character strings, 
 * they don't need to be. The same algorithms can easily be 
 * adapted to serve similar functions of ordered lists of any 
 * construct, e.g., permutations on a list of digits or shapes. 
 * In particular, a bitwise trie is keyed on the individual bits 
 * making up a short, fixed size of bits such as an integer 
 * number or memory address.
 * 
 * A common application of a trie is storing a predictive text 
 * or autocomplete dictionary, such as found on a mobile telephone. 
 * Such applications take advantage of a trie's ability to quickly 
 * search for, insert, and delete entries; however, if storing 
 * dictionary words is all that is required (i.e. storage of 
 * information auxiliary to each word is not required), a minimal 
 * deterministic acyclic finite state automaton would use less 
 * space than a trie. This is because an acyclic deterministic 
 * finite automaton can compress identical branches from the trie 
 * which correspond to the same suffixes (or parts) of different 
 * words being stored.
 * 
 * Tries are also well suited for implementing approximate matching 
 * algorithms,[6] including those used in spell checking and 
 * hyphenation[2] software.
 */
public class LowercaseVocabularyTrie {
	protected static final String ALPHABET = "abcdefghijklmnopqrstuvwxyz";
	protected static final int ALPHABET_SIZE = ALPHABET.length();
	
	public static class Node<T> {
		// this is not really part of the  trie, but
		// it helps the toString function print which char this
		// node is
		int alphaIndex;
		boolean word;
		// This can be the value associated with the key
		// If there is more than one value associated with
		// a key, the value can be a bucket
		T value; 
		Node[] children = new Node[ALPHABET_SIZE];
		public boolean isWord() {
			return word;
		}
		
		public String toString() {
			return ALPHABET.charAt(alphaIndex) + "";// + String.valueOf(word);
		}
	}
	
	protected Node root = new Node();
	protected int size;
	
	public Node contains(String s) {
		Node node = root;
		Node child = null;
		
		if(node == null)
			return node;
		
		for(int i = 0; i < s.length(); i++) {
			int index = ALPHABET.indexOf(s.charAt(i));
			child = node.children[index];
			
			if(child == null) {
				// there is no word that matches
				return null;
			}
			node = child;
		}
		
		return node;
	}
	
	public boolean insert(String word) {
		Node node = root;
		Node child = null;
		
		int wLen = word.length();
		int i = 0;
		for(; i < wLen; i++) {
			int index = ALPHABET.indexOf(word.charAt(i));
			child = node.children[index];
			
			if(child == null) {
				// add missing char as a child
				child = new Node();
				child.alphaIndex = index;
				// indicate this is a word and not a prefix
				if(i == (wLen -1))
					child.word = true;
				
				node.children[index] = child;
			}
			
			node = child; // keep looking for chars not in the trie
		}
		
		size++;
		return true;
	}
	
	/*
	 * During delete operation we delete the key in bottom up manner using recursion. 
	 * The following are possible conditions when deleting key from trie,
	 * 
	 * 	1. Key may not be there in trie. Delete operation should not modify trie.
	 * 
	 * 	2. Key present as unique key (no part of key contains another key (prefix), nor 
	 * 	   the key itself is prefix of another key in trie). Delete all the nodes.
	 * 
	 * 	3. Key is prefix key of another long key in trie. Unmark the leaf node.
	 * 
	 * 	4. Key present in trie, having atleast one other key as prefix key. Delete nodes 
	 * 	   from end of key until first leaf node of longest prefix key.
	 */
	public void delete(String key) {
		if(key == null)
			return;
		if(key.length() == 0)
			return;
		
		/*
		 * we don't check the return value, because
		 * we don't want to delete the root node, as
		 * it represents empty string
		 */
		doDelete(root, key, 0);
		size--;
	}
	
	/*
	 * The return value indicates to the recursive caller
	 * if they should delete the node passed to this function
	 * 
	 * It does not indicate if the delete was successful
	 */
	private boolean doDelete(Node node, String key, int level) {
		if(node != null) {
			// base case
			if(level == key.length()) {
				if(node.word == true) {
					// unmark char as the end of a word
					node.word = false;
					
					// if empty, node to be deleted
					if(!hasChildren(node)) {
						return true;
					}
					
					return false;
				}
			} else { // recursive case
				int index = ALPHABET.indexOf(key.charAt(level));
				
				if(doDelete(node.children[index], key, level+1)) {
					// last node marked delete it
					node.children[index] = null;
					
					// recursively climb up, and delete eligible nodes
					return (!node.word &&  !hasChildren(node));
				}
			}
		}
		
		return false;
	}
	
	public int size() {
		return size;
	}

	private boolean hasChildren(Node node) {
		for(int i = 0; i < ALPHABET_SIZE; i++) {
			if(node.children[i] != null)
				return true;
		}
		return false;
	}
	
	public void printInLexicographicOrder() {
		Node node = root;
		Node child = null;
		
		doRecursiveLexPrint(node, "");
	}
	
	private void doRecursiveLexPrint(Node node, String prefix) {
		if(node == null)
			return;
		
		Node child = null;
		
		// print me first, unless I'm root
		if(node != root) {
			prefix = prefix + ALPHABET.charAt(node.alphaIndex);
			if(node.word)
				System.out.println(prefix);
		}
		
		// print my children
		for(int i = 0; i < ALPHABET.length(); i++) {
			child = node.children[i];
			doRecursiveLexPrint(child, prefix);
		}
	}
	
	public static void main(String[] args) {
		LowercaseVocabularyTrie trie = new LowercaseVocabularyTrie();
		
		String abc = "abc";
		trie.insert(abc);
		String cow = "cow";
		trie.insert(cow);
		String abcdef = "abcdef";
		trie.insert(abcdef);
		String coward = "coward";
		trie.insert("she");
		trie.insert("sells");
		trie.insert("sea");
		trie.insert("shore");
		trie.insert("the");
		trie.insert("by");
		trie.insert("sheer");
		
		System.out.println(trie.contains(abc));
		System.out.println(trie.contains(cow));
		System.out.println(trie.contains(abcdef));
		System.out.println(trie.contains(coward));
		System.out.println(trie.contains("ab"));
		System.out.println(trie.contains("cowa"));
		System.out.println(trie.contains("s"));
		System.out.println(trie.size());
		trie.printInLexicographicOrder();
		trie.delete(abc);
		System.out.println(trie.size());
		
	}
}
