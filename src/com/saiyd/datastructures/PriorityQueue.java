package com.saiyd.datastructures;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;


public class PriorityQueue <T extends Comparable<T>> implements Iterable<T> {
	public enum Type { MAX, MIN };
	protected Type heapType;
	protected List<T> heapList = new ArrayList<T>();
	
	public PriorityQueue(Type type) {
		heapType = type;
	}
	
	public PriorityQueue(Type type, List<T> items) {
		heapType = type;
		heapList.addAll(items);
		buildHeap();
	}
	
	public String toString() {
		return heapList.toString();
	}
	
	public int size() {
		return heapList.size();
	}
	
	/*
	 * get the max or min
	 */
	public T peekFront() {
		if(heapList.size() > 0)
			return heapList.get(0);
		return null;
	}
	
	public T getFront() {
		if(heapList.size() < 1)
			return null;
		
		T retVal = heapList.get(0);
		int end = heapList.size() -1;
		heapList.set(0, heapList.get(end));
		heapList.remove(end);
		end = heapList.size() -1; // update end
		bubbleDown(heapList, end, 0);
		return retVal;
		/*
		 * if(heapSize < 1)
		 *    throw some error
		 * max = heapArray[0];
		 * heapArray[0] = heapArray[heapArray.length -1];
		 * shrink the heap array by 1 element
		 * maxHeapify(heapArray, indexZero)
		 * return max
		 */
	}
	
	/*
	 * Swim
	 */
	public void bubbleUp(int index, T key) {
		bubbleUpInternal(heapList, index, key);
	}
	
	protected void bubbleUpInternal(List<T> list, int index, T key) {
		/*
		 * if(key < heapArray[index])
		 *    throw some error
		 * 
		 * heapArray[index] = key;
		 * while( index > 0 && heapArray[parent(index)] < heapArray[index]) {
		 *   exchange heapArray[index] with heapArray[parent(index)]
		 *   index = parent(index)
		 */
		if(key.compareTo(list.get(index)) == -1);
		if(heapType == Type.MAX && key.compareTo(list.get(index)) == -1) {
			throw new IllegalArgumentException("The priority is already higher that requested");
		} else if(heapType == Type.MIN && key.compareTo(list.get(index)) == 1) {
			throw new IllegalArgumentException("The priority is already lower that requested");
		}
		
		list.set(index, key);
		
		if(heapType == Type.MAX) {
			while(index > 0 && list.get(parent(index)).compareTo(list.get(index)) == -1) {
				T temp = list.get(parent(index));
				list.set(parent(index), list.get(index));
				list.set(index, temp);
				
				index = parent(index);
			}
		} else if(heapType == Type.MIN) {
			while(index > 0 && list.get(parent(index)).compareTo(list.get(index)) == 1) {
				T temp = list.get(parent(index));
				list.set(parent(index), list.get(index));
				list.set(index, temp);
				
				index = parent(index);
			}
		}

			
	}
	
	public void insert(T key) {
		/*
		 * increase the heap array by 1
		 * set the new empty element to some marker for invalid values
		 * call increase(indexOfNewLastElement, key)
		 */
		heapList.add(key);
		bubbleUp(heapList.size() - 1, key);
	}
	
	
	public void buildHeap() {
		int N = heapList.size() - 1;
		int heapifyCount = (int) Math.floor((double) (N / 2));
		for(int i = heapifyCount; i >= 0; i--) {
			if(heapType == Type.MAX) {
				maxPQBubbleDown(heapList, N, i);
				//sink(i);
			} else if(heapType == Type.MIN) {
				minPQBubbleDown(heapList, N, i);
			}
		}
	}
	
	protected void bubbleDown(List<T> heapList, int end, int rootIndex) {
		if(heapType == Type.MAX) {
			maxPQBubbleDown(heapList, end, rootIndex);
		} else if(heapType == Type.MIN) {
			minPQBubbleDown(heapList, end, rootIndex);
		}		
	}
	
	private void exchange(int index1, int index2) {
		T temp = heapList.get(index1);
		heapList.set(index1, heapList.get(index2));
		heapList.set(index2, temp);
	}
	/*
	 * Sink
	 * 
	 * Restores the heap property 
	 * 
	 *  the above algorithm to correctly re-heapify the array, 
	 *  the node at index i and its two direct children must 
	 *  violate the heap property. 
	 *  If they do not, the algorithm will fall through with no change to the array. 
	 *  
	 * assumes that binary trees rooted at
	 * left(i) and right(i) are heaps
	 */
	private void maxPQBubbleDown(List<T> heapList, int heapEnd, int rootIndex) { // swim
		int leftChildIndex = left(rootIndex);
		int rightChildIndex = right(rootIndex);
		int largerIndex = rootIndex;
		
		//if(leftChildIndex <= numItemsToHeapify && heapList.get(leftChildIndex).compareTo(heapList.get(largerIndex)) > 0)
		if(leftChildIndex <= heapEnd && isGreaterThan(heapList, leftChildIndex, largerIndex))
			largerIndex = leftChildIndex;
		
		if(rightChildIndex <= heapEnd && isGreaterThan(heapList, rightChildIndex, largerIndex))
			largerIndex = rightChildIndex;
		
		if(largerIndex != rootIndex) {
			T temp = heapList.get(rootIndex);
			heapList.set(rootIndex, heapList.get(largerIndex));
			heapList.set(largerIndex, temp);
			maxPQBubbleDown(heapList, heapEnd, largerIndex);
		}
		//System.out.println(heapList);
	}
	
	private boolean isLessThan(List<T> list, int index1, int index2) {
		return list.get(index1).compareTo(list.get(index2)) < 0;
	}
	
	private boolean isGreaterThan(List<T> list, int index1, int index2) {
		return list.get(index1).compareTo(list.get(index2)) > 0;
	}
	
	private void minPQBubbleDown(List<T> heapList, int heapEnd, int rootIndex) {
		int leftChildIndex = left(rootIndex);
		int rightChildIndex = right(rootIndex);
		int smallestIndex = rootIndex;
		
		//if(leftChildIndex <= numItemsToHeapify && heapList.get(leftChildIndex).compareTo(heapList.get(smallestIndex)) == -1)
		if(leftChildIndex <= heapEnd && isLessThan(heapList, leftChildIndex, smallestIndex))
			smallestIndex = leftChildIndex;
		
		if(rightChildIndex <= heapEnd && isLessThan(heapList, rightChildIndex, smallestIndex))
			smallestIndex = rightChildIndex;
		
		if(smallestIndex != rootIndex) {
			T temp = heapList.get(rootIndex);
			heapList.set(rootIndex, heapList.get(smallestIndex));
			heapList.set(smallestIndex, temp);
			minPQBubbleDown(heapList, heapEnd, smallestIndex);
		}
		//System.out.println(heapList);
	}
	
	/*
	 * Returns the index of the parent
	 * of the specified node
	 */
	private int parent(int index) {
		return (index -1) >> 1; //zero base, if the array started at 1, then: index >> 1
	}
	
	/*
	 * Returns the index of the left
	 * child of the specified node
	 */
	private int left(int index) {
		return (index << 1) + 1; // zero base
	}
	
	/*
	 * Returns the index of the right
	 * child of the specified node
	 */
	private int right(int index) {
		return (index << 1) + 2; // zero base
	}
	
	@Override
	public Iterator<T> iterator() {
		return this.new PriorityQueueIterator();
	}
	
	class PriorityQueueIterator implements Iterator<T> {

		@Override
		public boolean hasNext() {
			return heapList.size() > 0;
		}

		@Override
		public T next() {
			return getFront();
		}

		@Override
		public void remove() {
			// not implemented
		}
		
	}
	
	public static void main(String[] args) {
		SecureRandom sr = new SecureRandom();
		
		ArrayList<Integer> arrayList = new ArrayList<Integer>(10);
		for(int i = 0; i < 10; i++) {
			arrayList.add(sr.nextInt(20));
		}
		
		ArrayList<Integer> origArrayList = new ArrayList<Integer>(arrayList);
		ArrayList<Integer> origArrayList2 = new ArrayList<Integer>(arrayList);
		System.out.println(origArrayList);
		
		PriorityQueue<Integer> pq = new PriorityQueue<Integer>(PriorityQueue.Type.MIN, arrayList);
		for(Integer i : pq) {
			System.out.print(i);
			System.out.print(" ");
		}
		System.out.println();
		// increase the priority queue of the 4th item
		pq = new PriorityQueue<Integer>(PriorityQueue.Type.MIN, origArrayList);
		pq.bubbleUp(3, 0);
		for(Integer i : pq) {
			System.out.print(i);
			System.out.print(" ");
		}
		System.out.println();
		// insert into the priority queue
		pq = new PriorityQueue<Integer>(PriorityQueue.Type.MIN, origArrayList);
		pq.insert(10);
		for(Integer i : pq) {
			System.out.print(i);
			System.out.print(" ");
		}
	}


}
