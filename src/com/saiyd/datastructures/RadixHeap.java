package com.saiyd.datastructures;

/*
 * http://ssp.impulsetrain.com/radix-heap.html
 * 
 * The Radix Heap is a priority queue that has better caching behavior than 
 * the well-known binary heap, but also two restrictions: 
 * 		(a) that all the keys in the heap are integers and 
 * 		(b) that you can never insert a new item that is smaller than all 
 * 		    the other items currently in the heap.
 * 
 * 
 * These restrictions are not that severe. The Radix Heap still works in many 
 * algorithms that use heaps as a subroutine: Dijkstra’s shortest-path algorithm, 
 * Prim’s minimum spanning tree algorithm, various sweepline algorithms in 
 * computational geometry.
 * 
 * Here is how it works. 
 * 
 * If we assume that the keys are 32 bit integers, the radix heap will have 33 
 * buckets, each one containing a list of items. We also maintain one global 
 * value last_deleted, which is initially MIN_INT and otherwise contains the 
 * last value extracted from the queue.
 * 
 * The invariant is this:
 * 
 * The items in bucket k differ from last_deleted in bit k−1, but not in bit k 
 * or higher. The items in bucket 0 are equal to last_deleted.
 */
public class RadixHeap {

}
