package com.saiyd.datastructures;

/*
 * In computer science, a radix tree (also patricia trie, radix trie or compact prefix tree) 
 * is a data structure that represents a space-optimized trie in which each node with only 
 * one child is merged with its parent. The result is that every internal node has up to the 
 * number of children of the radix r of the radix trie, where r is a positive integer and a 
 * power x of 2, having x ≥ 1. Unlike in regular tries, edges can be labeled with sequences 
 * of elements as well as single elements. This makes them much more efficient for small sets 
 * (especially if the strings are long) and for sets of strings that share long prefixes.
 * 
 * Unlike regular trees (where whole keys are compared en masse from their beginning up to 
 * the point of inequality), the key at each node is compared chunk-of-bits by chunk-of-bits, 
 * where the quantity of bits in that chunk at that node is the radix r of the radix trie. 
 * When the r is 2, the radix trie is binary (i.e., compare that node's 1-bit portion of the 
 * key), which minimizes sparseness at the expense of maximizing trie depth—i.e., maximizing 
 * up to conflation of nondiverging bit-strings in the key. When r is an integer power of 2 
 * greater or equal to 4, then the radix trie is an r-ary trie, which lessens the depth of 
 * the radix trie at the expense of potential sparseness.
 * 
 * As an optimization, edge labels can be stored in constant size by using two pointers to a 
 * string (for the first and last elements).[1]
 * 
 * Applications
 * 
 * Radix trees are useful for constructing associative arrays with keys that can be expressed 
 * as strings. They find particular application in the area of IP routing, where the ability 
 * to contain large ranges of values with a few exceptions is particularly suited to the 
 * hierarchical organization of IP addresses.[2] They are also used for inverted indexes of 
 * text documents in information retrieval.
 */
public class RadixTree {

}
