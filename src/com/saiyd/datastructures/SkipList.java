package com.saiyd.datastructures;

import java.lang.reflect.Array;
import java.security.SecureRandom;
import java.util.Arrays;

/*http://www.csee.umbc.edu/courses/undergraduate/341/fall01/Lectures/SkipLists/skip_lists/skip_lists.html
 * 
 * 			Average	    Worst case
 * Space	O(n)	    O(n log n)
 * Search	O(log n)	O(n)[1]
 * Insert	O(log n)	O(n)
 * Delete	O(log n)	O(n)
 * 
 * A data structure that allows fast search within an ordered sequence of 
 * elements. Fast search is made possible by maintaining a linked hierarchy 
 * of subsequences, each skipping over fewer elements. Searching starts in 
 * the sparsest subsequence until two consecutive elements have been found, 
 * one smaller and one larger than the element searched for. Via the linked 
 * hierarchy these two elements link to elements of the next sparsest 
 * subsequence where searching is continued until finally we are searching 
 * in the full sequence. The elements that are skipped over may be chosen 
 * probabilistically
 * 
 * A skip list is built in layers. The bottom layer is an ordinary ordered 
 * linked list. Each higher layer acts as an "express lane" for the lists 
 * below, where an element in layer i appears in layer i+1 with some fixed 
 * probability p (two commonly used values for p are 1/2 or 1/4). On average, 
 * each element appears in 1/(1-p) lists, and the tallest element (usually a 
 * special head element at the front of the skip list) in \log_{1/p} n\, lists.
 * 
 * A search for a target element begins at the head element in the top list, 
 * and proceeds horizontally until the current element is greater than or 
 * equal to the target. If the current element is equal to the target, it has 
 * been found. If the current element is greater than the target, or the 
 * search reaches the end of the linked list, the procedure is repeated after 
 * returning to the previous element and dropping down vertically to the next 
 * lower list. 
 * 
 * Height
 * 		An important aspect of a skip list is the maximum height, because if 
 * 		more time is spent moving down to the next level from a ridiculously 
 * 		high column compared to the rest, any gain from the skip list could be 
 * 		lost.
 * 
 * 		Fortunately, the height can be relatively small and still support a 
 * 		large list. A skip list with a maximum height of 16, for example, 
 * 		could hold approximately 2^16 nodes. So it is reasonable to say that 
 * 		the maximum height for a skip list will be somewhere between 16 and 
 * 		24, for most real world applications.
 */
public class SkipList <T extends Comparable<T>> {
	// max height of any element
	private final static int MAX_HEIGHT = 32;
	
	public static class Node<T> {
		T data;
		// An array of references that point to this node's successor in List i, where i is the index
		Node<T>[] next;
		/* The length of an edge from this node to the next in Lx
		 * In L0 all edge lengths are 1. In Lx the length is the sum
		 * of the edges skipped in the level below
		 */
		int[] length;
		
		Node(T data, int height) {
			this.data = data;
			next = (Node<T>[]) Array.newInstance(Node.class, height + 1);
			length = new int[height +1];
		}
		
		public int height() {
			return next.length -1;
		}
	}
	
	private final Node<T> sentinel;
	private final Node<T>[] stack;
	private int maxHeightOfAnyElement;
	private int size; // number of elements stored in the skiplist
	private SecureRandom rand = new SecureRandom();
	
	public SkipList() {
		size = 0;
		maxHeightOfAnyElement= 0;
		sentinel = new Node(null, MAX_HEIGHT);
		stack = (Node<T>[]) Array.newInstance(Node.class, MAX_HEIGHT);
	}
	
	/**
	 * Find the node that precedes list index i in the skiplist.
	 * 
	 * @param x - the value to search for
	 * @return the predecessor of the node at index i or the final
	 * node if i exceeds size() - 1.
	 */
	protected Node<T> findPredecessor(int index) {
		Node<T> curNode = sentinel;
		int height = maxHeightOfAnyElement;
		int j = -1; // index of the current node in list 0
		while(height >= 0) {
			while(curNode.next[height] != null && (j + curNode.length[height]) < index) {
				j += curNode.length[height];
				curNode = curNode.next[height];
			}
			height--;
		}
		return curNode;
	}
	
	public T get(int index) {
		if(index < 0 || index > size -1)
			throw new IndexOutOfBoundsException();
		return findPredecessor(index).next[0].data;
	}
	
	public T set(int index, T x) {
		if(index < 0 || index > size -1)
			throw new IndexOutOfBoundsException();
		Node<T> node = findPredecessor(index).next[0];
		T prevData = node.data;
		node.data = x;
		return prevData;
	}
	
	/**
	 * Insert a new node into the skiplist
	 * @param i the index of the new node
	 * @param w the node to insert
	 * @return the node u that precedes v in the skiplist
	 */
	protected Node<T> add(int index, Node<T> node) {
		Node<T> curNode = sentinel;
		int k = node.height();
		int curHeight = maxHeightOfAnyElement;
		int j = -1; // index of curNode
		while(curHeight >= 0) {
			while(curNode.next[curHeight] != null && (j + curNode.length[curHeight]) < index) {
				j += curNode.length[curHeight];
				curNode = curNode.next[curHeight];
			}
			
			curNode.length[curHeight]++; // accounts for new node in list 0
			
			if(curHeight <= k) {
				node.next[curHeight] = curNode.next[curHeight];
				curNode.next[curHeight] = node;
				node.length[curHeight] = curNode.length[curHeight] - (index - j);
				curNode.length[curHeight] = index - j;
			}
			
			curHeight--;
		}
		size++;
		return curNode;
	}
	/*
	 * Simulate repeatedly tossing a coin until it comes up tails.
	 * Note, this code will never generate a height greater than 32
	 * 
	 * http://eternallyconfuzzled.com/tuts/datastructures/jsw_tut_skip.aspx
	 * 
	 * We need is a function that returns a weighted random number based on 
	 * a probability equation. To get the binary tree-like structure in a 
	 * skip list, each height must be about half as probable as the height 
	 * below it.
	 * 
	 * Just create a random number, then walk through the bits of the number 
	 * testing for a 0 or a 1
	 * 
	 * On many systems, rand() returns a 16-bit random number, so the largest 
	 * height available on such a system using rand() and the bit stream solution 
	 * is 15. Therefore, it makes sense to use a random number generator that 
	 * is known to have random bits and return at least a 32-bit value.
	 * 
	 * Also note that a probability of 1/2 is not the best, theoretically. A 
	 * probability of 1/3 has seen better performance in real applications, 
	 * and 1/e is the theoretical optimum. 
	 */
	private int pickHeight() {
        int z = rand.nextInt();
        int k = 0;
        int m = 1;
        while ((z & m) != 0) {
            k++;
            m <<= 1;
        }
        
        return k;		
	}
	
	public void add(T data) {
		add(size, data);
	}
	
	public void add(int index, T data) {
		if(index < 0 || index > size)
			throw new IndexOutOfBoundsException();
		
		Node<T> x = new Node<T>(data, pickHeight());
		if(x.height() > maxHeightOfAnyElement)
			maxHeightOfAnyElement = x.height();
		add(index, x);
	}
	
	public T remove(int index) {
		if(index < 0 || index > size -1)
			throw new IndexOutOfBoundsException();
		
		T prevData = null;
		Node<T> curNode = sentinel;
		int curHeight = maxHeightOfAnyElement;
		int j = -1; // index of curNode
		
		while(curHeight >- 0) {
			while(curNode.next[curHeight] != null && (j + curNode.length[curHeight]) < index) {
				j += curNode.length[curHeight];
				curNode = curNode.next[curHeight];
			}
			
			curNode.length[curHeight]--; // for the node we are removing from L0
			
			if(j + curNode.length[curHeight] + 1 == index && curNode.next[curHeight] != null) {
				prevData = curNode.next[curHeight].data;
				curNode.length[curHeight] += curNode.next[curHeight].length[curHeight];
				curNode.next[curHeight] = curNode.next[curHeight].next[curHeight];
				
				if(curNode == sentinel && curNode.next[curHeight] == null)
					maxHeightOfAnyElement--;
			}
			
			curHeight--;
		}
		
		size--;
		return prevData;
	}
	
	public void clear() {
		size = 0;
		maxHeightOfAnyElement = 0;
		Arrays.fill(sentinel.length, 0);
		Arrays.fill(sentinel.next, null);
	}
	
	public int size() {
		return size;
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("{");
		Node<T> node = sentinel.next[0];
		while(node != null) {
			sb.append(node.data);
			if(node.next[0] != null)
				sb.append(", ");
			node = node.next[0];
		}
		sb.append("}");
		return sb.toString();
	}
	/*
	public boolean add(T newData) {
		Node<T> curNode = sentinel;
		int curHeight = MAX_HEIGHT;
		int compareVal = 0;
		
		// clear the stack
		for(int i=0; i < stack.length; i++) {
			stack[i] = null;
		}
		
		while(curHeight >= 0) {
			// go right
			while(curNode.next[curHeight] != null && (compareVal = curNode.next[curHeight].data.compareTo(newData)) <0)
				curNode = curNode.next[curHeight];
			
			if(curNode.next[curHeight] != null && compareVal == 0)
				return false; // can't add, its already here
			
			stack[curHeight--] = curNode;
		}
		
		Node<T> newNode = new Node<T>(newData, pickHeight());
		int someHeight = 0;
		while(someHeight < newNode.height())
			stack[++someHeight] = sentinel; //increasing height of skiplist
		
		for(int i=0; i < newNode.next.length; i++) {
			newNode.next[i] = stack[i].next[i];
			stack[i].next[i] = newNode;
		}
		return true;
	}
	*/
	
	public static void main(String[] args) {
		int n = 20;
		SkipList<Integer> l = new SkipList<Integer>();
		for (int i = 0; i < n; i++) {
			l.add(i);
		}
		System.out.println(l);
		for (int i = -1; i > -n; i--) {
			l.add(0,i);
		}
		System.out.println(l);
		for (int i = 0; i < 20; i++) {
			l.add(n+i,1000+i);
		}
		System.out.println(l);

	}
}
