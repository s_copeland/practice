package com.saiyd.datastructures.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.saiyd.datastructures.LinkedList;

public class TestLinkedList {
	private LinkedList<Integer> integerLinkedList;
	@Before
	public void setUp() throws Exception {
		integerLinkedList = new LinkedList<Integer>();
	}

	@After
	public void tearDown() throws Exception {
		integerLinkedList = null;
	}

	@Test
	public void testAddToBack() {
		integerLinkedList.addToBack(1);
		assertEquals(1, integerLinkedList.getSize());
		assertEquals(new Integer(1), integerLinkedList.getData(0));
		integerLinkedList.addToBack(2);
		assertEquals(2, integerLinkedList.getSize());
		assertEquals(new Integer(2), integerLinkedList.getData(1));
		integerLinkedList.addToBack(3);
		assertEquals(3, integerLinkedList.getSize());
		assertEquals(new Integer(3), integerLinkedList.getData(2));
		integerLinkedList.addToBack(4);
		assertEquals(4, integerLinkedList.getSize());
		assertEquals(new Integer(4), integerLinkedList.getData(3));
	}

	@Test
	public void testAddToFront() {
		integerLinkedList.addToFront(1);
		assertEquals(1, integerLinkedList.getSize());
		assertEquals(new Integer(1), integerLinkedList.getData(0));
		integerLinkedList.addToFront(2);
		assertEquals(2, integerLinkedList.getSize());
		assertEquals(new Integer(2), integerLinkedList.getData(0));
		assertEquals(new Integer(1), integerLinkedList.getData(1));
		integerLinkedList.addToFront(3);
		assertEquals(3, integerLinkedList.getSize());
		assertEquals(new Integer(3), integerLinkedList.getData(0));
		assertEquals(new Integer(2), integerLinkedList.getData(1));
		assertEquals(new Integer(1), integerLinkedList.getData(2));
		integerLinkedList.addToFront(4);
		assertEquals(4, integerLinkedList.getSize());
		assertEquals(new Integer(4), integerLinkedList.getData(0));
		assertEquals(new Integer(3), integerLinkedList.getData(1));
		assertEquals(new Integer(2), integerLinkedList.getData(2));
		assertEquals(new Integer(1), integerLinkedList.getData(3));
	}
	
	@Test
	public void testRemoveData() {
		integerLinkedList.addToFront(1);
		assertEquals(1, integerLinkedList.getSize());
		assertEquals(new Integer(1), integerLinkedList.getData(0));
		integerLinkedList.removeData(0);
		assertEquals(0, integerLinkedList.getSize());
		// remove from negative index
		
	}

}
