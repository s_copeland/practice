package com.saiyd.dynamicprograming;

/*
 * O(nm)
 * 
 * This is the same as the integer partitioning problem
 * 
 * http://www.algorithmist.com/index.php/Coin_Change
 * 
 * 
 * Given a value N, if we want to make change for N cents, and we have infinite supply of each of S = { S1, S2, .. , Sm} 
 * valued coins, how many ways can we make the change? The order of coins doesn�t matter.
 * 
 * For example, for N = 4 and S = {1,2,3}, there are four solutions: {1,1,1,1},{1,1,2},{2,2},{1,3}. So output 
 * should be 4. For N = 10 and S = {2, 5, 3, 6}, there are five solutions: {2,2,2,2,2}, {2,2,3,3}, {2,2,6}, {2,3,5} 
 * and {5,5}. So the output should be 5.
 * 
 * 1) Optimal Substructure
 * To count total number solutions, we can divide all set solutions in two sets.
 * 1) Solutions that do not contain mth coin (or Sm).
 * 2) Solutions that contain at least one Sm.
 * Let count(S[], m, n) be the function to count the number of solutions, then it can be written as sum of count(S[], m-1, n) and count(S[], m, n-Sm).
 * 
 * Therefore, the problem has optimal substructure property as the problem can be solved using solutions to subproblems.
 * 
 */
public class CoinChangeProblem1 {
	public int count(int S[], int m, int n) {
		int i, j, x, y;

		// We need n+1 rows as the table is constructed in bottom up manner
		// using
		// the base case 0 value case (n = 0)
		/*
		 * http://codercareer.blogspot.com/2011/12/no-26-minimal-number-of-coins-for.html
		 * 
		 * Each column except the first one is to denote the number of coins to make change for a specific value
		 * We can calculate the numbers in the Table 1 from left to right.
		 * 
		 * For instance, there are two numbers 4 and 2 under the column title �6�. We have two 
		 * alternatives to make change for 6: the first one is to add a coin with value 1 to a 
		 * set of coins whose total value is 5. Since the minimal number of coins to get value 5 
		 * is 3 (highlighted number under the column tile �5�), the number in the first cell 
		 * under column title �6� is 4 (4=3+1). The second choice is to add a coin with value 3 
		 * to a set of coins whose total value is 3. Since the minimal number of coins to get value 
		 * 3 is 1 (highlighted number under the column tile �3�), the number in the second cell 
		 * under column title �6� is 2 (2=1+1). We highlight the number 2 in the column under 
		 * tile 6 because 2 is less than 4.
		 * 
		 * Each element of the table represents the total # of coins, not necessarily for the 
		 * type of the column or row it is found in, but the total of the preceeding
		 * columns and at least one of the current columns
		 */
		int[][] table = new int[n + 1][m];

		// Fill the enteries for 0 value case (n = 0)
		for (i = 0; i < m; i++)
			table[0][i] = 1;

		// Fill rest of the table entries in bottom up manner
		for (i = 1; i < n + 1; i++) {
			for (j = 0; j < m; j++) {

				// Count of solutions including S[j]
				x = (i - S[j] >= 0) ? table[i - S[j]][j] : 0;

				// Count of solutions excluding S[j]
				y = (j >= 1) ? table[i][j - 1] : 0;

				// total count
				table[i][j] = x + y;
			}
		}
		return table[n][m - 1];
	}
	
	/*
	 * http://codercareer.blogspot.com/2011/12/no-26-minimal-number-of-coins-for.html
	 * 
	 * 
	 */
	public int min(int denom[], int amount) {
		// make the array that stores the min counts one bigger to
		// store the initial case 0
		int[] counts = new int[amount + 1];
		counts[0] = 0;
		int coins = 0;
		int i, j = 0;
		
		/*
		 * We're going to populate a table with the min # of
		 * coins for a increasing amount of money
		 */
		for (i = 1; i <= amount; i++) {
			coins = Integer.MAX_VALUE; // init to max, so its not the min by default
			
			// for each denomination
			for (j = 0; j < denom.length; j++) {
				// if the denom is <= the current total amount
				if(denom[j] <= i) {
					/*
					 * Update coins to the min of the previous value.
					 * This is initial MAX_VALUE and in following
					 * iterations this is counts[i - denom[j]] from
					 * the previous iteration.
					 * 
					 * counts[i - denom[j]] is at least 0 because of
					 * the if statement above. As j increments
					 * the i - denom[j] expression evaluates to
					 * a number that decreases toward 0
					 * 
					 * This in effect compares the min coins
					 * with the previous min coins in counts[]
					 */
					coins = Math.min(coins, counts[i - denom[j]]);
				}
			}
			
			if(coins < Integer.MAX_VALUE) {
				counts[i] = coins + 1;
			} else {
				counts[i] = Integer.MAX_VALUE;
			}
		}
		
		coins = counts[amount];
		return coins;
	}
	
	/*
	 * For a particular N and S = \{ S_1, S_2, ..., S_m \} 
	 * (now with the restriction that S_1 < S_2 < ... < S_m, 
	 * our solutions can be constructed in non-decreasing order), 
	 * the set of solutions for this problem, C(N,m), can be 
	 * partitioned into two sets: 
	 *   There are those sets that do not contain any Sm and
	 *   Those sets that contain at least 1 Sm
	 *   
	 *   If a solution does not contain Sm, then we can solve the 
	 *   subproblem of N with S = \{ S_1, S_2, ..., S_{m-1} \}, 
	 *   or the solutions of C(N,m - 1).
	 *   
	 *   If a solution does contain Sm, then we are using at least 
	 *   one Sm, thus we are now solving the subproblem of N - Sm, 
	 *   S = \{ S_1, S_2, ..., S_m \}. This is C(N - Sm,m).
	 *   
	 *   Thus, we can formulate the following:
	 *   C(N,m) = C(N,m - 1) + C(N - Sm,m)
	 *   
	 *   with the base cases:
	 *   C(N,m) = 1,N = 0 (one solution -- we have no money, exactly one way to solve the problem - by choosing no coin change, or, more precisely, to choose coin change of 0)
	 *   C( N, m ) = 0, N <= 0 (no solution -- negative sum of money)
	 *   C( N, m ) = 0, N >= 1, m <= 0 (no solution -- we have money, but no change available)
	 */
	/*public int countVer2(int[] S, int m, int n) {
		// we need room for the initial n = 0 case
		// so we increase n by one
		int[][] table = new int[n+1][m];
		
		for(int i = 0; i < n+1; i++) {
			for(int j = 0; j < m; j++) {
				if(i == 0) {
					//
					// one solution -- we have no money, exactly one way to solve the problem
					// by choosing no coin change, or, more precisely, to choose coin change of 0
					//
					table[i][j] = 1;
				} else if(j == 0) {
					// we don't have to worry about i-1 being negative
					// because the above if statement handles that case
					table[i][j] = table[i-1][j];
				} else if(S[j] > i) {
					table[i][j] = table[i][j -1];
				} else {
					table[i][j] = table[i - S[j]][j]+ table[i][j - 1];
				}
			}
		}
		return table[n][m-1];
	}*/

	public static void main(String[] args) {
		int[] array = { 1, 2, 3, 4 };
		int n = 5;
		CoinChangeProblem1 cp = new CoinChangeProblem1();

		System.out.println("Count = " + cp.count(array, array.length, n));
		System.out.println("Count = " + cp.min(array, n));
	}
}
