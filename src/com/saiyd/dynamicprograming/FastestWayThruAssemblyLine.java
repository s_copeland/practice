package com.saiyd.dynamicprograming;

public class FastestWayThruAssemblyLine {
	private final int numOfStationsPerAssemblyLine = 6;
	private int[][] assemblyTime = {
			{7, 9, 3, 4, 8, 4}, // line 1, from station 1 to n (minus one on both if zero based
			{8, 5, 6, 4, 5, 7}  // line 2
	};
	
	private int[][] intraLineTransitionTime = {
			{2, 3, 1, 3, 4}, // line 1 to line 2, from station 1 to n -1, we can't tranfer from the last station to another line
			{2, 1, 2, 2, 1}  // line 2 to line 1, from station 1 to n -1 
	};
	
	private int[] enterTime = {2, 4}; // line 1 and line 2
	private int[] exitTime = {3, 2};
	
	private int fStar = 0; // the fastest time thru the factory
	private int lStar = 0; // the line whose station n is used in a fastest way through the factory.
	
	private int[][] fastestTimeThruStation = new int[2][numOfStationsPerAssemblyLine];
	private int[][] lineNumberWhosePrevStationIsFastestThruThisOne = new int[2][numOfStationsPerAssemblyLine];
	
	public void fastestWay(int[][] a, int[][] t, int[] e, int[] x, int n) {
		fastestTimeThruStation[0][0] = e[0] + a[0][0]; // fastest time thru station 1 is, enter time + assembly time of station 1
		fastestTimeThruStation[1][0] = e[1] + a[1][0];
		
		int lastStation = n -1;
		
		for(int j = 1; j < n; j++) {
			if( (fastestTimeThruStation[0][j - 1] + a[0][j]) <= (fastestTimeThruStation[1][j - 1] + t[1][j-1] + a[0][j]) ) {
				fastestTimeThruStation[0][j] = fastestTimeThruStation[0][j - 1] + a[0][j];
				lineNumberWhosePrevStationIsFastestThruThisOne[0][j] = 0;
			} else {
				fastestTimeThruStation[0][j] = fastestTimeThruStation[1][j-1] + t[1][j-1] + a[0][j];
				lineNumberWhosePrevStationIsFastestThruThisOne[0][j] = 1;
			}
			
			if( (fastestTimeThruStation[1][j-1] + a[1][j]) <= (fastestTimeThruStation[0][j-1] + t[0][j-1] + a[1][j])) {
				fastestTimeThruStation[1][j] = fastestTimeThruStation[1][j-1] + a[1][j];
				lineNumberWhosePrevStationIsFastestThruThisOne[1][j] = 1;
			} else {
				fastestTimeThruStation[1][j] = fastestTimeThruStation[0][j-1] + t[0][j-1] + a[1][j];
				lineNumberWhosePrevStationIsFastestThruThisOne[1][j] = 0;				
			}
		}
		
		if( (fastestTimeThruStation[0][lastStation] + x[0]) <= (fastestTimeThruStation[1][lastStation] + x[1]) ) {
			fStar = fastestTimeThruStation[0][lastStation] + x[0];
			lStar = 0;
		} else {
			fStar = fastestTimeThruStation[1][lastStation] + x[1];
			lStar = 1;
		}
	}
	
	public void printStations() {
		int i = lStar;
		// make the stations 1 based instead of zero based to match the clsr book answers
		System.out.println("line " + (i + 1) + ", station " + (numOfStationsPerAssemblyLine));
		for(int j = numOfStationsPerAssemblyLine -1; j > 0; j--) {
			i = lineNumberWhosePrevStationIsFastestThruThisOne[i][j];
			System.out.println("line " + (i + 1) + ", station " + j); // make the stations 1 based instead of zero based to match the clsr book answers
		}
	}
	
	public static void main(String[] args) {
		FastestWayThruAssemblyLine f = new FastestWayThruAssemblyLine();
		f.fastestWay(f.assemblyTime, f.intraLineTransitionTime, f.enterTime, f.exitTime, f.numOfStationsPerAssemblyLine );
		f.printStations();
		
		// answer
		/*
		 * line 1, station 6
		 * line 2, station 5
		 * line 2, station 4
		 * line 1, station 3
		 * line 2, station 2
		 * line 1, station 1
		 */
	}
}
