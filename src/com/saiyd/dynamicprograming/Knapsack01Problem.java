package com.saiyd.dynamicprograming;

import java.util.List;

/*
 * We want to maximize value, but minimize weight

taking the item to the left means skip item i
	We are forced to make this choice if the item exceeds the current capacity based on its weight

add the value of the item + the of the item to the left and W_i rows down, which corresponds to reducing the capacity by w_i
	we get the max for the reduced capacity

	if we take the item, then we can only take the current max weight - the weight for the item
		W - W_i


total capacity = 6

item1 value = 3, weight = 4
item2 value = 2, weight = 3
item3 value = 4, weight = 2
item4 value = 4, weight = 3


each cell is the sum of the item values taken
	either the optimal solution if we skip item x
		or
	the sum of item x and the sum of the optimal sub problem which is W - W_i
 */
public class Knapsack01Problem {

	public static class Item {
		int value;
		int weight;
		
		public Item(int value, int weight) {
			this.value = value;
			this.weight = weight;
		}
	}
	
	public void getOptimalSack(Item[] items, int maxWeight) {
		// we need n + 1 items to represent the 0 value states, this can be optimized away
		// we need maxWeight + 1 items to represent the 0 weight
		
		int[][] table = new int[items.length + 1][maxWeight +1];
		// in c++ we would initialize the array to 0 for [0][x], in java we don't need to do this
		
		// the array is one based and items is zero based, use i-1
		for(int i = 1; i < table.length; i++) {
			for(int x = 0; x < table[0].length; x++) {
				// make sure the sub problem index is valid when we need to compare in 
				// the max function
				int subProblemValue = 0;
				if( (x -  items[i-1].weight) > 0) {
					subProblemValue = table[i-1][x - items[i-1].weight] + items[i-1].value;
				}
				table[i][x] = Math.max(
						table[i -1][x], 
						subProblemValue 
						);
			}
		}
		
		// trace backwards to get the items in the sack
		int i = table.length - 1;
		int x = table[0].length - 1;
		while(i > 0 && x > 0) {
			if(table[i][x] == 0 || table[i][x] == table[i-1][x]) {
				// we skipped item i, move to the side
				i--;
			} else {
				// we used item i
				System.out.print("Item " + i + " ");
				
				// move to the optimal sub problem added to this item
				i--;
				x = maxWeight - items[i-1].weight;
			}
		}
	}
	
	public static void main(String[] args) {
		Item[] items = new Item[4];
		items[0] = new Item(3, 4);
		items[1] = new Item(2, 3);
		items[2] = new Item(4, 2);
		items[3] = new Item(4, 3);
		
		Knapsack01Problem knap = new Knapsack01Problem();
		knap.getOptimalSack(items, 6);
	}
}
