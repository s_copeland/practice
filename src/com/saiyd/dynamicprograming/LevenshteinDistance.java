package com.saiyd.dynamicprograming;

/*
 * In information theory and computer science, the Levenshtein distance is a string 
 * metric for measuring the difference between two sequences. Informally, the 
 * Levenshtein distance between two words is the minimum number of single-character 
 * edits (i.e. insertions, deletions or substitutions) required to change one word 
 * into the other. It is named after Vladimir Levenshtein, who considered this 
 * distance in 1965.[1]
 * 
 * Levenshtein distance may also be referred to as edit distance, although that may 
 * also denote a larger family of distance metrics.[2]:32 It is closely related to 
 * pairwise string alignments.
 * 
 * Example[edit]
 * 	For example, the Levenshtein distance between "kitten" and "sitting" is 3, since the following three edits change one into the other, and there is no way to do it with fewer than three edits:
 * 
 * 	kitten → sitten (substitution of "s" for "k")
 * 	sitten → sittin (substitution of "i" for "e")
 * 	sittin → sitting (insertion of "g" at the end).
 * 
 * Upper and lower bounds[edit]
 * 	The Levenshtein distance has several simple upper and lower bounds. These include:
 * 
 * 	It is always at least the difference of the sizes of the two strings.
 * 	It is at most the length of the longer string.
 * 	It is zero if and only if the strings are equal.
 * 	If the strings are the same size, the Hamming distance is an upper bound on the Levenshtein distance.
 * 	The Levenshtein distance between two strings is no greater than the sum of their Levenshtein distances from a third string (triangle inequality).
 */
public class LevenshteinDistance {
	public int recursive(String s, int sLen, String t, int tLen) {
		/* base case: empty strings */
		if (sLen == 0)
			return tLen;
		if (tLen == 0)
			return sLen;

		int cost = 0;
		/* test if last characters of the strings match */
		if (s.charAt(sLen - 1) == t.charAt(tLen - 1))
			cost = 0;
		else
			cost = 1;

		/*
		 * return minimum of delete char from s, delete char from t, and delete
		 * char from both
		 */
		return minimum(recursive(s, sLen - 1, t, tLen) + 1,
				recursive(s, sLen, t, tLen - 1) + 1,
				recursive(s, sLen - 1, t, tLen - 1) + cost);
	}

	private int minimum(int a, int b, int c) {
		int min = Math.min(a, b);
		min = Math.min(min, c);
		return min;
	}

	public int iterativeDynamic(String sStr, String tStr) {

		// degenerate cases
		if (sStr.equals(tStr))
			return 0;
		if (sStr.length() == 0)
			return tStr.length();
		if (tStr.length() == 0)
			return sStr.length();

		char[] s = sStr.toCharArray();
		char[] t = tStr.toCharArray();

		// for all i and j, d[i,j] will hold the Levenshtein distance between
		// the first i characters of s and the first j characters of t;
		// note that d has (m+1)*(n+1) values
		int[][] d = new int[s.length + 1][t.length + 1];

		// we don't need to zero out the array in java

		// source prefixes can be transformed into empty string by
		// dropping all characters
		for (int i = 1; i <= s.length; i++) {
			d[i][0] = i;
		}

		// target prefixes can be reached from empty source prefix
		// by inserting every character
		for (int i = 1; i <= t.length; i++) {
			d[0][i] = i;
		}

		for (int j = 1; j <= t.length; j++) {
			for (int i = 1; i <= s.length; i++) {
				if (s[i - 1] == t[j - 1]) { // The string is indexed from 0 to <
											// len, the matrix is indexed from 0
											// to < len + 1
					d[i][j] = d[i - 1][j - 1]; // no operation required

				} else {
					d[i][j] = minimum(d[i - 1][j] + 1, // a deletion
							d[i][j - 1] + 1, // an insertion
							d[i - 1][j - 1] + 1 // a substitution
					);
				}
			}
		}

		return d[s.length][t.length];
	}

	/*
	 * It turns out that only two rows of the table are needed for the
	 * construction if one does not want to reconstruct the edited input strings
	 * (the previous row and the current row being calculated).
	 */
	public int optimizedIterativeDynamic(String sStr, String tStr) {
		// degenerate cases
		if (sStr.equals(tStr))
			return 0;
		if (sStr.length() == 0)
			return tStr.length();
		if (tStr.length() == 0)
			return sStr.length();

		char[] s = sStr.toCharArray();
		char[] t = tStr.toCharArray();

		// create two work vectors of integer distances
		int[] v0 = new int[t.length + 1];
		int[] v1 = new int[t.length + 1];

		// initialize v0 (the previous row of distances)
		// this row is A[0][i]: edit distance for an empty s
		// the distance is just the number of characters to delete from t
		for (int i = 0; i < v0.length; i++)
			v0[i] = i;

		for (int i = 0; i < s.length; i++) {
			// calculate v1 (current row distances) from the previous row v0

			// first element of v1 is A[i+1][0]
			// edit distance is delete (i+1) chars from s to match empty t
			v1[0] = i + 1;

			// use formula to fill in the rest of the row
			for (int j = 0; j < t.length; j++) {
				int cost = (s[i] == t[j]) ? 0 : 1;
				v1[j + 1] = minimum(v1[j] + 1, v0[j + 1] + 1, v0[j] + cost);
			}

			// copy v1 (current row) to v0 (previous row) for next iteration
			for (int j = 0; j < v0.length; j++)
				v0[j] = v1[j];
		}

		return v1[t.length];
	}

	public static void main(String[] args) {
		LevenshteinDistance ld = new LevenshteinDistance();
		String s = "kitten";
		String t = "sitting";

		System.out.println("s = " + s);
		System.out.println("t = " + t);
		System.out.println("Recursive Levenshtein distance = "
				+ ld.recursive(s, s.length(), t, t.length()));
		System.out.println("Dynamic Programming Levenshtein distance = "
				+ ld.iterativeDynamic(s, t));
		System.out
				.println("Optimized Dynamic Programming Levenshtein distance = "
						+ ld.optimizedIterativeDynamic(s, t));
	}
}
