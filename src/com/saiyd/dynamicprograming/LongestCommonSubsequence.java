package com.saiyd.dynamicprograming;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/*
 * The longest common subsequence (LCS) problem is the problem of finding the 
 * longest subsequence common to all sequences in a set of sequences (often 
 * just two sequences). It differs from problems of finding common substrings: 
 * unlike substrings, subsequences are not required to occupy consecutive 
 * positions within the original sequences. 
 * 
 * The longest common subsequence problem is a classic computer science problem, 
 * the basis of data comparison programs such as the diff utility, and has 
 * applications in bioinformatics. It is also widely used by revision control 
 * systems such as Git for reconciling multiple changes made to a revision-controlled 
 * collection of files.
 * 
 * Complexity
 * 
 * For the general case of an arbitrary number of input sequences, the problem is 
 * NP-hard.[1] When the number of sequences is constant, the problem is solvable 
 * in polynomial time by dynamic programming
 * 
 * Runtime
 * 
 * The running time of the dynamic programming approach is O(n × m)
 * 
 * Example
 * 	String 1 = BDCABA
 * 	String 2 = ABCBDAB
 * 	Answer =   BDAB
 * 
 * Prefixes
 * 		The subproblems become simpler as the sequences become shorter. Shorter 
 * 		sequences are conveniently described using the term prefix. A prefix of 
 * 		a sequence is the sequence with the end cut off. Let S be the sequence 
 * 		(AGCA). Then, the sequence (AG) is one of the prefixes of S. Prefixes are 
 * 		denoted with the name of the sequence, followed by a subscript to indicate 
 * 		how many characters the prefix contains.[4] The prefix (AG) is denoted S2, 
 * 		since it contains the first 2 elements of S. The possible prefixes of S are
 * 
 * 			S1 = (A)
 * 			S2 = (AG)
 * 			S3 = (AGC)
 * 			S4 = (AGCA).
 * 
 * 		The solution to the LCS problem for two arbitrary sequences, X and Y, amounts 
 * 		to construct some function, LCS(X, Y), that gives the longest subsequences 
 * 		common to X and Y. That function relies on the following two properties.
 * 
 * First property
 * 		Suppose that two sequences both end in the same element. To find their LCS, 
 * 		shorten each sequence by removing the last element, find the LCS of the 
 * 		shortened sequences, and to that LCS append the removed element.
 * 
 * 		For example, here are two sequences having the same last element: 
 * 			(BANANA) and (ATANA).
 * 
 * 		Remove the same last element. Repeat the procedure until you find no common 
 * 		last element. The removed sequence will be (ANA).
 * 
 * 		The sequences now under consideration: (BAN) and (AT)
 * 		The LCS of these last two sequences is, by inspection, (A).
 * 
 * 		Append the removed element, (ANA), giving (AANA), which, by inspection, is 
 * 		the LCS of the original sequences.
 * 
 * 		In terms of prefixes,
 * 			if xn=ym, LCS(Xn, Ym) = (LCS( Xn-1, Ym-1), xn)
 * 
 * 		where the comma indicates that the following element, xn, is appended to the 
 * 		sequence. Note that the LCS for Xn and Ym involves determining the LCS of the 
 * 		shorter sequences, Xn-1 and Ym-1.
 * 
 * Second property
 * 		Suppose that the two sequences X and Y do not end in the same symbol. Then the 
 * 		LCS of X and Y is the longer of the two sequences LCS(Xn,Ym-1) and LCS(Xn-1,Ym).
 * 
 * 		To understand this property, consider the two following sequences :
 * 			sequence X: ABCDEFG (n elements)
 * 			sequence Y: BCDGK (m elements)
 * 
 * 		The LCS of these two sequences either ends with a G (the last element of sequence X) 
 * 		or does not.
 * 
 * 		Case 1: the LCS ends with a G
 * 			Then it cannot end with a K. Thus it does not hurt to remove the K from 
 * 			sequence Y: if K were in the LCS, it would be its last character; as a 
 * 			consequence K is not in the LCS. We can then write: 
 * 
 * 				LCS(Xn,Ym) = LCS(Xn, Ym-1).
 * 
 * 		Case 2: the LCS does not end with a G
 * 			Then it does not hurt to remove the G from the sequence X (for the same 
 * 			reason as above). And then we can write: 
 * 
 * 				LCS(Xn,Ym) = LCS(Xn-1, Ym).
 * 
 * 		In any case, the LCS we are looking for is one of LCS(Xn, Ym-1) or LCS(Xn-1, Ym). 
 * 		Those two last LCS are both common subsequences to X and Y. LCS(X,Y) is the 
 * 		longest. Thus its value is the longest sequence of LCS(Xn, Ym-1) and LCS(Xn-1, Ym).
 * 
 */
public class LongestCommonSubsequence {
	private int[][] table;
	private char[] string1;
	private char[] string2;

	public int recurLCS(char[] x, char[] y, int m, int n) {
		if (m == 0 || n == 0)
			return 0;

		if (x[m - 1] == y[n - 1]) {
			return 1 + recurLCS(x, y, m - 1, n - 1);
		} else {
			return Math.max(recurLCS(x, y, m, n - 1), recurLCS(x, y, m - 1, n));
		}
	}

	/* Returns length of LCS for X[0..m-1], Y[0..n-1] */
	public int lcs(char[] x, char[] y, int m, int n) {
		string1 = x;
		string2 = y;
		table = new int[m + 1][n + 1];
		int i, j;

		/*
		 * Following steps build L[m+1][n+1] in bottom up fashion. Note that
		 * L[i][j] contains length of LCS of X[0..i-1] and Y[0..j-1]
		 */
		for (i = 0; i <= m; i++) {
			for (j = 0; j <= n; j++) {
				if (i == 0 || j == 0) {// handle the empty string case
					table[i][j] = 0;
				} else if (x[i - 1] == y[j - 1]) {
					table[i][j] = table[i - 1][j - 1] + 1;
				} else {
					//
					table[i][j] = Math.max(table[i - 1][j], table[i][j - 1]);
				}
			}
		}

		/* L[m][n] contains length of LCS for X[0..n-1] and Y[0..m-1] */
		return table[m][n];
	}

	/*
	 * The C matrix in the naive algorithm grows quadratically with the lengths
	 * of the sequences. For two 100-item sequences, a 10,000-item matrix would
	 * be needed, and 10,000 comparisons would need to be done. In most
	 * real-world cases, especially source code diffs and patches, the
	 * beginnings and ends of files rarely change, and almost certainly not both
	 * at the same time. If only a few items have changed in the middle of the
	 * sequence, the beginning and end can be eliminated. This reduces not only
	 * the memory requirements for the matrix, but also the number of
	 * comparisons that must be done.
	 */
	public int lcsInputOptimized(char[] x, char[] y) {
		int start = 0;
		int xEnd = x.length - 1;
		int yEnd = y.length - 1;

		// trim off the matching items at the beginning
		while (start <= xEnd && start <= yEnd && x[start] == y[start])
			start++;

		// trim off the matching items at the end
		while (start <= xEnd && start <= yEnd && x[xEnd] == y[yEnd]) {
			xEnd--;
			yEnd--;
		}

		string1 = x;
		string2 = y;
		int m = xEnd - start + 1;
		int n = yEnd - start + 1;

		table = new int[m + 1][n + 1];
		int i, j;
		/*
		 * Following steps build L[m+1][n+1] in bottom up fashion. Note that
		 * L[i][j] contains length of LCS of X[0..i-1] and Y[0..j-1]
		 */
		for (i = start; i <= xEnd; i++) {
			for (j = start; j <= yEnd; j++) {
				if (i == 0 || j == 0) {// handle the empty string case
					table[i][j] = 0;
				} else if (x[i - 1] == y[j - 1]) {
					table[i][j] = table[i - 1][j - 1] + 1;
				} else {
					//
					table[i][j] = Math.max(table[i - 1][j], table[i][j - 1]);
				}
			}
		}

		/* L[m][n] contains length of LCS for X[0..n-1] and Y[0..m-1] */
		return table[m][n];

	}

	public String getOneLCS() {
		StringBuilder sb = new StringBuilder();
		if (table != null && string1 != null && string2 != null)
			backTrack(sb, string1.length, string2.length);
		return sb.toString();
	}

	/*
	 * i and j index the table of the previously computed LCS i ranges from 0 to
	 * n + 1, j ranges from 0 to m + 1 This is because we add the 0 (empty
	 * string state)
	 * 
	 * string1 is of length n, string2 is of length m We need to reduce i and j
	 * by one to ensure they don't go out of bounds
	 */
	private void backTrack(StringBuilder sb, int i, int j) {
		if (i == 0 || j == 0) {
			// append empty string, aka do nothing
			return;
		} else if (string1[i - 1] == string2[j - 1]) {
			backTrack(sb, i - 1, j - 1);
			sb.append(string1[i - 1]);
		} else {
			if (table[i][j - 1] > table[i - 1][j]) {
				backTrack(sb, i, j - 1);
			} else {
				backTrack(sb, i - 1, j);
			}
		}
	}

	public static class StringBuilderComparator implements
			Comparator<StringBuilder> {

		@Override
		public int compare(StringBuilder arg0, StringBuilder arg1) {
			if (arg0 == null || arg1 == null)
				return -1; // less than?

			return arg0.toString().compareTo(arg1.toString());
		}

	}

	public static final StringBuilderComparator comparator = new StringBuilderComparator();

	public String getAllLCS() {
		Set<StringBuilder> ret = null;
		if (table != null && string1 != null && string2 != null)
			ret = backTrackAll(string1.length, string2.length);
		return ret.toString();
	}

	/*
	 * i and j index the table of the previously computed LCS i ranges from 0 to
	 * n + 1, j ranges from 0 to m + 1 This is because we add the 0 (empty
	 * string state)
	 * 
	 * string1 is of length n, string2 is of length m We need to reduce i and j
	 * by one to ensure they don't go out of bounds
	 */
	private Set<StringBuilder> backTrackAll(int i, int j) {
		Set<StringBuilder> retList = new TreeSet<StringBuilder>(comparator);

		if (i == 0 || j == 0) {
			// append empty string
			retList.add(new StringBuilder());
			return retList;
		} else if (string1[i - 1] == string2[j - 1]) {
			retList = backTrackAll(i - 1, j - 1);
			for (StringBuilder curSB : retList) {
				curSB.append(string1[i - 1]);
			}
		} else {
			if (table[i][j - 1] >= table[i - 1][j])
				retList = backTrackAll(i, j - 1);
			if (table[i - 1][j] >= table[i][j - 1])
				retList.addAll(backTrackAll(i - 1, j));

		}

		return retList;
	}

	/*
	 * function printDiff(C[0..m,0..n], X[1..m], Y[1..n], i, j) if i > 0 and j >
	 * 0 and X[i] = Y[j] printDiff(C, X, Y, i-1, j-1) print "  " + X[i] else if
	 * j > 0 and (i = 0 or C[i,j-1] ≥ C[i-1,j]) printDiff(C, X, Y, i, j-1) print
	 * "+ " + Y[j] else if i > 0 and (j = 0 or C[i,j-1] < C[i-1,j]) printDiff(C,
	 * X, Y, i-1, j) print "- " + X[i] else print ""
	 */
	private void printDiff() {
		printDiff(string1.length, string2.length);
	}

	private void printDiff(int i, int j) {
		int iIndex = i - 1;
		int jIndex = j - 1;

		if (i > 0 && j > 0 && string1[iIndex] == string2[jIndex]) {
			printDiff(i - 1, j - 1);
			System.out.println("  " + string1[iIndex]);
		} else if (j > 0 && (i == 0 || table[i][j - 1] >= table[i - 1][j])) {
			printDiff(i, j - 1);
			System.out.println("+ " + string2[jIndex]);
		} else if (i > 0 && (j == 0 || table[i][j - 1] < table[i - 1][j])) {
			printDiff(i - 1, j);
			System.out.println("- " + string1[iIndex]);
		} else {
			System.out.println("");
		}
	}

	public static void main(String[] args) {
		// char[] x = {'A','G','G','T','A','B'};
		// char[] y = {'G','X','T','X','A','Y','B'};
		// char[] x = { 'B','D','C','A','B','A' };
		// char[] y = { 'A','B','C','B','D','A','B'};
		char[] x = { 'P', 'R', 'C', 'A', 'B', 'T' };
		char[] y = { 'X', 'B', 'R', 'P', 'D', 'A', 'T' };

		LongestCommonSubsequence lcs = new LongestCommonSubsequence();
		System.out.println("Length of LCS is "
				+ lcs.recurLCS(x, y, x.length, y.length));
		System.out.println("Length of LCS is "
				+ lcs.lcs(x, y, x.length, y.length));
		System.out.println("An LCS is " + lcs.getOneLCS());
		System.out.println("All LCS are " + lcs.getAllLCS());
		lcs.printDiff();

		// don't call this if you called lcs() already, it will cause
		// getOneLCS() to fail
		System.out.println("Length of optimized LCS is "
				+ lcs.lcsInputOptimized(x, y));
	}
}
