package com.saiyd.dynamicprograming;

/*
 * bottom up approach
 * 
 * O(n^3) instead of O(2^n) via brue force or recursion
 */
public class MatrixChainOrder {
	private int[][] costs;
	private int[][] indexOfOptimalCost;
	
	/* Dynamic programming bottom up approach
	 * 
	 * Given an array p[] which represents the chain of matrices such that the ith matrix 
	 * Ai is of dimension p[i-1] x p[i]. We need to write a function MatrixChainOrder() 
	 * that should return the minimum number of multiplications needed to multiply the chain.
	 * 
	 * Example p[] = {30, 35, 15, 5, 10, 20, 25};
	 * 
	 *   for the matrices
	 *   	A1 = 30 x 35
	 *   	A2 = 35 x 15
	 *   	A3 = 15 x 5
	 *   	A4 = 5 x 10
	 *   	A5 = 10 x 20
	 *   	A6 = 20 x 25
	 *   
	 *   An array of p[i-1] x p[i] is a compact representation because you don't repeat matching
	 *   dimensions. For example the 35 in A1 with the 35 in A2. If these dimension don't match
	 *   you can't do the matrix multiplication anyway.
	 *   
	 *   Notice that 7 elements are required to represent 6 matrices
	 *   
	 *   
	 * General Algorithm
	 * 
	 * 	Take the sequence of matrices and separate it into two subsequences.
	 * 	Find the minimum cost of multiplying out each subsequence.
	 * 	Add these costs together, and add in the cost of multiplying the two result matrices.
	 * 	Do this for each possible position at which the sequence of matrices can be split, and take the minimum over all of them.  
	 */
	public void matrixChainOrder(int[] p) {
		/*
		 * Array p is one element longer than the number of
		 * matrices it represents. Our tables should be one
		 * element less in both dimensions
		 */
		int n = p.length - 1;
		
		costs = new int[n][n];
		/*
		 * index of the split of the array p where the binary multiplication
		 * operation is done. and index of 3 means (1,2,3) (4,5,6)
		 */
		indexOfOptimalCost = new int[n][n];
		
		/*
		 * We prepopulate the table with the cost of
		 * multiplying each matrix in isolation, which is zero
		 */
		for(int i = 0; i < n; i++) {
			//costs[i] = new int[n];
			costs[i][i] = 0; // initialized the diagonal, the cost to multiply a matrix against itself
			//indexOfOptimalCost[i] = new int[n];
		}
		
		/*
		 * We start with a chain length of 1 which represents a chain of
		 * 2 matrices, we use 1 here because our arrays are zero based
		 * 
		 * The 1st 2 for loops perform the diagonal walk across the table
		 * The 3rd for loop moves between i and j, each value being k
		 * 
		 * cost[i][j] = the min number of mults need to compute Ai ... Aj
		 * 
		 * cost[i][j] = 0 , if i = j
		 * cost[i][j] = min {from i <= k < j} of (cost[i][k] + cost[k+1][j] + (Pi-1 * Pk * Pj) )
		 * 		This is the minimum of each of the possible sub problems plus the cost of
		 * 		multiplying the 2 sub problems
		 */
		for(int L = 1; L < n; L++) { // L is the chain length
			for(int i = 0; i < (n - L); i++) {
				int j = i + L;
				costs[i][j] = Integer.MAX_VALUE;
				
				for(int k = i; k < j; k++) {
					// costs[i][k] and costs[k + 1][j] are the cost of the 2 sub problems of costs[i][j]
					
					// cost / # of scalar multiplications
					int q = costs[i][k] + costs[k + 1][j] + p[i] * p[k+1] * p[j+1];
					if(q < costs[i][j]) { // this was just initialized to MAX_VALUE
						costs[i][j] = q;
						// k is the index that achieved optimal cost
						indexOfOptimalCost[i][j] = k;
					}
				}
			}
		}
		int i = 0;
	}
	
	public void printOptimalParenthesizations() {
        boolean[] inAResult = new boolean[indexOfOptimalCost.length];
        /*
         * We recursively print the solution starting at the end
         * index 0,5 indicates that after index 2 is the split for the last
         * (0,1,2)(3,4,5)
         * mult operation. We recurse and look at the best way to
         * split matrices 0 thru 2, which is after zero
         * (0)(1,2)
         */
        printOptimalParenthesizations(indexOfOptimalCost, 0, indexOfOptimalCost.length - 1, inAResult);
        System.out.println();
    }
	
    void printOptimalParenthesizations(int[][]s, int i, int j,  /* for pretty printing: */ boolean[] inAResult) {
        /*if (i != j) {
            printOptimalParenthesizations(s, i, s[i][j], inAResult);
            printOptimalParenthesizations(s, s[i][j] + 1, j, inAResult);
            String istr = inAResult[i] ? "_result " : " ";
            String jstr = inAResult[j] ? "_result " : " ";
            System.out.println(" A_" + i + istr + "* A_" + j + jstr);
            inAResult[i] = true;
            inAResult[j] = true;
        }*/
        
        if(i == j) {
        	System.out.print("A" + (i + 1)); // make the matrices one based to match the CLSR answer
        } else {
        	System.out.print("(");
        	printOptimalParenthesizations(s, i, s[i][j], inAResult);
            printOptimalParenthesizations(s, s[i][j] + 1, j, inAResult);
        	System.out.print(")");
        }
    }
    
    public int getMinScalarMultToProcess() {
    	return costs[0][costs.length -1];
    }

    public static void main(String[] args) {
    	MatrixChainOrder mco = new MatrixChainOrder();
    	int[] input = {30, 35, 15, 5, 10, 20, 25};
    	mco.matrixChainOrder(input);
    	mco.printOptimalParenthesizations();
    	System.out.println("Min operations is " + mco.getMinScalarMultToProcess());
    }
}
