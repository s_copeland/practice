package com.saiyd.effectivejava.chp10;

public class AvoidExcessiveSynchronization {
	/*
	 * To avoid liveness and safety failures, never
	 * cede control to the client within a synchronized
	 * block.
	 * 
	 * IE, inside a synchronized block, do not invoke a
	 * method that is designed to be overridden or one
	 * provided by a client (a function object). All such
	 * methods are considered alien.
	 * 
	 * Calling the method from the synchronized block
	 * could cause exceptions, deadlocks, or data corruption
	 */
	
	/*
	 * Do as little work as possible inside synchronized regions
	 */
	
	/*
	 * Too much synchronization can prevent multicore cpus
	 * to lose opportunities for parallelism.
	 * 
	 * It can also limit the VM's ability to optimize code
	 * execution
	 */
	
	/*
	 * If a mutable class is intended for synchronized use and you
	 * can achieve significantly higher concurrency by synchronizing
	 * internally then you could by locking the entire object then
	 * do so.... otherwise don't synchronize internally. Let the client
	 * synchronize externally where it is appropriate.
	 * 
	 * When in doubt, don't synchronize your class, but document it
	 * as not thread safe
	 */
	
	/*
	 * If a method modifies a static field, you must synchronize access
	 * to that field even if the method is typically used only by
	 * a single thread. It is not possible for clients to
	 * perform external synchronization on such a method because there
	 * can be no guarantee that unrelated clients will do likewise.
	 */
}
