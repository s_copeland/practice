package com.saiyd.effectivejava.chp10;

public class DocumentThreadSafety {
	// javadoc does not include the synchronized modifier in output
	// The presence of the synchronized modifier in a method
	// declaration is an implementation detail, not part of the 
	// exported API
	
	/*
	 * A class must clearly document what level of thread safety it
	 * supports
	 * 
	 * Immutable - instances appear constant. No sync nedded
	 * 
	 * Unconditionally thread safe - instances are immutable, but
	 *   the class has sufficient internal synchronization that
	 *   its instances can be used concurrently without the need
	 *   for any external synchronization
	 *   
	 * Conditionally thread safe - liek unconditionally, except
	 *   that some methods require external synchronization
	 *   for safe concurrent use
	 *   Ex: collections returned by the Collections.synchronized wrappers
	 *   , whose iterators require external sync
	 *   
	 * Not thread safe - instances are mutable. Clients must surround
	 *   each method invocation with external sync of the clients choosing
	 *   
	 * Thread hostile - This class is not safe for concurrent use even if
	 *   all method invocations are surrounded by external sync. Thread
	 *   hostility usually results from modifying static data without sync.
	 */
	
	/*
	 * Consider using private lock objects. This removes the ability of
	 * a client to mount a denial of service attack by holding the
	 * publicly accessible lock for prolonged periods.
	 * 
	 * The private lock object idiom can only be used unconditionally
	 * thread-safe classes
	 */
	private final Object lock = new Object();
	
	public void doFoo() {
		synchronized(lock) {
			// do work
		}
	}
}
