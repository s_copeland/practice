package com.saiyd.effectivejava.chp10;

public class DontDependOnTheThreadScheduler {
	/*
	 * Any program that relies on the thread
	 * scheduler for correctness or performance
	 * is likely to be non portable
	 */
	
	/*
	 * The be way to write robust, responsive, portable
	 * programs is to ensure that the average number of runnable ( doesn't include waiting threads)
	 * threads is not significantly greater than he number of processors.
	 */
	
	/*
	 * Thread.yield has no testable semantics, don't depend on it
	 */
	
	/*
	 * Thread priorities are among the least portable features of the java
	 * platform
	 */
}
