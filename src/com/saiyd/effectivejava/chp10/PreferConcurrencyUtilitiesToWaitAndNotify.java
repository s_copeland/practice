package com.saiyd.effectivejava.chp10;

public class PreferConcurrencyUtilitiesToWaitAndNotify {
	/*
	 * Given the difficulty of using wait and notify
	 * correctly, you should use the higher level 
	 * concurrency utilities instead
	 */
	
	/*
	 * it is impossible to exclude concurrent activity
	 * from a concurrent collection; locking it will have
	 * not effect
	 * 
	 * You can't atomically compose method invocations
	 * on concurrent collections.
	 */
	
	/*
	 * For interval timing, always use System.nanoTime in
	 * preference to System.currentTimeMillis. It is
	 * more accurate and not affected by adjustments to the 
	 * system's real-time clock
	 */
	
	/*
	 * How to use wait
	 * 
	 * Always use the loop idiom to invoke the wait method;
	 * never invoke it outside of a loop
	 */
	private static Object obj = new Object();
	private static volatile boolean ready;
	static {
		synchronized(obj) {
			// The loop serves to test the condition before and after the wait
			// If the system was ready, before the wait, there is no guarantee
			// that notify would be called and this thread would be woken up - ensures liveness
			// Testing after the wait that the condition holds ensures safety
			while(!ready) {
				try {
					obj.wait();
				} catch (InterruptedException e) {
					/*
					 * Reasons why we could be interrupted
					 * 
					 * Another thread could have obtained the lock and changed the guarded state
					 * between the time a thread invoked notify and the time the waiting
					 * thread woke
					 * 
					 * Another thread could have invoked notify accidentally or maliciously
					 * when the condition did not hold. Any wait on publicly accessible
					 * object is susceptible to this problem.
					 * 
					 * The notifying thread could be overly generous in waking waiting threads.
					 * It might invoke notifyAll even if only some of the waiting threads
					 * have their condition satisfied
					 * 
					 * Threads could wake up in the absence of a notify due to a spurious wake up.q
					 */
				}
			}
			// do action
		}
	}
}
