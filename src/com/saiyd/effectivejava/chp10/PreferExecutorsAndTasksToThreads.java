package com.saiyd.effectivejava.chp10;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class PreferExecutorsAndTasksToThreads {
	// use the following instead of home grown threads and work queues
	static {
		ExecutorService exe = Executors.newSingleThreadExecutor();
		Runnable foo = new Runnable() {

			@Override
			public void run() {
				System.out.println("booya");
			}
			
		};
		
		exe.execute(foo);
		exe.shutdown();
	}
	
	/*
	 * Executors are better that the java.util.Timer class.
	 * 
	 * The timer class used a single thread for task execution,
	 * which can hurt timing accuracy in the precense of 
	 * long running tasks. If a Timer's thread throws
	 * an uncaught exception, the timer ceases to operate
	 */
}
