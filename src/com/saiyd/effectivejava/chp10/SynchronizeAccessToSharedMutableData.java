package com.saiyd.effectivejava.chp10;

import java.util.concurrent.atomic.AtomicLong;

public class SynchronizeAccessToSharedMutableData {
	/*
	 * The synchronized keyword helps prevents
	 * threads from seeing objects in inconsistent
	 * states.
	 */
	
	/*
	 * The synchronized keyword also ensures
	 * that each thread entering a synchronized
	 * block or method sees the effects of all
	 * previous modifications that were 
	 * guarded by the same lock
	 */
	
	/*
	 * Reading or writing a variable is atomic
	 * unless the variable is of type long or 
	 * double. 
	 * 
	 * This means reading an atomic variable is
	 * guaranteed to return a value written
	 * by some thread even if multiple threads
	 * modify the variable concurrently.
	 * 
	 * #########
	 * BUT it does not guarantee that a value
	 * written by on thread will be visible to
	 * another.
	 * #########
	 * 
	 * in the absence of synchronization, there
	 * is no guarantee as to when, if ever, a
	 * thread will see another's changes
	 */
	
	// Synchronization is required for reliable communication between threads
	// as well as for mutual exclusion
	
	/*
	 * Both reads and writes must be synchronized for it to work properly
	 */
	
	// The volatile modifier guarantees that any thread that reads a field
	// will see the most recent written value
	
	/*
	 * Be careful using the volatile keyword.
	 * 
	 * In the example below the increment operator is not
	 * atomic. It performs two operations on the nextSerialNumber field.
	 * First it reads the value and then writes a new value equal to the old 
	 * value plus one. If a second thread reads the field
	 * between the time the first thread reads the value and
	 * increments it... then the second thread will see the same value as
	 * the first and increment it to the same value.
	 * 
	 * Two threads would have the same serial number
	 * 
	 * Synchronization, IS REQUIRED HERE
	 */
	private static volatile int nextSerialNumber = 0;
	private static int getNextSerialNumber() {
		return nextSerialNumber++;
	}
	// better way
	private static final AtomicLong nextSerialNum = new AtomicLong();
	private static long getNextLongSerialNumber() {
		return nextSerialNum.getAndIncrement();
	}
	
	
	/*
	 * The best way to deal with these issues is to not share mutable data.
	 * Either share mutable data or don't share data at all
	 * 
	 * Confine mutable data to a single thread
	 * 
	 * It is acceptable for one thread to modify an object and then
	 * share it with other threads. Then you only need to synchronize
	 * the sharing of the object. The other threads can then
	 * read the object without further synchronization as long as it
	 * isn't modified. Such objects are said to be effectively
	 * immutable
	 * 
	 * Transfering such an object reference from one thread to
	 * others is called safe publication.
	 */
}
