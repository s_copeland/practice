package com.saiyd.effectivejava.chp10;

public class UseLazyInitializationJudiciously {
	/*
	 * If two or more threads share a lazily initialized
	 * field, it is critical that some form of synchronization
	 * be employed.
	 */
	
	/*
	 * If you use lazy initialization to break an initialization
	 * circularity, use a synchronized accessor
	 */
	private String field;
	synchronized String getField() {
		if(field == null)
			field = new String();
		return field;
	}
	
	/*
	 * The pattern is the same for static fields
	 */
	
	/*
	 * If you need to use lazy initialization for performance on
	 * a static field, use he lazy initialization holder class idiom
	 * AKA the initialize on-demand holder class idiom
	 * 
	 * It exploits the guarantee that a lass will not be initialized until
	 * it is used
	 * 
	 * A modern VM will synchronize field access only o initialize
	 * the class. Once the class is initialized, the VM will patch the code
	 * so that subsequent access to the field does not involve
	 * any testing or synchronization
	 */
	private static class FieldHolder{
		static final String fieldStatic = new String();
	}
	static String getStaticField() {
		return FieldHolder.fieldStatic;
	}
	
	/*
	 * If you need to use lazy initialization for performance on an instance field
	 * , use the double-check idiom
	 * 
	 * This idiom avoids the cost of locking when accessing the field after it
	 * has been initialized. Because there is no locking if the field is
	 * already initialized, it is critical that the field be declared volatile
	 * 
	 * This doesn't work prior java 1.5 because of the semantics of the volatile modifier
	 */
	private volatile String fieldDoubleCheck;
	String getFieldDoubleCheck() {
		String result = fieldDoubleCheck;
		if(result == null) {
			synchronized(this) {
				result = fieldDoubleCheck;
				if(result == null)
					fieldDoubleCheck = result = new String();
			}
		}
		return result;
	}
}
