package com.saiyd.effectivejava.chp2;

public class BaseCar implements Car {
	private int wheels, engineCylinders, horsePower;
	
	BaseCar(int wheels, int engineCylinders, int horsePower) {
		this.wheels = wheels;
		this.engineCylinders = engineCylinders;
		this.horsePower = horsePower;
	}
	
	@Override
	public int getWheels() {
		return wheels;
	}

	@Override
	public int getEngineCylinders() {
		return engineCylinders;
	}

	@Override
	public int getHorsePower() {
		return horsePower;
	}

}
