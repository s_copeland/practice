package com.saiyd.effectivejava.chp2;

public interface Car {

	public abstract int getWheels();

	public abstract int getEngineCylinders();

	public abstract int getHorsePower();

}