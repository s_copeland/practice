package com.saiyd.effectivejava.chp2;

/**
 * Factory for car instances
 * @author Saiyd
 *
 */
public class Cars {
	private static Car SINGLETON;
	
	private Cars() {
		throw new AssertionError(); // don't ever instantiate me
	}
	/*
	 * common names for static factory methods
	 * 
	 * valueOf
	 * of
	 * getInstance
	 * newInstance
	 * getType - used when the factory methods is in a different Type than returned
	 * newType - used when the factory methods is in a different Type than returned
	 */
	
	public static Car getBaseCar() {
		return new BaseCar(4, 4, 150);
	}
	
	public static Car getFullSizeCar() {
		return new FullSizeCar();
	}
	
	public static Car getBaseCarInstance() {
		if(SINGLETON == null) 
			SINGLETON = new BaseCar(4, 4, 150);
		return SINGLETON;
	}
	
	public static Car getSportsCar() {
		return new SportsCar();
	}
	

	public static void main(String[] args) {
		// PROS
		// using the factory methods makes it more clear what we are creating
		Car myCar = Cars.getBaseCar();
		Car yourCar = Cars.getFullSizeCar();
		// You can also control how many instances exist
		Car someCar = Cars.getBaseCarInstance();
		// you can also return subtypes
		// This allows APIs to return objects without making their classes public
		Car someOtherCar = Cars.getSportsCar();
		
		// CONS
		// 1. Classes without factory methods cannot be subclassed
	}
}
