package com.saiyd.effectivejava.chp2;

/**
 * The builder pattern simulates named optional parameters as found in Ada and Python
 * @author Saiyd
 *
 */
public class ComplexCar {
	// required
	private final int wheels;
	private final int engineCylinders;
	private final int horsePower;
	// optional
	private final boolean hasRadio;
	private final int speakers;
	private final int headlights;
	private final boolean tintedWindows;
	private final boolean leatherSeats;
	private final boolean defroster;
	private final int seats;
	private final int airbags;
	private final boolean powerWindows;
	private final boolean moonRoof;
	
	public static class Builder {
		// required
		private final int wheels;
		private final int engineCylinders;
		private final int horsePower;
		// optional  (notice these are not final and initalized to defaults)
		private boolean hasRadio = false;
		private int speakers = 0;
		private int headlights = 2;
		private boolean tintedWindows = false;
		private boolean leatherSeats = false;
		private boolean defroster = true;
		private int seats = 4;
		private int airbags = 2;
		private boolean powerWindows = false;
		private boolean moonRoof = false;
		
		/**
		 * The constructor only takes the required parameters
		 * @param wheels
		 * @param engineCylinders
		 * @param horsePower
		 */
		public Builder(int wheels, int engineCylinders, int horsePower) {
			this.wheels = wheels;
			this.engineCylinders = engineCylinders;
			this.horsePower = horsePower;
		}
		
		public Builder hasRadio(boolean val) {
			hasRadio = val;
			return this;
		}
		
		public Builder speakers(int val) {
			speakers = val;
			return this;
		}
		
		public Builder headlights(int val) {
			headlights = val;
			return this;
		}
		
		public Builder tintedWindows(boolean val) {
			tintedWindows = val;
			return this;
		}
		
		public Builder leatherSeats(boolean val) {
			leatherSeats = val;
			return this;
		}
		
		public Builder defroster(boolean val) {
			defroster = val;
			return this;
		}
		
		public Builder seats(int val) {
			seats = val;
			return this;
		}
		public Builder airbags(int val) {
			airbags = val;
			return this;
		}
		
		public Builder powerWindows(boolean val) {
			powerWindows = val;
			return this;
		}
		
		public Builder moonRoof(boolean val) {
			moonRoof = val;
			return this;
		}
		
		public ComplexCar build() {
			ComplexCar cc = new ComplexCar(this);
			// we could also check invariants here, like we woud in a constructor
			// if we found a violation we could throw an IllegalStateException
			return cc;
		}
	}

	private ComplexCar(Builder builder) {
		// required
		wheels = builder.wheels;
		engineCylinders = builder.engineCylinders;
		horsePower = builder.horsePower;
		// optional
		hasRadio = builder.hasRadio;
		speakers = builder.speakers;
		headlights = builder.headlights;
		tintedWindows = builder.tintedWindows;
		leatherSeats = builder.leatherSeats;
		defroster = builder.defroster;
		seats = builder.seats;
		airbags = builder.airbags;
		powerWindows = builder.powerWindows;
		moonRoof = builder.moonRoof;
	}
}
