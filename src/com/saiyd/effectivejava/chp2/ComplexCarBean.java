package com.saiyd.effectivejava.chp2;

/**
 * THIS ISN'T BETTER BECAUSE NOW THE CLASS CAN BE CREATED IN AN INCONSISTENT STATE
 * If some of the required parameters are missing
 * @author Saiyd
 *
 */
public class ComplexCarBean {
	// required
	private int wheels;
	private int engineCylinders;
	private int horsePower;
	// optional
	private boolean hasRadio;
	private int speakers;
	private int headlights;
	private boolean tintedWindows;
	private boolean leatherSeats;
	private boolean defroster;
	private int seats;
	private int airbags;
	private boolean powerWindows;
	private boolean moonRoof;
	public int getWheels() {
		return wheels;
	}
	public void setWheels(int wheels) {
		this.wheels = wheels;
	}
	public int getEngineCylinders() {
		return engineCylinders;
	}
	public void setEngineCylinders(int engineCylinders) {
		this.engineCylinders = engineCylinders;
	}
	public int getHorsePower() {
		return horsePower;
	}
	public void setHorsePower(int horsePower) {
		this.horsePower = horsePower;
	}
	public boolean isHasRadio() {
		return hasRadio;
	}
	public void setHasRadio(boolean hasRadio) {
		this.hasRadio = hasRadio;
	}
	public int getSpeakers() {
		return speakers;
	}
	public void setSpeakers(int speakers) {
		this.speakers = speakers;
	}
	public int getHeadlights() {
		return headlights;
	}
	public void setHeadlights(int headlights) {
		this.headlights = headlights;
	}
	public boolean isTintedWindows() {
		return tintedWindows;
	}
	public void setTintedWindows(boolean tintedWindows) {
		this.tintedWindows = tintedWindows;
	}
	public boolean isLeatherSeats() {
		return leatherSeats;
	}
	public void setLeatherSeats(boolean leatherSeats) {
		this.leatherSeats = leatherSeats;
	}
	public boolean isDefroster() {
		return defroster;
	}
	public void setDefroster(boolean defroster) {
		this.defroster = defroster;
	}
	public int getSeats() {
		return seats;
	}
	public void setSeats(int seats) {
		this.seats = seats;
	}
	public int getAirbags() {
		return airbags;
	}
	public void setAirbags(int airbags) {
		this.airbags = airbags;
	}
	public boolean isPowerWindows() {
		return powerWindows;
	}
	public void setPowerWindows(boolean powerWindows) {
		this.powerWindows = powerWindows;
	}
	public boolean isMoonRoof() {
		return moonRoof;
	}
	public void setMoonRoof(boolean moonRoof) {
		this.moonRoof = moonRoof;
	}
	
	
}
