package com.saiyd.effectivejava.chp2;

public class ComplexCarBigConstructor {
	// required
	private final int wheels;
	private final int engineCylinders;
	private final int horsePower;
	// optional
	private final boolean hasRadio;
	private final int speakers;
	private final int headlights;
	private final boolean tintedWindows;
	private final boolean leatherSeats;
	private final boolean defroster;
	private final int seats;
	private final int airbags;
	private final boolean powerWindows;
	private final boolean moonRoof;;
	
	/**
	 * THIS CLASS IS A GOOD CANDIATE FOR THE BUILDER PATTERN
	 * @param wheels
	 * @param engineCylinders
	 * @param horsePower
	 * @param hasRadio
	 * @param speakers
	 * @param headlights
	 * @param tintedWindows
	 * @param leatherSeats
	 * @param defroster
	 * @param seats
	 * @param airbags
	 * @param powerWindows
	 * @param moonRoof
	 */
	public ComplexCarBigConstructor(int wheels, int engineCylinders, int horsePower,
			boolean hasRadio, int speakers, int headlights,
			boolean tintedWindows, boolean leatherSeats, boolean defroster,
			int seats, int airbags, boolean powerWindows, boolean moonRoof) {
		super();
		this.wheels = wheels;
		this.engineCylinders = engineCylinders;
		this.horsePower = horsePower;
		this.hasRadio = hasRadio;
		this.speakers = speakers;
		this.headlights = headlights;
		this.tintedWindows = tintedWindows;
		this.leatherSeats = leatherSeats;
		this.defroster = defroster;
		this.seats = seats;
		this.airbags = airbags;
		this.powerWindows = powerWindows;
		this.moonRoof = moonRoof;
	}
}
