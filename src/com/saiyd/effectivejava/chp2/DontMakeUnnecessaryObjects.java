package com.saiyd.effectivejava.chp2;

public class DontMakeUnnecessaryObjects {
	public static void main(String[] args) {
		// don't do this
		String s = new String("this is dumb"); // we make the string twice for no reason
		// do this
		String s2 = "this is dumb"; // use a string literal instead
		
		
		//  watch out for autoboxing
		Long sum = 0L;
		for(long i=0; i <= Integer.MAX_VALUE; i++) {
			sum += i; // the problem is here
		}
		System.out.println(sum);
	}
}
