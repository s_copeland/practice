package com.saiyd.effectivejava.chp2;

import java.io.Serializable;

public class Singleton1 implements Serializable {
	private static final Singleton1 INSTANCE = new Singleton1();
	private Singleton1() {
		// this is to protect against abuses of the AccessibleObject.setAccessible(true) via reflection
		if(INSTANCE != null)
			throw new IllegalStateException("There can only be one high lander!!");
	}
	
	public Singleton1 getInstance() {
		return INSTANCE;
	}
	
	public String getReason() {
		return "Don't makes singletons...";
	}
	
	private Object readResolve() {
		return INSTANCE;
	}
}
