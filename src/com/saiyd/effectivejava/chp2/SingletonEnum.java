package com.saiyd.effectivejava.chp2;

public enum SingletonEnum {
	INSTANCE;
	
	public String getReason() {
		return "Don't makes singletons...";
	}
	
	// NO need to worry about serialization issues
}
