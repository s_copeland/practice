package com.saiyd.effectivejava.chp3;

public class Address {
	private String city;
	private String state;
	private String zipCode;
	private String street;
	private int streetBuildingNumber;
	private String unit;
	public Address(String city, String state, String zipCode, String street,
			int streetBuildingNumber, String unit) {
		super();
		this.city = city;
		this.state = state;
		this.zipCode = zipCode;
		this.street = street;
		this.streetBuildingNumber = streetBuildingNumber;
		this.unit = unit;
	}
	// If we want each instance to only be equal to itself then we just use Object.equals
	// we don't care if the class provides a logical equality
	// Also a super class cold haver already overridden equals properly for this class
}
