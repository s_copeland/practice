package com.saiyd.effectivejava.chp3;

public class Address2 {
	private String city;
	private String state;
	private String zipCode;
	private String street;
	private int streetBuildingNumber;
	private String unit;
	private float someFloat;
	private long someLong;
	private double someDouble;
	private boolean someBoolean;
	
	public Address2(String city, String state, String zipCode, String street,
			int streetBuildingNumber, String unit) {
		super();
		this.city = city;
		this.state = state;
		this.zipCode = zipCode;
		this.street = street;
		this.streetBuildingNumber = streetBuildingNumber;
		this.unit = unit;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + (someBoolean ? 1231 : 1237);
		long temp;
		temp = Double.doubleToLongBits(someDouble);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + Float.floatToIntBits(someFloat);
		result = prime * result + (int) (someLong ^ (someLong >>> 32));
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		result = prime * result + ((street == null) ? 0 : street.hashCode());
		result = prime * result + streetBuildingNumber;
		result = prime * result + ((unit == null) ? 0 : unit.hashCode());
		result = prime * result + ((zipCode == null) ? 0 : zipCode.hashCode());
		return result;
	}
	
	/*
	 * Implements an equivalence relation
	 *   reflexive - for any non null reference value x, x.equals(x) must return true;
	 *   symmetric - x.equals(y) must be true if and only if y.equals(x)
	 *   transitive - x.equals(y) and y.equals(z) then x.equals(z)
	 *   consistent - multiple invocations return the same value if no information used in equals has been modified
	 *   x.equals(null) is false
	 *   
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		// test for reflexive
		if (this == obj)
			return true;
		// false if other object is null
		
		/*
		 * This is not needed because we do an instanceof operator
		 */
		//if (obj == null)
		//	return false;
		/*
		 * This violates the Liskov substitution principle
		 * Any important property of a type should also hold for its subtypes
		 * So this method should work equally well for subtypes
		 */
		//if (getClass() != obj.getClass())
		//	return false;
		
		if(!(obj instanceof Address2))
			return false;
		
		Address2 other = (Address2) obj;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (someBoolean != other.someBoolean)
			return false;
		if (Double.doubleToLongBits(someDouble) != Double
				.doubleToLongBits(other.someDouble))
			return false;
		if (Float.floatToIntBits(someFloat) != Float
				.floatToIntBits(other.someFloat))
			return false;
		if (someLong != other.someLong)
			return false;
		if (state == null) {
			if (other.state != null)
				return false;
		} else if (!state.equals(other.state))
			return false;
		if (street == null) {
			if (other.street != null)
				return false;
		} else if (!street.equals(other.street))
			return false;
		if (streetBuildingNumber != other.streetBuildingNumber)
			return false;
		if (unit == null) {
			if (other.unit != null)
				return false;
		} else if (!unit.equals(other.unit))
			return false;
		if (zipCode == null) {
			if (other.zipCode != null)
				return false;
		} else if (!zipCode.equals(other.zipCode))
			return false;
		return true;
	}
}
