package com.saiyd.effectivejava.chp3;

import java.util.Comparator;

/*
 * This class provides a comparison strategy for another class
 */
public class ByAgeComparatorClass implements Comparator<ComparableClass> {

	/*
	 * The class we sort does not need to implement Comparable
	 * 
	 * (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(ComparableClass first, ComparableClass second) {
		if(first.getAge() < second.getAge())
			return -1;
		if(first.getAge() > second.getAge())
			return 1;
		return 0;
	}

}
