package com.saiyd.effectivejava.chp3;

public class CloneImmutable {
	private final int age;
	private final String name;
	
	public CloneImmutable(String name, int age) {
		this.name = name;
		this.age = age;
	}
	
	/*
	 * There is no need to add a clone method to an immutable
	 * object. The returned clone would be the same as the original
	 * which is read only. 
	 */
}
