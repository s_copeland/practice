package com.saiyd.effectivejava.chp3;

//if you implement cloneable you're expected to implement clone
/*
 * In order to properly impelemtn clone all super classes must have a well behaved clone implementation
 * that is public or protected
 */
public class CloneNonFinalClass implements Cloneable {
	private final int age;
	private final String name;
	public CloneNonFinalClass(int age, String name) {
		super();
		this.age = age;
		this.name = name;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * The clone method of Object returns a field by field copy of the object.
	 * 
	 * clone is implemented in native code
	 * 
	 * Covariant return types were added in java 1.5. This prevents all client 
	 * classes from having to cast the value returned by clone
	 * @see java.lang.Object#clone()
	 */
	@Override
	protected CloneNonFinalClass clone() throws CloneNotSupportedException {
		return (CloneNonFinalClass) super.clone();
	}
	
}
