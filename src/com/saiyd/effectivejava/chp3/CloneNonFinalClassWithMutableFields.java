package com.saiyd.effectivejava.chp3;

//if you implement cloneable you're expected to implement clone
/*
 * In order to properly implement clone all super classes must have a well behaved clone implementation
 * that is public or protected
 */
public class CloneNonFinalClassWithMutableFields implements Cloneable {
	static class Complex {
		private Complex next;
		private int value;
		public Complex(int value, Complex next) {
			super();
			this.next = next;
			this.value = value;
		}
		public Complex getNext() {
			return next;
		}
		public void setNext(Complex next) {
			this.next = next;
		}
		public int getValue() {
			return value;
		}
		
		public Complex getDeepCopy() {
			Complex retVal = new Complex(value, next);
			for(Complex cur = retVal; cur.next != null; cur = cur.next) {
				cur.next = new Complex(cur.next.value, cur.next.next);
			}
			return retVal;
		}
	}
	private final int age;
	private final String name;
	// clone is incompatible with the normal use of final fields referring to 
	// mutable objects, we can't make this field final because the clone method would not be able to modify it
	private Object[] elements;
	
	// when cloning objects with complex types, the default field by field copy of Object.clone()
	// may not be the correct behavior
	private Complex[] buckets;
	
	public CloneNonFinalClassWithMutableFields(int age, String name) {
		super();
		this.age = age;
		this.name = name;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * The clone method of Object returns a field by field copy of the object.
	 * 
	 * clone is implemented in native code
	 * 
	 * Covariant return types were added in java 1.5. This prevents all client 
	 * classes from having to cast the value returned by clone
	 * 
	 * super.clone() must be chained all the way up to Object.clone()
	 * 
	 * @see java.lang.Object#clone()
	 */
	@Override
	protected CloneNonFinalClassWithMutableFields clone() { // we don't have to throw a checked exception here
		try {
			CloneNonFinalClassWithMutableFields retVal = (CloneNonFinalClassWithMutableFields) super.clone();
			retVal.elements = elements.clone();
			
			// make sure the default behavior of Object.clone() is what we expect. Here we need a deep copy
			for(int i=0; i < buckets.length; i++) {
				if(buckets[i] != null)
					retVal.buckets[i] = buckets[i].getDeepCopy();
			}
			return retVal;
		} catch(CloneNotSupportedException e) {
			throw new AssertionError();
		}
	}
	
}
