package com.saiyd.effectivejava.chp3;

/*
 * anticommutation
 * transative
 * excepetion symmetry
 */
public class ComparableClass implements Comparable<ComparableClass> {
	enum SomeEnum { ONE, TWO, THREE };
	private int age;
	private float height;
	private double weight;
	private String name;
	private boolean dead;
	private SomeEnum someEnum;
	
	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public float getHeight() {
		return height;
	}

	public void setHeight(float height) {
		this.height = height;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isDead() {
		return dead;
	}

	public void setDead(boolean dead) {
		this.dead = dead;
	}

	public SomeEnum getSomeEnum() {
		return someEnum;
	}

	public void setSomeEnum(SomeEnum someEnum) {
		this.someEnum = someEnum;
	}

	/*
	 * This method should be consistent with equals, if not there must be a good reason
	 * 
	 * (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(ComparableClass other) {
		if(this == other)
			return 0;
		
		// we assume other is not null, if it is a NullPointerException is thrown
		
		if(age > other.age)
			return 1;
		if(age < other.age)
			return -1;
					
		int compareValue = Float.compare(height, other.height);
		if(compareValue != 0)
			return compareValue;
		
		compareValue = Double.compare(weight, other.weight);
		if(compareValue != 0)
			return compareValue;
		
		// this throws a null pointer exception if name is null
		compareValue = name.compareTo(other.name);
		if(compareValue != 0)
			return compareValue;
		
		// false before true
		if(!dead && other.dead)
			return -1; 
		if(dead && !other.dead)
			return 1;
		
		compareValue = someEnum.compareTo(other.someEnum);
		if(compareValue != 0)
			return compareValue;
					
		return 0;
	}

}
