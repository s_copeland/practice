package com.saiyd.effectivejava.chp3;


public class CopyConstructorNonFinalClass {
	private final int age;
	private final String name;
	public CopyConstructorNonFinalClass(int age, String name) {
		super();
		this.age = age;
		this.name = name;
	}
	
	/*
	 * copy constructor
	 */
	public CopyConstructorNonFinalClass(CopyConstructorNonFinalClass other) {
		this.age = other.age;
		this.name = other.name;
		
		// do a deep copy on any other fields
	}
	
	/*
	 * copy factory
	 */
	public CopyConstructorNonFinalClass newInstance(CopyConstructorNonFinalClass other) {
		return new CopyConstructorNonFinalClass(other.age, other.name);
	}
}
