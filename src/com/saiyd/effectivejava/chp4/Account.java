package com.saiyd.effectivejava.chp4;

public class Account {
	private int id;
	private String name;
	public Account(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	
}
