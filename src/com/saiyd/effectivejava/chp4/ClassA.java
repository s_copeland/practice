package com.saiyd.effectivejava.chp4;

public class ClassA {
	/*
	 * A common use for static member classes is
	 * for helper classes
	 */
	
	/*
	 * A common use for private static member classes
	 * is to represent component of an object. Like
	 * the Entry object of a Map
	 */
	/*
	 * A common use of non static member classes is
	 * to define an Adapter that allows an instance
	 * to be viewed as some unrelated class
	 */
	
	/*
	 * If you declare a member class that does not require
	 * access to an enclosing instance, always put the 
	 * static modifier in its declaration
	 */
}
