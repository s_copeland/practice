package com.saiyd.effectivejava.chp4;

/*
 * Its safe to use inheritance when extending classes specifically
 * designed for extension.
 * 
 * Its safe to use inheritance within the same package when the 
 * sub and super class are under the control of the same programmers
 * 
 * IT IS NOT SAFE to inherit from ordinary concrete classes across package boundaries
 * 
 * Inheritance violates encapsulation - a sub class depends upon the implementation
 * details of its superclass. The subclass must evolve in tandem witht the super class
 * 
 * 
 * This class has no dependencies on the implementation details of the Account class.
 * new method can be added to the Account class without affecting this class
 */
public class CompositionOverInheritance {
	private Account account;
	private String departmentPrefix;
	public CompositionOverInheritance(Account account, String departmentPrefix) {
		super();
		this.account = account;
		this.departmentPrefix = departmentPrefix;
	}
	
	// forwarding methods
	public int getId() {
		return account.getId();
	}
	public String getName() {
		return departmentPrefix + account.getName();
	}
}
