package com.saiyd.effectivejava.chp4;

class ExposedDataClass {
	// Its ok to expose data fields of package private classes
	private int x;
	private int y;
	// public classes should never do this
}
