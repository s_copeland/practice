package com.saiyd.effectivejava.chp4;

public class FunctionObject implements IDataEncryptor {
	// stateless, has no fields
	public byte[] encrypt(byte[] data) {
		// do super strong crypto
		return data; // whomp whomp
	}
}
