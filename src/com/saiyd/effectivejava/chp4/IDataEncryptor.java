package com.saiyd.effectivejava.chp4;

public interface IDataEncryptor {
	public byte[] encrypt(byte[] data);
}
