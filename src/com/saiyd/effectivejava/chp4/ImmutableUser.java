package com.saiyd.effectivejava.chp4;

/*
 * Rules for immutable classes
 * 
 * 1. Don't provide any mutator methods, who changes are externaly visible
 * 2. Ensure the class can't be extened
 * 3. Make all fields final
 * 4. Make all fields private
 * 5. Ensure exclusive access to any mutable components
 *    ensure the client cannot obtain a reference to mutable fields
 *    
 * Immutable objects are inherenty thread safe
 * 
 * Immutable objects can be costly, if there is one for each distinct value and they  are large
 */
public final class ImmutableUser {
	static class Name {
		private String first;
		private String last;
		
		
		public Name(String first, String last) {
			super();
			this.first = first;
			this.last = last;
		}
		
		public Name(Name other) {
			this.first = other.first;
			this.last = other.last;
		}
		public String getFirst() {
			return first;
		}
		public void setFirst(String first) {
			this.first = first;
		}
		public String getLast() {
			return last;
		}
		public void setLast(String last) {
			this.last = last;
		}
		
	}
	private final int id;
	private final Name name;
	
	public ImmutableUser(int id, Name name) {
		super();
		this.id = id;
		// never initialize from a client provided object reference
		//this.userName = userName;
		this.name = new Name(name);
	}
	
	/*
	 * If the class is really immutable, then you should not provide a copy contructor
	 * or implement Object.clone()
	 */
	public int getId() {
		return id;
	}
	public Name getName() {
		// Never return a reference from an accessor, make a defensive copy
		// return name;
		return new Name(name);
	}
	
}
