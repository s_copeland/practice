package com.saiyd.effectivejava.chp4;

import java.util.ArrayList;
import java.util.List;

public class Person {
	private String firstName;
	private String lastName;
	private List<Person> friends;
	
	public Person(String firstName, String lastName) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		friends = new ArrayList<Person>();
	}


	public String getFullName() {
		return firstName + " " + lastName;
	}
	
	public void addFriend(Person friend) {
		friends.add(friend);
	}
	
	public void addGirlFriend(Person girlFriend) {
		friends.add(girlFriend);
	}
}
