package com.saiyd.effectivejava.chp4;

enum SocialGroup {
	FAMILY, WORK, GYM, BAR;
}
