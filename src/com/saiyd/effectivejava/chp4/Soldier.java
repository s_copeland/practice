package com.saiyd.effectivejava.chp4;

/*
 * This class must evolve in tandem with the Person class because of inheritance
 */
public class Soldier extends Person {
	private String rank;
	
	public Soldier(String rank, String firstName, String lastName) {
		super(firstName, lastName);
		this.rank = rank;
	}

	/*
	 * The goal of this override is to only allow a soldier to have other soldier's
	 * as friends. The problem is that we rely on the current implementation of the
	 * super class which only has one method for adding friends. If the super class
	 * adds a new method for adding friends later on... then we are forced to
	 * add it to this class if we want to maintain this predicate
	 * 
	 * (non-Javadoc)
	 * @see com.saiyd.effectivejava.chp4.Person#addFriend(com.saiyd.effectivejava.chp4.Person)
	 */
	@Override
	public void addFriend(Person friend) {
		if(friend instanceof Soldier) {
			super.addFriend(friend);
		} else {
			throw new IllegalArgumentException();
		}
	}


	/*
	 * We didn't override addGirlFriend(), so its possible to break the rule that
	 * soldiers can only have other soldiers as friends
	 */
	
}
