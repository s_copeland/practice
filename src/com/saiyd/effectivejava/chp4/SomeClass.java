package com.saiyd.effectivejava.chp4;

/*
 * The need for protected members should be relatively rare
 */
public class SomeClass {
	/* mutable instance fields should never be public, non final fields and fina mutabe objects
	 * if they are you give up the ability to enforce invariants involing the field.
	 * Also you give up the ability to take action when the field is modified
	 * 
	 * classes with public mutable fields are not thread safe
	 */
	
	// non zero length arrays are always mutable, they should never be public or returned by reference
	public static final String[] ITEMS = {"these", "can", "be", "modified" };
	
	
	protected int value;
	/*
	 * This method is part of the API
	 * It represents a public commitment to an implementation detail
	 */
	protected int calculate() {
		return value;
	}
}
