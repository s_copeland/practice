package com.saiyd.effectivejava.chp4.composition;

class Blue implements IColor {

	@Override
	public int getColorRedValue() {
		return 0;
	}

	@Override
	public int getColorGreenValue() {
		return 0;
	}

	@Override
	public int getColorBlueValue() {
		return 255;
	}

}
