package com.saiyd.effectivejava.chp4.composition;

public class Car {

	private IEngineSound engineSound;
	private IBodyStyle bodyStyle;
	private IColor color;
	
	public Car(IEngineSound engineSound, IBodyStyle bodyStyle, IColor color) {
		this.engineSound = engineSound;
		this.bodyStyle = bodyStyle;
		this.color = color;
	}
	
	public String getSound() {
		return engineSound.getSound();
	}
	
	public String getBodyStyle() {
		return bodyStyle.getBodyStyle();
	}
	
	public int getColorRedValue() {
		return color.getColorRedValue();
	}
	public int getColorGreenValue() {
		return color.getColorGreenValue();
	}
	public int getColorBlueValue() {
		return color.getColorBlueValue();
	}
	
	
	public static void main(String[] args) {
		Car audiA4 = new Car(new FourCylinder(), new FourDoorBodyStyle(), new Red());
		Car bmw335 = new Car(new SixCylinder(), new TwoDoorBodyStyle(), new Blue());
	}
}
