package com.saiyd.effectivejava.chp4.composition;

class FourCylinder implements IEngineSound {

	@Override
	public String getSound() {
		return "Buzzzzzz";
	}

}
