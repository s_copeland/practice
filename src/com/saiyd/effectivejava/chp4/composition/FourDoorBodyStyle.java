package com.saiyd.effectivejava.chp4.composition;

class FourDoorBodyStyle implements IBodyStyle {

	@Override
	public String getBodyStyle() {
		return "4 Door";
	}

}
