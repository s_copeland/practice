package com.saiyd.effectivejava.chp4.composition;

interface IBodyStyle {
	public String getBodyStyle();
}
