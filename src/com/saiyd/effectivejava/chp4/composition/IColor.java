package com.saiyd.effectivejava.chp4.composition;

interface IColor {
	public int getColorRedValue();
	public int getColorGreenValue();
	public int getColorBlueValue();
}
