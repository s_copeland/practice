package com.saiyd.effectivejava.chp4.composition;

interface IEngineSound {
	public String getSound();
}
