package com.saiyd.effectivejava.chp4.composition;

class SixCylinder implements IEngineSound {

	@Override
	public String getSound() {
		return "Vrooom";
	}

}
