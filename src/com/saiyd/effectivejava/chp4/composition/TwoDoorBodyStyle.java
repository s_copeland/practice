package com.saiyd.effectivejava.chp4.composition;

class TwoDoorBodyStyle implements IBodyStyle {

	@Override
	public String getBodyStyle() {
		return "2 Door";
	}

}
