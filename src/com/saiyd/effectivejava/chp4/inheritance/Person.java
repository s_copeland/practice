package com.saiyd.effectivejava.chp4.inheritance;


public class Person {
	private String firstName;
	private String lastName;
	
	public Person(String firstName, String lastName) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;

		// Don't invoke overridable methods in the contructor, the sub class has not been initialized yet
		// this rule also applies for the clone() and readObject() methods, as they are another
		// way of constructing objects
	}


	public String getUpperFirstName() {
		return firstName.toUpperCase();
	}


	public String getUpperLastName() {
		return lastName.toUpperCase();
	}


	/*
	 * Document the self-use of overridable methods
	 * 
	 * Ex: This implementation creates a full name, by concatenating getUpperFirstName() + " " + getUpperLastName()
	 */
	public String getFullName() {
		return getUpperFirstName() + " " + getUpperLastName();
	}
	
	// Provide hooks into internal workings in the form of judiciously chosen protected methods
	
	// Test the class for inheritance by subclassing it
}
