package com.saiyd.effectivejava.chp4.interfaces;

/*
 * By convention, skeletal implementations are called 
 * Abstract<Interface name>
 */
public abstract class AbstractSodaFountain implements ISodaFountain {

	@Override
	public void pressWater() {
		// we only provide an implementation for water and leave the rest
	}

}
