package com.saiyd.effectivejava.chp4.interfaces;

// A Mixin interface, allows optional functionality to be added to a classes type
public interface IReplicatable {
	public void replicateTo(String server);
}
