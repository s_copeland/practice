package com.saiyd.effectivejava.chp4.interfaces;

public interface ISodaFountain {
	public void pressWater();
	public void pressCola();
	public void pressLemonade();
}
