package com.saiyd.effectivejava.chp4.interfaces;

// Don't do this!
// No class should ever implement this interface to get these constants
// if you want to avoid using PhysicalConstants.AVOGADROS_NUMBER use static imports of a
// noninstantiable utility class
public interface PhysicalConstants {
	static final double AVOGADROS_NUMBER = 6.02214199E23;
	static final double BOLTZMANN_CONSTANT = 1.3806503e-23;
}
