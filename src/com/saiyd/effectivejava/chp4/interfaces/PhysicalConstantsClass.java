package com.saiyd.effectivejava.chp4.interfaces;

public class PhysicalConstantsClass {
	private PhysicalConstantsClass() {
		// do nothing
	}
	static final double AVOGADROS_NUMBER = 6.02214199E23;
	static final double BOLTZMANN_CONSTANT = 1.3806503e-23;
}
