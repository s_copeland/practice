package com.saiyd.effectivejava.chp4.interfaces;

public interface Singer {
	void sing(Song song);
}
