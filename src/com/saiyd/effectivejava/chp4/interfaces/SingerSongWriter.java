package com.saiyd.effectivejava.chp4.interfaces;

// Interfaces allow the construction of nonhierarchical type frameworks
/*
 * The alternative is a bloated class hierarchy containing a separate class for every
 * supported combination of attributes. If there are n attributes in the type
 * system, there are 2^n possible combinations
 */
public interface SingerSongWriter extends Singer, SongWriter {

}
