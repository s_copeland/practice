package com.saiyd.effectivejava.chp4.interfaces;

public interface SongWriter {
	Song compose(String song);
}
