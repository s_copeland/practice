package com.saiyd.effectivejava.chp5;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Stack;

public class BoundedWildcards {
	// <? extends E> - means E and any subtype of E
	
	// <? super E> - means E and any super type of E
	
	/*
	 * For maximum flexibility, use wildcard types on input parameters
	 * that represent producers or consumers
	 * 
	 * PECS - an mnemonic that stands for, producer-extends, consumer-super
	 * 
	 * If a parameterized type represents a T producer use <? extends T>
	 * 
	 * If a parameterized type represents a T consumer use <? super T>
	 */
	
	static class SomeStack<T> extends Stack<T> {
		/*
		 * A generic pushAll method for a a stack class
		 * 
		 * The parameter src produces T instances added by the pushAll method
		 * to the stack
		 */
		public void pushAll(Iterable<? extends T> src) {
			for(T t : src) {
				push(t);
			}
		}
		
		/*
		 * A generic popAll method for a stack class
		 * 
		 * The parameter dst consumes T instances from the stack
		 */
		public void popAll(Collection<? super T> dst) {
			while(!isEmpty())
				dst.add(pop());
		}
		
		public void alpha(Collection<? extends T> src, T someObjT) {
			src.add(null);
			/*
			 * The following is not legal, while we know that
			 * src contains objects that are T or its sub types,
			 * we don't know what type of objects src actually contains
			 * so we can't add anything except null
			 */
			//src.add(someObjT);
		}
		
		public void beta(List<? super T> dst) {
			// because Object is the super type of all objects, this is legal
			Object o = dst.get(0);
			/*
			 * We can't do the following because while we know that 
			 * dst is some super type of T, we don't know what type
			 * of objects dst contains
			 */
			//T someTObj = dst.get(0);
		}
	}
	
	
	/*
	 * Don't use wildcard types as return types
	 */
	
	public static <E> Set<E> union(Set<? extends E> s1, Set<? extends E> s2) {
		Set<E> result = new HashSet<E>(s1);
		result.addAll(s2);
		return result;
	}
	
	// Explict type parameters help when he compiler doesn't infer the type that you want
	static {
		Set<Integer> integers = new HashSet<Integer>();
		Set<Double> doubles = new HashSet<Double>();
		
		// Explict type parameters help when he compiler doesn't infer the type that you want
		// Set<Number> numbers = BoundedWildcards.union(integers, doubles); // this won't compile
		Set<Number> numbers = BoundedWildcards.<Number>union(integers, doubles);
	}
	
	
	/*
	 * Always use Comparable<? super T> and Comparator<? super T> in preference to
	 * Comparable<T> and Comparator<T>.
	 * 
	 * A Comparable<T> and Comparator<T> consumes instances of T and produces integers indicating
	 * order relations.
	 * 
	 * Comparable<? super T> - means that the type has to implement comparable of itself
	 * or its superclass
	 */
	
	/*
	 * If a type parameter appears only once in a method declaration, replace it with a wildcard
	 */
	/*
	 * If you want to provide a generic API using a wildcard, but later on need the actual
	 * type of the wild card, use a helper method to capture the type
	 */
	public static void swap(List<?> list, int i, int j) {
		swapHelper(list, i, j);
	}
	
	private static <E> void swapHelper(List<E> list, int i, int j) {
		list.set(i, list.set(j, list.get(i)));
	}
	
	static {
		// The cast method is the dynamic analog to java's cast operator
		Number num = Integer.class.cast(15);
		
		//
		Class<?> unboundedType = null;
		try {
			unboundedType = Class.forName("java.lang.Integer");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Class<? extends Number> result = unboundedType.asSubclass(Integer.class);
	}
}
