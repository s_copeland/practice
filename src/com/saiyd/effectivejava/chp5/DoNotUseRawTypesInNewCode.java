package com.saiyd.effectivejava.chp5;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class DoNotUseRawTypesInNewCode {
	// Except for class literals, you must use raw types in class literals
	private Class<List> listClassRawType = List.class;
	private Class<String[]> stringArrayRawType = String[].class;
	private Class<Integer> intPrimitiveRawType = int.class;
	// the following are now legal
	// List<String>.class
	// List<?>.class
	
	private List rawListType; // This is a raw type because it does not provide a type parameter
	private List<Integer> parameterizedType; // This is a parameterized types of the generic type List<E>
	// List<E> is a generic type
	
	
	/*
	 * This list raw type has opted out of generic type checking at compile time
	 * 
	 * Ex: someList = new List<String>(); is legal
	 */
	private List someList;
	
	/*
	 * This list parameterized type has explicitly told the compile
	 * that it can hold objects of any type
	 * 
	 * Ex: someList2 = new List<String(); is not legal
	 * 
	 * List<String> is a subtype of the raw type list, but not of
	 * the parameterized type List<Object>
	 */
	private List<Object> someList2;
	
	/*
	 * If you want to use a generic type, but
	 * don't care what the actual type parameter is,
	 * use a ? instead. An unbounded wildcard type
	 * 
	 * You can only put null into a List<?>, no other types are allowed
	 * where you can put any object into the List raw type
	 */
	private List<?> someListOfAnyType; /// List of some type
	
	private int countNumberOfElementsInCommon(Set<?> set1, Set<?> set2) {
		int result = 0;
		for(Object o : set1) {
			if(set2.contains(o))
				result++;
		}
		return result;
	}
}
