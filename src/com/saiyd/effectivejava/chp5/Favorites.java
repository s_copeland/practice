package com.saiyd.effectivejava.chp5;

import java.util.HashMap;
import java.util.Map;

public class Favorites implements TypeSafeHeterogeneousContainer {
	private Map<Class<?>, Object> favorites = new HashMap<Class<?>, Object>();
	@Override
	public <T> void putFavorites(Class<T> type, T instance) {
		if(type == null)
			throw new NullPointerException("Type is null");
		favorites.put(type, type.cast(instance)); // ensure the type of the instance matches
	}

	@Override
	public <T> T getFavorite(Class<T> type) {
		return type.cast(favorites.get(type));
	}

}
