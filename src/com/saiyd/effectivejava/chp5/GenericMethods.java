package com.saiyd.effectivejava.chp5;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class GenericMethods {
	// The generic type parameter list appears between the method's modifiers and the return type
	public static <E> Set<E> union(Set<E> s1, Set<E> s2) {
		Set<E> result = new HashSet<E>(s1);
		result.addAll(s2);
		return result;
	}
	
	// generic factory method
	public static <K,V> HashMap<K,V> createHashMap() {
		return new HashMap<K,V>();
	}
	
	// type inference makes it easier by figuring out the type for the parameters K and V
	Map<String,List<String>> someMap = createHashMap();
}
