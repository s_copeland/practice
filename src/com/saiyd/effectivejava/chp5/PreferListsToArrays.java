package com.saiyd.effectivejava.chp5;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PreferListsToArrays {
	// Array's are covariant, an example of  covariance follows
	private Object[] objArray;
	private String[] stringArray; // String[] is a subtype of Object[]
	
	// Generic are invariant, neither is a subtype nor a super type 
	private List<Object> objList;
	private List<String> stringList; // stringList is not a sub type of objList and objList is not a super type 
	
	/*
	 * The preceeding creates the following dilemma
	 */
	private static Object[] objectArray = new Long[1];
	static {
		objectArray[0] = "This fails at runtime";
		
		// this won't compile and is safer
		//List<Object> ol = new ArrayList<Long>();
		//ol.add("This won't compile");
		
		// with the array you find out you made a mistake at runtime
	}
	
	/*
	 * Arrays are reified, this means that they know and enforce their type at runtime
	 * 
	 * Generics are implemented by type erasure and only enforce their contraints at
	 * compile time. They discard their type information at runtime. Erasure allows generic
	 * types to work with legacy code before generics were introduced
	 * 
	 * Only parameterized types that are reifiable are unbounded wildcard types.
	 * List<?>
	 */
	List<?>[] unboundedWildCardTypeArray; // this is legal
	
	// is illegal to create an array of a generic type
	// List<E>[] illegal1;
	// new List<String>[]
	// new E[]
	
	// Below is an example of the problem that could occur if this was allowed
	// List<String>[] stringLists = new List<String[1];
	// List<Integer> intList = Arrays.asList(42);
	// Object[] objects = stringLists;
	// objects[0] = intList;
	// String s = stringLists[0].get(0);  // this is a problem..., the compiler would automatically cast to an Integer to a String
}
