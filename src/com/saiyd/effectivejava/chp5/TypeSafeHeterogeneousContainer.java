package com.saiyd.effectivejava.chp5;

public interface TypeSafeHeterogeneousContainer {
	public <T> void putFavorites(Class<T> type, T instance);
	public <T> T getFavorite(Class<T> type);
}
