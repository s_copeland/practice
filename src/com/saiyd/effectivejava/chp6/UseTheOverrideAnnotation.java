package com.saiyd.effectivejava.chp6;

public class UseTheOverrideAnnotation {
	static class Bigram {
		private final char first;
		private final char second;
		
		public Bigram(char first, char second) {
			this.first = first;
			this.second = second;
		}
		
		/* THE PROBLEM IS HERE, if we used @Override the compiler would
		 * have let us know that we did not override Object.equals, but
		 * overloaded it instead
		 */
		public boolean equals(Bigram other) {
			return other.first == first && other.second == second;
		}
		
		public int hashCode() {
			return 31 * first + second;
		}
	}
}
