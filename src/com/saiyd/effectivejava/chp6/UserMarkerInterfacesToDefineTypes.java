package com.saiyd.effectivejava.chp6;

public interface UserMarkerInterfacesToDefineTypes {
	/*
	 * Marker interfaces define a type that is implemented by instances of the marked
	 * class; while marker annotations do not
	 * 
	 * The existence of this type allows you to catch errors
	 * at compile time that you couldn't catch until
	 * runtime if you used a marker annotation
	 */
	
	/*
	 * Marker interfaces can be targeted more precisely.
	 * 
	 * If an annotation tpe is declared with target ElementType.TYPE,
	 * it can be applied to any class or interface.
	 * 
	 * If you want to have a marker that is only applicable to
	 * implementations of a certain interface, YOU CAN have the
	 * marker interface extend the previously mentioned interface.
	 * This would be an example of a restricted marker interface
	 */
}
