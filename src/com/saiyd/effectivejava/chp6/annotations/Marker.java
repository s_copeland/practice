package com.saiyd.effectivejava.chp6.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

// Marker annotations have no parameters
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Marker {
	/*
	 * It is possible to add more information to a marker annotation after
	 * it is already in use, by adding one or more annotation type
	 * elements with defaults... vs a marker interface. Where it is 
	 * more difficult to add methods to an interface after it 
	 * has been implemented
	 */
}
