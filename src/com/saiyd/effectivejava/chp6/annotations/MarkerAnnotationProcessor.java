package com.saiyd.effectivejava.chp6.annotations;

import java.lang.reflect.Method;

public class MarkerAnnotationProcessor {
	public static void main(String[] args) throws ClassNotFoundException {
		Class markedClass = Class.forName("com.saiyd.effectivejava.chp6.annotations.MarkedClass");
		
		for(Method m : markedClass.getDeclaredMethods()) {
			if(m.isAnnotationPresent(Marker.class))
				System.out.println("Found Marker annotation");
		}
	}
}
