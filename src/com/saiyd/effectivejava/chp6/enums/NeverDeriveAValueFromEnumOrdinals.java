package com.saiyd.effectivejava.chp6.enums;

public enum NeverDeriveAValueFromEnumOrdinals {
	SOLO, DUET, TRIO, QUARTET, QUINTET, SEXTET, SEPTET, OCTET, NONET, DECTET;
	
	/*
	 * If we add or remove a enum type this breaks
	 */
	public int countOfMusicians() {return ordinal() + 1;}
	
	/*
	 * Do this instead
	 */
	
	enum Ensemble {
		SOLO(1), DUET(2), TRIO(3), QUARTET(4), QUINTET(5), SEXTET(6), SEPTET(7), OCTET(8), NONET(9), DECTET(10);
		
		private final int numOfMusicians;
		
		Ensemble(int size) {
			numOfMusicians = size;
		}
		
		public int countOfMusicians() {
			return numOfMusicians;
		}
	}
}
