package com.saiyd.effectivejava.chp6.enums;

import java.util.EnumMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class UseEnumMapInsteadofOrdinalIndexing {
	static class Herb {
		enum Type {ANNUAL, PERENNIAL, BIENNIAL}
		
		final String name;
		final Type type;
		
		Herb(String name, Type type) {
			this.name = name;
			this.type = type;
		}
		
		@Override
		public String toString() {
			return name;
		}
	}
	
	private static Herb[] garden = new Herb[16];
	
	public static void main(String[] args) {
		
		
		Set<Herb>[] herbsByType = // Indexed by Herb.Type.ordinal(), DON'T DO THIS
				(Set<Herb>[]) new Set[Herb.Type.values().length]; // This uncheck cast is required because arrays aren't compatible with generics
		
		for(int i=0; i < herbsByType.length; i++)
			herbsByType[i] = new HashSet<Herb>();
		
		for(Herb h : garden)
			herbsByType[h.type.ordinal()].add(h); // Don't do this!!, its possible to get an array index out of bounds exception
		
		for(int i=0; i < herbsByType.length; i++) {
			System.out.printf("%s: %s%n", Herb.Type.values()[i], herbsByType[i]);
		}
	}
	
	
	/*
	 * The array Set<Herb>[] herbsByType is effectively serving as a map, we should use a map instead
	 */
	public static void correctWay() {
		Map<Herb.Type, Set<Herb>> herbsByType = new EnumMap<Herb.Type, Set<Herb>>(Herb.Type.class);
		for(Herb.Type t : Herb.Type.values())
			herbsByType.put(t,  new HashSet<Herb>());
		
		for(Herb h : garden)
			herbsByType.get(h.type).add(h);
		
		System.out.println(herbsByType);
	}
	
	
	/* Two dimensional arrays of enums
	 * that maps two phases to a phase
	 * transition
	 */
	static enum Phase {
		SOLID, LIQUID, GAS;
		
		enum Transition {
			MELT, FREEZE, BOIL, CONDENSE, SUBLIME, DEPOSIT;
			
			// rows indexed by src-oridnal, cols by dst-ordinal
			private static final Transition[][] TRANSITIONS = {
				{null, MELT, SUBLIME},
				{FREEZE, null, BOIL},
				{DEPOSIT, CONDENSE, null}
			};
			
			public static Transition from(Phase src, Phase dst) {
				return TRANSITIONS[src.ordinal()][ dst.ordinal()];
			}
		}
	}

	
	// better approach
	static enum Phase2 {
		SOLID, LIQUID, GAS;
		
		enum Transition {
			MELT(SOLID, LIQUID), 
			FREEZE(LIQUID, SOLID), 
			BOIL(LIQUID, GAS), 
			CONDENSE(GAS, LIQUID), 
			SUBLIME(SOLID, GAS), 
			DEPOSIT(GAS, SOLID);
			
			private final Phase2 src;
			private final Phase2 dst;
			
			Transition(Phase2 src, Phase2 dst) {
				this.src = src;
				this.dst = dst;
			}
			
			private static final Map<Phase2, Map<Phase2, Transition>> m = new EnumMap<Phase2, Map<Phase2, Transition>>(Phase2.class);
			static {
				for(Phase2 p : Phase2.values())
					m.put(p, new EnumMap<Phase2, Transition>(Phase2.class));
				for(Transition trans : Transition.values())
					m.get(trans.src).put(trans.dst, trans);
			}
			
			
			public static Transition from(Phase2 src, Phase2 dst) {
				return m.get(src).get(dst);
			}
		}
	}
	
	/*
	 * The following is illegal
	 */
	//static enum CannotExtend extends Phase2 {	
	//}
	
	/*
	 * This is legal
	 */
	static interface ISomeInterface {
		public void doBlat();
	}
	static enum CanImplement implements ISomeInterface {
		VAL1 {
			@Override public void doBlat() { 
				// do nothing 
			}
		},
		VAL2 {
			@Override public void doBlat() { 
				// do nothing 
			}
		}
	}
}
