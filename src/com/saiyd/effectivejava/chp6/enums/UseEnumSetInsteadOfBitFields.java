package com.saiyd.effectivejava.chp6.enums;

import java.util.EnumSet;
import java.util.Set;

public class UseEnumSetInsteadOfBitFields {
	/*
	 * Example bit fields
	 */
	public static final int STYLE_BOLD = 1 << 0;
	public static final int STYLE_ITALIC = 1 << 1;
	public static final int STYLE_UNDERLINE = 1 << 2;
	public static final int STYLE_STRIKETHROUGH = 1 << 3;
	
	// parameter is the bitwise OR of zero or more style constants
	public void applyStyles(int styles) {
		
	}
	
	
	
	/* Use of EnumSet
	 * 
	 */
	enum Style { BOLD, ITALIC, UNDERLINE, STRIKETHROUGH }
	
	// Any Set could be passed in, but EnumSet is the best
	public void applyStyles(Set<Style> styles) {
		
	}
	
	
	public static void main(String[] args) {
		UseEnumSetInsteadOfBitFields a = new UseEnumSetInsteadOfBitFields();
		a.applyStyles(EnumSet.of(Style.BOLD, Style.ITALIC));
	}
}
