package com.saiyd.effectivejava.chp6.enums;

public enum UseEnumsInsteadOfIntConstants {
	/*
	 * Don't uset the int enum pattern.
	 * 
	 * It doesn't provide type safety
	 * It doesn't provide namespace and usually relies on prefixes
	 * It makes programs brittle.
	 *   int enums are compile time constants
	 *   if the int associated with an enum constant is changed
	 *   the clients need to be recompiled
	 * 
	 * There isn't an easy way to iterate over all int enum constants
	 * in a group or to determine how many there are
	 * 
	 * 
	 */
	
	/*
	 * Enums are immutable so all fields should be final
	 */
	
	// Examples of constnt specific class bodies, this may not be the best approach
	// if you want to share code among enum constants
	ONE {  int apply(int arg) { return arg *2;}},
	TWO { int apply(int arg) { return arg *5;}}
}
