package com.saiyd.effectivejava.chp7;

/*
 * This is part of the general principle that
 * you should attempt to detect errors as soon
 * as possible
 */
public class CheckParametersForValidity {
	// use exceptions on public methods when preconditions, postconditions, and invariants are violated
	public static void concatenate(String s1, String s2) {
		if(s1.isEmpty())
			throw new IllegalArgumentException("s1 must not be empty");
		if(s2.isEmpty())
			throw new IllegalArgumentException("s2 must not be empty");
	}
	
	// you can use assertions on non public methods when preconditions, postconditions, and invariants
	// are violated
	//
	// Don't forget to enabe assertions on the compiler...
	private static void sort(long a[], int offset, int length) {
		assert a != null;
		assert offset >= 0 && offset <= a.length;
		assert length >= 0 && length <= a.length - offset;
	}
}
