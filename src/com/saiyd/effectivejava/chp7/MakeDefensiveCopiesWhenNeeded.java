package com.saiyd.effectivejava.chp7;

import java.util.Date;

public class MakeDefensiveCopiesWhenNeeded {
	// this class is SUPPOSED to be immutable
	static class Period {
		private final Date start;
		private final Date end;
		
		public Period(Date start, Date end) {
			if(start.compareTo(end) > 0)
				throw new IllegalArgumentException(start + " after " + end);
			this.start = start;
			this.end = end;
		}

		// the accessors allow access to the internal references... bad
		public Date getStart() {
			return start;
		}

		public Date getEnd() {
			return end;
		}
	}
	
	public static void main(String[] args) {
		// break Period's immutability
		Date start = new Date();
		Date end = new Date();
		Period p = new Period(start, end);
		
		end.setYear(77); // whomp whomp... we modified p's internal end  object
	}
	
	static class ImmutabePeriod {
		private final Date start;
		private final Date end;
		
		public ImmutabePeriod(Date start, Date end) {
			/*
			 * Defensive copies are made before checking the validity
			 * of the parameters, and the validity check is performed
			 * on the copies rather than the originals
			 * 
			 * This is important because it protects the class
			 * against changes from another thread during the "windows of vulnerability"
			 * between the time the parameters are checked and the
			 * time they are copied.
			 * 
			 * This is known as a time-of-check/time-of-use attack TOCTOU
			 * 
			 * We also don't use the clone method because the clone
			 * method can return an malicous sub class. The malicious sub class
			 * could record a reference to each instance in a private static list
			 * at creation time and allow the attacker access to the list
			 */
			this.start = new Date(start.getTime());
			this.end = new Date(end.getTime());
			
			if(this.start.compareTo(this.end) > 0)
				throw new IllegalArgumentException(this.start + " after " + this.end);
		}

		/*
		 * return defensive copies
		 */
		public Date getStart() {
			return new Date(start.getTime());
		}

		public Date getEnd() {
			return new Date(end.getTime());
		}
	}
}
