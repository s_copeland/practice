package com.saiyd.effectivejava.chp7;

public class Prefer2ElementEnumTypesToBooleanParameters {
	enum TemperatureScale {FAHRENHEIT, CELSIUS}
	
	static class Thermometer {
		private Thermometer(TemperatureScale scale) {
			
		}
		
		public static Thermometer newInstance(TemperatureScale scale) {
			return new Thermometer(scale);
		}
	}
	
	public static void main(String[] args) {
		Thermometer t = Thermometer.newInstance(TemperatureScale.FAHRENHEIT);
		// the previous is more clear than something like the following
		//Thermometer t = Thermometer.newInstance(true);
		
		// you can also expand it later, by adding KELVIN to the enum
	}
}
