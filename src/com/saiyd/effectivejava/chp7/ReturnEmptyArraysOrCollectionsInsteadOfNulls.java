package com.saiyd.effectivejava.chp7;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ReturnEmptyArraysOrCollectionsInsteadOfNulls {
	private List<String> listOfItems= new ArrayList<String>();
	
	public String[] getItems() {
		if(listOfItems.size() == 0)
			return null;
		
		String[] retArray = new String[listOfItems.size()];
		return listOfItems.toArray(retArray);
	}
	
	/*
	 * The previous getItems() method forces the client code
	 * to do extra null checking work
	 */
	//String[] items = getItems();
	//if(items != null && Arrays.asList(items).contains("whomp"))
	//	System.out.println("...whoooomp");
	
	
	/*
	 * We can do the following instead
	 */
	private static final String[] EMPTY_ITEMS_ARRAY = new String[0];
	public String[] getItems2() {
		if(listOfItems.size() == 0)
			return listOfItems.toArray(EMPTY_ITEMS_ARRAY);
		
		String[] retArray = new String[listOfItems.size()];
		return listOfItems.toArray(retArray);
	}
	
	/*
	 * If we are using collections we can use the following
	 */
	public List<String> getItems3() {
		if(listOfItems.isEmpty()) {
			return Collections.emptyList();
		} else {
			return new ArrayList<String>(listOfItems);
		}
	}
}
