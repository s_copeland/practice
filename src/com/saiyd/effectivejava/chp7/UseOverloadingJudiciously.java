package com.saiyd.effectivejava.chp7;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class UseOverloadingJudiciously {
	static class CollectionClassifier {
		public static String classify(Set<?> s) {
			return "Set";
		}
		
		public static String classify(List<?> list) {
			return "List";
		}
		
		public static String classify(Collection<?> c) {
			return "Unknown Collection";
		}
		
		public static void main(String[] args) {
			/*Collection<?> collections = {
					new HashSet<String>(),
					new ArrayList<BigInteger>(),
					new HashMap<String, String>().values()
			};
			*/
			
			/*
			 * The choice of which overloading to invoke is made at compile time
			 * 
			 * Selection of overloaded methods is static, while selection among
			 * overridden methods is dynamic
			 */
		}
	}
}
