package com.saiyd.effectivejava.chp7;

public class UseVarArgsJudicuously {
	// passing one or more arguements to a function with var args
	public static void processInts(int firstParam, int... followingParams) {
		int sum = firstParam;
		for(int curInt : followingParams) {
			sum += curInt;
		}
		System.out.println("Sume = " + sum);
	}
	
	// Only use varargs when a call really operates on a variable length sequence of values
	
	
	/*
	 * Methods with either of the following signatures
	 * will accept any parameter list. Any compile-time
	 * type-checking is lost
	 */
	int suspect1(Object... args) {
		return 0;
	}
	
	<T> int suspect2(T... args) {
		return 0;
	}
	
	/*
	 * Be careful when using varargs in performance critical
	 * situations. Every invocation of the varargs method
	 * causes an array allocation and initialization
	 */
}
