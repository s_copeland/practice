package com.saiyd.effectivejava.chp8;

public class AvoidFloatAndDoubleIfExactAnswersAreRequired {
	/*
	 * float and double types are designed primarily  for scientific
	 * and engineering calculations. They perform binary floating-point
	 * arithmetic
	 * 
	 * They do not provide exact results
	 * 
	 * Do not use them for money
	 * 
	 * use BigDecimal, int or long for monetary calculations
	 */
}
