package com.saiyd.effectivejava.chp8;

public class BewareThePerformanceOfStringConcatenation {
	/*
	 * Using the + string concatenation operator repeatedly
	 * to concatenate n strings requires time quadratic in n
	 * 
	 * This is an unfortunate consequence of the fact tht strings
	 * are immutable. When 2 strings are concatenated the
	 * contents of both are copied
	 */
	static {
		String someOtherString = "whomp";
		String result = "";
		for(int i=0; i < 1000; i++) {
			result += someOtherString;
		}
	}
	
	/*
	 * Use string builder instead, the unsynchronized replacement
	 * for StringBuffer
	 */
	static {
		String someOtherString = "whomp";
		StringBuilder sb = new StringBuilder(64);
		for(int i=0; i < 1000; i++) {
			sb.append(someOtherString);
		}
	}
}
