package com.saiyd.effectivejava.chp8;

public class MinimizeTheScopeOfLocalVariables {
	// declare a variable where it is first used
	
	// every variable should have an initializer, except in try-catch blocks
	// for variables used outside the block
	
	// prefer the for-each loop for iterating over a collection
	
	// prefer for loops to while loops
	
	// keep methods small and focused
}
