package com.saiyd.effectivejava.chp8;

public class OptimizeJudiciously {
	/*
	 * Don't sacrifice sound architectural principles for
	 * performance
	 * 
	 * Strive to write good programs rather than fast ones
	 */
	
	/*
	 * Consider the performance consequences of your API design
	 * decisions.
	 * 	 Ex: making a public tpe mutable may require more defensive copying
	 *   Ex: using inheritance in a public class instead of composition
	 *       ties the classes forever. This can place artificial limits
	 *       on performance of subclasses
	 *   Ex: using an implementation type instead of an interfaces
	 *       prevents you from using faster future implementations
	 */
}
