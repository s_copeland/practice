package com.saiyd.effectivejava.chp8;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;



public class PreferForEachLoopsToTraditionaForLoops {
	static class Thing {
		private String name = "What is this?";
	}
	static class Things implements Iterable<Thing> {

		@Override
		public Iterator<Thing> iterator() {
			// TODO Auto-generated method stub
			return null;
		}
		
	}
	
	public static void main(String[] args) {
		// old ways
		Collection<Integer> c = new ArrayList<Integer>();
		for(Iterator i = c.iterator(); i.hasNext();) {
			
		}
		
		char[] chars = new char[4];
		for(int i=0; i < chars.length; i++) {
			
		}
		
		/*
		 * new way
		 */
		for(Integer curInt : c) {
			
		}
		
		for(char curChar : chars) {
			
		}
		
		/*
		 * for-each loops let you iterate over any object
		 * that implements Iterable
		 */
		Things things = new Things();
		for(Thing thing : things) {
			
		}
	}
	
	/*
	 * You can't use for-each loops when you need to do any of
	 * the following things
	 * 
	 * 1. Filtering - you need to traverse a colection and remove
	 * selected elements. If so then you need to use an explicit
	 * iterator
	 * 
	 * 2. Transforming - If you need to traverse a list or array and replace
	 * some or all of the values of its elements, then you need the
	 * list iterator or array index
	 * 
	 * 3. Parallel iteration - If you need to traverse multiple collections
	 * in parallel, then you need explicit control over the iterator or index,
	 * so that they can be advanced together
	 */

}
