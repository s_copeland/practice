package com.saiyd.effectivejava.chp8;

public class PreferInterfacesToReflection {
	/*
	 * You lose all the benefits of compile-time checking, including
	 * exception checking
	 * 
	 * The code required to perform reflective access is clumsy
	 * and verbose
	 * 
	 * Performance suffers. 
	 */
	
	/*
	 * Generally, objects should not be accessed reflectively in
	 * normal applications at runtime
	 */
	
	// You can create instances reflectively and access them normally
	// via their interface or superclass
}
