package com.saiyd.effectivejava.chp8;

import java.util.Comparator;

public class PreferPrimitiveTypesToBoxedPrimitives {
	static Integer someInt;
	// auto boxing and unboxing were introduced in java 1.5
	
	// boxed primitives has an identity and values
	// primitives only have a value
	
	// primitives are more space efficient
	
	// Applying the == opertor to boxed primitives is almost always wrong
	static {
		Comparator<Integer> naturalOrder = new Comparator<Integer>() {

			@Override
			public int compare(Integer first, Integer second) {
				// The first evaluation causes both Integer objects to
				// be unboxed and compared, if first is not less than second, the second evaluation
				// occurs. The second evaluation uses the == operator
				// which tests the Integer object's identities, which
				// are never the same
				return first < second ? -1 : (first == second ? 0 : 1);
			}
			
		};
		
		/*
		 * The easiest way to fix this problem is to add two local variables
		 */
		Comparator<Integer> naturalOrder2 = new Comparator<Integer>() {

			@Override
			public int compare(Integer first, Integer second) {
				int f = first;
				int s = second;
				return f < s ? -1 : (f == s ? 0 : 1);
			}
			
		};
		
		
		// auto-unboxing null object references cause NPE
		someInt = 42;
		
		// auto-boxing and unboxing can be slow
		Long sum = 0L;
		for(long i = 0; i < Integer.MAX_VALUE; i++) {
			sum += i;
		}
	}
	
	/*
	 * use boxed primitives in Collections, as type
	 * parameters in parameterized types
	 * 
	 * they must be used when making reflective method
	 * invocations
	 */
}
