package com.saiyd.effectivejava.chp8;

public class ReferToOjbectsByTheirInterfaces {
	/*
	 * If appropriate interface types exist,
	 * then parameters, return values, variables,
	 * and fields shoudl all be declared using interface
	 * types
	 */
	
	/*
	 * It is entirely appropriate to refer to an object
	 * by a class raher than an interface if no appropriate
	 * interface exists
	 * 
	 * Ex: String, BigInteger, Random
	 * 
	 * Also, for objects in a framework whose fundamental
	 * types are lasses rather than interfaces. If an
	 * object belongs to such a class-based framework,
	 * it is preferable to refer to it by the relevant
	 * base class. Ex: java.util.TimerTask
	 * 
	 * Also, for cases where a cass implements an interface,
	 * but provide extra methods not found on the interface.
	 * Ex: PriorityQueue
	 */
}
