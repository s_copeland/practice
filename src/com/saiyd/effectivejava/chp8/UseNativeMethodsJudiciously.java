package com.saiyd.effectivejava.chp8;

public class UseNativeMethodsJudiciously {
	// it is rarely advisable to use native methods for improved performance
	
	// Applications using JNI are less portable
	// Memory issues
	
	// The cost of going into and out of native code can decrease
	// performance
}
