package com.saiyd.effectivejava.chp9;

public class AvoidUnnecessaryUseOfCheckedExceptions {
	/*
	 * Requiring a user to handle a checked exception is
	 * justified if the exceptional condition cannot be
	 * prevented by proper use of the API and the user can
	 * take some useful action once confronted with the
	 * exception
	 * 
	 * If both of these conditions do not hold, then
	 * an unchecked exceeption is more appropriate
	 */
	
	/*
	 * One way to turn a checked exception into an
	 * unchecked exception is to break the method 
	 * that throws the exception into two methods.
	 * The first method returns a boolean that
	 * indicates whether the exception would be
	 * thrown.
	 * 
	 * This refactoring is inappropriate if the
	 * object is accessed concurrently without
	 * external synchronization or it is subject
	 * to externally induced state transitions.
	 * 
	 * This is because the objects state may change between
	 * calls to the first and second methods
	 */
	static class BadMonkeyChecked extends Exception {

	}
	
	static class BadMonkeyUnchecked extends RuntimeException {
		
	}
	
	static {
		// instead of this
		try {
			doMonkeyBusiness();
		} catch(BadMonkeyChecked e) {
			// handle exceptional condition
		}
		
		// do this
		if(verifyMonkeyIsReady()) {
			doMonkeyBusiness2();
		} else {
			// handle exceptional condition
		}
	}
	
	private static boolean verifyMonkeyIsReady() {
		return false;
	}
	private static void doMonkeyBusiness() throws BadMonkeyChecked {
		throw new BadMonkeyChecked();
	}
	private static void doMonkeyBusiness2() {
		throw new BadMonkeyUnchecked();
	}
}
