package com.saiyd.effectivejava.chp9;

public class DoNotIgnoreExceptions {
	private static void saveToTheCloud() throws Exception {
		throw new Exception();
	}
	/*
	 * Don't do this
	 */
	static {
		try {
			saveToTheCloud();
		} catch(Exception e) {
		
		}
	}
	
	/*
	 * At the very least put a comment
	 * explaining why you are ignoring it
	 */
	static {
		try {
			saveToTheCloud();
		} catch(Exception e) {
			// ignore, we will try again later
		}
	}
}
