package com.saiyd.effectivejava.chp9;

public class DocumentAllExceptionsThrownByEachMethod {
	/*
	 * Always declare checked exceptions individually, and document
	 * precisely the conditions under which each one is
	 * thrown using Javadoc @throws tags
	 * 
	 * Never declare a method that throws Exception or Throwable.
	 * It denys the user any guidance to the exceptions the method
	 * is capable of throwing
	 */
	
	/*
	 * It is wise to document unchecked exceptions that a method
	 * can throw
	 */
	
	/*
	 * Methods in an interface should document unchecked exceptions.
	 * This documentation forms part of the interface's
	 * general contract
	 * 
	 * This is an ideal and is not always achievable
	 */
}
