package com.saiyd.effectivejava.chp9;

public class FavorTheUseOfStandardExceptions {
	// it makes your APIs easier to learn
	
	/*
	 *  IllegalArgumentException - generally thrown
	 *  when the caller passes in an argument whose value
	 *  is inappropriate
	 *  
	 *  ex: passing in a negative number to a function
	 *  that expects positive numbers
	 *  
	 */
	
	/*
	 * IlegalStateException - generally thrown if the invocation
	 * is illegal because of the state of the receiving object.
	 * 
	 * ex: the object is not properly initialized
	 */
	
	/*
	 * Arguably, all erroneous method invocations boil down to
	 * an  illegal argument or state, but it helps to be more
	 * specific if possible
	 */
	
	
}
