package com.saiyd.effectivejava.chp9;

public class IncludeFailureCaptureInformationInDetailMessages {

	/*
	 * An exception's toString() method should return as much
	 * information as possible concerning the cause of the
	 * failure for future analysis
	 * 
	 * Include the values of all parameters and fields that
	 * contributed to the exception
	 * 
	 * One way to ensure that exceptions contain failure-capture
	 * information is to require this information in their
	 * constructors
	 */
	
	static class IndexOutOfBoundException extends Exception {
		private final int lowerBound;
		private final int upperBound;
		private final int index;
		public IndexOutOfBoundException(int lowerBound, int upperBound, int index) {
			super("Lower bound: " + lowerBound + ", Upper bound: " + upperBound + ", Index: " + index);
			// save the failure-capture info
			this.lowerBound = lowerBound;
			this.upperBound = upperBound;
			this.index = index;
		}
		public int getLowerBound() {
			return lowerBound;
		}
		public int getUpperBound() {
			return upperBound;
		}
		public int getIndex() {
			return index;
		}
	}
}
