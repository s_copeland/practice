package com.saiyd.effectivejava.chp9;

/*
 * Use checked exceptions for recoverable conditions
 * and runtime exceptions for programming errors
 */
public class Item58 {
	/*
	 * Use checked exceeptions for conditions from
	 * which the caller can reasonably be expected
	 * to recover
	 */
	
	/*
	 * Unchecked exceptions are runtime exceptions
	 * and errors
	 * 
	 * These generally are not caught. If a program
	 * throws one of these exceptions, generally, recovery is
	 * impossible and continued executions would
	 * do more harm than good
	 */
	
	/*
	 * Use runtime exceptions to indicate programming errors
	 * 
	 * Many runtime exceptions indicate precondition
	 * violations.
	 * ex: ArrayIndexOutOfBoundsException
	 */
	
	/*
	 * errors are usually reserved for use by the JVM
	 * 
	 * Therefore, all of the unchecked throwables you
	 * implement should subclass RuntimeException directly
	 * or indirectly
	 */
	
	/*
	 * Don't subclass throwable. Exceptions of this type
	 * have the same behavior as they would if they subclassed
	 * Exception. It only serves to confuse users of your API
	 */
}
