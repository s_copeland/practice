package com.saiyd.effectivejava.chp9;

import java.util.EmptyStackException;

public class StriveForFailureAtomicity {
	/*
	 * After an object throws an exception, it is
	 * generally desirable that the object still be
	 * in a well-defined, usable state, even if the failure
	 * occurred in the midst of performing the operation
	 * 
	 * A failed method invocation should leave the object in 
	 * the state it was prior to the invocation
	 * 
	 * A method with this property is said to be failure atomic
	 */
	
	// If the object is immutable, then failure atomicity is free
	
	/*
	 * For mutable objects, check parameters for validity before
	 * performing the operation
	 */
	private static int stackSize;
	private static Object pop() {
		if(stackSize == 0)
			throw new EmptyStackException();
		// else... 
		return new Object(); // I don't feel like finishing this example...
	}
	
	/*
	 * You can also order the operations of a method, so that
	 * the failure is detected before any modifications to the
	 * object are performed
	 */
	
	/*
	 * Also, you can write recovery code that intercepts a failure and
	 * rolls back to the prior state
	 */
	
	/*
	 * You can also perform the operation on a temporary copy of the object.
	 * And then replace the object with the temp if the operation
	 * is successful
	 */
	
	/*
	 * Failure-atomicity is not always achievable. For example 2 threads
	 * may modify an unsynchronized object
	 */
}
