package com.saiyd.effectivejava.chp9;

public class ThrowExceptionsAppropriateToTheAbstraction {
	/*
	 * It is disconcerting when a method throws an exception
	 * that has no apparent connection to the task it
	 * performs. This often happens when a method propagates
	 * an exception thrown by a lower-level abstraction.
	 * 
	 * It pollutes the API of the higher level with implementation
	 * details
	 * 
	 * If the implementation changes in a subsequent release, the
	 * exceptions it throws may change too, potentially breaking
	 * existing clients
	 */
	
	/*
	 * Higher ayers should catch lower-level exceptions, and in
	 * their place, throw exceptions that can be explained in terms
	 * of the higher-level abstraction
	 * 
	 * This is known as exceptions translation
	 */
	static class UserLevelException extends Exception {
		
	}
	static class ChainedUserLevelException extends Exception {
		public ChainedUserLevelException(Throwable t) {
			super(t);
		}
	}
	
	private static void doWork() throws UserLevelException {
		try {
			Integer.parseInt("whomp");
		} catch(NumberFormatException e) {
			// translate
			throw new UserLevelException();
		}
	}
	
	/*
	 * Exception Chaining
	 */
	private static void doWork2() throws ChainedUserLevelException {
		try {
			Integer.parseInt("whooooomp");
		} catch(NumberFormatException e) {
			throw new ChainedUserLevelException(e);
		}
	}
}
