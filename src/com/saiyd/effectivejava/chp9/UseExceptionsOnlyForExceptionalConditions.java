package com.saiyd.effectivejava.chp9;

public class UseExceptionsOnlyForExceptionalConditions {
	/* placing code inside a try-catch block inhibits
	 * certain optimizations that jvms might otherwise perform
	 */
	
	// don't use exceptions for ordinary control flow
	
	/* A well designed API must not force its clients to use exceptions for ordinary control flow
	 * 
	 * A class with a state-dependent method that can only be invoked
	 * under certain unpredictable conditions should have
	 * a separate state-testing method. That methods would indicate
	 * if it is ok to invoke the state-dependent method
	 * 
	 * Ex: Iterator.hasNext() and Iterator.next()
	 * 
	 * If Iterator lacked the hasNext() method, the client would have to do this
	 * try {
	 *     Iterator<Foo> i = collection.iterator();
	 *     while(true) {
	 *         Foo foo = i.next();
	 *         ...
	 *     }
	 * } catch(NoSuchElementException e) {
	 * 
	 * }
	 */
	
	/*
	 * If an object is to be accessed concurrently without external synchronization
	 * or is subject to externally induced state transitions, you
	 * must use a distinguished return value, as the object's state
	 * could change in the interval between the invocation of a
	 * state-testing method and its state-dependent method.
	 * 
	 * Performance concerns may dictate that a distinguished return value be
	 * used if a separate state-testing method would duplicate the work
	 * of the state-dependent method
	 * 
	 */
}
