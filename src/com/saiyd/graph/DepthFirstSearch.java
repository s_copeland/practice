package com.saiyd.graph;

import java.util.Stack;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;

public class DepthFirstSearch {

	public static <T> boolean search(Vertex<T> start, ISearchSuccess<T> searchSuccess, Stack<Vertex<T>> path)
	{
		// check if we're stuck in a cycle
		if(path.contains(start))
			return false;
		
		path.push(start);
		
		boolean retVal = searchSuccess.isFound(start);
		if(retVal) {
			return true;
		} else {
			// continue searching
			Iterator<Vertex<T>> it = start.iterator();
			while(it.hasNext()) {
				Vertex<T> curVertex = it.next();
				// recursive call
				if(search(curVertex, searchSuccess, path))
					return true;
			}
		}
		
		path.pop();
		
		return false;
	}
	
	public static <T> boolean coloredSearch(Vertex<T> start, ISearchSuccess<T> searchSuccess, Stack<Vertex<T>> path) {
		path.push(start);
		
		if(searchSuccess.isFound(start))
			return true;
		
		start.setState(VertexState.PARTIAL_EXPLORED);
		
		// keep searching
		Iterator<Vertex<T>> it = start.iterator();
		while(it.hasNext()) {
			Vertex<T> curVertex = it.next();
			if(curVertex.getState() == VertexState.NOT_EXPLORED && coloredSearch(curVertex, searchSuccess, path)) {
				return true;
			}
		}
		
		start.setState(VertexState.FULL_EXPLORED);
		
		return false;
	}
	
	public static List<Vertex<Integer>> generatePetersenGraph() {
		List<Vertex<Integer>> retVal = new ArrayList<Vertex<Integer>>();
		// create 10 nodes
		for(int i=0; i < 10; i++) {
			retVal.add(new Vertex<Integer>(i));
		}
		
		int[][] edges =
		    {{0,1}, {1,0}, {1,2}, {2,1}, {2,3}, {3,2}, {3,4}, {4,3}, {4,0}, {0,4},
		     {5,6}, {6,5}, {6,7}, {7,6}, {7,8}, {8,7}, {8,9}, {9,8}, {9,5}, {5,9},
		     {5,0}, {0,5}, {6,2}, {2,6}, {7,4}, {4,7}, {8,1}, {1,8}, {9,3}, {3,9}};
		
		for(int[] e : edges) {
			retVal.get(e[0]).addAdjacentVertex(retVal.get(e[1]));
		}
		
		return retVal;
	}
	
	public static void main(String[] args) {
		List<Vertex<Integer>> petersenGraph = generatePetersenGraph();
		Stack<Vertex<Integer>> path = new Stack<Vertex<Integer>>();
		ISearchSuccess<Integer> searchSuccess = new EqualitySuccess<Integer>(7);
		
		long startTime = System.nanoTime();
		
		if(search(petersenGraph.get(0), searchSuccess, path)) {
			System.out.print("Path found: ");
			for(Vertex<Integer> v : path) {
				System.out.print(v.getData() + " ");
			}
			System.out.println();
			System.out.println("Duration = " + (System.nanoTime() - startTime));
		} else {
			System.out.println("No path found");
			System.out.println("Duration = " + (System.nanoTime() - startTime));
		}
		
		
		// colored depth first search
		path = new Stack<Vertex<Integer>>();
		
		// mark all nodes as not explored
		for(Vertex<Integer> v : petersenGraph) {
			v.setState(VertexState.NOT_EXPLORED);
		}
		
		startTime = System.nanoTime();
		
		if(coloredSearch(petersenGraph.get(0), searchSuccess, path)) {
			System.out.print("Path found: ");
			for(Vertex<Integer> v : path) {
				System.out.print(v.getData() + " ");
			}
			System.out.println();
			System.out.println("Duration = " + (System.nanoTime() - startTime));
		} else {
			System.out.println("No path found");
			System.out.println("Duration = " + (System.nanoTime() - startTime));
		}
	}
}
