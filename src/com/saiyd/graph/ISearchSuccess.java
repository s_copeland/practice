package com.saiyd.graph;

public interface ISearchSuccess<T> {
	public boolean isFound(Vertex<T> vertex);
}
