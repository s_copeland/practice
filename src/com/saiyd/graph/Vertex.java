package com.saiyd.graph;

import java.util.LinkedList;
import java.util.List;
import java.util.Iterator;

public class Vertex<T> {
	private T mData;
	private VertexState mState;
	private List<Vertex<T>> mAdjacentVertexes = new LinkedList<Vertex<T>>();
	
	public Vertex(T data)
	{
		mData = data;
	}
	public T getData() {
		return mData;
	}
	public void setData(T data) {
		this.mData = data;
	}

	public VertexState getState() {
		return mState;
	}
	public void setState(VertexState mState) {
		this.mState = mState;
	}
	public void addAdjacentVertex(Vertex<T> vertex) {
		mAdjacentVertexes.add(vertex);
	}
	
	public boolean removeAdjacentVertex(Vertex<T> vertex) {
		return mAdjacentVertexes.remove(vertex);
	}
	
	public Iterator<Vertex<T>> iterator() {
		return mAdjacentVertexes.iterator();
	}
}
