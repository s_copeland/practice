package com.saiyd.graph;

public enum VertexState {
	NOT_EXPLORED, PARTIAL_EXPLORED, FULL_EXPLORED;
}
