package com.saiyd.graph.datastructures;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import com.saiyd.graph2.Graph;
import com.saiyd.graph2.Graph.EdgeDirection;
import com.saiyd.graph2.Graph.Type;


/*
 * A disjoint-set data structure maintains a colleciton S = {S1, S2, ..., Sk}
 * of disjoint dynamic sets. Each set is identified by a "representative", which
 * is some member of the set. In some applications it doesn't matter which member
 * is used as the representative; we only care that if we ask for the 
 * representative of a dyanmic set twice without modifying the set between the 
 * requests, we get the same answer both times. 
 * 
 * It supports the following operations
 * 		makeSet(x) - creates a new set whose only member is x
 * 		union(x, y) - unites the dynamic sets that contain x and y, 
 * 					  the sets Sx and Sy containing x and y are destroyed in the process
 * 		findSet(x) - returns a reference to the representative of the set containing x
 * 
 * Applications
 * 
 * 	Disjoint sets are using in finding the connected components of a graph.
 * 		Note: when the edges of the graph are "static" - not changing over time - the 
 * 			  connected components can be computed faster by using depth-first search.
 * 			  Sometimes, however, the edges are added dynamically and we need to maintain
 * 			  the connected components as each edge is added. In this case, the 
 * 			  implementation given here can be more efficient than running a new depth-first
 * 			  search for each new edge. 
 */
public class DisjointSet <T extends Comparable<T>> {
	private List<SetElementList<SetElement<DisjointSetVertex<T>>>> listOfSets = new LinkedList<SetElementList<SetElement<DisjointSetVertex<T>>>>();
	
	public String toString() {
		return listOfSets.toString();
	}
	
	public void makeSet(DisjointSetVertex<T> vertex) {
		SetElement<DisjointSetVertex<T>> newHead = new SetElement<DisjointSetVertex<T>>(vertex);
		vertex.setSetElement(newHead);
		
		newHead.listRepresentative = newHead;
		SetElementList<SetElement<DisjointSetVertex<T>>> setElementList = new SetElementList<SetElement<DisjointSetVertex<T>>>(newHead);
		
		newHead.setElementList = setElementList;
		
		
		listOfSets.add(setElementList);
	}
	
	/*
	 * This method uses a weighted-union heuristic. It
	 * always append the smaller list to the larger.
	 * 
	 */
	public DisjointSetVertex<T> union(DisjointSetVertex<T> vertex1, DisjointSetVertex<T> vertex2) {
		SetElementList<SetElement<DisjointSetVertex<T>>> set1 = vertex1.getSetElement().setElementList; 
		SetElementList<SetElement<DisjointSetVertex<T>>> set2 = vertex2.getSetElement().setElementList;
		
		//System.out.println(set1);
		//System.out.println(set2);
		//System.out.println();
		
		SetElementList<SetElement<DisjointSetVertex<T>>> smaller;
		SetElementList<SetElement<DisjointSetVertex<T>>> larger;
		
		// append the smaller to the larger
		if(set1.size > set2.size) {
			larger = set1;
			smaller = set2;
		} else {
			larger = set2;
			smaller = set1;
		}
		
		// link the smaller to the larger
		System.out.println("Linking " + smaller.head + " to " + larger.tail);
		larger.tail.next = smaller.head;
		larger.tail = smaller.head;
		
		
		SetElement <DisjointSetVertex<T>> curSmallerSetElement = smaller.head;
		
		while(curSmallerSetElement != null) {
			curSmallerSetElement.listRepresentative = larger.head;
			curSmallerSetElement.setElementList = larger.head.setElementList;
			
			larger.tail = curSmallerSetElement;
			curSmallerSetElement = curSmallerSetElement.next;
		}
		
		larger.size += smaller.size;
		
		listOfSets.remove(smaller);
		System.out.println("smaller "+ smaller);
		System.out.println("larger "+ larger);
		System.out.println();
		
		return larger.head.listRepresentative.object;
	}
	
	/*
	 * Determines what list this vertex is a member of
	 */
	public DisjointSetVertex<T> find(DisjointSetVertex<T> vertex) {
		if(vertex != null && vertex.getSetElement() != null)
			return vertex.getSetElement().setElementList.head.object;
		return null;
	}
	
}
