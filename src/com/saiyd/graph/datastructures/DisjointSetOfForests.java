package com.saiyd.graph.datastructures;

import java.util.LinkedList;
import java.util.List;


/*
 * A disjoint set where each set is represented by a tree
 * data structure, in which each node holds a reference
 * to its parent node. 
 * 
 * The representative node of each set is the root of that
 * set's tree.
 * 
 * The worst case running time using both union-by-rank
 * and path compression is O(alpha(n)) where alpha(n) is
 * a very slowly growing function. In any conceivable application
 * of a disjoint-set datastructure, alpha(n) <= 4.
 * Thus we can view the running time as linear in m in
 * all practical situations.
 */
public class DisjointSetOfForests <T extends Comparable<T>> {
	private List<DisjointSetTreeVertex<T>> listOfSets = new LinkedList<DisjointSetTreeVertex<T>>();
	
	public DisjointSetOfForests() {
		
	}
	
	/*
	 * This function is used by the OffLineMinimum Disjoin set problem
	 */
	public DisjointSetTreeVertex<T> findSetFollowing(DisjointSetTreeVertex<T> refSet) {
		int index = listOfSets.indexOf(refSet);
		index++; // move to the following item
		
		DisjointSetTreeVertex<T> ret = null;
		if(index < listOfSets.size())
			ret = listOfSets.get(index );
		return ret;
	}
	
	public void makeSet(DisjointSetTreeVertex<T> vertex) {
		vertex.setRank(0);
		// by default this node is its own root, in the constructor
		listOfSets.add(vertex);
	}
	
	/*
	 * implements path compression
	 * 
	 * It is used to make each node in the tree point directly
	 * to the root.
	 * 
	 * The method uses a two-pass method. It makes one pass up
	 * the tree to find the root, and makes a second path back down
	 * the tree to update each node so that they point to the
	 * root
	 */
	public DisjointSetTreeVertex<T> find(DisjointSetTreeVertex<T> vertex) {
		if(vertex.getParent() != vertex) {
			vertex.setParent(find(vertex.getParent()));
		}
		return vertex.getParent();
	}
	
	public DisjointSetTreeVertex<T> union(DisjointSetTreeVertex<T> vertex1, DisjointSetTreeVertex<T> vertex2) {
		DisjointSetTreeVertex<T> root1 = find(vertex1);
		DisjointSetTreeVertex<T> root2 = find(vertex2);
		return link(root1, root2);
	}
	
	public DisjointSetTreeVertex<T> unionSecondToFirst(DisjointSetTreeVertex<T> vertex1, DisjointSetTreeVertex<T> vertex2) {
		DisjointSetTreeVertex<T> root1 = find(vertex1);
		DisjointSetTreeVertex<T> root2 = find(vertex2);
		return linkSecondToFirst(root1, root2);
	}
	
	/*
	 * implements union-by-rank
	 * 
	 * This is similar to weighted-union for linked list based disjoint sets.
	 * The idea is to make the root of the tree with fewer nodes point to
	 * the root of the tree with more nodes
	 * 
	 * By itself only, union-by-rank yields a running time of O(m lg n)
	 */
	private DisjointSetTreeVertex<T> link(DisjointSetTreeVertex<T> root1, DisjointSetTreeVertex<T> root2) {
		int root1Rank = root1.getRank();
		int root2Rank = root2.getRank();
		
		if(root1Rank > root2Rank) {
			root2.setParent(root1);
			listOfSets.remove(root2);
			return root1;
		} else {
			root1.setParent(root2);
			listOfSets.remove(root1);
			if(root1Rank == root2Rank) {
				root2.setRank(root1Rank + 1);
			}
			return root2;
		}
	}
	
	private DisjointSetTreeVertex<T> linkSecondToFirst(DisjointSetTreeVertex<T> root1, DisjointSetTreeVertex<T> root2) {
		root2.setParent(root1);
		listOfSets.remove(root2);
		return root1;
	}
	
	public String toString() {
		return listOfSets.toString();
	}
}
