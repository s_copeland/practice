package com.saiyd.graph.datastructures;

import com.saiyd.graph2.Vertex;

public class DisjointSetTreeVertex <T extends Comparable<T>> extends Vertex<T> {
	
	private DisjointSetTreeVertex<T> parent;
	/* 
	 * an upper bound on the height of this vertex
	 * (the number of edges in the longest path
	 * between x and a descendant leaf)
	 */
	private int rank; 
	
	public DisjointSetTreeVertex(int id, T data) {
		super(id, data, 0);
		parent = this; //I am my own root by default
	}

	public DisjointSetTreeVertex<T> getParent() {
		return parent;
	}

	public void setParent(DisjointSetTreeVertex<T> parent) {
		this.parent = parent;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}


}
