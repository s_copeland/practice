package com.saiyd.graph.datastructures;

import com.saiyd.graph2.Vertex;

public class DisjointSetVertex <T extends Comparable<T>> extends Vertex<T> {
	
	private SetElement<DisjointSetVertex<T>> setElement;
	
	public DisjointSetVertex(int id, T data) {
		super(id, data, 0);
		// TODO Auto-generated constructor stub
	}

	public SetElement<DisjointSetVertex<T>> getSetElement() {
		return setElement;
	}

	public void setSetElement(SetElement<DisjointSetVertex<T>> setElement) {
		this.setElement = setElement;
	}
}
