package com.saiyd.graph.datastructures;


class SetElement <T> {
	SetElement<T> next;
	SetElement<T> listRepresentative;
	T object;
	SetElementList<SetElement<T>> setElementList;
	
	SetElement(T object) {
		this.object = object;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		// if we are the 1st element in the list
		if(object.equals(listRepresentative.object))
			sb.append("{");
		
		sb.append(object.toString());
		if(next != null) {
			sb.append(", ");
			sb.append(next.toString());
		} else {
			sb.append("}");
		}
		
		return sb.toString();
	}
}