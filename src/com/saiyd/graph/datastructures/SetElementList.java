package com.saiyd.graph.datastructures;

class SetElementList <T> {
	T head;
	T tail;
	int size;
	
	SetElementList(T newHead) {
		head = newHead;
		tail = head;
		size = 1;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("{");
		sb.append(head.toString());
		sb.append("}");
		return sb.toString();
	}
	
	
}