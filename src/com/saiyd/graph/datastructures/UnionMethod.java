package com.saiyd.graph.datastructures;

enum UnionMethod { NAIVE, UNION_BY_RANK }