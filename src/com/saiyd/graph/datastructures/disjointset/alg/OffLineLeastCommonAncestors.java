package com.saiyd.graph.datastructures.disjointset.alg;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.saiyd.datastructures.LeftChildRightSiblingTree.Node;
import com.saiyd.datastructures.LeftChildRightSiblingTree.NodeColor;
import com.saiyd.graph.datastructures.DisjointSetOfForests;
import com.saiyd.graph.datastructures.DisjointSetTreeVertex;

/*
 * Tarjan's off-line least-common-ancestor algorithm
 * 
 * An algorithm for computing lowest common ancestors for pairs of nodes in a tree, 
 * based on the union-find data structure. The lowest common ancestor of two nodes 
 * d and e in a rooted tree T is the node g that is an ancestor of both d and e and 
 * that has the greatest depth in T. It is named after Robert Tarjan, who discovered 
 * the technique in 1979. Tarjan's algorithm is offline; that is, unlike other lowest 
 * common ancestor algorithms, it requires that all pairs of nodes for which the 
 * lowest common ancestor is desired must be specified in advance. The simplest 
 * version of the algorithm uses the union-find data structure, which unlike other 
 * lowest common ancestor data structures can take more than constant time per operation 
 * when the number of pairs of nodes is similar in magnitude to the number of nodes. 
 * A later refinement by Gabow & Tarjan (1983) speeds the algorithm up to linear time.
 * 
 * Applications
 * 
 * The problem of computing lowest common ancestors of classes in an inheritance 
 * hierarchy arises in the implementation of object-oriented programming systems 
 * (Aït-Kaci et al. 1989). The LCA problem also finds applications in models of 
 * complex systems found in distributed computing (Bender et al. 2005).
 */
public class OffLineLeastCommonAncestors {
	public static class NodePair {
		private Node<Integer> first;
		private Node<Integer> second;
		
		public NodePair(Node<Integer> first, Node<Integer> second) {
			this.first = first;
			this.second = second;
		}

		public Node<Integer> getFirst() {
			return first;
		}

		public Node<Integer> getSecond() {
			return second;
		}

	}
	
	private DisjointSetOfForests<Integer> djs = new DisjointSetOfForests<Integer>();
	private Map<DisjointSetTreeVertex<Integer>, Node<Integer>> ancestorMap = new HashMap<>();
	private Multimap<Integer, NodePair> pairMap = HashMultimap.create();
	private Map<Integer, DisjointSetTreeVertex<Integer>> nodeIdToVertexMap = new HashMap<>();
	
	public OffLineLeastCommonAncestors(List<NodePair> pairList) {
		for(NodePair pair : pairList) {
			pairMap.put(pair.first.getData(), pair);
			
		}
	}
	
	public DisjointSetTreeVertex<Integer> lca(Node<Integer> treeNode) {
		DisjointSetTreeVertex<Integer> djsVertex = new DisjointSetTreeVertex<Integer>(treeNode.getData(), treeNode.getData());
		djs.makeSet(djsVertex);
		ancestorMap.put(djs.find(djsVertex), treeNode);
		nodeIdToVertexMap.put(treeNode.getData(), djsVertex);
		
		Node child = treeNode.getLeftChild();
		while(child != null) {
			// recursive call
			DisjointSetTreeVertex<Integer> childVertex = lca(child);
			djs.union(djsVertex, childVertex);
			
			ancestorMap.put(djs.find(djsVertex), treeNode);
			//System.out.println(ancestorMap);
			child = child.getRightSibling();
		}
		
		treeNode.setColor(NodeColor.BLACK);
		/*
		 *  The lowest common ancestor of the pair {u,v} is available as Find(v).ancestor immediately (and only immediately) 
		 *  after u is colored black, provided v is already black. 
		 *  Otherwise, it will be available later as Find(u).ancestor, immediately after v is colored black.
		 */
		for(NodePair pair  : pairMap.get(treeNode.getData())) {
			if(pair.second.getColor() == NodeColor.BLACK) {
				Node<Integer> leastCommonAncestor = ancestorMap.get(djs.find(nodeIdToVertexMap.get(pair.second.getData())));
				System.out.println("The least common ancestor of " + treeNode.getData() + " and " + pair.second.getData() + " is " + leastCommonAncestor.getData());
			}
		}
		return djsVertex;
	}
	
	public static void main(String[] args) {
		// create the test tree
		
		Node<Integer> root = new Node<>();
		root.setData(0);
		
		Node<Integer> tierOneFirstChild = new Node<>();
		tierOneFirstChild.setData(1);
		tierOneFirstChild.setParent(root);
		root.setLeftChild(tierOneFirstChild);
		
		Node<Integer> tierTwoFirstChild = new Node<>();
		tierTwoFirstChild.setData(2);
		tierTwoFirstChild.setParent(tierOneFirstChild);
		tierOneFirstChild.setLeftChild(tierTwoFirstChild);
		Node<Integer> tierTwoSibling = new Node<>();
		tierTwoSibling.setData(3);
		tierTwoSibling.setParent(tierOneFirstChild);
		tierTwoFirstChild.setRightSibling(tierTwoSibling);
		
		Node<Integer> tierOneSibling = new Node<>();
		tierOneSibling.setData(4);
		tierOneSibling.setParent(root);
		tierOneFirstChild.setRightSibling(tierOneSibling);
		
		// on the right side now, instead of the left
		tierTwoFirstChild = new Node<>();
		tierTwoFirstChild.setData(5);
		tierTwoFirstChild.setParent(tierOneSibling);
		tierOneSibling.setLeftChild(tierTwoFirstChild);
		
		tierTwoSibling = new Node<>();
		tierTwoSibling.setData(6);
		tierTwoSibling.setParent(tierOneSibling);
		Node<Integer> nodeSix = tierTwoSibling;
		tierTwoFirstChild.setRightSibling(tierTwoSibling);
		
		Node<Integer> nextTierTwoSibling = new Node<>();
		nextTierTwoSibling.setData(7);
		nextTierTwoSibling.setParent(tierOneSibling);
		tierTwoSibling.setRightSibling(nextTierTwoSibling);
		
		Node<Integer> tierThreeFirstChild = new Node<>();
		tierThreeFirstChild.setData(8);
		tierThreeFirstChild.setParent(nextTierTwoSibling);
		nextTierTwoSibling.setLeftChild(tierThreeFirstChild);
		
		Node<Integer> tierThreeSibling = new Node<>();
		tierThreeSibling.setData(9);
		tierThreeSibling.setParent(nextTierTwoSibling);
		tierThreeFirstChild.setRightSibling(tierThreeSibling);
		Node<Integer> nodeNine = tierThreeSibling;
		
		
		
		List<NodePair> pairList = new ArrayList<NodePair>();
		pairList.add(new NodePair(nodeSix,nodeNine));
		pairList.add(new NodePair(nodeNine,nodeSix));
		
		
		OffLineLeastCommonAncestors lca = new OffLineLeastCommonAncestors(pairList);
		lca.lca(root);
	}
}
