package com.saiyd.graph.datastructures.disjointset.alg;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import com.saiyd.graph.datastructures.DisjointSet;
import com.saiyd.graph.datastructures.DisjointSetOfForests;
import com.saiyd.graph.datastructures.DisjointSetTreeVertex;
import com.saiyd.graph.datastructures.DisjointSetVertex;
import com.saiyd.sorting.InsertionSort;

/*
 * The off-line minimum problem asks us to maintain a dynamic set T of
 * elements from the domain {1,2,...,n} under the operations INSERT
 * and EXTRACT-MIN. We are given a sequence S of n INSERT and m EXTRACT-MIN
 * calls, where each key in {1,2,...,n} is inserted exactly once. We wish to
 * determine which key is returned by each EXTRACT-MIN call. Specifically, we
 * wish to fill in an array extracted[1..m], where for i = 1,2,...,m , extracted[i]
 * is the key returned by the iTh EXTRACT-MIN call. The problem is "off-line" in
 * the sense that we are allowed to process the entire sequence S before
 * determining any of the returned keys. 
 */
public class OffLineMinimum {
	private DisjointSetOfForests<Integer> djs;
	private Map<Integer, DisjointSetTreeVertex<Integer>> domainToObjectmap;

	/*
	 * Each insert is represented by a number and each extraction is
	 * represented by an E
	 * 
	 * 4 8 E 3 E 9 2 6 E E E 1 7 E 5 
	 * 
	 * We break the sequence into homogeneous subsequences, where each
	 * E represents a single extraction and each Ij represents a 
	 * (possibly empty) sequence of insert calls. For each subsequence
	 * Ij, we initially place the keys inserted by these operations into
	 * a set Kj, which is empty if Ij is empty.
	 * 
	 * I1 E I2 E I3 ...
	 * 
	 * 
	 */
	private static final int DOMAIN_MIN = 0;
	private static final int DOMAIN_MAX = 10;
	
	public static class Operation {
		private boolean extraction;
		private int insertValue;
		
		public Operation(int insertValue) {
			if(insertValue < DOMAIN_MIN || insertValue > DOMAIN_MAX)
				throw new IllegalArgumentException();
			this.insertValue = insertValue;
		}
		public Operation() {
			extraction = true;
		}
		public boolean isExtraction() {
			return extraction;
		}
		public int getInsertValue() {
			return insertValue;
		}
		
	}
	

	public List<Integer> process(List<Operation> inputSeq) {
		int insertCount = 0;
		int extractCount = 0;
		int homogeneousInsertSetId = 0;
		
		djs = new DisjointSetOfForests<Integer>();
		domainToObjectmap = new TreeMap<Integer, DisjointSetTreeVertex<Integer>>();
		DisjointSetTreeVertex<Integer> prevInsertOpSetRepresentative = null;

		
		for(Operation op : inputSeq) {
			if(!op.isExtraction()) {
				DisjointSetTreeVertex<Integer> insertOp = new DisjointSetTreeVertex<Integer>(op.getInsertValue(), homogeneousInsertSetId);
				djs.makeSet(insertOp);
				domainToObjectmap.put(op.getInsertValue(), insertOp);
				
				if(prevInsertOpSetRepresentative != null) {
					prevInsertOpSetRepresentative = djs.union(insertOp, prevInsertOpSetRepresentative);
				} else {
					prevInsertOpSetRepresentative = insertOp;
				}
				insertCount++;
			} else {
				if(prevInsertOpSetRepresentative == null) // make an empty set between consecutive extractions
					djs.makeSet(new DisjointSetTreeVertex<Integer>(-1, homogeneousInsertSetId)); // insert a new "empty set", we assume -1 is not in the domain
				prevInsertOpSetRepresentative = null; // start a new homogenous set of insertions after this extraction
				extractCount++;
				homogeneousInsertSetId++;
			}
		}
		
		
		int[] extracted = offLineMinimum(extractCount);
		ArrayList<Integer> result = new ArrayList<>();
		for(int i = 0; i < extracted.length; i++) {
			result.add(extracted[i]);
		}
		return result;		
	}
	
	private int[] offLineMinimum(int extractionCount) {
		int[] extracted = new int[extractionCount];
		Set<Integer> keys = domainToObjectmap.keySet();
		
		for(Integer i : keys) {
			DisjointSetTreeVertex<Integer> setRep = djs.find(domainToObjectmap.get(i));
			if(setRep != null && setRep.getData() != null) {
				int j = setRep.getData(); 
				if(j != extractionCount) {
					extracted[j] = i;
					
					/*
					 * Let L be the smallest value greater than j
					 * for which the set Kl exists
					 */
					DisjointSetTreeVertex<Integer> nextSetRep = djs.findSetFollowing(setRep);
					if(nextSetRep != null) {
						djs.unionSecondToFirst(nextSetRep, setRep);
					}
				}
			}
		}
		return extracted;
	}
	
	public static void main(String[] args) {
		OffLineMinimum offMin = new OffLineMinimum();
		// 4 8 E 3 E 9 2 6 E E E 1 7 E 5 
		List<Operation> list = new ArrayList<Operation>();
		list.add(new Operation(4));
		list.add(new Operation(8));
		list.add(new Operation()); // the default constructor is for the extract operation
		list.add(new Operation(3));
		list.add(new Operation());
		list.add(new Operation(9));
		list.add(new Operation(2));
		list.add(new Operation(6));
		list.add(new Operation());
		list.add(new Operation());
		list.add(new Operation());
		list.add(new Operation(1));
		list.add(new Operation(7));
		list.add(new Operation());
		list.add(new Operation(5));
		
		List<Integer> result = offMin.process(list);
		System.out.println(result);
		
		// check the result
		List<Integer> correctAnswer = new ArrayList<Integer>();
		correctAnswer.add(4);
		correctAnswer.add(3);
		correctAnswer.add(2);
		correctAnswer.add(6);
		correctAnswer.add(8);
		correctAnswer.add(1);
		
		System.out.println("The result is correct = " + correctAnswer.containsAll(result));
	}
}
