package com.saiyd.graph2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.saiyd.graph.datastructures.DisjointSetVertex;


public class AdjacencyList  <T extends Comparable<T>, E extends Vertex<T>> {
	private Map<E, List<E>> vertexMap = new HashMap<E, List<E>>();
	
	public Set<E> getVertices() {
		return vertexMap.keySet();
	}	
	
	public E getVertex(int id) {
		Set<E> keys = vertexMap.keySet();
		E retVal = null;
		for(E vertex : keys) {
			if(vertex.getId() == id) {
				retVal = vertex;
				break;
			}
		}
		return retVal;
	}
	

	
	public void addVertex(E vertex) {
		List<E> adjacencyVertexList = vertexMap.get(vertex);
		// if it doesn't exist add an empty list
		if(adjacencyVertexList == null) {
			adjacencyVertexList = new ArrayList<E>();
			vertexMap.put(vertex, adjacencyVertexList);
		}
			
	}
	
	public void removeVertex(E vertex) {
		vertexMap.remove(vertex);
	}
	
	public void addEdge(E srcVertex, E dstVertex) {
		List<E> adjacentVertexList = vertexMap.get(srcVertex);
		if(adjacentVertexList == null) {
			adjacentVertexList = new ArrayList<E>();
		}
		
		adjacentVertexList.add(dstVertex);
		
		vertexMap.put(srcVertex, adjacentVertexList);
	}
	
	public boolean removeEdge(E srcVertex, E dstVertex) {
		List<E> adjacentVertexList = vertexMap.get(srcVertex);
		if(adjacentVertexList != null) {
			return adjacentVertexList.remove(dstVertex);
		}
		return false;
	}
	
	public List<E> getList(E vertex) {
		return vertexMap.get(vertex);
	}
	

	public void setAllVertexState(VertexState state) {
		Set<E> keys = vertexMap.keySet();
		for(E vertex : keys) {
			vertex.setState(state);
		}
	}
}
