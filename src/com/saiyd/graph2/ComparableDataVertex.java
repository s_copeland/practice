package com.saiyd.graph2;

import com.saiyd.graph2.Vertex;

public class ComparableDataVertex <T extends Comparable<T>, V extends Vertex<T>> implements Comparable<ComparableDataVertex<T,V>>{
	private T data;
	private V vertex;
	public ComparableDataVertex(T data, V  vertex) {
		this.data = data;
		this.vertex = vertex;
	}

	public V getVertex() {
		return vertex;
	}

	@Override
	public int compareTo(ComparableDataVertex<T,V> other) {
		return this.data.compareTo(other.data);
	}

}
