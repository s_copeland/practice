package com.saiyd.graph2;

public class Edge <T extends Comparable<T>> implements Comparable<Edge<T>> {
	private Vertex<T> srcVertex;
	private Vertex<T> dstVertex;
	private int weight;
	private int capacity;
	private int flow;
	private int cost;
	
	public Edge(Vertex<T> srcVertex, Vertex<T> dstVertex, int weight) {
		super();
		this.srcVertex = srcVertex;
		this.dstVertex = dstVertex;
		this.weight = weight;
		// link the vertex to the edge, so it knows its incident edges
		this.srcVertex.addEdge(this); 
		this.dstVertex.addEdge(this);
	}
	public Edge(Vertex<T> srcVertex, Vertex<T> dstVertex, int weight, int capacity, int flow, int cost) {
		super();
		this.srcVertex = srcVertex;
		this.dstVertex = dstVertex;
		this.weight = weight;
		this.capacity = capacity;
		this.flow = flow;
		this.cost = cost;
		// link the vertex to the edge, so it knows its incident edges
		this.srcVertex.addEdge(this); 
		this.dstVertex.addEdge(this);
	}
	public int getWeight() {
		return weight;
	}
	public void setWeight(int weight) {
		this.weight = weight;
	}
	public Vertex<T> getSrcVertex() {
		return srcVertex;
	}
	public void setSrcVertex(Vertex<T> srcVertex) {
		this.srcVertex = srcVertex;
	}
	public Vertex<T> getDstVertex() {
		return dstVertex;
	}
	public void setDstVertex(Vertex<T> dstVertex) {
		this.dstVertex = dstVertex;
	}
	
	public Vertex<T> getOtherEnd(Vertex<T> vertex) {
		if(srcVertex.equals(vertex)) {
			return dstVertex;
		} else if(dstVertex.equals(vertex)) {
			return srcVertex;
		} else {
			throw new IllegalArgumentException("The vertex is not an endpoint of this edge");
		}
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((dstVertex == null) ? 0 : dstVertex.hashCode());
		result = prime * result
				+ ((srcVertex == null) ? 0 : srcVertex.hashCode());
		result = prime * result + weight;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;

		if (!(obj instanceof Edge))
			return false;
		
		Edge other = (Edge) obj;
		if (dstVertex == null) {
			if (other.dstVertex != null)
				return false;
		} else if (!dstVertex.equals(other.dstVertex))
			return false;
		if (srcVertex == null) {
			if (other.srcVertex != null)
				return false;
		} else if (!srcVertex.equals(other.srcVertex))
			return false;
		if (weight != other.weight)
			return false;
		return true;
	}
	@Override
	public int compareTo(Edge<T> other) {
		if(this == other)
			return 0;
		
		if(weight > other.weight)
			return 1;
		if(weight < other.weight)
			return -1;
		return 0;
	}

	@Override
	public String toString() {
		return "Edge [" + srcVertex + " -> " + dstVertex
				+ ", weight=" + weight + ", capacity=" + capacity + ", flow="
				+ flow + ", cost=" + cost + "]\n";
	}
	public int getFlow() {
		return flow;
	}
	public void setFlow(int flow) {
		this.flow = flow;
	}
	public int getCost() {
		return cost;
	}
	public int getCapacity() {
		return capacity;
	}
	public int residualCapacityTo(Vertex<T> vertex) {
		if(srcVertex.equals(vertex)) {
			return flow; // backward edge
		} else if(dstVertex.equals(vertex)) {
			return capacity - flow ; // forward edge
		} else {
			throw new IllegalArgumentException("The vertex is not an endpoint of this edge");
		}
	}
	/*public int totalCostTo(Vertex<T> vertex) {
		if(srcVertex.equals(vertex)) {
			return vertex.getDistance() - cost; // backward edge
		} else if(dstVertex.equals(vertex)) {
			return vertex.getDistance() + cost; // forward edge
		} else {
			throw new IllegalArgumentException("The vertex is not an endpoint of this edge");
		}		
	}*/
    /**
     * Increases the flow on the edge in the direction to the given vertex.
     *   If <tt>vertex</tt> is the tail vertex, this increases the flow on the edge by <tt>delta</tt>;
     *   if <tt>vertex</tt> is the head vertex, this decreases the flow on the edge by <tt>delta</tt>.
     * @param vertex one endpoint of the edge
     * @throws java.lang.IllegalArgumentException if <tt>vertex</tt> is not one of the endpoints
     *   of the edge
     * @throws java.lang.IllegalArgumentException if <tt>delta</tt> makes the flow on
     *   on the edge either negative or larger than its capacity
     * @throws java.lang.IllegalArgumentException if <tt>delta</tt> is <tt>NaN</tt>
     */
    public void addResidualFlowTo(Vertex<T> vertex, double delta) { // add flow in the direction of vertex
    	if(srcVertex.equals(vertex)) {
    		flow -= delta; // backward edge, because its coming toward me
		} else if(dstVertex.equals(vertex)) {
			flow += delta; // forward edge
		} else {
			throw new IllegalArgumentException("The vertex is not an endpoint of this edge");
		}
    	
        if (!(flow >= 0))
        	throw new IllegalArgumentException("Flow is negative");
        if (!(flow <= capacity)) 
        	throw new IllegalArgumentException("Flow exceeds capacity");
    }	
}
