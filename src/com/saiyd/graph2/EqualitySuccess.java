package com.saiyd.graph2;

public class EqualitySuccess<T extends Comparable<T>> implements ISearchSuccess<T> {

	private T mSearchValue;
	
	public EqualitySuccess(T value) {
		mSearchValue = value;
	}
	
	@Override
	public boolean isFound(Vertex<T> vertex) {
		if(vertex ==  null)
			return false;
		
		return (vertex.getData() == mSearchValue);
	}

}
