package com.saiyd.graph2;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.saiyd.graph.datastructures.DisjointSetTreeVertex;
import com.saiyd.graph.datastructures.DisjointSetVertex;
import com.saiyd.graph2.alg.BreadthFirstSearch;
import com.saiyd.graph2.alg.PathBasedStronglyConnectedComponent;
import com.saiyd.graph2.alg.StronglyConnectedComponents;
import com.saiyd.graph2.alg.TarjanStronglyConnectedComponent;
import com.saiyd.graph2.alg.TopologicalSort;

public class Graph <T extends Comparable<T>, E extends Vertex<T>> {
	public enum Type {DIRECTED, UNDIRECTED };
	public enum EdgeDirection { NORMAL, TRANSPOSE };
	
	private static final String RING_GRAPH_JSON = "ring-graph.json";
	private AdjacencyList<T, E> adjacencyList = new AdjacencyList<T, E>();
	private List<Edge<T>> edgeList = new ArrayList<Edge<T>>();
	private Type type;
	private int srcVertexId;
	private int sinkVertexId;
	
	private static class VertexPair {
		private int srcId;
		private int dstId;
		
		public VertexPair(int src, int dst) {
			srcId = src;
			dstId = dst;
		}
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + dstId;
			result = prime * result + srcId;
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			VertexPair other = (VertexPair) obj;
			if (dstId != other.dstId)
				return false;
			if (srcId != other.srcId)
				return false;
			return true;
		}
	}
	
	private Map<VertexPair,Edge<T>> pairToEdgeMap = new HashMap<>();
	
	private Graph(Type type){
		this.type = type;
	}
	
	public int getSrcVertexId() {
		return srcVertexId;
	}

	public int getSinkVertexId() {
		return sinkVertexId;
	}

	public Type getType() {
		return type;
	}

	public List<Edge<T>> getEdgeList() {
		return edgeList;
	}

	public void removeEdge(Edge<T> edge) {
		edgeList.remove(edge);
	}
	
	public Edge<T> find(int srcId, int dstId) {
		return pairToEdgeMap.get(new VertexPair(srcId, dstId));
	}
	
	public void addEdge(Edge<T> edge) {
		edgeList.add(edge);
		pairToEdgeMap.put(new VertexPair(edge.getSrcVertex().getId(), edge.getDstVertex().getId()), edge);
	}
	
	public void removeVertex(E vertex) {
		// remove any associated edges
		// TODO - find a better way to find an edge
		List<Edge<T>> edgesToRemove = new ArrayList<Edge<T>>();
		
		for(Edge<T> e : edgeList) {
			if(e.getSrcVertex() == vertex || e.getDstVertex() == vertex) {
				edgesToRemove.add(e);
			}
		}
		edgeList.removeAll(edgesToRemove);

		adjacencyList.removeVertex(vertex);
	}
	
	public Set<E> getVertices() {
		return new HashSet<E>(adjacencyList.getVertices());
	}
	
	public E getVertex(int id) {
		return adjacencyList.getVertex(id);
	}
	
	public void addVertex(E vertex) {
		adjacencyList.addVertex(vertex);
	}
	
	public void setSourceVertex(E vertex) {
		this.srcVertexId = vertex.getId();
	}
	
	public void setSinkVertex(E vertex) {
		this.sinkVertexId = vertex.getId();
	}
	
	public List<E> getAdjacencyList(E vertex) {
		// TODO - check for null and return an empty list if null
		return adjacencyList.getList(vertex);
	}
	
	public AdjacencyList<T, E> getAdjacencyList() {
		return adjacencyList; // TODO you should make a defensive copy here... maybe
	}
	
	public void setResetAllVertexState() {
		adjacencyList.setAllVertexState(VertexState.NOT_EXPLORED);
	}
	
	public static Graph<Integer, Vertex<Integer>> newInstance(int[][] weightedAdjacencyMatrix, int rows, Type graphType) {
		Graph<Integer, Vertex<Integer>> retVal = null;
		
		if(weightedAdjacencyMatrix != null) {
			retVal = new Graph<Integer, Vertex<Integer>>(graphType);
			// add all the vertices
			for(int i = 0; i < rows; i ++) {
				Vertex<Integer> curVertex = new Vertex<Integer>(i, i, 0);
				curVertex.setLabel(String.valueOf(i));
				retVal.adjacencyList.addVertex(curVertex);
			}
			
			/* Add all the edges
			 * 
			 */
			Vertex<Integer> src = null;
			Vertex<Integer> dst = null;
			Edge<Integer> edge = null;
			for(int i = 0; i < rows; i++) {
				for(int j = 0; j < rows; j++) {
					// we don't want to add edges that are loops, so we enforce i != j
					if(weightedAdjacencyMatrix[i][j] != Integer.MAX_VALUE && i != j) {
						src = retVal.adjacencyList.getVertex(i);
						dst = retVal.adjacencyList.getVertex(j);
						retVal.adjacencyList.addEdge(src, dst);
						
						edge = new Edge<Integer>(src, dst, weightedAdjacencyMatrix[i][j]);
						src.addEdge(edge); // link the vertex to the edge, so it knows its incident edges
						dst.addEdge(edge);
						retVal.edgeList.add(edge);
						retVal.pairToEdgeMap.put(new VertexPair(src.id, dst.id), edge);
					}
				}
			}
		}
		return retVal;
	}

	public static Graph<String, Vertex<String>> newInstance(String json, Type graphType, EdgeDirection edgeDirection) {
		Graph<String, Vertex<String>> retVal = new Graph<String, Vertex<String>>(graphType);
		
		Gson gson = new Gson();
		JsonGraph jsonGraph = gson.fromJson(json, JsonGraph.class);
		
		Map<Integer,Vertex<String>> idToVertexMap = new HashMap<Integer, Vertex<String>>();
		// populate the graph
		
		// create all the vertices
		for(JsonVertex jsonVertex : jsonGraph.getVertices()) {
			// create a new vertex with the data
			Vertex<String> curVertex = new Vertex<String>(jsonVertex.getId(), jsonVertex.getData(), jsonVertex.getWeight(), jsonVertex.isSource(), jsonVertex.isSink());
			
			// the last sink and source will be the final one
			if(jsonVertex.isSink())
				retVal.sinkVertexId = jsonVertex.getId();
			if(jsonVertex.isSource())
				retVal.srcVertexId = jsonVertex.getId();
			
			if(jsonVertex.getLabel() == null) {
				curVertex.setLabel(String.valueOf(jsonVertex.getData()));
			} else {
				curVertex.setLabel(jsonVertex.getLabel());
			}
			
			curVertex.setPartition(jsonVertex.getPartition());
			
			retVal.adjacencyList.addVertex(curVertex);
			
			idToVertexMap.put(jsonVertex.getId(), curVertex);
		}
		
		for(JsonEdge jsonEdge : jsonGraph.getEdges()) {
			Vertex<String> srcVertex = null;
			Vertex<String> dstVertex = null;
			Edge<String> edge = null;
			
			if(edgeDirection == EdgeDirection.TRANSPOSE) {
				// reverse the edge direction
				dstVertex = idToVertexMap.get(jsonEdge.getSrcVertexId());
				srcVertex = idToVertexMap.get(jsonEdge.getDstVertexId());
			} else {
				srcVertex = idToVertexMap.get(jsonEdge.getSrcVertexId());
				dstVertex = idToVertexMap.get(jsonEdge.getDstVertexId());
			}
			
			edge = new Edge<String>(srcVertex, dstVertex, jsonEdge.getWeight(), jsonEdge.getCapacity(), jsonEdge.getFlow(), jsonEdge.getCost());
			retVal.edgeList.add(edge);
			retVal.pairToEdgeMap.put(new VertexPair(srcVertex.id, dstVertex.id), edge);
			
			retVal.adjacencyList.addEdge(srcVertex, dstVertex);
			
			if(graphType == Type.UNDIRECTED) {
				retVal.adjacencyList.addEdge(dstVertex, srcVertex);
				
				edge = new Edge<String>(dstVertex, srcVertex, jsonEdge.getWeight(), jsonEdge.getCapacity(), jsonEdge.getFlow(), jsonEdge.getCost());
				retVal.edgeList.add(edge);
				retVal.pairToEdgeMap.put(new VertexPair(dstVertex.id, srcVertex.id), edge);
			}
		}
		
		return retVal;
	}
	
	public static Graph<String, DisjointSetVertex<String>> newDSJInstance(String json, Type graphType, EdgeDirection edgeDirection) {
		Graph<String, DisjointSetVertex<String>> retVal = new Graph<String, DisjointSetVertex<String>>(graphType);
		
		Gson gson = new Gson();
		JsonGraph jsonGraph = gson.fromJson(json, JsonGraph.class);
		
		Map<Integer,DisjointSetVertex<String>> idToVertexMap = new HashMap<Integer, DisjointSetVertex<String>>();
		// populate the graph
		
		// create all the vertices
		for(JsonVertex jsonVertex : jsonGraph.getVertices()) {
			// create a new vertex with the data
			DisjointSetVertex<String> curVertex = new DisjointSetVertex<String>(jsonVertex.getId(), jsonVertex.getData());
			retVal.adjacencyList.addVertex(curVertex);
			
			idToVertexMap.put(jsonVertex.getId(), curVertex);
		}
		
		for(JsonEdge jsonEdge : jsonGraph.getEdges()) {
			DisjointSetVertex<String> srcVertex = null;
			DisjointSetVertex<String> dstVertex = null;
			Edge<String> edge = null;
			
			if(edgeDirection == EdgeDirection.TRANSPOSE) {
				// reverse the edge direction
				dstVertex = idToVertexMap.get(jsonEdge.getSrcVertexId());
				srcVertex = idToVertexMap.get(jsonEdge.getDstVertexId());
			} else {
				srcVertex = idToVertexMap.get(jsonEdge.getSrcVertexId());
				dstVertex = idToVertexMap.get(jsonEdge.getDstVertexId());
			}
			
			edge = new Edge<String>(srcVertex, dstVertex, jsonEdge.getWeight());
			retVal.edgeList.add(edge);
			retVal.pairToEdgeMap.put(new VertexPair(srcVertex.id, dstVertex.id), edge);
			
			retVal.adjacencyList.addEdge(srcVertex, dstVertex);
			if(graphType == Type.UNDIRECTED) {
				retVal.adjacencyList.addEdge(dstVertex, srcVertex);
				
				edge = new Edge<String>(dstVertex, srcVertex, jsonEdge.getWeight());
				retVal.edgeList.add(edge);
				retVal.pairToEdgeMap.put(new VertexPair(dstVertex.id, srcVertex.id), edge);
			}
		}
		
		return retVal;
	}
	
	public static Graph<String, DisjointSetTreeVertex<String>> newDSTJInstance(String json, Type graphType, EdgeDirection edgeDirection) {
		Graph<String, DisjointSetTreeVertex<String>> retVal = new Graph<String, DisjointSetTreeVertex<String>>(graphType);
		
		Gson gson = new Gson();
		JsonGraph jsonGraph = gson.fromJson(json, JsonGraph.class);
		
		Map<Integer,DisjointSetTreeVertex<String>> idToVertexMap = new HashMap<Integer, DisjointSetTreeVertex<String>>();
		// populate the graph
		
		// create all the vertices
		for(JsonVertex jsonVertex : jsonGraph.getVertices()) {
			// create a new vertex with the data
			DisjointSetTreeVertex<String> curVertex = new DisjointSetTreeVertex<String>(jsonVertex.getId(), jsonVertex.getData());
			retVal.adjacencyList.addVertex(curVertex);
			
			idToVertexMap.put(jsonVertex.getId(), curVertex);
		}
		
		for(JsonEdge jsonEdge : jsonGraph.getEdges()) {
			DisjointSetTreeVertex<String> srcVertex = null;
			DisjointSetTreeVertex<String> dstVertex = null;
			Edge<String> edge = null;
			
			if(edgeDirection == EdgeDirection.TRANSPOSE) {
				// reverse the edge direction
				dstVertex = idToVertexMap.get(jsonEdge.getSrcVertexId());
				srcVertex = idToVertexMap.get(jsonEdge.getDstVertexId());
			} else {
				srcVertex = idToVertexMap.get(jsonEdge.getSrcVertexId());
				dstVertex = idToVertexMap.get(jsonEdge.getDstVertexId());
			}
			
			edge = new Edge<String>(srcVertex, dstVertex, jsonEdge.getWeight());
			retVal.edgeList.add(edge);
			
			retVal.adjacencyList.addEdge(srcVertex, dstVertex);
			if(graphType == Type.UNDIRECTED) {
				retVal.adjacencyList.addEdge(dstVertex, srcVertex);
				
				edge = new Edge<String>(dstVertex, srcVertex, jsonEdge.getWeight());
				retVal.edgeList.add(edge);
			}
		}
		
		return retVal;
	}
	
	public static void main(String[] args) {
		//createRingGraph();
		
		/*Graph<String> graph = loadGraph("graph-1.json", Type.UNDIRECTED, EdgeDirection.NORMAL);
		graph.toString();
		BreadthFirstSearch<String> bfs = new BreadthFirstSearch<String>();
		ISearchSuccess<String> searchSuccess = new EqualitySuccess<String>("Hello");
		bfs.coloredSearch(graph, graph.getVertex(2), searchSuccess);
		bfs.printBFSPathFrom(graph.getVertex(2), graph.getVertex(0));
		
		Graph<String> topologicalGraph = loadGraph("topological-sort-graph.json", Type.DIRECTED, EdgeDirection.NORMAL);
		TopologicalSort<String> topSort = new TopologicalSort<String>();
		List<Vertex<String>> sortedList = topSort.sort(topologicalGraph);
		System.out.println(sortedList);
		*/
		
		StronglyConnectedComponents<String> scc = new StronglyConnectedComponents<String>();
		Graph<String, Vertex<String>> sccGraph = loadGraph("strongly-connected-components-graph.json", Type.DIRECTED, EdgeDirection.NORMAL);
		Graph<String, Vertex<String>> sccGraphTransposed = loadGraph("strongly-connected-components-graph.json", Type.DIRECTED, EdgeDirection.TRANSPOSE);
		List<Set<Vertex<String>>>sccList = scc.findStronglyConnectedComponents(sccGraph, sccGraphTransposed);
		System.out.println(sccList);
		
		
		TarjanStronglyConnectedComponent<String> tscc = new TarjanStronglyConnectedComponent<String>();
		Graph<String, Vertex<String>> tsccGraph = loadGraph("strongly-connected-components-graph.json", Type.DIRECTED, EdgeDirection.NORMAL);
		List<Set<Vertex<String>>> tsccList = tscc.findStronglyConnectedComponents(tsccGraph);
		System.out.println(tsccList);
		
		PathBasedStronglyConnectedComponent<String> pbscc = new PathBasedStronglyConnectedComponent<String>();
		Graph<String,Vertex<String>> pbsccGraph = loadGraph("strongly-connected-components-graph.json", Type.DIRECTED, EdgeDirection.NORMAL);
		List<Set<Vertex<String>>> pbsccList = pbscc.findStronglyConnectedComponents(pbsccGraph);
		System.out.println(pbsccList);
	}
	
	static String readFile(String path, Charset encoding) 
			  throws IOException 
	{
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return encoding.decode(ByteBuffer.wrap(encoded)).toString();
	}
	
	public static Graph<String, Vertex<String>> loadGraph(String graphFileName, Type graphType, EdgeDirection edgeDirection) {
		Graph<String, Vertex<String>> retGraph = new Graph<String, Vertex<String>>(graphType);
		
		try {
			String fileJson = readFile(graphFileName, Charset.defaultCharset());
			retGraph = newInstance(fileJson, graphType, edgeDirection);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return retGraph;
	}
	
	public static Graph<String, DisjointSetVertex<String>> loadDSVGraph(String graphFileName, Type graphType, EdgeDirection edgeDirection) {
		Graph<String, DisjointSetVertex<String>> retGraph = new Graph<String, DisjointSetVertex<String>>(graphType);
		
		try {
			String fileJson = readFile(graphFileName, Charset.defaultCharset());
			retGraph = newDSJInstance(fileJson, graphType, edgeDirection);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return retGraph;
	}
	
	public static Graph<String, DisjointSetTreeVertex<String>> loadDSTVGraph(String graphFileName, Type graphType, EdgeDirection edgeDirection) {
		Graph<String, DisjointSetTreeVertex<String>> retGraph = new Graph<String, DisjointSetTreeVertex<String>>(graphType);
		
		try {
			String fileJson = readFile(graphFileName, Charset.defaultCharset());
			retGraph = newDSTJInstance(fileJson, graphType, edgeDirection);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return retGraph;
	}

	private static void createRingGraph() {
		JsonGraph jsonGraph = new JsonGraph();
		
		List<JsonVertex> vertices = new ArrayList<JsonVertex>();
		for(int i = 0; i < 4; i++) {
			JsonVertex curVertex = new JsonVertex();
			curVertex.setId(i);
			curVertex.setData(Integer.toString(i));
			vertices.add(curVertex);
		}
		
		jsonGraph.setVertices(vertices);
		
		List<JsonEdge> edges = new ArrayList<JsonEdge>();
		// connect the vertices in a cycle
		for(int i = 0; i < 4; i++) {
			int j = (i + 1) % 4;
			
			JsonEdge curEdge = new JsonEdge();
			curEdge.setSrcVertexId(i);
			curEdge.setDstVertexId(j);
			
			edges.add(curEdge);
		}
		
		jsonGraph.setEdges(edges);
		
		GsonBuilder gb = new GsonBuilder();
		Gson gson = gb.setPrettyPrinting().create();
		
		String json = gson.toJson(jsonGraph);
		try {
			PrintWriter pw = new PrintWriter(RING_GRAPH_JSON);
			pw.write(json);
			pw.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
