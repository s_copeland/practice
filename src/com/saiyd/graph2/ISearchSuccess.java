package com.saiyd.graph2;

public interface ISearchSuccess<T extends Comparable<T>> {
	public boolean isFound(Vertex<T> vertex);
}
