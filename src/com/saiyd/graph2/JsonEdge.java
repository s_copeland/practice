package com.saiyd.graph2;

public class JsonEdge {
	private int srcVertexId;
	private int dstVertexId;
	private int weight;
	private int capacity;
	private int flow;
	private int cost;
	
	public int getSrcVertexId() {
		return srcVertexId;
	}
	public void setSrcVertexId(int srcVertexId) {
		this.srcVertexId = srcVertexId;
	}
	public int getDstVertexId() {
		return dstVertexId;
	}
	public void setDstVertexId(int dstVertexId) {
		this.dstVertexId = dstVertexId;
	}
	public int getWeight() {
		return weight;
	}
	public void setWeight(int weight) {
		this.weight = weight;
	}
	public int getCapacity() {
		return capacity;
	}
	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}
	public int getFlow() {
		return flow;
	}
	public void setFlow(int flow) {
		this.flow = flow;
	}
	public int getCost() {
		return cost;
	}
	public void setCost(int cost) {
		this.cost = cost;
	}
}
