package com.saiyd.graph2;

import java.util.ArrayList;
import java.util.List;

public class JsonGraph {
	private List<JsonVertex> vertices = new ArrayList<JsonVertex>();
	private List<JsonEdge> edges = new ArrayList<JsonEdge>();
	public List<JsonVertex> getVertices() {
		return vertices;
	}
	public void setVertices(List<JsonVertex> vertices) {
		this.vertices = vertices;
	}
	public List<JsonEdge> getEdges() {
		return edges;
	}
	public void setEdges(List<JsonEdge> edges) {
		this.edges = edges;
	}
}
