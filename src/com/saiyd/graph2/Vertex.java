package com.saiyd.graph2;

import java.util.LinkedList;
import java.util.List;

public class Vertex<T extends Comparable<T>> implements Comparable<Vertex<T>> {
	protected int id;
	protected String label;
	protected T mData;
	private VertexState mState;
	private final int weight;
	private List<Edge<T>> forwardEdges = new LinkedList<Edge<T>>();
	private List<Edge<T>> backwardEdges = new LinkedList<Edge<T>>();
	private Object linkingObj; // this is a hack for binomial trees and...
	private boolean source;
	private boolean sink;
	private String partition;
	private int distance;
	private static boolean useDistanceCompare;
	
	public Vertex(int id, T data, int weight)
	{
		this.id = id;
		mData = data;
		mState = VertexState.NOT_EXPLORED;
		this.weight = weight;
	}
	public Vertex(int id, T data, int weight, boolean source, boolean sink)
	{
		this.id = id;
		mData = data;
		mState = VertexState.NOT_EXPLORED;
		this.weight = weight;
		this.source = source;
		this.sink = sink;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public T getData() {
		return mData;
	}
	public void setData(T data) {
		this.mData = data;
	}

	public VertexState getState() {
		return mState;
	}
	public void setState(VertexState mState) {
		this.mState = mState;
	}
	public List<Edge<T>> getEdges() {
		LinkedList<Edge<T>> ret = new LinkedList<Edge<T>>(forwardEdges);
		ret.addAll(backwardEdges);
		return ret;
	}
	public List<Edge<T>> getForwardEdges() {
		return new LinkedList<Edge<T>>(forwardEdges);
	}
	public List<Edge<T>> getBackwardEdges() {
		return new LinkedList<Edge<T>>(backwardEdges);
	}
	public void addEdge(Edge<T> e) {
		if(e.getSrcVertex().equals(this)) {
			forwardEdges.add(e);
		} else if(e.getDstVertex().equals(this)) {
			backwardEdges.add(e);
		} else {
			throw new IllegalArgumentException("This vertex is neither a source or destination of this edge. It should not be added.");
		}
	}
	public int getWeight() {
		return weight;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public boolean equals(Object other) {
		// test for reflexive
		if(this == other)
			return true;
		
		// respect Liskov substitution principle
		if(!(other instanceof Vertex))
			return false;
		
		// cast and compare fields
		Vertex otherVertex = (Vertex) other;
		if(this.id == otherVertex.id)
			return true;
		return false;
	}
	
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}
	/*@Override
	public String toString() {
		return "Vertex [id=" + id + ", mData=" + mData + ", mState=" + mState
				+ "]";
	}*/
	@Override
	public String toString() {
		return "Vertex [" + label + " = "+ mData + "]";
	}
	@Override
	public int compareTo(Vertex<T> o) {
		if(useDistanceCompare) {
			return this.distance - o.distance;
		}
		return this.mData.compareTo(o.mData);
	}
	public Object getLinkingObj() {
		return linkingObj;
	}
	public void setLinkingObj(Object linkingObj) {
		this.linkingObj = linkingObj;
	}
	public boolean isSource() {
		return source;
	}
	public boolean isSink() {
		return sink;
	}
	public String getPartition() {
		return partition;
	}
	public void setPartition(String partition) {
		this.partition = partition;
	}
	public int getDistance() {
		return distance;
	}
	public void setDistance(int distance) {
		this.distance = distance;
	}
	public boolean isUseDistanceCompare() {
		return useDistanceCompare;
	}
	public static void setUseDistanceCompare(boolean useDistanceCompare) {
		useDistanceCompare = useDistanceCompare;
	}
}
