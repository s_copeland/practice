package com.saiyd.graph2;

public enum VertexState {
	NOT_EXPLORED, PARTIAL_EXPLORED, FULL_EXPLORED;
}
