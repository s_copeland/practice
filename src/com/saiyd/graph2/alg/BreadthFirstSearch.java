package com.saiyd.graph2.alg;

import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import com.saiyd.graph2.ISearchSuccess;
import com.saiyd.graph2.Graph;
import com.saiyd.graph2.Vertex;
import com.saiyd.graph2.VertexState;

/*
 * Explores the edges of the graph to "discover"
 * every vertex that is reachable from the starting
 * vertex.
 * 
 * it produces a breadth-first tree with root source
 * that contains all reachable vertices
 * 
 * For any vertex v reachable from the source, the
 * path in the breadth-first tree from the source to v
 * corresponds to a "shortest path" from source to v.
 * 
 * IT DOES NOT FIND THE SHORTEST PATH ON WEIGHTED GRAPHS
 * UNLESS ALL THE WEIGHTS ARE THE SAME VALUE FOR EACH EDGE
 * 
 * This algorithm works for directed and undirected graphs
 * 
 * 
 * This algorithm discovers all verties at distance k from
 * the source before discovering any vertices at distance
 * k + 1
 * 
 * The BFS procedure builds a breath-first tree as it searches the graph. Also 
 * known as a predecessor subgraph.
 * 
 * Running Time
 * 		After initialization, no vertex is ever whitened, and 
 * 		thus the test "if(curAdjacentVertex.getState() == VertexState.NOT_EXPLORED)"
 * 		ensures that each vertex is enqueued and dequeued at most once.
 * 		The operation of enqueuing and dequeuing is O(1), so the time
 * 		dedicated to queuing operations is O(V).
 * 
 * 		Because the adjacency list of each vertex is scanned only
 * 		when the vertex is dequeued, each adjacency list is scanned at most once.
 * 		The sum of the lengths of all adjacency list is bigTheta(E), the total
 * 		time spent scanning adjacency lists is O(E).
 * 
 * 		The overhead for initialization is O(V), so the total running time of
 * 		BFS is O(V + E).
 * 
 * 		BFS runs in time linear in the size of the adjacency list representation of
 * 		G
 */
public class BreadthFirstSearch <T extends Comparable<T>> {
	private Map<Vertex<T>, Integer> distanceFromStartMap = new HashMap<Vertex<T>, Integer>();
	private Map<Vertex<T>, Vertex<T>> bfsParentMap = new HashMap<Vertex<T>, Vertex<T>>();
	/*
	 * 
	 */
	public void coloredSearch(Graph<T,Vertex<T>> g, Vertex<T> startVertex, ISearchSuccess<T> searchSuccess) {
		distanceFromStartMap = new HashMap<Vertex<T>, Integer>();
		
		// ensure the vertex state are all set to NOT_EXPLORED
		g.setResetAllVertexState();
		
		// color the start vertex partially explored
		startVertex.setState(VertexState.PARTIAL_EXPLORED);
		//System.out.println(startVertex);
		// record the distance of the start vertex from itself
		distanceFromStartMap.put(startVertex, 0);
		
		Queue<Vertex<T>> queue = new ArrayDeque<Vertex<T>>();
		
		// add the start vertex
		queue.add(startVertex);
		
		Vertex<T> curVertex = null;
		while(!queue.isEmpty()) {
			curVertex = queue.remove();
			
			
			// check for success
			if(searchSuccess.isFound(curVertex))
				break;
			
			// retrieve the adjacency list for this vertex
			List<Vertex<T>> adjList = g.getAdjacencyList(curVertex);
			
			for(Vertex<T> curAdjacentVertex : adjList) {
				if(curAdjacentVertex.getState() == VertexState.NOT_EXPLORED) {
					curAdjacentVertex.setState(VertexState.PARTIAL_EXPLORED);
					//System.out.println(curAdjacentVertex);
					
					// record our distance from the start vertex
					int parentDistance = distanceFromStartMap.get(curVertex);
					distanceFromStartMap.put(curAdjacentVertex, parentDistance + 1);
					
					// record our bfs parent node
					bfsParentMap.put(curAdjacentVertex, curVertex);
					
					queue.add(curAdjacentVertex);
				}
					
			}
			// once we've explored all of this vertexes adjacent vertexes
			curVertex.setState(VertexState.FULL_EXPLORED);
			//System.out.println(curVertex);
		}
	}
	
	public void printBFSPathFrom(Vertex<T> start, Vertex<T> dest) {
		if(start.equals(dest)) {
			System.out.println(start);
		} else if(bfsParentMap.get(dest) == null) {
			System.out.println("No path from " + start.getClass() + " to " + dest.getData() + " exists");
		} else {
			printBFSPathFrom(start, bfsParentMap.get(dest));
			System.out.println(dest);
		}
	}
}
