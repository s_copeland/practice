package com.saiyd.graph2.alg;

import java.util.Set;

import com.saiyd.graph.datastructures.DisjointSet;
import com.saiyd.graph.datastructures.DisjointSetVertex;
import com.saiyd.graph2.Graph;
import com.saiyd.graph2.Graph.EdgeDirection;
import com.saiyd.graph2.Graph.Type;
/*
 * An undirected graph is connect if every pair of vertices is
 * connected by a path.
 * 
 * The connected components of a graph are the equivalence classes
 * of  vertices under the "is reachable from" relation.
 * 
 * 
 */

public class ConnectedComponents {
	
	private DisjointSet<String> djs;

	/*
	 * One application of disjoint-set data structures is determining the
	 * connected components of an undirected graph
	 */
	public static void main(String[] args) {
		ConnectedComponents cc = new ConnectedComponents();
		long start = System.nanoTime();
		cc.processGraph("connected-components-graph.json");
		System.out.println("Duration = " + (System.nanoTime() - start));
	}
	
	public ConnectedComponents() {
		djs = new DisjointSet<String>();
	}

	public void processGraph(String graphName) {
		Graph<String, DisjointSetVertex<String>> sccGraph = Graph.loadDSVGraph(graphName, Type.UNDIRECTED, EdgeDirection.NORMAL);
		
		/*
		 * Create N disjoint sets with one node each
		 */
		Set<DisjointSetVertex<String>> verticies = sccGraph.getVertices();
		for(DisjointSetVertex<String> v : verticies) {
			djs.makeSet(v);
		}
		
		/*
		 * For each edge in the graph (u, v)
		 */
		for(DisjointSetVertex<String> curVertex : verticies) {
			for(DisjointSetVertex<String> adjVertex : sccGraph.getAdjacencyList(curVertex)) {
				/*
				 * Determine if vertex u and v are in the same disjoint set
				 */
				if(djs.find(curVertex) != djs.find(adjVertex))
					djs.union(curVertex, adjVertex); // if no then put them in the same disjoint set
			}
		}
		
		System.out.println(djs);
	}
	
	public boolean isInSameComponent(DisjointSetVertex<String> vertex1, DisjointSetVertex<String> vertex2) {
		if(djs.find(vertex1) == djs.find(vertex2)) {
			return true;
		} else {
			return false;
		}
	}
}
