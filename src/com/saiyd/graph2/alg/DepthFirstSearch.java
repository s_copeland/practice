package com.saiyd.graph2.alg;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.saiyd.graph2.Graph;
import com.saiyd.graph2.ISearchSuccess;
import com.saiyd.graph2.Vertex;
import com.saiyd.graph2.VertexState;
import com.saiyd.graph2.alg.DepthFirstSearch.TraversalType;

/*
 * Running time 
 * 		The time to initialize the graph and search for white
 * 		vertices in the coloredSearch method take time
 * 		bigTheta(V), this does not include the call to dfsVisit
 * 
 * 		The dfsVisit method is invoked only on non explorered verticies
 * 		and the first thing it does it set the vertex to paritally 
 * 		explored. During dfsVisit the for loop is executed |adj[v]|
 * 		times |adj[v]| = bigTheta(E)
 * 
 * 
 * 		There for the running time is bigTheta(V + E)
 * 
 * Properties of DFS
 * 		The predecessor subgraph forms a forest of trees
 * 		
 * 		Discovery and finishing times have "parenthesis structure"
 * 		Ex: if start was indicated by "(" and end by ")" then
 * 			the history of discoveries and finishings would make
 * 			well-formed expressions in the sense that the parenthesis
 * 			are properly nested
 * 
 * Parenthesis Structure Theorem
 * 		For a Graph with vertices u and v, exactly one of the following
 * 		is true.  d[u] = discovery of u, f[] = finishing of u
 * 
 * 		The intervals [d[u], f[u]] and [d[v], f[v]] are entirely  disjoint
 * 		and neither u nor v is descendant of the other in the depth-first forest
 * 
 * 		The interval [d[u], f[u]] is contained entirely with in the interval [d[v], f[v]],
 * 		and u is a descendant of v in a depth-first tree
 * 
 * 		The interval [d[v], f[v]] is contained entirely with in the interval [d[u], f[u]],
 * 		and v is a descendant of u in a depth-first tree
 * 
 * 
 *  Nesting of descendants' intervals
 *  	Vertex v is a proper descendant of vertex u in the depth-first forest iff
 *  	d[u] < d[v] < f[v] < f[u]
 *  
 *  
 *  White path theorem
 *  	In a depth-first forest of a graph, vertex v is a descendant of vertex u iff
 *  	at the time d[u] that the search discovers u, vertex v can be reached from u along
 *  	a path consisting entirely of white edges
 *  
 *  Classification of Edges
 *  	Tree edges - edges in the depth-first forest. Edge (u, v) is a tree edge if
 *                   v was first discovered by exploring edge (u, v)
 *                   
 *      Back edges - edges (u, v) connecting a vertex u to an ancestor v in a depth-first
 *      			 tree. Self-loops, which may occur in directed graphs, are considered to be back edges.
 *      
 *      Forward edges - non-tree edges (u, v) connecting a vertex u o a descendant v in a
 *      			    depth-first tree
 *      
 *      Cross edges - are all other edges. They can go between vertices in the same depth-first
 *      			  tree, as long as one vertex is not an ancestor of the other, or they can
 *      			  go between vertices in different depth-first trees
 *      
 *  In a depth-first search of an undirected graph, every edge is either a tree
 *  edge or a back edge
 */
public class DepthFirstSearch <T extends Comparable<T>> implements ISearch<T> {
	public static enum TraversalType { PREORDER, INORDER, POSTORDER };
	
	public static interface VertexEventListener <T extends Comparable<T>> {
		public void onNewStartingVertex(Vertex<T> vertex);
		public void onFoundUnexplored(Vertex<T> vertex);
		public void onFullyExplored(Vertex<T> vertex, int startTime, int EndTime);
	}
	
	public static interface VertexVisitor <T extends Comparable<T>> {
		public void onPreOrderVisit(Vertex<T> vertex);
		public void onInOrderVisit(Vertex<T> vertex);
		public void onPostOrderVisit(Vertex<T> vertex);
	}
	
	/*
	 * When the search is complete, this will contain a forest of trees
	 */
	private Map<Vertex<T>, Vertex<T>> dfsParent = new HashMap<Vertex<T>, Vertex<T>>();
	private Map<Vertex<T>, TimeInterval> explorationTimeMap = new HashMap<Vertex<T>, TimeInterval>();
	private Map<TimeInterval, Vertex<T>> invertedExplorationTimeMap = new HashMap<TimeInterval, Vertex<T>>();
	private int time;
	private VertexEventListener<T> eventListener;
	private VertexVisitor<T> visitor;
	private boolean print;
	private TraversalType traversalType = TraversalType.PREORDER;
	
	
	public void setEventListener(VertexEventListener<T> eventListener) {
		this.eventListener = eventListener;
	}
	public void setVisitor(VertexVisitor<T> visitor) {
		this.visitor = visitor;
	}

	public Set<Vertex<T>> getVerticesOrderByDescEndExploreTime() {
		Set<Vertex<T>> retVal = new LinkedHashSet<Vertex<T>>();
		Set<TimeInterval> allTimeIntervals = invertedExplorationTimeMap.keySet();
		
		// sort the time intervals, their compareto method sorts them in reverse order
		Set<TimeInterval> treeSet = new TreeSet<TimeInterval>();
		treeSet.addAll(allTimeIntervals); // sort all the time intervals
		//System.out.println(treeSet);
		
		// create the set of associated vertices
		for(TimeInterval ti : treeSet) {
			Vertex<T> v = invertedExplorationTimeMap.get(ti);
			// create new vertices that do not have any previous state
			Vertex<T> tempVertex = new Vertex<T>(v.getId(), v.getData(), v.getWeight());
			retVal.add(tempVertex);
		}
		
		return retVal;
	}

	/* (non-Javadoc)
	 * @see com.saiyd.graph2.alg.ISearch#search(com.saiyd.graph2.Graph, com.saiyd.graph2.Vertex, com.saiyd.graph2.ISearchSuccess)
	 */
	@Override
	public void search(Graph<T,Vertex<T>> g, Vertex<T> startVertex, ISearchSuccess<T> searchSuccess) {
		// ensure that the start vertex is at the beginning of the list
		Set<Vertex<T>> vertices = g.getVertices();
		vertices.remove(startVertex);
		LinkedHashSet<Vertex<T>> temp = new LinkedHashSet<Vertex<T>>();
		temp.add(startVertex);
		temp.addAll(vertices);
		coloredSearch(g, temp, searchSuccess);
	}
	
	public void coloredSearch(Graph<T,Vertex<T>> g, Set<Vertex<T>> callerVertexSearchOrder, ISearchSuccess<T> searchSuccess) {
		dfsParent = new HashMap<Vertex<T>, Vertex<T>>();
		explorationTimeMap = new HashMap<Vertex<T>, TimeInterval>();
		invertedExplorationTimeMap = new HashMap<TimeInterval, Vertex<T>>();
		
		// initialize the graph
		g.setResetAllVertexState();
		
		time = 0;
		
		// use a different vertex search order if provided by the caller
		Set<Vertex<T>> searchOrder = g.getVertices();
		if(callerVertexSearchOrder != null)
			searchOrder = callerVertexSearchOrder;
		
		// according to the Intro to Algorithms book, the order of these
		// vertices in the following set tend not to cause problems in practice
		// , as any depth-first search result can ususally be used effectively,
		// with essentially equivalent results
		for(Vertex<T> curVertex : searchOrder) {
			if(curVertex.getState() == VertexState.NOT_EXPLORED) {
				if(eventListener != null)
					eventListener.onNewStartingVertex(curVertex);
				boolean found = dfsVisit(g,curVertex,searchSuccess);
				if(found)
					break;
				if(print)
					System.out.println();
			}
		}
	}


	private boolean dfsVisit(Graph<T,Vertex<T>> g, Vertex<T> curVertex, ISearchSuccess<T> searchSuccess) {
		
		if(visitor != null && traversalType == TraversalType.PREORDER) {
			visitor.onPreOrderVisit(curVertex);
		}
		
		curVertex.setState(VertexState.PARTIAL_EXPLORED);
		
		if(searchSuccess != null && searchSuccess.isFound(curVertex))
			return true;
		
		time++;
		TimeInterval ti = explorationTimeMap.get(curVertex);
		if(ti == null) {
			ti = new TimeInterval();
		}
		ti.start = time;
		explorationTimeMap.put(curVertex, ti);
		
		/*
		 * only do an in-order sort for binary trees
		 */
		List<Vertex<T>> adjList = g.getAdjacencyList(curVertex);
		int adjListIndex = 0;
		
		// according to the Intro to Algorithms book, the order of these
		// vertices in the following set tend not to cause problems in practice
		// , as any depth-first search result can usually be used effectively,
		// with essentially equivalent results
		for(Vertex<T> adjVertex : adjList) {
			if(adjVertex.getState() == VertexState.NOT_EXPLORED) {
				if(print)
					System.out.println("Found unexplored vertex " + adjVertex);
				
				if(eventListener != null)
					eventListener.onFoundUnexplored(adjVertex);
				
				if(visitor != null && traversalType == TraversalType.INORDER && adjListIndex == 1) {
					visitor.onInOrderVisit(curVertex);
				}
				
				dfsParent.put(adjVertex, curVertex);
				if(dfsVisit(g, adjVertex, searchSuccess))
					return true;
				
				adjListIndex++;
			}
		}
		
		curVertex.setState(VertexState.FULL_EXPLORED);
		time++;
		ti.end = time;
		// we add to the inverted map here to avoid modifications to the key object
		invertedExplorationTimeMap.put(ti, curVertex);
		
		if(eventListener != null)
			eventListener.onFullyExplored(curVertex, ti.start, ti.end);
		
		if(visitor != null && traversalType == TraversalType.INORDER && adjList.size() <= 1) {
			visitor.onInOrderVisit(curVertex);
		}
		
		if(visitor != null && traversalType == TraversalType.POSTORDER) {
			visitor.onPostOrderVisit(curVertex);
		}
		return false;
	}

	public Map<Vertex<T>, Vertex<T>> getDfsParent() {
		// make a defensive copy
		return new HashMap<Vertex<T>, Vertex<T>>(dfsParent);
	}

	public void setPrint(boolean b) {
		this.print = b;
	}

	public void setTraversal(TraversalType t) {
		traversalType = t;
	}
	
	
}
