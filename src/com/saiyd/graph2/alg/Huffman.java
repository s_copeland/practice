package com.saiyd.graph2.alg;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Arrays;

import com.saiyd.datastructures.PriorityQueue;
import com.saiyd.io.BinaryStdIn;
import com.saiyd.io.BinaryStdOut;

/*
 * Greedy Algorithm
 * 
 * This class is an adaptation of the Huffman.java class
 * http://algs4.cs.princeton.edu/55compression/Huffman.java.html
 * 
 * Lossless compression
 * 
 * Used in MP3 files
 * 
 * Create variable length codes that can be concatenated together in
 * an manner which they can be decoded in an unambiguous way. 
 * 
 * Neither code is a prefix of the other, this removes the ambiguity
 * 
 * Finds optimal compression.
 */
public class Huffman {
	// alphabet size of extended ASCII
	private static final int ALPHABET_SIZE = 256;
	
	private static class Node implements Comparable<Node> {
		private final char ch;
		private final int freq;
		private final Node left;
		private final Node right;
		
		Node(char ch, int freq, Node left, Node right) {
			this.ch = ch;
			this.freq = freq;
			this.left = left;
			this.right = right;
		}
		
	     // is the node a leaf node?
        private boolean isLeaf() {
            assert (left == null && right == null) || (left != null && right != null);
            return (left == null && right == null);
        }
		
		@Override
		public int compareTo(Node other) {
			return this.freq - other.freq;
		}
		
	}
	
	public static void compress(String inputString) throws IOException {
		if(inputString == null)
			throw new IllegalArgumentException();
		
		char[] inputChars = inputString.toCharArray();
		
		// create a frequency table for the input
		int[] freqTable = new int[ALPHABET_SIZE];
		for(int i=0; i < inputChars.length; i++) {
			freqTable[inputChars[i]]++;
		}
		
		//System.out.print(Arrays.toString(freqTable));
		
		// build the Huffman trie
		Node root = buildTrie(freqTable);
		
		// build code table
		String[] codeTable = new String[ALPHABET_SIZE];
		buildCode(codeTable, root, "");
		
		printCodeTable(codeTable);
		
		//OutputStreamWriter fos = new OutputStreamWriter(new FileOutputStream(outputFile));
		writeTrie(root); // print the trie for the decoder
		//fos.close();
		
		// print number of bytes in original uncompressed message
		BinaryStdOut.write(inputChars.length);
		
		/*
		 * Usse Huffman code to encode input
		 */
		for(int i=0; i < inputChars.length; i++) {
			String code = codeTable[inputChars[i]];
			for(int j=0; j < code.length(); j++) {
				if(code.charAt(j) == '0') {
					BinaryStdOut.write(false);
				} else if(code.charAt(j) == '1') {
					BinaryStdOut.write(true);
				} else {
					throw new IllegalStateException();
				}
			}
		}
		
		BinaryStdOut.close();
	}

	private static void writeTrie(Node node) {
        if (node.isLeaf()) {
        	BinaryStdOut.write(true);
        	BinaryStdOut.write(node.ch, 8);
            return;
        }
        BinaryStdOut.write(false);
        writeTrie(node.left);
        writeTrie(node.right);
	}

	public static void printCodeTable(String[] codeTable) {
		// print
		for(int i=0; i < codeTable.length; i++) {
			if(codeTable[i] != null)
				System.out.println(codeTable[i]);
		}
	}
	
	/*
	 * Make a lookup table from symbols and their encodings
	 */
	private static void buildCode(String[] codeTable, Node node, String s) {
		if(!node.isLeaf()) {
			buildCode(codeTable, node.left, s + '0');
			buildCode(codeTable, node.right, s + '1');
		} else {
			codeTable[node.ch] = s;
		}
	}

	private static Node buildTrie(int[] freqTable) {
		PriorityQueue<Node> minPQ = new PriorityQueue<Node>(PriorityQueue.Type.MIN);
		
		// initialize the priority queue with singleton trees for
		// each symbol
		for(char i=0; i < ALPHABET_SIZE; i++) {
			if(freqTable[i] > 0)
				minPQ.insert(new Node(i, freqTable[i], null, null));
		}
		
		// merge the two smallest trees
		while(minPQ.size() > 1) {
			Node left = minPQ.getFront();
			Node right = minPQ.getFront();
			Node parent = new Node('\0', left.freq + right.freq, left, right);
			minPQ.insert(parent);
		}
		
		return minPQ.getFront();
	}
	
	/*
	 * Expand Huffman-encoded input from standard input and write to 
	 * standard output
	 */
	public static void expand() {
		// read in Huffman trie from input stream
		Node root = readTrie();
		
		// number of bytes to write
		int length = BinaryStdIn.readInt();
		
		// decode using Huffman trie
		for(int i=0; i < length; i++) {
			Node x = root;
			while(!x.isLeaf()) {
				boolean bit = BinaryStdIn.readBoolean();
				if(bit) {
					x = x.right;
				} else {
					x = x.left;
				}
			}
			BinaryStdOut.write(x.ch, 8);
		}
		BinaryStdOut.close();
	}
	
	private static Node readTrie() {
		boolean isLeaf = BinaryStdIn.readBoolean();
		if(isLeaf) {
			return new Node(BinaryStdIn.readChar(), -1, null, null);
		} else {
			return new Node('\0', -1, readTrie(), readTrie());
		}
	}

	public static void main(String[] args) throws IOException {
		String s = "Hello World!";
		compress(s);
	}
}
