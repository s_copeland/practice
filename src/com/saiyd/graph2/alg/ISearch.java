package com.saiyd.graph2.alg;

import com.saiyd.graph2.Graph;
import com.saiyd.graph2.ISearchSuccess;
import com.saiyd.graph2.Vertex;

public interface ISearch<T extends Comparable<T>> {

	public abstract void search(Graph<T, Vertex<T>> g, Vertex<T> startVertex,
			ISearchSuccess<T> searchSuccess);

}