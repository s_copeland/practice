package com.saiyd.graph2.alg;

import java.util.List;

import com.saiyd.Summable;
import com.saiyd.datastructures.PriorityQueue;


/*
 * An optimal binary tree is a binary weighted tree (where the weights are on the nodes and not the edges)
 * with a minimal tree weight.
 * 
 * Tree weight is calculated
 * 		treeWeight = sumOfAll(leafWeight * level number)
 * 
 * 		level number is the length of the path from the root
 * 		to the leaf 
 * 
 * Finding a merge of multiple lists that minimizes the worst case number
 * of comparisons is the equivalent of finding a optimal binary tree with
 * weights equal to the length of the lists
 * 
 * If we do not plan on modifying a search tree, and we know exactly how often each item will be accessed, 
 * we can construct[3] an optimal binary search tree, which is a search tree where the average cost of 
 * looking up an item (the expected search cost) is minimized.
 * 
 * Even if we only have estimates of the search costs, such a system can considerably speed up lookups 
 * on average. For example, if you have a BST of English words used in a spell checker
 */
public class OptimalNodeWeightedBinaryTree <T extends Comparable<T> & Summable<T>> {

	private static class Node <T extends Comparable<T>> implements Comparable<Node<T>> {
		//private final S symbol;
		private final T weight;
		private final Node<T> left;
		private final Node<T> right;
		
		//Node(S symbol, T weight, Node<S,T> left, Node<S,T> right) {
		Node(T weight, Node<T> left, Node<T> right) {
			//this.symbol = symbol;
			this.weight = weight;
			this.left = left;
			this.right = right;
		}
		
		/*public S getSymbol() {
			return S;
		}*/
	     // is the node a leaf node?
        private boolean isLeaf() {
            assert (left == null && right == null) || (left != null && right != null);
            return (left == null && right == null);
        }
		
		@Override
		public int compareTo(Node<T> other) {
			return this.weight.compareTo(other.weight);
		}

		public T getWeight() {
			return weight;
		}
		
	}
	
	private Node<T> buildOptimalBinaryTree(List<T> items) {
		if(items.size() < 2)
			throw new IllegalArgumentException("There must be at least 2 items");
		
		if(items.size() == 2) {
			return new Node<T>(items.get(0), new Node<T>(items.get(1), null, null), null);
		} else {
			PriorityQueue<Node<T>> minPQ = new PriorityQueue<Node<T>>(PriorityQueue.Type.MIN);
			
			// initialize the priority queue with singleton trees for
			// weight
			for(char i=0; i < items.size(); i++) {
				minPQ.insert(new Node<T>(items.get(i), null, null));
			}
			
			// merge the two smallest trees
			while(minPQ.size() > 1) {
				Node<T> left = minPQ.getFront();
				Node<T> right = minPQ.getFront();
				Node<T> parent = new Node<T>(left.weight.sum(right.weight), left, right);
				minPQ.insert(parent);
			}
			return minPQ.getFront();
		}
	}
}
