package com.saiyd.graph2.alg;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import com.saiyd.graph2.Graph;
import com.saiyd.graph2.Vertex;
import com.saiyd.graph2.VertexState;

/*
 * http://en.wikipedia.org/wiki/Path-based_strong_component_algorithm
 * 
 */
public class PathBasedStronglyConnectedComponent <T extends Comparable<T>> {
	private static class PreOrderNumber {
		private int id;

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + id;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;

			if (!(obj instanceof PreOrderNumber))
				return false;
			PreOrderNumber other = (PreOrderNumber) obj;

			if (id != other.id)
				return false;
			return true;
		}

	}
	
	private Map<Vertex<T>, Boolean> vertexSCCAssignmentMap = new HashMap<Vertex<T>, Boolean>();
	private Map<Vertex<T>, PreOrderNumber> lowLinkMap = new HashMap<Vertex<T>, PreOrderNumber>();
	private List<Set<Vertex<T>>> sccList;
	private Stack<Vertex<T>> stack; // contains all the vertices that have not yet been assigned to a strongly connected component, in the order in which the depth-first search reaches the vertices
	private Stack<Vertex<T>> stack2; // contains vertices that have not yet been determined to belong to different strongly connected components from each other. 
	private int countOfVerticesReached;
	
	public List<Set<Vertex<T>>> findStronglyConnectedComponents(Graph<T,Vertex<T>> g) {
		sccList = new ArrayList<Set<Vertex<T>>>();
		stack = new Stack<Vertex<T>>();
		stack2 = new Stack<Vertex<T>>();
		countOfVerticesReached = 0;
		
		vertexSCCAssignmentMap = new HashMap<Vertex<T>, Boolean>();
		
		for(Vertex<T> curVertex : g.getVertices()) {
			if(curVertex.getState() == VertexState.NOT_EXPLORED) {
				dfsVisit(g,curVertex);
			}
		}
		
		return sccList;
	}
	
	private void dfsVisit(Graph<T,Vertex<T>> g, Vertex<T> curVertex) {
		curVertex.setState(VertexState.PARTIAL_EXPLORED);
		
		// record the smallest index of any node known to be reachable from this node, including itself
		PreOrderNumber preOrderNumber = new PreOrderNumber();
		preOrderNumber.id = countOfVerticesReached;
		lowLinkMap.put(curVertex, preOrderNumber);
		countOfVerticesReached++;
		
		stack.push(curVertex);
		stack2.push(curVertex);
		
		for(Vertex<T> adjVertex : g.getAdjacencyList(curVertex)) {
			Boolean isAssignedToSCC = vertexSCCAssignmentMap.get(adjVertex);
			
			if(adjVertex.getState() == VertexState.NOT_EXPLORED) {
				dfsVisit(g, adjVertex);
			} else if(isAssignedToSCC == null && !stack2.isEmpty()) {
				PreOrderNumber adjPreOrderNumber = lowLinkMap.get(adjVertex);
				Vertex<T> stackTop = null;
				PreOrderNumber stackPeekPreOrderNumber = null;
				do {
					stackTop = stack2.pop();
					if(!stack2.isEmpty())
						stackPeekPreOrderNumber = lowLinkMap.get(stack2.peek());
				} while(!stack2.isEmpty() && stackPeekPreOrderNumber.id <= adjPreOrderNumber.id);
			}
		}
		
		curVertex.setState(VertexState.FULL_EXPLORED);
		// we don't care about end time
		
		if(!stack2.isEmpty() && curVertex == stack2.peek()) {
			// start a new strongly connected component
			Set<Vertex<T>> scc = new HashSet<Vertex<T>>();
			Vertex<T> stackTop = null;
			do {
				stackTop = stack.pop();
				vertexSCCAssignmentMap.put(stackTop, true);
				scc.add(stackTop);
			} while(stackTop != curVertex);
			
			sccList.add(scc);
			
			stack.pop();
		} 
	}
}
