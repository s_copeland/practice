package com.saiyd.graph2.alg;

import java.util.LinkedList;
import java.util.List;


import java.util.Stack;

import com.saiyd.graph2.Graph;
import com.saiyd.graph2.Vertex;
import com.saiyd.graph2.VertexState;
import com.saiyd.graph2.Graph.EdgeDirection;
import com.saiyd.graph2.Graph.Type;


/*
 * A depth first search.
 * 
 * http://en.wikipedia.org/wiki/Tree_traversal
 * 
 * Post-order
 * Traverse the left subtree.
 * Traverse the right subtree.
 * Visit the root.
 * 
 * Also known as Reverse polish notation
 * Ex: The algebraic expression ((x-4)^2)*((y+2)/3)
 *     In polish notation is x4-2^y2+3/*
 *     

                       *
                    /     \

                 ^          /
               /  \        /  \

             -     2      +    3

           /   \        /   \

          x     4      y     2
 */
public class PostorderSort <T extends Comparable<T>> {
	private List<Vertex<T>> linkedList;
	
	/*
	 * outputList should be a linked list to all for O(1) head insertions
	 */
	public List<Vertex<T>> sort(Graph<T,Vertex<T>> g, Vertex<T> rootVertex) {
		linkedList = new LinkedList<Vertex<T>>();
		
		// initialize the graph
		g.setResetAllVertexState();
		
		dfsVisit(g,rootVertex);

		return linkedList;
	}


	private void dfsVisit(Graph<T,Vertex<T>> g, Vertex<T> curVertex) {
		if(curVertex == null)
			return;

		List<Vertex<T>> adjList =  g.getAdjacencyList(curVertex);
		int adjListSize = adjList.size();
		
		Vertex<T> child = null;

		if(adjListSize > 0) {
			child = adjList.get(0);
			if(child != null) {
				dfsVisit(g, child); // visit left node
			}
		}
		
		if(adjListSize > 1) {
			child = adjList.get(1);
			if(child != null) {
				dfsVisit(g, child);
			}
		}
		
		// add after all children
		linkedList.add(curVertex);
	}

	public List<Vertex<T>> sortViaIteration(Graph<T,Vertex<T>> g, Vertex<T> rootVertex) {
		linkedList = new LinkedList<Vertex<T>>();
		
		// initialize the graph
		g.setResetAllVertexState();
		
		Stack<Vertex<T>> stack = new Stack<Vertex<T>>();
		stack.push(rootVertex);
		Vertex<T> curNode = null;
		
		while(!stack.isEmpty()) {
			curNode = stack.pop();
			curNode.setState(VertexState.PARTIAL_EXPLORED);
			
			List<Vertex<T>> adjList =  g.getAdjacencyList(curNode);
			
			boolean hasRightChild = adjList != null && adjList.size() > 1 && adjList.get(1) != null && adjList.get(1).getState() == VertexState.NOT_EXPLORED;
			boolean hasLeftChild = adjList != null && adjList.size() > 0 && adjList.get(0) != null && adjList.get(0).getState() == VertexState.NOT_EXPLORED;

			if(hasLeftChild || hasRightChild){
				stack.push(curNode);
			}
			
			// check for a right node
			if(hasRightChild) {
				stack.push(adjList.get(1));
			}
			// set the left node as the next to process
			if(hasLeftChild) {
				stack.push(adjList.get(0));
			}
			
			if(!hasLeftChild && !hasRightChild)
				linkedList.add(curNode);
		}

		return linkedList;
	}

	public static void main(String[] args) {
		Graph<String, Vertex<String>> graph = Graph.loadGraph("pre-in-post-order-sort-graph.json", Type.DIRECTED, EdgeDirection.NORMAL);
		PostorderSort<String> pos = new PostorderSort<String>();
		List<Vertex<String>>  sortedGraph = pos.sort(graph, graph.getVertex(5));
		System.out.println(sortedGraph);
		sortedGraph = pos.sortViaIteration(graph, graph.getVertex(5));
		System.out.println(sortedGraph);
	}
}
