package com.saiyd.graph2.alg;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.Stack;

import com.saiyd.graph2.Graph;
import com.saiyd.graph2.ISearchSuccess;
import com.saiyd.graph2.Vertex;
import com.saiyd.graph2.VertexState;
import com.saiyd.graph2.Graph.EdgeDirection;
import com.saiyd.graph2.Graph.Type;
import com.saiyd.graph2.alg.DepthFirstSearch.TraversalType;
import com.saiyd.graph2.alg.DepthFirstSearch.VertexEventListener;
import com.saiyd.graph2.alg.DepthFirstSearch.VertexVisitor;

/*
 * A depth first search.
 * 
 * http://en.wikipedia.org/wiki/Tree_traversal
 * 
 * Pre-order
 * Visit the root.
 * Traverse the left subtree.
 * Traverse the right subtree.
 * 
 * Also known as Polish notation
 * Ex: The algebraic expression ((x-4)^2)*((y+2)/3)
 *     In polish notation is *^-x42/+y23
 *     

                       *
                    /     \

                 ^          /
               /  \        /  \

             -     2      +    3

           /   \        /   \

          x     4      y     2
 */
public class PreorderSort <T extends Comparable<T>> {
	private List<Vertex<T>> linkedList;
	
	/*
	 * outputList should be a linked list to all for O(1) head insertions
	 */
	public List<Vertex<T>> sort(Graph<T,Vertex<T>> g, Vertex<T> rootVertex) {
		linkedList = new LinkedList<Vertex<T>>();
		
		// initialize the graph
		g.setResetAllVertexState();
		
		dfsVisit(g,rootVertex);

		return linkedList;
	}


	private void dfsVisit(Graph<T,Vertex<T>> g, Vertex<T> curVertex) {
		if(curVertex == null)
			return;

		// add before all children
		linkedList.add(curVertex);
		
		List<Vertex<T>> adjList =  g.getAdjacencyList(curVertex);
		int adjListSize = adjList.size();
		
		Vertex<T> child = null;

		if(adjListSize > 0) {
			child = adjList.get(0);
			if(child != null) {
				dfsVisit(g, child); // visit left node
			}
		}
		
		if(adjListSize > 1) {
			child = adjList.get(1);
			if(child != null) {
				dfsVisit(g, child);
			}
		}
		

	}

	public List<Vertex<T>> sortViaIteration(Graph<T,Vertex<T>> g, Vertex<T> rootVertex) {
		linkedList = new LinkedList<Vertex<T>>();
		
		// initialize the graph
		g.setResetAllVertexState();
		
		Stack<Vertex<T>> stack = new Stack<Vertex<T>>();
		stack.push(rootVertex);
		Vertex<T> curNode = null;
		
		while(!stack.isEmpty()) {
			curNode = stack.pop();
			
			linkedList.add(curNode);
			List<Vertex<T>> adjList =  g.getAdjacencyList(curNode);
			// check for a right node
			if(adjList != null && adjList.size() > 1 && adjList.get(1) != null) {
				stack.push(adjList.get(1));
			}
			// set the left node as the next to process
			if(adjList != null && adjList.size() > 0 && adjList.get(0) != null) {
				stack.push(adjList.get(0));
			}
		}

		return linkedList;
	}


	public static void main(String[] args) {
		Graph<String, Vertex<String>> graph = Graph.loadGraph("pre-in-post-order-sort-graph.json", Type.DIRECTED, EdgeDirection.NORMAL);
		PreorderSort<String> pos = new PreorderSort<String>();
		List<Vertex<String>>  sortedGraph = pos.sort(graph, graph.getVertex(5));
		System.out.println(sortedGraph);
		sortedGraph = pos.sortViaIteration(graph, graph.getVertex(5));
		System.out.println(sortedGraph);
	}
}
