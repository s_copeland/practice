package com.saiyd.graph2.alg;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.saiyd.graph2.Graph;
import com.saiyd.graph2.Vertex;

/*
 * Kosaraju's algorithm uses two passes of depth first search. 
 * The first, in the original graph, is used to choose the order 
 * in which the outer loop of the second depth first search tests 
 * vertices for having been visited already and recursively 
 * explores them if not. The second depth first search is on the 
 * transpose graph of the original graph, and each recursive 
 * exploration finds a single new strongly connected component.[1] 
 * It is named after S. Rao Kosaraju, who described it 
 * (but did not publish his results) in 1978; Micha Sharir later 
 * published it in 1981
 * 
 * An undirected graph is connect if every pair of vertices is
 * connected by a path.
 * 
 * The connected components of a graph are the equivalence classes
 * of  vertices under the "is reachable from" relation.
 * 
 * An undirected graph is connected if it has exactly one
 * connected component, that is, if every vertex is reachable from
 * every other vertex.
 * 
 * -------------------------------
 * A directed graph is strongly connected if every two vertices
 * are reachable from each other. 
 * 
 * The strongly connected components of a directed graph are the
 * equivalence classes of vertices under the "are mutually reachable"
 * relation.
 * 
 * A directed graph is strongly connected if it has only one 
 * strongly connected component.
 * 
 * Runtime: O(V + E)  with an adjacency list representation of the graph
 * 			O(V^2)    If the graph is represented as an adjacency matrix
 * 
 * It is the conceptually simplest efficient algorithm, but is not as 
 * efficient in practice as Tarjan's strongly connected components 
 * algorithm and the path-based strong component algorithm, which perform 
 * only one traversal of the graph.
 */
public class StronglyConnectedComponents <T extends Comparable<T>> implements DepthFirstSearch.VertexEventListener<T> {
	private List<Set<Vertex<T>>> sccList;
	private Set<Vertex<T>> curSccComponent;
	
	public List<Set<Vertex<T>>> findStronglyConnectedComponents(Graph<T,Vertex<T>> graph, Graph<T,Vertex<T>> transposedGraph) {
		sccList= new ArrayList<Set<Vertex<T>>>();
		/*
		 * Step 1
		 * 
		 * Call DFS on G to compute the finishing times for each vertex
		 */
		DepthFirstSearch<T> dfs = new DepthFirstSearch<T>();
		dfs.coloredSearch(graph, null, null);
		
		/*
		 * Step 2
		 * Create the graph G^T, that is the transpose of the original graph
		 * 
		 * This is already done for use here
		 */
		
		/*
		 * Step 3
		 * 
		 * Call DFS on G^T, but consider vertices in the order of
		 * decreasing finishing time
		 */
		DepthFirstSearch<T> dfs2 = new DepthFirstSearch<T>();
		dfs2.setEventListener(this);
		dfs2.coloredSearch(transposedGraph, dfs.getVerticesOrderByDescEndExploreTime(), null);
		
		/*
		 * Step 4
		 * 
		 * Output the vertices of each tree in the depth-first forest from the 2nd
		 * DFS of G^T as a separate strongly connected component
		 */
		return sccList;
	}

	@Override
	public void onNewStartingVertex(Vertex<T> vertex) {
		curSccComponent = new HashSet<Vertex<T>>();
		// we wait until we have at least one element to add to the sccList
	}

	@Override
	public void onFoundUnexplored(Vertex<T> vertex) {
		// only add to the sccList when we have at least one element
		if(curSccComponent.size() == 0)
			sccList.add(curSccComponent);
		
		curSccComponent.add(vertex);
	}

	@Override
	public void onFullyExplored(Vertex<T> vertex, int startTime, int EndTime) {
		// do nothing
	}
}
