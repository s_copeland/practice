package com.saiyd.graph2.alg;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import com.saiyd.graph2.Graph;
import com.saiyd.graph2.Vertex;
import com.saiyd.graph2.VertexState;

/*
 * http://en.wikipedia.org/wiki/Tarjan%27s_strongly_connected_components_algorithm
 * 
 * Complexity: The Tarjan procedure is called once for each node; the forall statement 
 * considers each edge at most twice. The algorithm's running time is therefore linear 
 * in the number of edges in G, i.e. O(|E|).
 * 
 * Runtime: worst case O(|V| + |E|)
 * 
 * The test for whether w is on the stack should be done in constant time, for example, 
 * by testing a flag stored on each node that indicates whether it is on the stack.

 * While there is nothing special about the order of the nodes within each strongly 
 * connected component, one useful property of the algorithm is that no strongly 
 * connected component will be identified before any of its successors. 
 * Therefore, the order in which the strongly connected components are identified 
 * constitutes a reverse topological sort of the DAG formed by the strongly connected 
 * components.[2]
 */
public class TarjanStronglyConnectedComponent <T extends Comparable<T>> {
	private static class LowLink {
		private int id;

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + id;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;

			if (!(obj instanceof LowLink))
				return false;
			LowLink other = (LowLink) obj;

			if (id != other.id)
				return false;
			return true;
		}

	}
	
	private Map<Vertex<T>, TimeInterval> explorationTimeMap = new HashMap<Vertex<T>, TimeInterval>();
	private Map<Vertex<T>, LowLink> lowLinkMap = new HashMap<Vertex<T>, LowLink>();
	private List<Set<Vertex<T>>> sccList;
	private Stack<Vertex<T>> stack;
	private int time;
	
	public List<Set<Vertex<T>>> findStronglyConnectedComponents(Graph<T,Vertex<T>> g) {
		sccList = new ArrayList<Set<Vertex<T>>>();
		stack = new Stack<Vertex<T>>();
		explorationTimeMap = new HashMap<Vertex<T>, TimeInterval>();
		
		for(Vertex<T> curVertex : g.getVertices()) {
			if(curVertex.getState() == VertexState.NOT_EXPLORED) {
				dfsVisit(g,curVertex);
			}
		}
		
		return sccList;
	}
	
	private void dfsVisit(Graph<T,Vertex<T>> g, Vertex<T> curVertex) {
		curVertex.setState(VertexState.PARTIAL_EXPLORED);
		
		// record the initial visit time
		time++;
		TimeInterval ti = explorationTimeMap.get(curVertex);
		if(ti == null) {
			ti = new TimeInterval();
		}
		ti.start = time;
		explorationTimeMap.put(curVertex, ti);
		
		// record the smallest index of any node known to be reachable from this node, including itself
		LowLink ll = new LowLink();
		ll.id = time;
		lowLinkMap.put(curVertex, ll);
		
		stack.push(curVertex);
		
		for(Vertex<T> adjVertex : g.getAdjacencyList(curVertex)) {
			if(adjVertex.getState() == VertexState.NOT_EXPLORED) {
				dfsVisit(g, adjVertex);
				// find the lowlink info for the just visited vertex
				LowLink adjVertexLowLink = lowLinkMap.get(adjVertex);
				// set the low link to the min
				ll.id = Math.min(ll.id, adjVertexLowLink.id);
			} else if(stack.contains(adjVertex)) {
				TimeInterval adjTimeInterval = explorationTimeMap.get(adjVertex);
				// The successsor vertex is in the stack and hence in the current SCC
				ll.id = Math.min(ll.id, adjTimeInterval.start);
			}
		}
		
		curVertex.setState(VertexState.FULL_EXPLORED);
		// we don't care about end time
		
		if(ll.id == ti.start) {
			// start a new strongly connected component
			Set<Vertex<T>> scc = new HashSet<Vertex<T>>();
			Vertex<T> stackTop = null;
			do {
				stackTop = stack.pop();
				scc.add(stackTop);
			} while(stackTop != curVertex);
			
			sccList.add(scc);
		} 
	}
}
