package com.saiyd.graph2.alg;


public class TimeInterval implements Comparable<TimeInterval> {
	int start;
	int end;
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + end;
		result = prime * result + start;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;

		if (!(obj instanceof TimeInterval))
			return false;
		TimeInterval other = (TimeInterval) obj;
		if (end != other.end)
			return false;
		if (start != other.start)
			return false;
		return true;
	}
	
	/*
	 * This method sort in reverse order
	 * 
	 * (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(TimeInterval other) {
		if(other == null)
			return 0;
		
		if(this.end < other.end)
			return 1;
		if(this.end > other.end)
			return -1;
		return 0;
	}
	@Override
	public String toString() {
		return "TimeInterval [end=" + end + "]";
	}

}