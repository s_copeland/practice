package com.saiyd.graph2.alg;

import java.util.LinkedList;
import java.util.List;

import com.saiyd.graph2.Graph;
import com.saiyd.graph2.Vertex;
import com.saiyd.graph2.alg.DepthFirstSearch.VertexEventListener;

/*
 * A topological sort of a directed acyclic graph is a linear
 * ordering of all its vertices such that if the graph
 * contains an edge (u, v), then u appears before v in the
 * ordering.
 * 
 * If the graph is not acyclic then no linear ordering is possible.
 * 
 * A topological sort can be viewed as an ordering of its vertices
 * along a horizontal line so that all direct edges go from left to right.
 * 
 * Lemma 22.1
 * 		A directed graph is acyclic iff a DFS of the graph yields no
 * 		back edges.
 * 
 * Runtime:  O(V + E)
 * 			This is because DFS takes Theta(V + E) time and it takes
 * 			O(1) time to insert each of the |V| vertices onto the
 * 			front of the linked list.
 */
public class TopologicalSort <T extends Comparable<T>> implements VertexEventListener<T> {
	private DepthFirstSearch<T> dfs = new DepthFirstSearch<T>();
	private List<Vertex<T>> linkedList;
	
	/*
	 * outputList should be a linked list to all for O(1) head insertions
	 */
	public List<Vertex<T>> sort(Graph<T,Vertex<T>> g) {
		linkedList = new LinkedList<Vertex<T>>();
		
		dfs.setEventListener(this);
		dfs.coloredSearch(g, null, null);
		dfs.setEventListener(null);
		return linkedList;
	}

	@Override
	public void onFullyExplored(Vertex<T> vertex, int startTime, int endTime) {
		// insert at the front
		linkedList.add(0, vertex);
		//System.out.print(endTime);
		//System.out.print(" ");
		//System.out.println(vertex);
	}

	@Override
	public void onNewStartingVertex(Vertex<T> vertex) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onFoundUnexplored(Vertex<T> vertex) {
		// TODO Auto-generated method stub
		
	}
}
