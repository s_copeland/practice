package com.saiyd.graph2.alg;

import java.util.List;

import com.saiyd.graph2.Edge;
import com.saiyd.graph2.Graph;
import com.saiyd.graph2.Vertex;
import com.saiyd.graph2.Graph.EdgeDirection;
import com.saiyd.graph2.Graph.Type;

public class TreeTest <T extends Comparable<T>, V extends Vertex<T>> {
	public boolean isATree(Graph<T, V> graph) {
		if(graph.getEdgeList().size() == 0 && graph.getAdjacencyList().getVertices().size() == 1) {
			return true;
		} else {
			V leafVertex = findLeafVertex(graph);
			if(leafVertex != null) {
				graph.removeVertex(leafVertex); // this also remove any associated edges, there should only be one
				return isATree(graph);
			} else {
				return false;
			}
		}
	}
	
	/*
	 * find a leaf node in a directed graph
	 */
	private V findLeafVertex(Graph<T, V> graph) {
		V ret = null;
		for(V curVertex : graph.getVertices()) {
			List<V> adjList = graph.getAdjacencyList(curVertex);
			// check for an out degree of one, loops don't count
			if(adjList.size() == 1 && adjList.get(0) != curVertex) {
				ret = curVertex;
				break;
			} else if(adjList.size() == 0) {
				// check if any one else points to me
				int inDegree = 0;
				for(Edge<T> e : graph.getEdgeList()) {
					if(e.getDstVertex() == curVertex)
						inDegree++;
				}
				
				if(inDegree == 1) {
					ret = curVertex;
					break;
				}
			}
				
		}
		return ret;
	}
	
	public static void main(String[] args) {
		Graph<String, Vertex<String>> ccGraph = Graph.loadGraph("connected-components-graph.json", Type.DIRECTED, EdgeDirection.NORMAL);
		TreeTest<String, Vertex<String>> tt = new TreeTest<String, Vertex<String>>();
		System.out.println(tt.isATree(ccGraph));
		
		ccGraph = Graph.loadGraph("tree-1-graph.json", Type.DIRECTED, EdgeDirection.NORMAL);
		System.out.println(tt.isATree(ccGraph));
		
		ccGraph = Graph.loadGraph("one-vertex-graph.json", Type.DIRECTED, EdgeDirection.NORMAL);
		System.out.println(tt.isATree(ccGraph));
	}
}
