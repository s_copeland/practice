package com.saiyd.graph2.alg.allpairsshortestpaths;

/*
 * Runtime: Theta(V^3)
 * 
 * Negative weight edges may be present, but we assume there are no
 * negative weight cycles.
 * 
 * In Floyd-Warshall, we use a different characterization of the structure of
 * a shortest path than we used in the matrix-multiplication-based all-pairs
 * algorithms. The algorithm considers the "intermediate" vertices of a
 * shortest path, where an intermediate vertex of a simple path p = (v1,v2,...,vL)
 * is any vertex of p other than v1 or vL, that is any vertex in the set {v2,v3,...vL-1}
 * 
 * If k is not an intermediate vertex of path p, then all intermediate vertices of
 * path p are in the set {1,2,...,k-1}. Thus, a shortest path from vertex i
 * to vertex j with all intermediate verticies in the set {1,2,...,k-1} is also
 * a shortest path from i to j with all intermediate vertices in the set {1,2,...,k}
 * 
 * If k is an intermediate vertex of path p, then we break p down into 
 * i (p1, some possibly indirect path to) k (p2, some possibly indirect path to) j.
 * By lemma 24.1, p1 is a shortest path from i to k with all intermediate vertices in 
 * the set {1,2,...,k}. Because vertex k is not an an intermediate vertex of path p1
 * , we see that p1 is a shortest path from i to k with all intermediate vertices in 
 * the set {1,2,...,k-1}. Similarly, p2 is a shortest path from vertex k to vertex j
 * with all intermediate vertices in the set {1,2,...,k-1}
 */
public class FloydWarshall {

	/*
	 * returns a matrix of shortest path pairs for all vertices
	 */
	public int[][] process(int[][] input, int rows) {
		int[][] d = new int[rows][rows];
		
		// set d[][] to input[][]
		for(int i = 0; i < rows; i++) {
			System.arraycopy(input[i], 0, d[i], 0, rows); // the graph is n x n, ie rows x rows
		}
		
		for(int k = 0; k < rows; k++) {
			for(int i = 0; i < rows; i++) {
				for(int j = 0; j < rows; j++) {
					// make sure we don't wrap around from infinity to negative infinity
					if(d[i][k] == Integer.MAX_VALUE || d[k][j] == Integer.MAX_VALUE) {	
						d[i][j] = Math.min(d[i][j], Integer.MAX_VALUE );
					} else {
						d[i][j] = Math.min(d[i][j], (d[i][k] + d[k][j]) );
					}		
				}
			}
		}
		
		return d;
	}
	
	public static void main(String[] args) {
		FloydWarshall fw = new FloydWarshall();
		
		int[][] input = {
				{0,3,8,Integer.MAX_VALUE,-4},
				{Integer.MAX_VALUE,0,Integer.MAX_VALUE,1,7},
				{Integer.MAX_VALUE,4,0,Integer.MAX_VALUE,Integer.MAX_VALUE},
				{2,Integer.MAX_VALUE,-5,0,Integer.MAX_VALUE},
				{Integer.MAX_VALUE,Integer.MAX_VALUE,Integer.MAX_VALUE,6,0}
		};
		
		int[][] output = fw.process(input, 5);
		System.out.println();
	}
}
