package com.saiyd.graph2.alg.allpairsshortestpaths;

import com.saiyd.graph2.Edge;
import com.saiyd.graph2.Graph;
import com.saiyd.graph2.Graph.Type;
import com.saiyd.graph2.alg.singlesourceshortestpath.BellmanFord;
import com.saiyd.graph2.alg.singlesourceshortestpath.Dijkstra;
import com.saiyd.graph2.Vertex;

/* 
 * Johnson's algorithm is a way to find the shortest paths between all pairs 
 * of vertices in a sparse, edge weighted, directed graph. It allows some of 
 * the edge weights to be negative numbers, but no negative-weight cycles may 
 * exist. It works by using the Bellman–Ford algorithm to compute a transformation 
 * of the input graph that removes all negative weights, allowing Dijkstra's 
 * algorithm to be used on the transformed graph.[1][2] It is named after 
 * Donald B. Johnson, who first published the technique in 1977.
 * 
 * 
 * Runtime: O(V^2 * lg V + VE)
 * 
 * 		For sparse graphs, it is asymptotically better than either repeated
 * 		squaring of matrices or the Floyd-Warshall algorithm.
 * 
 * The algorithm either returns a matrix of shortest-path weights for all pairs
 * of vertices or reports that the input graph contains a negative weight cycle.
 * 
 * The algorithm uses the technique of "reweighting". If all edge weights w in
 * graph G = (V,E) are nonnegative, we can find shortest paths between all pairs
 * of vertices by running Dijkstra's algorithm once from each vertex; with the
 * Fibonacci-heap min-priority queue, the running time of this all-pairs algorithm
 * is O(V^2 * lg V + VE). If G has negative weight edges but no negative weight
 * cycles, we simply compute a new set of nonnegative edge weights that allow
 * us to use the same method.
 * 
 * Johnson's algorithm consists of the following steps:[1][2]
 * 
 * 		First, a new node q is added to the graph, connected by zero-weight edges 
 * 		to each of the other nodes.
 * 
 * 		Second, the Bellman–Ford algorithm is used, starting from the new vertex q, 
 * 		to find for each vertex v the minimum weight h(v) of a path from q to v. If 
 * 		this step detects a negative cycle, the algorithm is terminated.
 * 
 * 		Next the edges of the original graph are reweighted using the values computed 
 * 		by the Bellman–Ford algorithm: an edge from u to v, having length w(u,v), is 
 * 		given the new length w(u,v) + h(u) − h(v).
 * 
 * 		Finally, q is removed, and Dijkstra's algorithm is used to find the shortest 
 * 		paths from each node s to every other vertex in the reweighted graph.
 */
public class JohnsonsAlgorithmForSparseGraphs {

	/*
	 * returns a matrix of shortest path pairs for all vertices
	 */
	public int[][] process(int[][] graph, int rows) {
		/* Here we create a graph G' with a new node with zero weight edges to all
		 * nodes. Since java initialized ints to zero we don't have
		 * to do much work. we copy into a matrix that is
		 * n+1 x n+1
		 */
		int[][] gPrime = new int[rows+1][rows+1];
		for(int i = 1; i <= rows; i++) {
			gPrime[i][0] = Integer.MAX_VALUE;  // there is no edge from existing nodes to the new node
			System.arraycopy(graph[i-1], 0, gPrime[i], 1, rows); // the graph is n x n, ie rows x rows
		}
		
		// create a graph object for use with bellman-ford
		Graph<Integer, Vertex<Integer>> gPrimeGraph = Graph.newInstance(gPrime, rows+1, Type.DIRECTED);
		BellmanFord<Integer, Vertex<Integer>> bellman = new BellmanFord<>();
		if(!bellman.execute(gPrimeGraph, gPrimeGraph.getVertex(0)))
			return null; // the graph contains a negative cycle
			
		// else
		
		Graph<Integer, Vertex<Integer>> gGraph = Graph.newInstance(graph, rows, Type.DIRECTED);
		Edge<Integer> origE = null;
		// reweight the original graph
		for(Edge<Integer> e : gPrimeGraph.getEdgeList()) {
			int hu = bellman.getDistanceMap().get(e.getSrcVertex());
			int hv = bellman.getDistanceMap().get(e.getDstVertex());
			int newWeight = e.getWeight() +  hu - hv;
			e.setWeight(newWeight); // we don't really need to do this step
			
			/* find the edge in the original graph
			 * 
			 * Minus one on each id, because vertex zero in g is vertex 1 in g prime
			 */
			origE = gGraph.find(e.getSrcVertex().getId() - 1, e.getDstVertex().getId() -  1); 
			if(origE != null)
				origE.setWeight(newWeight);
		}
		
		int[][] distanceMatrix = new int[rows][rows];
		
		// run dijkstra from each vertex on the original graph
		Dijkstra<Integer, Vertex<Integer>> dijkstra = new Dijkstra<>();
		Integer hu = null;
		Integer hv = null;
		
		for(Vertex<Integer> u  : gGraph.getVertices()) {
			dijkstra.execute(gGraph, u);
			for(Vertex<Integer> v  : gGraph.getVertices()) {
				int spFromUtoV = dijkstra.getDistanceMap().get(v);
				/*
				 * bellman ford was run on gPrim which has a new node at index zero
				 * we need to adjust the id of all nodes to acquire the correct
				 * distance calculation for graph g
				 */
				u.setId(u.getId() + 1);
				v.setId(v.getId() + 1);
				hu = bellman.getDistanceMap().get(u);
				hv = bellman.getDistanceMap().get(v);
				u.setId(u.getId() - 1);
				v.setId(v.getId() - 1);
				
				if(hu != null && hv != null)
					distanceMatrix[u.getId()][v.getId()] = spFromUtoV + hv - hu;
					
			}
		}
			
		
		return distanceMatrix;
	}
	
	public static void main(String[] args) {
		JohnsonsAlgorithmForSparseGraphs jafsg = new JohnsonsAlgorithmForSparseGraphs();
		int[][] input = {
				{0,3,8,Integer.MAX_VALUE,-4},
				{Integer.MAX_VALUE,0,Integer.MAX_VALUE,1,7},
				{Integer.MAX_VALUE,4,0,Integer.MAX_VALUE,Integer.MAX_VALUE},
				{2,Integer.MAX_VALUE,-5,0,Integer.MAX_VALUE},
				{Integer.MAX_VALUE,Integer.MAX_VALUE,Integer.MAX_VALUE,6,0}
		};
		
		int[][] distanceMatrix = jafsg.process(input, input.length);
		System.out.println(distanceMatrix);
	}
}
