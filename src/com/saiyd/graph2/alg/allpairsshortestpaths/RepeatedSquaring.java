package com.saiyd.graph2.alg.allpairsshortestpaths;

/*
 * Here we present a dynamic-programming algorithm for all-pairs shortest-paths
 * problem on a directed graph. Each major loop of the dynamic program will
 * invoke an operation that is very similar to matrix multiplication, so
 * that the algorithm will look like repeated matrix multiplication. 
 * 
 * Dynamic programming steps
 * 
 * 	1. Characterize the structure of an optimal solution
 * 	2. Recursively define the value of an optimal solution
 * 	3. Compute the value of an optimal solution in a bottom up fashion
 * 
 * The structure of a shortest path
 * 
 * 	For the all-pairs shortest-paths problem , we have proven that all
 * 	subpaths of a shortest path are shortest paths. Suppose that the graph is
 * 	represented by an adjacency matrix W = (wij). Consider a shortest path p
 * 	from vertex i to vertex j, and suppose that p contains at most m edges.
 * 	Assuming that there are no negative-weight cycles, m is finite. If i = j,
 * 	then p has weight 0 and no edges. If vertices i and j are distinct, then we
 * 	decompose path p into i (p', some possibly indirect path to) k (direct edge to) j,
 * 	where p' now contains at most m -1 edges. By Lemma 24.1, p' is a shortest path
 * 	from i to k, and so shortestPath(i,j) = shortestPath(i,k) + weightOfEdge(k,j).
 * 
 * A recursive solution to the all-pairs shortest-paths problem
 *  
 *  Now, let Lij(m) be the minimum weight of any path from vertex i to vertex j that
 *  contains at most m edges. When m = 0, there is a shortest path from i to j with
 *  no edges if and only if i = j.
 *  	
 *  	Lij(0) = { 0  if i = j ,  infinity  if i != j }
 *  
 *   For m >= 1, we compute Lij(m) as the minimum of Lij(m-1) (weight of the shortest
 *   path from i to j consisting of at most m-1 edges) and the minimum weight of any
 *   path from i to j consisting of at most m edges, obtained by looking at all 
 *   possible predecessors k of j.
 *   
 *   	Lij(m) = min(1<=k<=n) of { Lik(m-1) + wkj }
 *   
 *   If the graph contains no negative weight cycles, then for every pair of vertices
 *   i and j for which shortestpath(i,j) < infinity, there is a shortest path from i to j
 *   that is simple and thus contains at most n-1 edges. A path from vertex i to vertex j
 *   with more than n-1 edges cannot have lower weight than a shortest path from i to j.
 *   The actual shortest path weights are therefore given by
 *   
 *   	shortestPath(i,j) = Lij(n-1) = Lij(n) = Lij(n+1) = ...
 *   
 * Repeated Squaring
 * 
 *   	Our goal is not to compute all L matrices: we are interested only in matrix L(n-1)
 *   	Recall that the absence of negative weight cycles implies L(m) = L(n-1) for all
 *   	integers m >= n - 1. Just as traditional matrix multiplication is associative, so
 *   	is the matrix multiplication defined by extendShortestPaths(). Therefore, we can
 *   	compute L(n-1) with only ceil(lg(n-1)) matrix products.
 *   
 *   		L(1) = W
 *   		L(2) = W^2 = W * W
 *   		L(4) = W^4 = W^2 * W^2
 *   		L(8) = W^8 = W^4 * W^4
 * Runtime:
 * 		Theta(n^3 lg n)
 * 
 * 		Since each, ceil(lg(n-1)) matrix products takes Theta(n^3) time.
 */
public class RepeatedSquaring {

	/*
	 * returns a matrix of shortest path pairs for all vertices
	 */
	public int[][] process(int[][] input, int rows) {
		int m = 1;
		while(m < (rows -1)) {
			input = extendShortestPaths(input, input, rows);
			m = 2 * m;
		}
		return input;
	}

	private int[][] extendShortestPaths(int[][] input, int[][] input2, int rows) {
		int[][] LPrime = new int[rows][rows];
		
		for(int i = 0; i < rows; i++) {
			for(int j = 0; j < rows; j++) {
				LPrime[i][j] = Integer.MAX_VALUE;
				for(int k = 0; k < rows; k++) {
					// make sure we don't wrap around from infinity to negative infinity
					if(input[i][k] == Integer.MAX_VALUE || input[k][j] == Integer.MAX_VALUE) {	
						LPrime[i][j] = Math.min(LPrime[i][j], Integer.MAX_VALUE );
					} else {
						LPrime[i][j] = Math.min(LPrime[i][j], (input[i][k] + input[k][j]) );
					}
				}
			}
		}
		return LPrime;
	}
	
	private int enforceInfinityRules(int dSrc, int w) {
		// enforce infinity rules
		// positive infinity + any number = positive infinity
		// negative infinity - any number = negative infinity
		if(dSrc == Integer.MAX_VALUE)
			w = 0;
		return w;
	}
	
	public static void main(String[] args) {
		RepeatedSquaring rs = new RepeatedSquaring();
		
		int[][] input = {
				{0,3,8,Integer.MAX_VALUE,-4},
				{Integer.MAX_VALUE,0,Integer.MAX_VALUE,1,7},
				{Integer.MAX_VALUE,4,0,Integer.MAX_VALUE,Integer.MAX_VALUE},
				{2,Integer.MAX_VALUE,-5,0,Integer.MAX_VALUE},
				{Integer.MAX_VALUE,Integer.MAX_VALUE,Integer.MAX_VALUE,6,0}
		};
		
		int[][] output = rs.process(input, 5);
		System.out.println();
	}
}
