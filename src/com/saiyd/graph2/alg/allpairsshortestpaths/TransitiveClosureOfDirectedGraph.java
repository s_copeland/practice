package com.saiyd.graph2.alg.allpairsshortestpaths;

/*
 * Given a directed graph G = (V,E) with vertex set V = {1,2,...,n}, we may wish
 * to find out whether there is a path in G from i to j for all vertex
 * pairs i, j in V.
 * 
 * The transitive closure of G is defined as the graph G* = (V, E*), where
 * 
 * 		E* = {(i,j) : there is a path from vertex i to vertex j in G}
 * 
 * One way to compute the transitive closure of a graph in Theta(n^3) time is
 * to assign a weight of 1 to each edge of E and run the Floyd-Warshall algorithm.
 * If there is a path from vertex i to vertex j, we get Dij < n. Otherwise, we
 * get Dij = infinity.
 * 
 * There is another Theta(n^3) way of computing the transitive closure that can
 * save time and space. This method involves substitution of the logical 
 * operations OR and AND for the arithmetic operations min and + in the Floyd-Warshall
 * algorithm. 
 * 
 * For i, j, k = 1,2,...,n we define Tij(k) to be 1 if there exists a path in graph
 * G from vertex i to vertex j with all intermediate vertices in the set {1,2,...,k},
 * and 0 otherwise. We construct the transitive closure G* = (V, E*) by putting
 * edge (i,j) into E* if and only if Tij(n) = 1
 */
public class TransitiveClosureOfDirectedGraph {

	/*
	 * returns a transitive closure of the input graph
	 */
	public int[][] process(int[][] input, int vertexCount) {
		// initialize...
		for(int i = 0; i < vertexCount; i++) {
			for(int j = 0; j < vertexCount; j++) {
				if(i == j || input[i][j] != Integer.MAX_VALUE) {
					input[i][j] = 1;
				} else {
					input[i][j] = 0;
				}
			}
		}
		
		for(int k = 0; k < vertexCount; k++) {
			for(int i = 0; i < vertexCount; i++) {
				for(int j = 0; j < vertexCount; j++) {
					input[i][j] = input[i][j] | (input[i][k] & input[k][j]);	
				}
			}
		}		
		return input;
	}
	
	public static void main(String[] args) {
		TransitiveClosureOfDirectedGraph tc = new TransitiveClosureOfDirectedGraph();
		
		int[][] input = {
				{0,3,8,Integer.MAX_VALUE,-4},
				{Integer.MAX_VALUE,0,Integer.MAX_VALUE,1,7},
				{Integer.MAX_VALUE,4,0,Integer.MAX_VALUE,Integer.MAX_VALUE},
				{2,Integer.MAX_VALUE,-5,0,Integer.MAX_VALUE},
				{Integer.MAX_VALUE,Integer.MAX_VALUE,Integer.MAX_VALUE,6,0}
		};
		
		int[][] output = tc.process(input, 5);
		System.out.println();
	}
}
