package com.saiyd.graph2.alg.maxflow;

import com.saiyd.graph2.Edge;
import com.saiyd.graph2.Graph;
import com.saiyd.graph2.Vertex;
import com.saiyd.graph2.Graph.EdgeDirection;
import com.saiyd.graph2.Graph.Type;

public class BipartiteMatching {

	/*
	 * Example: Five applicants have been interviewed for five job openings.
	 * The applicants have listed the jobs for which they are qualified.
	 * The task is to match applicants to jobs such that each job opening
	 * is assigned to exactly one qualified applicant. 
	 * 
	 * We can use the Ford-Fulkerson method to solve this problem.
	 * 
	 * This technique is known as problem reduction. We reduce the Bipartite
	 * Matching problem to a Maximum Flow problem.
	 * 
	 * In Bipartite matching, selecting a match between an element of S and
	 * an element of T prevents either element from being matched again. To
	 * produce the same behavior in a flow network graph we construct the 
	 * graph as follows.
	 * 
	 * set S in the Bipartite matching problem has N vertices
	 * set T in the Bipartite matching problem has M vertices
	 * 
	 * The flow network graph will have N + M + 2 vertices
	 * 		N from S
	 * 		M from T
	 * 		A new source 
	 * 		A new sink
	 * 
	 * The flow network graph will have N + M + K edges
	 * 		N from the new source to each element of S, each with a flow of 1
	 * 		M to the new sink from each element of T, each with a flow of 1
	 * 		L edges from the parings in the bipartite problem, each with a flow of 1
	 * 
	 * We claim that computing the Max Flow in the graph will produce a
	 * maximum matching set for the original bipartite matching problem.
	 * 
	 * In the modified graph we find the vertices in S that have an edge to a 
	 * vertex in T with a flow of one. We edges are the bipartite match
	 * 
	 * Analysis:
	 * 	The resulting graph was converted from the original in N + M + K steps.
	 * 	To determine the edges in the bipartite match requires K steps (well not in my algorithm) :(
	 */
	public static void main(String[] args) {
		Graph<String, Vertex<String>> graph = Graph.loadGraph("bipartite-to-max-flow.json", Type.DIRECTED, EdgeDirection.NORMAL);
		final String S_SET = "S";
		final String T_SET = "T";
		
		// convert the bipartite graph to a flow network, 
		// add a new source and sink, we assume there is no node with id zero
		Vertex<String> source = new Vertex<String>(0, "s", 0, true, false);
		graph.addVertex(source);
		graph.setSourceVertex(source);
		
		// we assume there is no node with id of 100
		Vertex<String> sink = new Vertex<String>(100, "t", 0, false, true);
		graph.addVertex(sink);
		graph.setSinkVertex(sink);
		
		// add edges from the new source to vertices in set S
		Edge<String> edge = null;
		
		for(Vertex<String> v : graph.getVertices()) {
			if(v.getPartition() != null && v.getPartition().equals(S_SET)) {
				edge = new Edge<String>(source, v, 0, 1, 0, 0);
				graph.addEdge(edge);
			} else if(v.getPartition() != null && v.getPartition().equals(T_SET)) {
				edge = new Edge<String>(v, sink, 0, 1, 0, 0);
				graph.addEdge(edge);
			}
		}
		
		FordFulkersonMethod<String> edmondsKarp = new FordFulkersonMethod<String>(graph);
		
		if(edmondsKarp.maxFlowViaEdmondsKarp()) {
			System.out.println("The max flow via Edmonds Karp is " + edmondsKarp.getMaxFlow());
		} else {
			System.out.println("There is no flow possible for this graph");
		}
		
		// find the vertices in S that have an edge to a vertex in T with a flow of one
		for(Vertex<String> v : graph.getVertices()) {
			if(v.getPartition() != null && v.getPartition().equals(S_SET)) {
				// check if we have an edge to T
				for(Edge<String> e : v.getForwardEdges()) {
					if(e.getDstVertex().getPartition().equals(T_SET) && e.getFlow() == 1)
						System.out.println(v.getLabel() + " matches to " + e.getDstVertex().getLabel());
				}
			}
		}
		
		System.out.println("-------Answer-------");
		System.out.println("a matches to z");
		System.out.println("b matches to x");
		System.out.println("c matches to y");
	}

}
