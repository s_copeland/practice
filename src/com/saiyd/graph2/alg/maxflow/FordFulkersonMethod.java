package com.saiyd.graph2.alg.maxflow;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.Stack;

import com.saiyd.datastructures.BinaryHeap;
import com.saiyd.datastructures.BinomialHeap;
import com.saiyd.datastructures.BinomialTree;
import com.saiyd.datastructures.BinomialTree.Node;
import com.saiyd.graph2.Edge;
import com.saiyd.graph2.Graph;
import com.saiyd.graph2.Vertex;
import com.saiyd.graph2.Graph.EdgeDirection;
import com.saiyd.graph2.Graph.Type;
import com.saiyd.graph2.alg.ISearch;

/*
 * Runtime: O(E * maximumFlow)
 */
public class FordFulkersonMethod <T extends Comparable<T>> {
	private final Graph<T,Vertex<T>> graph;
    private Map<Vertex<T>, Boolean> markedMap;     // marked[v] = true iff s->v path in residual graph
    private Map<Vertex<T>,Edge<T>> edgeTowardSrcMap;    // edgeTo.get(v) = last edge on shortest residual s->v path
    private int maxFlow;         // current value of max flow
    private int cost;
	private final Vertex<T> source;
	private final Vertex<T> sink;
	
	/*
	 * Construct an instance to compute the maximum flow across the
	 * graph, using the given search method to find augmenting paths.
	 */
	public FordFulkersonMethod(Graph<T,Vertex<T>> graph) {
		this.graph = graph;
		source = graph.getVertex(graph.getSrcVertexId());
		sink = graph.getVertex(graph.getSinkVertexId());
	}
	
	/*
	 * Compute maximal flow for the flow network
	 * 
	 * AKA The Edmonds Karp algorithm
	 */
	public boolean maxFlowViaEdmondsKarp() {
		boolean foundAnAugmentingPath = false;

		Vertex<T> v = null;
        // while there exists an augmenting path, use it
        maxFlow = excess(sink);
		while(findAugmentingPathViaBFS()) {
			// compute bottleneck capacity
            int bottle = Integer.MAX_VALUE;
            for (v = sink; v != source; v = edgeTowardSrcMap.get(v).getOtherEnd(v)) {
                bottle = Math.min(bottle, edgeTowardSrcMap.get(v).residualCapacityTo(v));
            }

            // augment flow
            for (v = sink; v != source; v = edgeTowardSrcMap.get(v).getOtherEnd(v)) {
            	edgeTowardSrcMap.get(v).addResidualFlowTo(v, bottle); 
            }
            
            maxFlow += bottle;
            foundAnAugmentingPath = true;
		}
		return foundAnAugmentingPath;
	}
	
	/*
	 * Compute maximal flow for the flow network
	 * 
	 * AKA The Ford Fulkerson method
	 */
	public boolean maxFlowViaFordFulkerson() {
		boolean foundAnAugmentingPath = false;

		Vertex<T> v = null;
        // while there exists an augmenting path, use it
        maxFlow = excess(sink);
		while(findAugmentingPathViaDFS()) {
			// compute bottleneck capacity
            int bottle = Integer.MAX_VALUE;
            for (v = sink; v != source; v = edgeTowardSrcMap.get(v).getOtherEnd(v)) {
                bottle = Math.min(bottle, edgeTowardSrcMap.get(v).residualCapacityTo(v));
            }

            // augment flow
            for (v = sink; v != source; v = edgeTowardSrcMap.get(v).getOtherEnd(v)) {
            	edgeTowardSrcMap.get(v).addResidualFlowTo(v, bottle); 
            }
            
            maxFlow += bottle;
            foundAnAugmentingPath = true;
		}
		return foundAnAugmentingPath;
	}
	
	/*
	 * Compute maximal flow with the minimum cost for 
	 * the flow network
	 * 
	 */
	public boolean maxFlowWithMinCost() {
		boolean foundAnAugmentingPath = false;

		Vertex<T> v = null;
        // while there exists an augmenting path, use it
        maxFlow = excess(sink);
		while(findShortestAugmentingPath()) {
			// compute bottleneck capacity
            int bottle = Integer.MAX_VALUE;
            for (v = sink; v != source; v = edgeTowardSrcMap.get(v).getOtherEnd(v)) {
                bottle = Math.min(bottle, edgeTowardSrcMap.get(v).residualCapacityTo(v));
            }
            
            // augment flow
            for (v = sink; v != source; v = edgeTowardSrcMap.get(v).getOtherEnd(v)) {
            	edgeTowardSrcMap.get(v).addResidualFlowTo(v, bottle);
            	
            	if(edgeTowardSrcMap.get(v).getSrcVertex().equals(v)) {
            		cost -= bottle * edgeTowardSrcMap.get(v).getCost(); // backward edge
        		} else  {
        			cost += bottle * edgeTowardSrcMap.get(v).getCost(); // forward edge
        		} 
            }
            
            maxFlow += bottle;
            foundAnAugmentingPath = true;
		}
		return foundAnAugmentingPath;
	}

    // return excess flow at vertex v
    private int excess(Vertex<T> v) {
        int excess = 0;
        for (Edge<T> e : v.getEdges()) {
            if (v == e.getSrcVertex()) 
            	excess -= e.getFlow();
            else               
            	excess += e.getFlow();
        }
        return excess;
    }

    // is v in the s side of the min s-t cut?
    /**
     * Is vertex <tt>v</tt> on the <tt>s</tt> side of the minimum st-cut?
     * @return <tt>true</tt> if vertex <tt>v</tt> is on the <tt>s</tt> side of the micut,
     *    and <tt>false</tt> if vertex <tt>v</tt> is on the <tt>t</tt> side.
     * @throws IndexOutOfBoundsException unless 0 <= v < V
     */
    public boolean inCut(Vertex<T> v)  {
        //validate(v, marked.length);
    	boolean ret = false;
    	if(markedMap.get(v) != null)
    		ret = true;
        return ret;
    }

    // throw an exception if v is outside prescibed range
    private void validate(int v, int V)  {
        if (v < 0 || v >= V)
            throw new IndexOutOfBoundsException("vertex " + v + " is not between 0 and " + (V-1));
    }
	
	public int getMaxFlow() {
		return maxFlow;
	}

	public int getCost() {
		return cost;
	}

	private boolean findAugmentingPathViaBFS() {
		// begin potential augmenting path at source
		edgeTowardSrcMap = new HashMap<>(graph.getVertices().size());
		markedMap = new HashMap<>(graph.getVertices().size());
		
        // breadth-first search
        Deque<Vertex<T>> queue = new ArrayDeque<>();
        queue.addLast(source);
        markedMap.put(source, true);
        
        Vertex<T> v = null;
        Vertex<T> w = null;
        while (!queue.isEmpty() && markedMap.get(sink) == null) {
            v = queue.removeFirst();

            for (Edge<T> e : v.getEdges()) {
                w = e.getOtherEnd(v);

                // if residual capacity from v to w
                if (e.residualCapacityTo(w) > 0) {
                    if (markedMap.get(w) == null) {
                    	edgeTowardSrcMap.put(w, e);
                    	markedMap.put(w, true);
                        queue.addLast(w);
                    }
                }
            }
        }
        if(markedMap.get(sink) == null)
        	return false;
		return true;
	}
	
	private boolean findAugmentingPathViaDFS() {
		// begin potential augmenting path at source
		edgeTowardSrcMap = new HashMap<>(graph.getVertices().size());
		markedMap = new HashMap<>(graph.getVertices().size());
		
        // breadth-first search
        Stack<Vertex<T>> stack = new Stack<>();
        stack.push(source);
        markedMap.put(source, true);
        
        Vertex<T> v = null;
        Vertex<T> w = null;
        while (!stack.isEmpty() && markedMap.get(sink) == null) {
            v = stack.pop();

            for (Edge<T> e : v.getEdges()) {
                w = e.getOtherEnd(v);

                // if residual capacity from v to w
                if (e.residualCapacityTo(w) > 0) {
                    if (markedMap.get(w) == null) {
                    	edgeTowardSrcMap.put(w, e);
                    	markedMap.put(w, true);
                        stack.push(w);
                    }
                }
            }
        }
        if(markedMap.get(sink) == null)
        	return false;
		return true;
	}
	
	private boolean findShortestAugmentingPath() {
		// begin potential augmenting path at source
		edgeTowardSrcMap = new HashMap<>(graph.getVertices().size());
		markedMap = new HashMap<>(graph.getVertices().size());
		
		Vertex.setUseDistanceCompare(true);
		Vertex<T> min = new Vertex<>(Integer.MIN_VALUE, null, 0);
		Vertex<T> max = new Vertex<>(Integer.MAX_VALUE, null, 0);
		BinomialHeap<Vertex<T>> heap = new BinomialHeap<>(min, max);
		Map<Integer,Boolean> inQueue = new HashMap<>();
		BinomialTree.Node<Vertex<T>> hNode = null;
		
		// initial the distance from the source to each vertex
		for(Vertex<T> v : graph.getVertices()) {
			if(v.equals(source)) {
				v.setDistance(0);
				hNode = new BinomialTree.Node<>();
				hNode.setData(v);
				v.setLinkingObj(hNode); // we do this so we can use decrease key later
				heap.insert(v);
				inQueue.put(v.getId(), true);
			} else {
				v.setDistance(Integer.MAX_VALUE);
			}
		}
		
		Vertex<T> v = null;
		Vertex<T> w = null;
		while( (v = heap.extractMinValue()) != null) {
			inQueue.put(v.getId(), false);
			
			// we're done when we reach the sink
			if(v.equals(sink))
				break;
			
			for (Edge<T> e : v.getEdges()) {
                w = e.getOtherEnd(v);
                
                if(w.equals(source) || w.equals(v))
                	continue;

                // if residual capacity from v to w
                if (e.residualCapacityTo(w) > 0) {
                	int newDist = 0;
                	if(v.equals(e.getSrcVertex())) {
                		newDist = v.getDistance() + e.getCost();
                	} else {
                		newDist = v.getDistance() - e.getCost();
                	}
                	
                	if(0 <= newDist && newDist < w.getDistance()) {
                		edgeTowardSrcMap.put(w, e);
                		w.setDistance(newDist);
                		if(inQueue.get(w.getId()) != null && inQueue.get(w.getId()) == true) {
                			heap.decreaseKey((Node<Vertex<T>>) w.getLinkingObj(), w);
                		} else {
            				hNode = new BinomialTree.Node<>();
            				hNode.setData(w);
            				w.setLinkingObj(hNode); // we do this so we can use decrease key later
            				heap.insert(w);
            				
            				inQueue.put(w.getId(), true);
                		}
                	}
                }
            }
		}
		
		Vertex.setUseDistanceCompare(false);
		
		return sink.getDistance() != Integer.MAX_VALUE;
	}
	
	public static void main(String[] args) {
		Graph<String, Vertex<String>> graph = Graph.loadGraph("ford-fulkerson-graph-1.json", Type.DIRECTED, EdgeDirection.NORMAL);
		FordFulkersonMethod<String> ek = new FordFulkersonMethod<String>(graph);
		if(ek.maxFlowViaEdmondsKarp()) {
			System.out.println("The max flow via Edmonds-Karp is   " + ek.getMaxFlow());
		} else {
			System.out.println("There is no flow possible for this graph");
		}
		
		graph = Graph.loadGraph("ford-fulkerson-graph-1.json", Type.DIRECTED, EdgeDirection.NORMAL);
		FordFulkersonMethod<String> ffm = new FordFulkersonMethod<String>(graph);
		if(ffm.maxFlowViaFordFulkerson()) {
			System.out.println("The max flow via Ford-Fulkerson is " + ffm.getMaxFlow());
		} else {
			System.out.println("There is no flow possible for this graph");
		}
		
		graph = Graph.loadGraph("min-cost-flow.json", Type.DIRECTED, EdgeDirection.NORMAL);
		FordFulkersonMethod<String> mfc = new FordFulkersonMethod<String>(graph);
		if(mfc.maxFlowWithMinCost()) {
			System.out.println("The max flow is " + mfc.getMaxFlow() + " with min cost of " + mfc.getCost());
		} else {
			System.out.println("There is no flow possible for this graph");
		}
	}
}
