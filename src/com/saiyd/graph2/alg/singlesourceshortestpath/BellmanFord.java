package com.saiyd.graph2.alg.singlesourceshortestpath;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.saiyd.graph2.Edge;
import com.saiyd.graph2.Graph;
import com.saiyd.graph2.Vertex;
import com.saiyd.graph2.Graph.EdgeDirection;
import com.saiyd.graph2.Graph.Type;

/*
 * This algorithm solves the single source shortest path problem in
 * the general case in which edge weights may be negative.
 * 
 * The algorithm returns a boolean value indicating if there is a 
 * negative weight cycle that is reachable from the source. If there
 * is such a cycle, the algorithm indicates that no solution exists.
 * If there is no such cycle, the algorithm produces the shortest
 * paths and their weights.
 * 
 * Runtime: O(V * E)
 * 		Initialization takes Theta(V) time
 * 		Each of the V-1 passes over the edges takes Theta(E)
 * 
 * Requires and adjacency list as input to achieve the runtime performance
 */
public class BellmanFord <T extends Comparable<T>, V extends Vertex<T>> {
	private Map<Vertex<T>,Integer> distance;
	private Map<Vertex<T>,Vertex<T>> predecessor;
	
	public boolean execute(Graph<T, V> graph,  V src) {
		int numOfVertices = graph.getVertices().size();
		Set<V> vertices = graph.getVertices();
		
		distance = new HashMap<>(numOfVertices);
		predecessor = new HashMap<>(numOfVertices);
		
		// initialize
		for(V vertex : vertices) {
			if(vertex != src)
				distance.put(vertex, Integer.MAX_VALUE);
				// predecessor map is already null
		}
		distance.put(src, 0);
		
		// Step 2: relax edges repeatedly
		List<Edge<T>> edgeList = null;
		for(int i = 0; i < numOfVertices -1; i++) {
			edgeList = graph.getEdgeList();
			if(edgeList != null) {
				for(Edge<T> e : edgeList) {
					// relax
					int dDst = distance.get(e.getDstVertex());
					int dSrc = distance.get(e.getSrcVertex());
					int w = e.getWeight();
					
					w = enforceInfinityRules(dSrc, w);

					if(dDst > (dSrc + w)) {
						distance.put(e.getDstVertex(), dSrc + w);
						predecessor.put(e.getDstVertex(), e.getSrcVertex());
					} 
				}
			}
		}
		
		// Step 3: check for negative-weight cycles
		edgeList = graph.getEdgeList();
		if(edgeList != null) {
			for(Edge<T> e : edgeList) {
				int dDst = distance.get(e.getDstVertex());
				int dSrc = distance.get(e.getSrcVertex());
				int w = e.getWeight();
				
				w = enforceInfinityRules(dSrc, w);
				
				if(dDst > (dSrc + w))
					return false;
			}
		}
		
		return true;
	}

	private int enforceInfinityRules(int dSrc, int w) {
		// enforce infinity rules
		// positive infinity + any number = positive infinity
		// negative infinity - any number = negative infinity
		if(dSrc == Integer.MAX_VALUE)
			w = 0;
		return w;
	}
	
	public Map<Vertex<T>, Integer> getDistanceMap() {
		Map<Vertex<T>,Integer> ret = new HashMap<>();
		for(Vertex<T> key : distance.keySet()) {
			ret.put(key, distance.get(key));
		}
		return ret;
	}
	
	public static void main(String[] args) {
		Graph<String, Vertex<String>> graph = Graph.loadGraph("bellmanford-graph.json", Type.DIRECTED, EdgeDirection.NORMAL);
		Vertex<String> src = graph.getVertex(0);
		BellmanFord<String, Vertex<String>> bellman = new BellmanFord<>();
		
		boolean foundAllShortestPaths = bellman.execute(graph, src);
		System.out.println("BellmanFord found all shortest paths from  the src = " + foundAllShortestPaths);
		if(foundAllShortestPaths) {
			System.out.println("Node distance map = " + bellman.getDistanceMap());
			System.out.println("Answer =            {Vertex [t]=2, Vertex [x]=4, Vertex [y]=7, Vertex [z]=-2, Vertex [s]=0}");
		}
		
		graph = Graph.loadGraph("negative-cycle-bellmanford-graph.json", Type.DIRECTED, EdgeDirection.NORMAL);
		src = graph.getVertex(0);
		bellman = new BellmanFord<>();
		
		foundAllShortestPaths = bellman.execute(graph, src);
		System.out.println("BellmanFord found all shortest paths from  the src = " + foundAllShortestPaths);
		if(foundAllShortestPaths) {
			System.out.println("Node distance map = " + bellman.getDistanceMap());
			System.out.println("Answer = THIS SHOULD HAVE RETURNED FALSE!!");
		}
	}
}
