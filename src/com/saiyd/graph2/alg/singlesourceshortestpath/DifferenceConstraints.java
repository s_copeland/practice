package com.saiyd.graph2.alg.singlesourceshortestpath;

/*
 * Here we investigate a special case of linear programming that
 * can be reduced to finding shortest paths from a single source.
 * The single-source shortest path problem that results can then
 * be solved using the Bellman-Ford algorithm, thereby also solving
 * the linear-programming problem.
 * 
 * In the general linear-programming problem, we are given a M x N
 * matrix A an m-vector b, and a n-vector c. We wish to find a vector
 * x of n elements that maximizes the objective function 
 * 	sigma(from i=1 to n) of(ci*xi)
 * subject to the m constraints given by A*x <= b
 * 
 * Sometimes we don't really care about the objective function; we
 * just wish to find any feasible solution, that is, any vector x that
 * satisfies A*x <= b, or to determine that no feasible solution exists.
 * 
 * System of difference constraints - ??
 * 
 * This problem is equivalent to finding the unknowns xi, for 
 * i = 1,2,...,5, such that the following 8 difference constraints are
 * satisfied:
 * 		x1 - x2 <= 0
 * 		x1 - x5 <= -1
 * 		x2 - x5 <= 1
 * 		x3 - x1 <= 5
 * 		x4 - x1 <= 4
 * 		x4 - x3 <= -1
 * 		x5 - x3 <= -3
 * 		x5 - x4 <= -3
 * 
 * One solution to this problem is x = (-5, -3, 0, -1, -4). Another
 * solution is x' = (0, 2, 5, 4, 1). These two solutions are related:
 * each component of x' is larger than the corresponding component of x.
 * This is not a coincidence.
 * 
 * Lemma 24.8
 * 		Let x = (x1, x2,...,xn) be a solution to a system A*x <= b of
 * 		difference constraints, and let d be any constant. Then
 * 		x + d = (x1 + d, x2 + d,...,xn + d) is a solution to A*x <= b
 * 		as well.
 * 
 * Applications
 * 	
 * 	Systems of difference constraints occur in many different applications
 * 	For example, the unknowns xi may be times at which events are to occur.
 * 	Each constraint can be viewed as stating that there must be at least a
 * 	certain amount of time, or at most a certain amount of time, between
 * 	two events.
 * 
 * 	Perhaps the events are jobs to be performed during the assembly of a
 * 	product. If we apply an adhesive that takes 2 hours to set at time x1
 * 	and we have to wait until it sets to install a part at time x2, then
 * 	we have the constraint that x2 >= x1 + 2 or, equivalently, that 
 * 	x1 - x2 <= -2. Alternatively, we might require that the part be installed
 * 	after the adhesive has been applied but not later than the time that the 
 * 	adhesive has set halfway. In this case, we get the pair of constraints
 * 	x2 >= x1 and x2 <= x1 + 1 or, equivalently x1 - x2 <= 0 and x2 - x1 <= 1
 * 
 * Constraint Graphs
 * 
 * 	Given a system A*x <= b of difference constraints, the corresponding
 * 	constraint graph is a weighted, directed graph G = (V,E), where
 * 		V = {v0,v1,...,vn}
 * 			and
 * 		E = { (vi,vj) : xj - xi <= bk is a constraint}
 * 				union { (v0,v1),(v0,v2),(v0,v3),...,(v0,vn) }
 * 
 * 	The additional vertex v0 is incorporated to guarantee that every other
 * 	vertex is reachable from it. Thus, the vertex set V consists of a
 * 	vertex vi for each unknown xi, plus an additional vertex v0. The edge
 * 	set E contains an edge for each difference constraint, plus an edge
 * 	(v0,vi) for each unknown xi.
 * 
 * 	If xj - xi <= bk is a difference constraint, then the weight of edge
 * 	(vi,vj) is weight(vi,vj) = bk. The weight of each edge leaving v0 is 0.
 * 
 * Theorem 24.9
 *
 *	Given a system A*x <= b of difference constraints, let G =(V,E) be the
 *	corresponding constraint graph. If G contains no negative weight cycles,
 *	then 
 *		s() = shortestPath()
 *		x = ( s(v0,v1), s(v0,v1), s(v0,v2),...,s(v0,vn) )
 *
 *	is a feasible solution for the system. If G contains a negative-weight
 *	cycle, then there is no feasible solution for the system.
 *
 *	A system of difference constraints with m constrains on n unknowns 
 *	produces a graph with n+1 vertices and n+m edges. Thus using Bellman-Ford,
 *	we can solve the sytem in O((n+1)(n+m)) = O(n^2 + n*m) time.
 */
public class DifferenceConstraints {

}
