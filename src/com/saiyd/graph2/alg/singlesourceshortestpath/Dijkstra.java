package com.saiyd.graph2.alg.singlesourceshortestpath;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.saiyd.datastructures.BinomialHeap;
import com.saiyd.datastructures.BinomialTree;
import com.saiyd.graph2.ComparableDataVertex;
import com.saiyd.graph2.Edge;
import com.saiyd.graph2.Graph;
import com.saiyd.graph2.Vertex;
import com.saiyd.graph2.Graph.EdgeDirection;
import com.saiyd.graph2.Graph.Type;

/*
 * Dijkstra's algorithm solves the problem on a weighted, directed graph
 * for the case in which all edge weights are nonnegative.
 * 
 * Runtime:
 * 		The runtime depends on the implementation of the priority queue.
 * 		
 * 		If the graph is sufficiently sparse, with E = o(V^2 / lg V), it 
 * 		is practical to implement the priority  queue with a binary heap.
 * 			O( (V + E) lg V), which is O(E lg V) if all vertices are reachable
 * 			from the source.
 * 
 * 		A runtime of O(V lg V + E) can be achieved with a fibonacci heap.With
 * 		this heap the decreaseKey() inside of extractMin() are O(1) vs O(lgV)
 * 		in the binary heap and binomial heap
 * 
 * Requires and adjacency list as input to achieve the runtime performance
 */
public class Dijkstra <T extends Comparable<T>, V extends Vertex<T>> {
	private Map<Vertex<Integer>,Integer> distance;
	private Map<Vertex<Integer>,Vertex<Integer>> predecessor;
	
	public void execute(Graph<T, V> graph,  V src) {
		int numOfVertices = graph.getVertices().size();

		distance = new HashMap<>(numOfVertices);
		predecessor = new HashMap<>(numOfVertices);
		
		Vertex<Integer> min = new Vertex<>(Integer.MIN_VALUE, Integer.MIN_VALUE, 0);
		Vertex<Integer> max = new Vertex<>(Integer.MAX_VALUE, Integer.MAX_VALUE, 0);
		BinomialHeap<Vertex<Integer>> bHeap = new BinomialHeap<>(min, max);
		
		BinomialTree.Node<Vertex<Integer>> node = null;
		// initialize
		for(V v : graph.getVertices()) {
			if(v != src) {
				distance.put((Vertex<Integer>) v, Integer.MAX_VALUE); // I had to cast, there generics are out of whack....
				v.setData((T) new Integer(Integer.MAX_VALUE));
				// predecessor map is already null
				node = new BinomialTree.Node<>();
				node.setData((Vertex<Integer>) v); // I had to cast, there generics are out of whack....
				v.setLinkingObj(node);
				bHeap.insert(node);
			}
		}
		distance.put((Vertex<Integer>) src, 0);
		src.setData((T) new Integer(0));
		node = new BinomialTree.Node<>();
		node.setData((Vertex<Integer>) src); // I had to cast, there generics are out of whack....
		src.setLinkingObj(node);
		bHeap.insert(node);
		
		
		Vertex<Integer> cdv = null;
		while((cdv = bHeap.extractMinValue()) != null) {
			List<Edge<Integer>> edgeList = cdv.getEdges();
			for(Edge<Integer> e : edgeList) {
				// relax
				int dDst = distance.get(e.getDstVertex());
				int dSrc = distance.get(e.getSrcVertex());
				int w = e.getWeight();
				
				// enforce infinity rules
				// positive infinity + any number = positive infinity
				// negative infinity - any number = negative infinity
				if(dSrc == Integer.MAX_VALUE)
					w = 0;
				
				int newEstimate = dSrc + w;
				if(dDst > newEstimate) {
					distance.put(e.getDstVertex(), newEstimate);
					e.getDstVertex().setData(newEstimate);
					predecessor.put(e.getDstVertex(), e.getSrcVertex());
					bHeap.decreaseKey((BinomialTree.Node<Vertex<Integer>>) e.getDstVertex().getLinkingObj(), e.getDstVertex());
				}
			}
			/*
			 * 1. extract the min vertex M
			 * 2. Add M to the set of vertices whose final shortest-path weights from source have already been determined
			 * 3. Loop
			 * 		3.1 for each vertex v that is adjacent to M
			 * 				do relax(u,v,w)
			 */
		}

	}
	
	public Map<Vertex<Integer>, Integer> getDistanceMap() {
		Map<Vertex<Integer>,Integer> ret = new HashMap<>();
		for(Vertex<Integer> key : distance.keySet()) {
			ret.put(key, distance.get(key));
		}
		return ret;
	}
	
	public static void main(String[] args) {
		Graph<String, Vertex<String>> graph = Graph.loadGraph("dijkstra-graph.json", Type.DIRECTED, EdgeDirection.NORMAL);
		Vertex<String> src = graph.getVertex(0);
		Dijkstra<String, Vertex<String>> dijkstra = new Dijkstra<>();
		
		dijkstra.execute(graph, src);
		System.out.println("Dijkstra Single Source Shortest paths");
		System.out.println("Node distance map = " + dijkstra.getDistanceMap());
		System.out.println("Answer =            {Vertex [t]=8, Vertex [x]=9, Vertex [y]=5, Vertex [z]=7, Vertex [s]=0}");

	}
}
