package com.saiyd.graph2.alg.singlesourceshortestpath;

/*
 * Many other problems can be solved by the algorithm
 * for the single-source problem, including the following
 * variants.
 * 
 * Single-destination shortest-path problem
 * 
 * 		Find a shortest path to a given destination vertex T
 * 		from each vertex V. By reversing the direction of each
 * 		edge in the graph, we can reduce this problem to a
 * 		single-source problem.
 * 
 * Single-pair shortest-path problem
 * 
 * 		Find a shortest path from U to V for a given vertices
 * 		U and V. If we solve the single-source problem with
 * 		source vertex U, we solve this problem also. Moreover,
 * 		no algorithms for this problem are known that run
 * 		asymptotically faster than the best single-source 
 * 		algorithms in the worst case.
 * 
 * All-pairs shortest-path problem
 * 
 * 		Find a shortest path from U to V for every pair of vertices
 * 		U and V. Although this problem can be solved by running a
 * 		single-source algorithm once from each vertex, it can
 * 		usually be solved faster. Additionally, its structure is
 * 		of interest in its own right.
 * 
 * Optimal substructure of a shortest path
 * 
 * 		Shortest-path algorithms typically rely on the property that
 * 		a shortest path between two vertices contains other shortest
 * 		paths within it. 
 * 
 * SP(X) is shortestPath(X)
 * W(X) is weight(X)
 * D(X) is a shortestpathEstimate(X)
 * 
 * Properties of shortest paths and relaxation
 * 
 * 		Triangle inequality
 * 			- For any edge (u,v) in E, we have SP(s,v) <= SP(s,u) + W(u,v)
 * 
 * 		Upper-bound property
 * 			We always have D(v) >= SP(s,v) for all vertices v in V, and 
 * 			once D(v) achieves the value SP(s,v), it never changes
 * 
 * 		No-path property
 * 			If there is no path from s to v, then we always have
 * 			D(v) = SP(s,v) = infinity
 * 
 * 		Convergence property
 * 			If the path from s to (possibly not direct) u to (directly) v is
 * 			a shortest path in the graph for some u, v in V, and if 
 * 			D(u) = SP(s,u) at any time prior to relaxing edge (u,v), then 
 * 			D(v) = SP(s,v) at all times afterward.
 * 
 * 		Path-relaxation property
 * 			If p = <v0,v1,...,vk> is a shortest path from s = v0 to vk, and	
 * 			the edges of p are relaxed in the order (v0,v1),(v1,v2),...,(vk-1,vk),
 * 			then D(vk) = SP(s,vk). This property holds regardless of any other
 * 			relaxation steps that occur, even if they are intermixed with 
 * 			relaxations of the edges of p.
 * 
 * 		Predecessor-subgraph property
 * 			Once D(v) = SP(s,v) for all v in V, the predecessor subgraph is a
 * 			shortest-paths tree rooted at s.
 */
public class Info {

}
