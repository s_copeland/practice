package com.saiyd.graph2.alg.singlesourceshortestpath;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.saiyd.graph2.Edge;
import com.saiyd.graph2.Graph;
import com.saiyd.graph2.Vertex;
import com.saiyd.graph2.Graph.EdgeDirection;
import com.saiyd.graph2.Graph.Type;
import com.saiyd.graph2.alg.TopologicalSort;

/*
 * Runtime: Theta(V + E)
 * 	Topological sort can be done in Theta(V + E)
 * 	initialization takes Theta(V)
 * 	There is one iteration per vertex in the loop
 * 		For each vertex, the edges that leave the vertex are each examined exactly one
 * 	There are a total of E iterations of the inner for loop. Because
 * 	each iteration of the inner for loop takes Theta(1) time, the
 * 	total running time is Theta(V + E) 
 * 
 * Requires and adjacency list as input to achieve the runtime performance
 * 
 * It solves related problems, such as finding longest paths.
 * 
 * Single-source longest paths problem in edge-weighted DAGs. We can solve the single-source 
 * longest paths problems in edge-weighted DAGs by initializing the distTo[] values to 
 * negative infinity and switching the sense of the inequality in relax(). 
 * 
 * Critical path method. We consider the parallel precedence-constrained job scheduling 
 * problem: Given a set of jobs of specified duration to be completed, with precedence 
 * constraints that specify that certain jobs have to be completed before certain other 
 * jobs are begun, how can we schedule the jobs on identical processors (as many as needed) 
 * such that they are all completed in the minimum amount of time while still respecting 
 * the constraints?
 * 
 * This problem can be solved by formulating it as a longest paths problem in an 
 * edge-weighted DAG: Create an edge-weighted DAG with a source s, a sink t, and two 
 * vertices for each job (a start vertex and an end vertex). For each job, add an edge 
 * from its start vertex to its end vertex with weight equal to its duration. For each 
 * precedence constraint v->w, add a zero-weight edge from the end vertex corresponding 
 * to v to the beginning vertex corresponding to w. Also add zero-weight edges from the 
 * source to each job's start vertex and from each job's end vertex to the sink.
 */
public class SSSPInDirectedAcyclicGraph <T  extends Comparable<T>, V extends Vertex<T>> {
	private Map<Vertex<T>,Integer> distance;
	private Map<Vertex<T>,Vertex<T>> predecessor;
	
	public void execute(Graph<T, Vertex<T>> graph,  V src) {
		// check if graph is a DAG
		if(graph.getType() != Graph.Type.DIRECTED)
			throw new IllegalArgumentException("The graph must be a DAG");
		
		// TODO - check if we are acyclic
		
		TopologicalSort<T> topSort = new TopologicalSort<T>(); 
		List<Vertex<T>> vertices = topSort.sort(graph);
		
		int numOfVertices = graph.getVertices().size();
		
		distance = new HashMap<>(numOfVertices);
		predecessor = new HashMap<>(numOfVertices);
		
		// initialize
		for(Vertex<T> vertex : vertices) {
			if(vertex != src)
				distance.put(vertex, Integer.MAX_VALUE);
				// predecessor map is already null
		}
		distance.put(src, 0);
		
		List<Edge<T>> edgeList = null;
		Vertex<T> vertex = null;
		for(int i = 0; i < numOfVertices -1; i++) {
			vertex = vertices.get(i);
			edgeList = vertex.getEdges();
			if(edgeList != null) {
				for(Edge<T> e : edgeList) {
					// relax
					int dDst = distance.get(e.getDstVertex());
					int dSrc = distance.get(e.getSrcVertex());
					int w = e.getWeight();
					
					// enforce infinity rules
					// positive infinity + any number = positive infinity
					// negative infinity - any number = negative infinity
					if(dSrc == Integer.MAX_VALUE)
						w = 0;
					
					if(dDst > (dSrc + w)) {
						distance.put(e.getDstVertex(), dSrc + w);
						predecessor.put(e.getDstVertex(), e.getSrcVertex());
					}
				}
			}
		}
		
		/*
		 * topologically sort the vertices of G
		 * initialize
		 * for each vertex u, taken in topological sorted order
		 *    do for each vertex v in adjaceny list of u
		 *        do relax(u,v,w)
		 */
	}
	
	public Map<Vertex<T>, Integer> getDistanceMap() {
		Map<Vertex<T>,Integer> ret = new HashMap<>();
		for(Vertex<T> key : distance.keySet()) {
			ret.put(key, distance.get(key));
		}
		return ret;
	}
	
	public static void main(String[] args) {
		Graph<String, Vertex<String>> graph = Graph.loadGraph("sssp-in-dag-graph.json", Type.DIRECTED, EdgeDirection.NORMAL);
		Vertex<String> src = graph.getVertex(0);
		SSSPInDirectedAcyclicGraph<String, Vertex<String>> ssspDag = new SSSPInDirectedAcyclicGraph<>();
		
		ssspDag.execute(graph, src);
		System.out.println("Single Source Shortest paths in directed acyclic graphs");
		System.out.println("Node distance map = " + ssspDag.getDistanceMap());
		System.out.println("Answer =            {Vertex [t]=2, Vertex [x]=6, Vertex [y]=5, Vertex [z]=3, Vertex [r]=2147483647, Vertex [s]=0}");

	}
}
