package com.saiyd.graph2.alg.spanningtree;

/*
 * The following is the greedy strategy for a generic minimum
 * spanning tree algorithm, which grows the tree one edge at
 * a time. The algorithm manages a set of edges called A, 
 * while maintaining the following loop invariant.
 * 
 * Prior to each iteration, A is a subset of some minimum spanning tree.
 * 
 * A safe edge is an edge that can be added without violating the
 * invariant.
 * 
 */
public class GenericMSTAlgorithm {
	/*
	public List<Edges> genericMST(Graph g, WeightFunction w) {
	 	initialize set A to empty set
	 	
		while A does not form a spanning tree
			do find an edge (u, v) that is safe for A
				Add edge (u, v) to set A
		return A		
	}
	*/
	
	/*
	 * We use the loop invariant as follows:
	 * 
	 * Intialization: After line 18, the set A trivially satisfies the loop invariant
	 * 
	 * Maintenance: The loop in lines 20 - 22 maintaines the invariant by adding
	 * 				only safe edges
	 * 
	 * Termination: All edges added to A are in a minimum spanning tree, and so
	 * 				the set A is returend in line 23 must be a minimum spanning tree
	 */
}
