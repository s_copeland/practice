package com.saiyd.graph2.alg.spanningtree;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.saiyd.graph.datastructures.DisjointSetOfForests;
import com.saiyd.graph.datastructures.DisjointSetTreeVertex;
import com.saiyd.graph2.Edge;
import com.saiyd.graph2.Graph;
import com.saiyd.graph2.Vertex;
import com.saiyd.graph2.Graph.EdgeDirection;
import com.saiyd.graph2.Graph.Type;

/*
 * In this MST algorithm the set of edges named A
 * is a forest. The safe edge added to A is always
 * a least-weight edge in the graph that connects
 * two distinct components
 * 
 * This algorithm is a greedy algorithm, because
 * at each step it adds to the forest an edge of least
 * possible weight. 
 * 
 * The runtime of Kruskals MST is dependent on the 
 * implementation of the disjoin-set data structure.
 * The Disjoint Set of Forest with union-by-rank and
 * path-compression is asymptotically the fastest known.
 * 
 * Runtime is O(E lg E) , where E is the the number of edges
 * 			or
 * 			  O(E lg V)
 */
public class KruskalMST <T extends Comparable<T>, V extends Vertex<T>> {
	private Set<Edge<T>> edgesInMST = new LinkedHashSet<Edge<T>>();
	
	public KruskalMST() {
		
	}
	
	public Set<Edge<T>> findMST(Graph<T, V> graph) {
		DisjointSetOfForests<T> dsof = new DisjointSetOfForests<T>();
		for(V vertex : graph.getVertices()) {
			dsof.makeSet((DisjointSetTreeVertex<T>) vertex); // this is hack, because I'm too tired to make all these classes generic
		}
		
		// sort the edges into nondecreasing order by weight
		List<Edge<T>> edgeList = graph.getEdgeList();
		java.util.Collections.sort(edgeList);
		
		for(Edge<T> curEdge : edgeList) {
			DisjointSetTreeVertex<T> root1 = dsof.find((DisjointSetTreeVertex<T>) curEdge.getSrcVertex());
			DisjointSetTreeVertex<T> root2 = dsof.find((DisjointSetTreeVertex<T>) curEdge.getDstVertex());
			/* 
			 * both edges must be different trees in the disjoint set of forests
			 * 
			 * If both edge belonged to the same tree in the forest, then if could not be added to
			 * the set of edges in the MST without creating a cycle
			 */
			if(!root1.equals(root2)) {
				// add the edge to the MST
				edgesInMST.add(curEdge);
				dsof.union((DisjointSetTreeVertex<T>) curEdge.getSrcVertex(), (DisjointSetTreeVertex<T>) curEdge.getDstVertex());
			}
		}
		return edgesInMST;
	}
	
	public static void main(String[] args) {
		Graph<String, DisjointSetTreeVertex<String>> graph = Graph.loadDSTVGraph("kruskal-graph.json", Type.DIRECTED, EdgeDirection.NORMAL);
		KruskalMST<String,DisjointSetTreeVertex<String>> kruskalMST = new KruskalMST<String,DisjointSetTreeVertex<String>>();
		kruskalMST.findMST(graph);
		System.out.println(kruskalMST.edgesInMST);
	}
}
