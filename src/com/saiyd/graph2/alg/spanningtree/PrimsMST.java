package com.saiyd.graph2.alg.spanningtree;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.saiyd.datastructures.IndexedPriorityQueue;
import com.saiyd.datastructures.PriorityQueue;
import com.saiyd.graph.datastructures.DisjointSetOfForests;
import com.saiyd.graph.datastructures.DisjointSetTreeVertex;
import com.saiyd.graph2.Edge;
import com.saiyd.graph2.Graph;
import com.saiyd.graph2.Vertex;
import com.saiyd.graph2.Graph.EdgeDirection;
import com.saiyd.graph2.Graph.Type;
import com.saiyd.graph2.VertexState;

/*
 * This algorithm has the property that the edges in the set A
 * always form a single tree. 
 * 
 * This algorithm is a greedy algorithm. At each step
 * of the algorithm, one of several possible choices
 * must be made. The greedy strategy advocates making
 * the choice that is the best at the moment. Such a
 * strategy is not generally guaranteed to find globally
 * optimal solutions to problems.
 * 
 * Runtime is O(E + V lg V) , where E is the the number of edges and V is the number of vertices
 * 
 * The performance of Prims depends on how we implement the min
 * priority queue Q. If Q is implemented as a binary heap, we
 * can achieve a runtime of O(E log V).
 * 
 * The performance can be improved by using Fibonacci heaps, which have
 * O(lg V) EXTRACT-MIN time and O(1) DECREASE-KEY time. 
 * 		Runtime: O(E + V lg V)
 * 
 * Applications
 * 
 * Prim's algorithm has many applications, such as in the generation of this maze, which applies Prim's algorithm to a randomly weighted grid graph.
 */
public class PrimsMST <T extends Comparable<T>, V extends Vertex<T>> {
	// node id to edge map
	private Map<Integer,Edge<T>> edgesInMST = new HashMap<Integer, Edge<T>>(); // parent
	private IndexedPriorityQueue<Integer> ipq;
	private Map<Vertex<T>,Integer> vertexToWeightMap; // key
	
	private static class QueueElement <T extends Comparable<T>, V extends Vertex<T>> {
		V vertex;
		boolean isInQueue;
	}
	
	public PrimsMST() {
		
	}
	
	public Map<Integer,Edge<T>> findMST(Graph<T, V> graph, int rootVertexId) {
		if(graph.getType() != Graph.Type.UNDIRECTED)
			throw new IllegalArgumentException("This algorithm only works on undirected graphs");
		
		ipq = new IndexedPriorityQueue<Integer>(IndexedPriorityQueue.Type.MIN);
		vertexToWeightMap = new HashMap< Vertex<T>, Integer>();
		graph.setResetAllVertexState();
		
		for(V vertex : graph.getVertices()) {
			if(vertex.getState() == VertexState.NOT_EXPLORED)
				prim(graph, vertex);
		}
		return edgesInMST;
	}
	
	private void prim(Graph<T,V> graph, V vertex) {
		vertexToWeightMap.put(vertex, 0);
		ipq.insert(vertex.getId(), 0);
		while(!ipq.isEmpty()) {
			int vertexId = ipq.getFrontIndex();
			scan(graph, vertexId);
		}
	}
	
	private void scan(Graph<T,V> graph, int vertexId) {
		V vertex = graph.getVertex(vertexId);
		vertex.setState(VertexState.PARTIAL_EXPLORED);
		for(Edge<T> edge : vertex.getEdges()) {
			Vertex<T> other = edge.getOtherEnd(vertex);
			if(other.getState() != VertexState.NOT_EXPLORED)
				continue;
			
			int curMinEdgeWeight = Integer.MAX_VALUE;
			Integer curMinEdgeWeightObj = vertexToWeightMap.get(other);
			if(curMinEdgeWeightObj != null) {
				curMinEdgeWeight = curMinEdgeWeightObj.intValue();
			}
			
			if(edge.getWeight() < curMinEdgeWeight) {
				vertexToWeightMap.put(other, edge.getWeight());
				edgesInMST.put(other.getId(), edge);
				
				if(ipq.contains(other.getId())) {
					ipq.decreaseKey(other.getId(), vertexToWeightMap.get(other));
				} else {
					ipq.insert(other.getId(), vertexToWeightMap.get(other));
				}
			}
		}
	}
	
	public static void main(String[] args) {
		Graph<String, Vertex<String>> graph = Graph.loadGraph("kruskal-graph.json", Type.UNDIRECTED, EdgeDirection.NORMAL);
		PrimsMST<String,Vertex<String>> primMST = new PrimsMST<String,Vertex<String>>();
		Map<Integer,Edge<String>> mst = primMST.findMST(graph, 0);
		System.out.println(mst);
	}
}
