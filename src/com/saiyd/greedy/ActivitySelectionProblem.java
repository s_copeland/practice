package com.saiyd.greedy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/*
 * The problem of scheduling several competing activities
 * that require exclusive use of a common resource, with the 
 * goal of selecting the maximum size set of mutually
 * compatible activities.
 * 
 *  Each activity has a start time and finish time. If selected
 *  the activity takes place at the half open interval [start, finish)
 *  Including start and up to but not including finish.
 *  
 *  Two activities are compatible if their time intervals do not
 *  overlap
 */
public class ActivitySelectionProblem {
	private static class ActivityInterval implements Comparable<ActivityInterval> {
		private int start;
		private int finish;
		public ActivityInterval(int start, int finish) {
			this.start = start;
			this.finish = finish;
		}
		@Override
		public int compareTo(ActivityInterval other) {
			return this.finish - other.finish;
		}
		@Override
		public String toString() {
			return "[" + start + "," + finish + "] ";
		}
	}
	public List<ActivityInterval> recursiveActivitySelector(ActivityInterval[] activities, int i, int j, List<ActivityInterval> retList) {
		if(retList == null) {
			retList = new ArrayList<ActivityInterval>();
			retList.add(activities[i]);
		}
		int m = i + 1;
		/*
		 * Look for the first activity that is compatible with the activity
		 * indicated by i
		 */
		while(m < j && activities[m].start < activities[i].finish ) {
			m++;
		}
		
		if(m < j) {
			retList.add(activities[m]);
			recursiveActivitySelector(activities, m, j, retList);
			return retList;
		} else {
			return Collections.<ActivityInterval>emptyList();
		}
	}
	
	public List<ActivityInterval> iterativeActivitySelector(ActivityInterval[] activities) {
		List<ActivityInterval> retList = new ArrayList<ActivityInterval>();
		
		int n = activities.length;
		retList.add(activities[0]);
		int i = 0;
		for(int m = 1; m < n; m++) {
			if(activities[m].start >= activities[i].finish) {
				retList.add(activities[m]);
				i = m;
			}
		}
		
		return retList;
	}
	
	public static void main(String[] args) {
		// sort the activities into monotonically increasing finish time
		ActivityInterval[] list = new ActivityInterval[11];
		list[0] = new ActivityInterval(1,4);
		list[1] = new ActivityInterval(3,5);
		list[2] = new ActivityInterval(0,6);
		list[3] = new ActivityInterval(5,7);
		list[4] = new ActivityInterval(3,8);
		list[5] = new ActivityInterval(5,9);
		list[6] = new ActivityInterval(6,10);
		list[7] = new ActivityInterval(8,11);
		list[8] = new ActivityInterval(8,12);
		list[9] = new ActivityInterval(2,13);
		list[10] = new ActivityInterval(12,14);

		ActivitySelectionProblem asp = new ActivitySelectionProblem();
		List<ActivityInterval> optimalSchedule = asp.recursiveActivitySelector(list, 0, list.length, null);
		System.out.println(optimalSchedule);
		
		optimalSchedule = asp.iterativeActivitySelector(list);
		System.out.println(optimalSchedule);
	}
}
