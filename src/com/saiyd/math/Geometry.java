package com.saiyd.math;

public class Geometry {
	public static class Point {
		public double x;
		public double y;
	}
	
	public static double distanceBetweenTwoPoints(Point a, Point b) {
		double diffOfXSquared = Math.pow( (b.x - a.x), 2.0d);
		double diffOfYSquared = Math.pow( (b.y - a.y) , 2.0d);
		return Math.sqrt(diffOfXSquared + diffOfYSquared);
	}
	
	/*
	 *  equation of a circle
	 *  
	 *  To get the radius, it is the same as the distance between two points
	 */
	public double getRadiusSquared(Point center, Point pointOnCircle) {
		double diffOfXSquared = Math.pow( (pointOnCircle.x - center.x), 2.0d);
		double diffOfYSquared = Math.pow( (pointOnCircle.y - center.y), 2.0d);
		
		return diffOfXSquared + diffOfYSquared;
	}
	
	public Point midPointOfLineSegment(Point a, Point b) {
		Point ret = new Point();
		ret.x = (a.x + b.x) / 2.0d;
		ret.y = (a.y + b.y) / 2.0d;
		return ret;
	}
	
	public double slopeOfALine(Point a, Point b) {
		double diffOfY = b.y - a.y;
		double diffOfX = b.x - a.x;
		
		return diffOfY / diffOfX;
	}
	
	/*
	 * Equation of a line
	 * 
	 * slope intercept form
	 * 
	 * 		y = mx + b
	 */
	
	
}
