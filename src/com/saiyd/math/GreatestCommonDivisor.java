package com.saiyd.math;

public class GreatestCommonDivisor {
	public long gcd(long a, long b) {
		long factor = Math.max(a, b);
		for (long loop = factor; loop > 1; loop--) {
			if (a % loop == 0 && b % loop == 0) {
				return loop;
			}
		}
		return 1;
	}
	
	public static void main(String[] args) {
		GreatestCommonDivisor gcm = new GreatestCommonDivisor();
		System.out.println("gcm of 10 and 25 is " + gcm.gcd(10, 25));
	}
}
