package com.saiyd.math;

import java.util.Arrays;

public class Matrix {
	private int[][] table;

	public Matrix(int[][] input) {
		table = input;
	}
	
	public Matrix add(Matrix other) {
		if(table == null || other.table == null)
			return null;
		
		if(table.length != other.table.length)
			return null;
		
		if(table[0] == null || other.table[0] == null)
			return null;
		
		if(table[0].length != other.table[0].length)
			return null;
		
		int[][] m = new int[table.length][table[0].length];
		
		for(int i = 0; i < table.length; i++) {
			for(int j = 0; j < table[0].length; j++) {
				m[i][j] = table[i][j] + other.table[i][j];
			}
		}
		
		return new Matrix(m);
	}
	
	public Matrix subtract(Matrix other) {
		if(table == null || other.table == null)
			return null;
		
		if(table.length != other.table.length)
			return null;
		
		if(table[0] == null || other.table[0] == null)
			return null;
		
		if(table[0].length != other.table[0].length)
			return null;
		
		int[][] m = new int[table.length][table[0].length];
		
		for(int i = 0; i < table.length; i++) {
			for(int j = 0; j < table[0].length; j++) {
				m[i][j] = table[i][j] - other.table[i][j];
			}
		}
		
		return new Matrix(m);
	}
	
	public Matrix negate() {
		if(table == null)
			return null;
		
		int[][] m = new int[table.length][table[0].length];
		
		for(int i = 0; i < table.length; i++) {
			for(int j = 0; j < table[0].length; j++) {
				m[i][j] = -table[i][j];
			}
		}
		
		return new Matrix(m);
	}
	
	public Matrix scalarProduct(int scalar) {
		if(table == null)
			return null;
		
		int[][] m = new int[table.length][table[0].length];
		
		for(int i = 0; i < table.length; i++) {
			for(int j = 0; j < table[0].length; j++) {
				m[i][j] = scalar * table[i][j];
			}
		}
		
		return new Matrix(m);
	}
	
	public Matrix product(Matrix other) {
		if(table == null || other.table == null)
			return null;
		
		if(table[0] == null || other.table[0] == null)
			return null;
		
		// my column count must equals the others row count
		// m x p and p x n
		if(table[0].length != other.table.length)
			return null;
		
		int myRows = table.length;
		int myCols = table[0].length;
		int otherRows = other.table.length;
		int otherCols = other.table[0].length;
		
		// new table of m x n
		int[][] m = new int[table.length][other.table[0].length];
		

		for(int i = 0; i < myRows; i++) {
			for(int j = 0; j < otherCols; j++) {
				for(int k = 0; k < myCols; k++) {
					m[i][j] += table[i][k] * other.table[k][j];
				}
			}
		}
		
		return new Matrix(m);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(table);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Matrix other = (Matrix) obj;
		if (!Arrays.deepEquals(table, other.table))
			return false;
		return true;
	}
	
	/*public boolean equals(int[][] a, int[][] b) {
		boolean ret = false;
		
		if(a == null || b == null)
			return false;
		
		if(a.length != b.length)
			return false;
		
		if(a[0] == null || b[0] == null)
			return false;
		
		if(a[0].length != b[0].length)
			return false;
		
		for()
		
		return ret;
	}*/
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("Matrix\n");
		for(int i = 0; i < table.length; i++) {
			sb.append("[");
			for(int j = 0; j < table[0].length; j++) {
				sb.append(table[i][j]);
				if(j < table[0].length - 1)
					sb.append(", ");
			}
			sb.append("]\n");
		}
		
		return sb.toString();
	}

	public static void main(String[] args) {
		int[][] a = { 
				{-3, 5, 4 },
				{2, 2, -4 },
				{0, 9, -4 }
		};
		int[][] b = { 
				{-3, 5, 4 },
				{2, 2, -4 },
				{0, 9, -4 }
		};
		// different
		int[][] c = { 
				{3, -5, 4 },
				{2, 2, -4 },
				{0, 9, -4 }
		};
		Matrix m = new Matrix(a);
		Matrix n = new Matrix(b);
		
		System.out.println("Test of same matrices = " + m.equals(n));
		
		m = new Matrix(a);
		n = new Matrix(c);
		
		System.out.println("Test of different matrices = " + m.equals(n));
		
		m = new Matrix(null);
		n = new Matrix(null);
		
		System.out.println("Test of null matrices = " + m.equals(n));
		
		System.out.println("---------------------");
		
		m = new Matrix(a);
		n = new Matrix(c);
		System.out.println("Matrix addition");
		System.out.println("Matrix a");
		System.out.println(m);
		System.out.println("Matrix b");
		System.out.println(n);
		System.out.println("Answer");
		System.out.println(m.add(n));
		
		System.out.println("---------------------");
		
		m = new Matrix(a);
		n = new Matrix(c);
		System.out.println("Matrix subtraction");
		System.out.println("Matrix a");
		System.out.println(m);
		System.out.println("Matrix b");
		System.out.println(n);
		System.out.println("Answer");
		System.out.println(m.subtract(n));
		
		System.out.println("---------------------");
		
		m = new Matrix(a);
		System.out.println("Matrix negation");
		System.out.println("Matrix a");
		System.out.println(m);
		System.out.println("Answer");
		System.out.println(m.negate());
		
		System.out.println("---------------------");
		
		m = new Matrix(a);
		System.out.println("Matrix scalar product with 3");
		System.out.println("Matrix a");
		System.out.println(m);
		System.out.println("Answer");
		System.out.println(m.scalarProduct(3));
		
		m = new Matrix(a);
		n = new Matrix(c);
		System.out.println("Matrix product");
		System.out.println("Matrix a");
		System.out.println(m);
		System.out.println("Matrix b");
		System.out.println(n);
		System.out.println("Answer");
		System.out.println(m.product(n));
	}
}
