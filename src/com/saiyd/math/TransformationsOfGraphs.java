package com.saiyd.math;

/*
 * Vertical Shifting
 * 
 * 		The graph of y = f(x) + k, for k > 0, is the same as the graph of
 * 		y = f(x) shifted up k unites;
 * 
 * 		The graph of y = f(x) + k, for k < 0, is the same as the graph of
 * 		y = f(x) shifted down k units
 * 
 * Vertical Stretching and Compression
 * 
 * 		The graph of y = af(x), for a > 1, is the same as the graph of
 * 		y = f(x) stretched, with respect to the y-axis, by a factor of a.
 * 
 * 		The graph of y = af(x), for 0 < a < 1, is the same as the graph of
 * 		y = f(x) compressed, with respect to the y-axis, by a factor of 1/a
 * 
 * Horizontal Shifting
 * 
 * 		The graph of y = f(x + h), for h > 0, is the same as the graph of 
 * 		y = f(x) shifted left h units.
 * 
 * 		The graph of y = f(x - h), for h > 0, is the same as the graph of
 * 		y = f(x) shifted right h units
 * 
 * Horizontal Stretching and Compression
 * 
 * 		The graph of y = f(ax), for a > 1, is the same as the graph of 
 * 		y = f(x) compressed, with respect to the x-axis, by a factor of a.
 * 
 * 		The graph of y = f(ax), for 0 < a < 1, is the same as teh graph of 
 * 		y = f(x) stretched, with respect to the x-axis, by a factor of 1/a
 * 
 * Reflections with repect to a coordinate axis
 * 
 * 		The graph of y = -f(x) is the same as the graph of y = f(x) reflected
 * 		across the x-axis. 
 * 
 * 		The graph of y = f(-x) is the same as the graph of y = f(x) reflected
 * 		across the y-axis.
 * 
 */
public class TransformationsOfGraphs {

}
