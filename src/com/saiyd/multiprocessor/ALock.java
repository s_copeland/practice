package com.saiyd.multiprocessor;

import java.util.concurrent.atomic.AtomicInteger;

/*
 * A simple array based queue lock. The threads share an
 * AtomicInteger field "tail". To acquire the lock, each thread
 * atomically increments tail. The resulting value will be the
 * thread's slot. The slot if used as an index into a boolean
 * flag array. 
 * 
 * If the thread's flag is true, then the thread has permission to
 * acquire the lock. To acquire the lock, a thread spins until the
 * flag at its slot becomes true. To release the lock, the thread
 * sets the flag at its slot to false, and sets the flag at the next
 * slot to true. All arithmetic is module n, where n is at least
 * as large as the maximum number of concurrent threads.
 * 
 * Note:
 * 
 * Contention may still occur because of a phenomenon called false sharing,
 * which occurs when adjacent data items (such as array elements)
 * share a single cache line. A write to one item invalidates that item's
 * cache line, which causes invalidation traffic to processors that are
 * spinning on unchanged but near items that happen to fall in the same cache
 * line. 
 * 
 * One way to avoid false sharing is to pad array elements so that distinct
 * elements are mapped to distinct cache lines. For example we can pad the
 * original flag locations by increasing the flag array fourfold. We increment
 * from one location i to the next by computing 4(i + 1) mod 32 instead of
 * i +1 mod 8
 */
public class ALock {
	private ThreadLocal<Integer> mySlotIndex = new ThreadLocal<Integer>() {
		protected Integer iniitalValue() {
			return 0;
		}
	};
	
	private AtomicInteger tail;
	private boolean[] flag;
	private int size;
	
	public ALock(int capacity) { 
		size = capacity;
		tail = new AtomicInteger(0);
		flag = new boolean[capacity];
		flag[0] = true;
	}
	
	public void lock() {
		int slot = tail.getAndIncrement() % size;
		mySlotIndex.set(slot);
		while(!flag[slot]) {}; // spin
	}
	
	public void unlock() {
		int slot = mySlotIndex.get();
		flag[slot] = false;
		flag[(slot + 1) % size] = true;
	}
}
