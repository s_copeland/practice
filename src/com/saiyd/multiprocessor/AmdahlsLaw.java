package com.saiyd.multiprocessor;

public class AmdahlsLaw {
	/*
	 * n - number of processors
	 * f - fraction of work that can be done in parallel
	 */
	public double calculateSpeedUp(int n, double f) {
		// With n concurrent processors, the parallel part takes time f/n
		
		// The sequential part takes time 1 - f, whatever can't be done in parallel
		
		/* Amdahl's law says that the speed up is the ratio between the
		 * sequential (single-processor) time and the parallel time
		 * 
		 * 1 / ( 1 - f + (f/n))
		 */
		
		double oneMinusF = 1.0d - f;
		double fDividedByN = f / (double) n;
		double sum = oneMinusF + fDividedByN;
		
		return 1.0d / sum;
	}
	
	public static void main(String[] args) {
		AmdahlsLaw al = new AmdahlsLaw();
		
		int processors = 6;
		double fractOfParallelizableWork = 5.0d / 6.0d;
		test(al, processors, fractOfParallelizableWork);
		
		processors = 11;
		fractOfParallelizableWork = 10.0d / 11.0d;
		test(al, processors, fractOfParallelizableWork);
	}

	private static void test(AmdahlsLaw al, int processors,
			double fractOfParallelizableWork) {
		System.out.println("Number of processors = " + processors);
		System.out.println("Fraction of parallizable work = " + fractOfParallelizableWork);
		System.out.println(al.calculateSpeedUp(processors, fractOfParallelizableWork) );
		System.out.println();
	}
}
