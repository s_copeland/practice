package com.saiyd.multiprocessor;

import java.util.concurrent.atomic.AtomicReference;

/*
 * ALock is not space efficient. I requires a know bound of n on the max number of
 * concurrent threads, and it allocates an array of that size per lock. Thus 
 * synchronizing L distinct objects requires O(Ln) space, even if a thread accesses
 * only one lock at a time.
 * 
 * The CLHLock needs only O(L + n) space (if we recylce QNodes and each thread accesses
 * at most one lock at a time).
 * 
 * Like the ALock, this algorithm has each thread spin on a distinct location, so
 * when on thread releases its lock, it invalidates only its successor's cache. This
 * algorithm requires much less space than the ALock, and does not require knowledge of
 * the number of threads that might access the lock. Like ALock it provides first come
 * first serve fairness.
 * 
 * One disadvantage of this lock algorithm is that it performs poorly on cache-less
 * NUMA architectures. Each thread spins waiting for its predecessor's node's locked
 * field to become false. If this memory location is remote, then performance suffers.
 * On cache-coherent architectures, however this approach should work well.
 */
public class CLHLock {
	private class QNode {
		boolean locked;
	}
	
	private AtomicReference<QNode> tail = new AtomicReference<QNode>(new QNode());
	private ThreadLocal<QNode> myPred;
	private ThreadLocal<QNode> myNode;
	
	public CLHLock() {
		tail = new AtomicReference<QNode>(new QNode());
		myNode = new ThreadLocal<QNode>() {
			protected QNode initialValue() {
				return new QNode();
			}
		};
		myPred = new ThreadLocal<QNode>() {
			protected QNode initialValue() {
				return null;
			}
		};
	}
	
	public void lock() {
		QNode qnode = myNode.get();
		qnode.locked = true;
		QNode pred = tail.getAndSet(qnode);
		myPred.set(pred);
		while(pred.locked) {}; // spin
	}
	
	public void unlock() {
		QNode qnode = myNode.get();
		qnode.locked = false;
		myNode.set(myPred.get());
	}
}
