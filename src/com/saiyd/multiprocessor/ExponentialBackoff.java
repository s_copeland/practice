package com.saiyd.multiprocessor;

import java.util.Random;

public class ExponentialBackoff {
	private final int minDelay, maxDelay;
	private int limit;
	private final Random random;
	
	public ExponentialBackoff(int min, int max) {
		minDelay = min;
		maxDelay = max;
		limit = minDelay;
		random = new Random();
	}
	
	public void backOff() throws InterruptedException {
		int delay = random.nextInt(limit);
		limit = Math.min(maxDelay, 2 * limit);
		Thread.sleep(delay);
	}
}
