package com.saiyd.multiprocessor;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class FIFOReadWriteLock {
	private int readAcquires, readReleases;
	private boolean writer;
	Lock lock;
	Condition condition;
	Lock readLock, writeLock;
	
	public FIFOReadWriteLock() {
		readAcquires = readReleases = 0;
		writer = false;
		lock = new ReentrantLock();
		condition = lock.newCondition();
		readLock = new ReadLock();
		writeLock = new WriteLock();
	}
	
	public Lock readLock() {
		return readLock;
	}
	
	public Lock writeLock() {
		return writeLock;
	}
	
	private class ReadLock implements Lock {
		public void lock() {
			lock.lock();
			try {
				readAcquires++;
				while(writer) {
					try {
						condition.await();
					} catch (InterruptedException e) {
						// ignore spurious wake ups
					}
				}
			} finally {
				lock.unlock();
			}
		}
		
		public void unlock() {
			lock.lock();
			try {
				readReleases++;
				if(readAcquires == readReleases)
					condition.signalAll();
			} finally {
				lock.unlock();
			}
		}

		@Override
		public void lockInterruptibly() throws InterruptedException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public Condition newCondition() {
			return lock.newCondition();
		}

		@Override
		public boolean tryLock() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean tryLock(long time, TimeUnit unit)
				throws InterruptedException {
			// TODO Auto-generated method stub
			return false;
		}
	}
	
	private class WriteLock implements Lock {
		public void lock() {
			lock.lock();
			try {
				while(readAcquires != readReleases) {
					try {
						condition.await();
					} catch (InterruptedException e) {
						// ignore spurious wake ups
					}
				}
				writer = true;
			} finally {
				lock.unlock();
			}
		}
		
		public void unlock() {
			writer = false;
		}

		@Override
		public void lockInterruptibly() throws InterruptedException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public Condition newCondition() {
			return lock.newCondition();
		}

		@Override
		public boolean tryLock() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean tryLock(long time, TimeUnit unit)
				throws InterruptedException {
			// TODO Auto-generated method stub
			return false;
		}
	}
}
