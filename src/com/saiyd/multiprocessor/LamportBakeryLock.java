package com.saiyd.multiprocessor;

/*
 * http://en.wikipedia.org/wiki/Lamport%27s_bakery_algorithm
 * 
 * Deadlock free
 * First come first served
 * Satisfies mutual exclusion
 * 
 * In this algorithm tickets act as timestamps: they establish an
 * order among the contending threads.
 * 
 * Why is this not practical
 * 
 * 		The principal drawback is the need to read and write n
 * 		distinct locations, where n (which may be very large) is
 * 	 	the maximum number of concurrent threads.
 */
public class LamportBakeryLock {
	private int[] ticket;
	private boolean[] entering;
	
	public LamportBakeryLock(int numThreads) {
		ticket = new int[numThreads];
		entering = new boolean[numThreads];
	}
	
	public void lock(int threadId) {
		entering[threadId] = true;
		int max = 0;
		// find the max ticket
		for(int i = 0; i < ticket.length; i++) {
			if(ticket[i] > max)
				max = ticket[i];
		}
		
		ticket[threadId] = 1 + max;
		entering[threadId] = false;
		
		for(int i = 0; i < ticket.length; i++) {
			if(i != threadId) {
				while(entering[i])
					Thread.yield(); // wait while other thread picks a ticket
				while(ticket[i] != 0 && (ticket[threadId] > ticket[i] || (ticket[threadId] == ticket[i] && threadId > i) ))
					Thread.yield();
			}
		}
		
		// Enter critical section
	}
	
	public void unlock(int threadId) {
		ticket[threadId] = 0;
	}
}
