package com.saiyd.multiprocessor;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/*
 * A FIFO queue using locks and conditions
 */
public class LockedBoundedQueue<T> {
	private final Lock lock = new ReentrantLock();
	private final Condition notFull = lock.newCondition();
	private final Condition notEmpty = lock.newCondition();
	private final T[] queue;
	private int tail, head, count;
	
	public LockedBoundedQueue(int capacity) {
		queue = (T[]) new Object[100];
	}
	
	public void enqueue(T item) {
		lock.lock();
		try {
			while(count == queue.length) {
				try {
					notFull.await();
				} catch (InterruptedException e) {
					// ignore spurious wakeups
				}
			}
			
			queue[tail] = item;
			if(++tail == queue.length)
				tail = 0;
			++count;
			notEmpty.signal();
		} finally {
			lock.unlock();
		}
	}
	
	public T dequeue() {
		lock.lock();
		try {
			while(count == 0) {
				try {
					notEmpty.await();
				} catch (InterruptedException e) {
					// ignore spurious wake ups
				}
			}
			
			T x = queue[head];
			if(++head == queue.length)
				head = 0;
			--count;
			notFull.signal();
			return x;
		} finally {
			lock.unlock();
		}
	}
}
