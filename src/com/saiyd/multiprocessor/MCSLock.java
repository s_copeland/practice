package com.saiyd.multiprocessor;

import java.util.concurrent.atomic.AtomicReference;

/*
 * Better for NUMA architectures than CLH
 */
public class MCSLock {
	private class QNode {
		boolean locked;
		QNode next;
	}
	
	private AtomicReference<QNode> tail;
	private ThreadLocal<QNode> myNode;
	
	public MCSLock() {
		tail = new AtomicReference<QNode>(null);
		myNode = new ThreadLocal<QNode>() {
			protected QNode initialValue() {
				return new QNode();
			}
		};
	}
	
	public void lock() {
		QNode qnode = myNode.get();
		QNode pred = tail.getAndSet(qnode);
		if(pred != null) {
			qnode.locked = true;
			pred.next = qnode;
			// wait until the predecessor gives up the lock
			while(qnode.locked) {};
		}
	}
	
	public void unlock() {
		QNode qnode = myNode.get();
		if(qnode.next == null) {
			if(tail.compareAndSet(qnode, null))
				return;
			// wait until predecessor fills in its next field
			while(qnode.next == null) {};
		}
		qnode.next.locked = false;
		qnode.next = null;
	}
}
