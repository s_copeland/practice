package com.saiyd.multiprocessor;

/*
 * In computer science, mutual exclusion refers to the requirement of ensuring 
 * that no two concurrent processes[a] are in their critical section at the 
 * same time; it is a basic requirement in concurrency control, to prevent race 
 * conditions. Here, a critical section refers to a period when the process 
 * accesses a shared resource, such as shared memory
 * 
 * 
 * Locks
 * 		A lock is a synchronization mechanism for enforcing limits on access to 
 * 		a resource in an environment where there are many threads of execution. 
 * 		A lock is designed to enforce a mutual exclusion concurrency control policy.
 * 
 * Reentrant mutexes
 * 		A reentrant mutex is a mutual exclusion, recursive lock mechanism. In a 
 * 		reentrant mutex, the same thread can acquire the lock multiple times. 
 * 		However, the lock must be released the same number of times or else other 
 * 		threads will be unable to acquire the lock.
 * 
 * 		Recursive locks (also called recursive thread mutex) are those that allow 
 * 		a thread to recursively acquire the same lock that it is holding. Note that 
 * 		this behavior is different from a normal lock. In the normal case if a thread 
 * 		that is already holding a normal lock attempts to acquire the same lock again, 
 * 		then it will deadlock. Recursive locks behave exactly like normal locks when 
 * 		another thread tries to acquire a lock that is already being held. Note that 
 * 		the recursive lock is said to be released if and only if the number of times it 
 * 		has been acquired matches the number of times it has been released by the owner 
 * 		thread.
 * 
 * 		The Java language's native synchronization mechanisms have used recursive 
 * 		locks since Java's inception. Syntactically, a lock is a block of code with 
 * 		the 'synchronized' keyword preceding it and any Object reference in parentheses 
 * 		that will be used as the mutex. Inside the synchronized block, the given 
 * 		object can be used as a condition variable by doing a wait(), notify(), or 
 * 		notifyAll() on it. Thus all Objects are both recursive mutexes and condition 
 * 		variables.
 * 
 * Semaphores
 * 		A semaphore is a variable or abstract data type that is used for controlling 
 * 		access, by multiple processes, to a common resource in a parallel programming 
 * 		or a multi user environment.
 * 		
 * 		A useful way to think of a semaphore is as a record of how many units of a 
 * 		particular resource are available, coupled with operations to safely (i.e., 
 * 		without race conditions) adjust that record as units are required or become 
 * 		free, and, if necessary, wait until a unit of the resource becomes available. 
 * 		Semaphores are a useful tool in the prevention of race conditions; however, 
 * 		their use is by no means a guarantee that a program is free from these problems. 
 * 		Semaphores which allow an arbitrary resource count are called counting semaphores, 
 * 		while semaphores which are restricted to the values 0 and 1 (or locked/unlocked, 
 * 		unavailable/available) are called binary semaphores.
 * 
 * Semaphores vs. mutexes
 * 		A mutex is essentially the same thing as a binary semaphore and sometimes uses 
 * 		the same basic implementation. The differences between them are in how they 
 * 		are used. While a binary semaphore may be used as a mutex, a mutex is a more 
 * 		specific use-case, which allows extra guarantees:
 * 		
 * 		Mutexes have a concept of an owner. Only the process that locked the mutex is 
 * 		supposed to unlock it. If the owner is stored by the mutex this can be verified 
 * 		at runtime.
 * 
 * 		Mutexes may provide priority inversion safety. If the mutex knows its current 
 * 		owner, it is possible to promote the priority of the owner whenever a higher-priority 
 * 		task starts waiting on the mutex.
 * 
 * 		Mutexes may also provide deletion safety, where the process holding the mutex 
 * 		cannot be accidentally deleted.
 * 
 * Monitors
 * 		Monitors are a structured way of combining synchronization and data. A class encapsulates
 * 		both data and methods in the same way that a monitor combines data, methods, and 
 * 		synchronization in a single modular package.
 * 
 * 		A monitor is a synchronization construct that allows threads to have both mutual 
 * 		exclusion and the ability to wait (block) for a certain condition to become true. 
 * 		Monitors also have a mechanism for signalling other threads that their condition 
 * 		has been met. A monitor consists of a mutex (lock) object and condition variables. 
 * 		A condition variable is basically a container of threads that are waiting on a 
 * 		certain condition. Monitors provide a mechanism for threads to temporarily give up 
 * 		exclusive access in order to wait for some condition to be met, before regaining 
 * 		exclusive access and resuming their task.
 * 
 * Message passing
 * 		Message passing sends a message to a process (which may be an actor or object) and 
 * 		relies on the process and the supporting infrastructure to select and invoke the 
 * 		actual code to run. Message passing differs from conventional programming where a 
 * 		process, subroutine, or function is directly invoked by name. 
 * 
 * Tuple space
 * 		A tuple space is an implementation of the associative memory paradigm for 
 * 		parallel/distributed computing. It provides a repository of tuples that can be accessed 
 * 		concurrently. As an illustrative example, consider that there are a group of processors 
 * 		that produce pieces of data and a group of processors that use the data. Producers post 
 * 		their data as tuples in the space, and the consumers then retrieve data from the space 
 * 		that match a certain pattern. This is also known as the blackboard metaphor. Tuple 
 * 		space may be thought as a form of distributed shared memory.
 * 
 * Readers-writer lock
 * 		 A readers-writer (RW) or shared-exclusive lock (also known as a multiple 
 * 		readers/single-writer lock[1] or multi-reader lock[2]) is a synchronization primitive 
 * 		that solves one of the readers-writers problems. A RW lock allows concurrent access for 
 * 		read-only operations, while write operations require exclusive access. This means that 
 * 		multiple threads can read the data in parallel but an exclusive lock is needed for 
 * 		writing or modifying data. When a writer is writing the data, readers will be blocked 
 * 		until the writer is finished writing. A common use might be to control access to a data 
 * 		structure in memory that can't be updated atomically and isn't valid (and shouldn't be 
 * 		read by another thread) until the update is complete.
 * 
 * 		Readers–writer locks are usually constructed on top of mutexes and condition variables, 
 * 		or on top of semaphores.
 * 		
 * 		The read-copy-update (RCU) algorithm is one solution to the readers-writers problem. 
 * 		RCU is wait-free for readers. The Linux-Kernel implements a special solution for few 
 * 		writers called seqlock.
 * 
 * 		RW locks can be designed with different priority policies for reader vs. writer access. 
 * 		The lock can either be designed to always give priority to readers (read-preferring), 
 * 		to always give priority to writers (write-preferring) or be unspecified with regards to 
 * 		priority. These policies lead to different tradeoffs with regards to concurrency and 
 * 		starvation.
 * 		
 * 		Read-preferring RW locks allow for maximum concurrency, but can lead to write-starvation 
 * 		if contention is high. This is because writer threads will not be able to acquire the lock 
 * 		as long as at least one reading thread holds it. Since multiple reader threads may hold 
 * 		the lock at once, this means that a writer thread may continue waiting for the lock 
 * 		while new reader threads are able to acquire the lock, even to the point where the writer 
 * 		may still be waiting after all of the readers which were holding the lock when it first 
 * 		attempted to acquire it have released the lock.
 * 		
 * 		Write-preferring RW locks avoid the problem of writer starvation by preventing any new 
 * 		readers from acquiring the lock if there is a writer queued and waiting for the lock. 
 * 		The writer will then acquire the lock as soon as all readers which were already holding 
 * 		the lock have completed. The downside is that write-preferring locks allows for less 
 * 		concurrency in the presence of writer threads, compared to read-preferring RW locks. 
 * 		Also the lock is less performant because each operation, taking or releasing the lock 
 * 		for either read or write, is more complex, internally requiring taking and releasing two 
 * 		mutexes instead of one. This variation is sometimes also known as "write-biased" 
 * 		readers-writer lock.
 * 		
 * 		Unspecified priority RW locks does not provide any guarantees with regards read vs. 
 * 		write access. Unspecified priority can in some situations be preferable if to allows for 
 * 		a more efficient implementation.
 */
public class MutualExclusion {

}
