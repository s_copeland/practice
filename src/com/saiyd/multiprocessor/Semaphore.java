package com.saiyd.multiprocessor;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Semaphore {
	private final int capacity;
	private int permits;
	private Lock lock;
	private Condition condition;
	
	public Semaphore(int capacity) {
		this.capacity = capacity;
		permits = 0;
		lock = new ReentrantLock();
		condition = lock.newCondition();
	}
	
	public void acquire() {
		lock.lock();
		try {
			while(permits == capacity) {
				try {
					condition.await();
				} catch (InterruptedException e) {
					// ignore spurious wake ups
				}
			}
			permits++;
		} finally {
			lock.unlock();
		}
	}
	
	public void release() {
		lock.lock();
		try {
			permits--;
			condition.signalAll();
		} finally {
			lock.unlock();
		}
	}
}
