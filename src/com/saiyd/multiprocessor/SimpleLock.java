package com.saiyd.multiprocessor;


public class SimpleLock {
	private static final long NOBODY = -1;
    private long owner = NOBODY;

    public synchronized void lock() throws InterruptedException, IllegalStateException {
        throwIf(ownerIsCurrentThread(), "the lock has already been acqired");
        long threadId = Thread.currentThread().getId();
        while (isLocked()) {
            log("waiting for lock on thread", threadId);
            wait();
        }
        owner = threadId;
        log("lock acquired, owner =", owner);
    }

    public synchronized void unlock() throws IllegalStateException {
        throwIf(!isLocked(), "only a locked lock can be unlocked");
        throwIf(!ownerIsCurrentThread(), "only owner can unlock the lock");
        log("unlocking, owner =", owner);
        owner = NOBODY;
        notify();
    }

    private synchronized boolean isLocked() {
        return owner != NOBODY;
    }

    private boolean ownerIsCurrentThread() {
        return owner == Thread.currentThread().getId();
    }

    private void throwIf(boolean condition, String message) throws IllegalStateException {
        if (condition) {
            throw new IllegalStateException(message);
        }
    }

    private void log(String message, long threadId) {
        System.out.println(String.format("MyLock: %s %s", message, threadId));
    }

    
    public static void main(String[] args) throws InterruptedException {
    	SimpleLock sl = new SimpleLock();
    	try {
    		sl.lock();
    		sl.lock();
    	} catch (IllegalStateException e) {
    		System.out.println(e.getLocalizedMessage());
			// this is what we expect
		} finally {
    		sl.unlock();
    	}
    }
}
