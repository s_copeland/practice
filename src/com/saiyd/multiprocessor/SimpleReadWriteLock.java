package com.saiyd.multiprocessor;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class SimpleReadWriteLock  {
	private int readers;
	private boolean writer;
	private Lock lock;
	private Condition condition;
	private Lock readLock, writeLock;
	
	public SimpleReadWriteLock() {
		writer = false;
		readers = 0;
		lock = new ReentrantLock();
		readLock = new ReadLock();
		writeLock = new WriteLock();
		condition = lock.newCondition();
	}
	
	public Lock readLock() {
		return readLock;
	}
	
	public Lock writeLock() {
		return writeLock;
	}
	
	class ReadLock implements Lock {
		public void lock() {
			lock.lock();
			try {
				while(writer) {
					try {
						condition.await();
					} catch (InterruptedException e) {
						// ignore spurious wake ups
					}
				}
				
				readers++;
			} finally {
				lock.unlock();
			}
		}
		
		public void unlock() {
			lock.lock();
			try {
				readers--;
				if(readers == 0)
					condition.signalAll();
			} finally {
				lock.unlock();
			}
		}

		@Override
		public void lockInterruptibly() throws InterruptedException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public Condition newCondition() {
			return lock.newCondition();
		}

		@Override
		public boolean tryLock() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean tryLock(long time, TimeUnit unit)
				throws InterruptedException {
			// TODO Auto-generated method stub
			return false;
		}
	}
	
	class WriteLock implements Lock {
		public void lock() {
			lock.lock();
			try {
				while(readers > 0) {
					try {
						condition.await();
					} catch (InterruptedException e) {
						// ignore spurious wake ups
					}
				}
				
				writer = true;
			} finally {
				lock.unlock();
			}
		}
		
		public void unlock() {
			writer = false;
			condition.signalAll();
		}

		@Override
		public void lockInterruptibly() throws InterruptedException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public Condition newCondition() {
			return lock.newCondition();
		}

		@Override
		public boolean tryLock() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean tryLock(long time, TimeUnit unit)
				throws InterruptedException {
			// TODO Auto-generated method stub
			return false;
		}
	}
}
