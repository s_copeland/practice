package com.saiyd.multiprocessor;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

public class SimpleReentrantLock implements Lock {
	private Lock lock;
	private Condition condition;
	private long owner, holdCount;
	
	public SimpleReentrantLock() {
		// There is no NonReentrant lock in Java, our SimpleLock class does not have a condition support
		//lock = new SimpleLock();
		condition = lock.newCondition();
		owner = 0;
		holdCount = 0;
	}
	@Override
	public void lock() {
		long me = Thread.currentThread().getId();
		lock.lock();
		if(owner == me) {
			holdCount++;
			return;
		}

		while(holdCount != 0) {
			try {
				condition.await();
			} catch (InterruptedException e) {
				// ignore spurious wake ups
			}
		}
		owner = me;
		holdCount = 1;
	}

	@Override
	public void lockInterruptibly() throws InterruptedException {
		// TODO Auto-generated method stub

	}

	@Override
	public Condition newCondition() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean tryLock() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean tryLock(long time, TimeUnit unit)
			throws InterruptedException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void unlock() {
		lock.lock();
		try {
			if(holdCount == 0 || owner != Thread.currentThread().getId())
				throw new IllegalMonitorStateException();
			holdCount--;
			if(holdCount == 0)
				condition.signal();
		} finally {
			lock.unlock();
		}

	}

}
