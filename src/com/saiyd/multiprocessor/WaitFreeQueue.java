package com.saiyd.multiprocessor;

/*
 * This class assumes there is a single thread
 * enqueuer and a single thread dequeuer.
 */
public class WaitFreeQueue<T> {
	private volatile int head = 0, tail = 0;
	private T[] queue;
	
	public WaitFreeQueue(int capacity) {
		queue = (T[]) new Object[capacity];
		head = 0;
		tail = 0;
	}
	
	public void enqueue(T item) throws QueueFullException {
		if(tail - head == queue.length)
			throw new QueueFullException();
		queue[tail % queue.length] = item;
		tail++;
	}
	
	public T dequeue() throws EmptyException {
		if(tail - head == 0)
			throw new EmptyException();
		T ret = queue[head %  queue.length];
		head++;
		return ret;
	}
}
