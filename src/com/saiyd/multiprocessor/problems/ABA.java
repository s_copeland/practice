package com.saiyd.multiprocessor.problems;

/*
 * In multithreaded computing, the ABA problem occurs during synchronization, when 
 * a location is read twice, has the same value for both reads, and "value is the same" 
 * is used to indicate "nothing has changed". However, another thread can execute 
 * between the two reads and change the value, do other work, then change the value 
 * back, thus fooling the first thread into thinking "nothing has changed" even though 
 * the second thread did work that violates that assumption.
 * 
 * The ABA problem occurs when multiple threads (or processes) accessing shared 
 * memory interleave.
 * 
 * Going back to the previous example of Charlie and his briefcase, what could Charlie 
 * have done differently?
 * 
 * There are a number of ways that Charlie could have prevented this from happening, 
 * even though he can't open the briefcase. For one, he could've chained the real briefcase 
 * to his arm. That way, Albert would have to cut the chain to steal the briefcase, and 
 * Charlie would notice the cut chain and sound the alarm. This is what the LL/SC 
 * instruction on some processors attempts to do. Another solution would be for Charlie 
 * to write down the serial number of his real briefcase, and check it after every time 
 * he looks away from it. This is how Double-Word Compare-And-Swap Tagging works. Automatic 
 * Garbage Collection, along with other techniques like Hazard Pointers, deal with this 
 * problem by ensuring that there is no other briefcase in the world that looks like 
 * Charlie's briefcase. When Charlie, his boss, and whoever else cares about the briefcase 
 * don't need it anymore, it is destroyed. Then, and only then, is another briefcase that 
 * looks like his allowed to be created.
 * 
 * Natalie should have been more observant and watched the traffic light at all times. 
 * But such an attempt is inefficient in programming. It would be better to use a traffic 
 * system that notifies all waiting motorists about all changes. Then ABA problem can be 
 * circumvented when all motorists and the traffic system itself are actors in a system 
 * where all changes are sent to all parties that wish to be informed.
 */
public class ABA {

}
