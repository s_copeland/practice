package com.saiyd.numbers;

/**
 * https://en.wikipedia.org/wiki/Base36
 */
public class Base36 {
	private static final int BASE_36 = 36;
	
	private char[] alphabet = { '0', '1', '2', '3', '4', '5', '6', '7', '8',
			'9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L',
			'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y',
			'Z' };
	
	public String convert(long value) {
		// the max char[] supported by a 64 long is 13 chars
		StringBuilder sb = new StringBuilder();
		do {
			int index = (int) (value % BASE_36);
			sb.insert(0, alphabet[index]);
		} while((value /= BASE_36) > 0);
		
		return sb.toString();
	}
	
	public static void main(String[] args) {
		Base36 b36 = new Base36();
		for(int i = 0; i < 40; i++ ) {
			System.out.println(i + " -> " + b36.convert(i));
		}
		for(int i = 70999000; i < 71000000; i++ ) {
			System.out.println(i + " -> " + b36.convert(i));
		}
	}
}
