package com.saiyd.numbers;

import java.util.HashMap;
import java.util.Map;

/**
 * https://en.wikipedia.org/wiki/Base62
 */
public class Base62 {
	private static final int BASE_62 = 62;

	private static final char[] alphabet = { '0', '1', '2', '3', '4', '5', '6', '7', '8',
			'9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L',
			'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y',
			'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
			'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y',
			'z' };
	private static final Map<Character,Integer> alphabetIndexMap = new HashMap<>();
	static {
		for(int i = 0; i < alphabet.length; i++) {
			alphabetIndexMap.put(alphabet[i], i);
		}
	}
	
	public String encode(long value) {
		StringBuilder sb = new StringBuilder();
		int offset = BASE_62 - 1;
		do{
			int index = (int) value % BASE_62;
			sb.insert(0, alphabet[index]);
		} while( (value /= BASE_62) > 0);
		return sb.toString();
	}
	
	public int decode(String s) {
		int i = 0;
		char[] input = s.toCharArray();
		for(int j = 0; j < input.length; j++) {
			i = (i * BASE_62) + alphabetIndexMap.get(input[j]);
		}
		return i;
	}
	
	public static void main(String[] args) {
		Base62 b62 = new Base62();
		String saiyd = "saiyd";
		for(int i = 0; i < saiyd.length(); i++) {
			String ev = b62.encode( saiyd.charAt(i));
			System.out.println(i + " -> " + ev);
			System.out.println(b62.decode(ev) + " <- " + ev);
		}
		
//		for(int i = 70999000; i < 71000000; i++ ) {
//			String ev = b62.encode(i);
//			System.out.println(i + " -> " + ev);
//			System.out.println(b62.decode(ev) + " <- " + ev);
//		}
	}
}
