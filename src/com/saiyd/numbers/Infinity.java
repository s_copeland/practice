package com.saiyd.numbers;

public class Infinity {
	public static void main(String[] args) {
		double one = 1;
		double infinity = Double.POSITIVE_INFINITY;
		
		double add = one + infinity;
		
		System.out.println(add);
	}
}
