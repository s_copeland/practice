package com.saiyd.numbers;

/*
	The square root of a fraction sqrt(9/10) is equal to sqrt(9) / sqrt(10)

	https://www.youtube.com/watch?v=ilWdLd2r1To

	https://en.wikipedia.org/wiki/Methods_of_computing_square_roots
 */
public class SquareRoot {
	public double squareRoot(double input) {
		if(input < 0)
			return -1;
		if(input == 0 || input == 1)
			return input;
		
		double precision = 0.00001; // define the precision, so we stop
		double start = 0;
		double end = input;
		
		// we define these two start/end values because usually 
		// 0 < sqrt(input) < a
		//
		// however, if input < 1, then 
		// 0 < input < sqrt(input
		if(input < 1)
			end = 1;
		
		while(end-start > precision) {
			double mid = (start + end) /2;
			double midSqr = mid * mid;
			if(midSqr == input)
				return mid; // we found the exact value
			else if(midSqr < input)
				start = mid; // focus on the right half
			else
				end = mid; // focus on the left half
		}
		
		return (start + end) / 2;
	}
	
	public static void main(String[] args) {
		SquareRoot sqrt = new SquareRoot();
		System.out.println("square root of 50 = " + sqrt.squareRoot(50));
		System.out.println("square root of 100 = " + sqrt.squareRoot(100));
		System.out.println("square root of 0.9 = " + sqrt.squareRoot(0.9d));

	}

}
