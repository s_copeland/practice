package com.saiyd.numbertheory;

/**
 * In combinatorics, the nth Bell number, named after Eric Temple Bell, 
 * is the number of partitions of a set with n members, or equivalently, 
 * the number of equivalence relations on it. Starting with B0 = B1 = 1, 
 * the first few Bell numbers are:

   1, 1, 2, 5, 15, 52, 203, 877, 4140, 21147, 115975, � (sequence A000110 in OEIS). 
    
 * @author scopeland
 *
 */
public class BellNumber {
	
	public static final int MAX_ROW = 25; // this is the max when using longs before the calculate values wrap back around
	public static final long[][] TABLE;
	static {
		TABLE = new long[MAX_ROW][MAX_ROW];
		TABLE[0][0] = 1;

		int curRowLen = 1;
		for (int i = 0; i < (MAX_ROW - 1); i++) {

			bellRow(curRowLen, TABLE[i][curRowLen -1], TABLE[i], TABLE[i+1]);
			
			curRowLen++;
		}
		/*for(int i=0; i < (MAX_ROW-1); i++) {
			for (int j = 0; j < MAX_ROW; j++) {
				System.out.print(TABLE[i][j] + " ");
			}
			
			System.out.println();
		}*/
	}

	long calculateNth(int nThBellNumber) {
		return TABLE[nThBellNumber][nThBellNumber];
	}
	public static void main(String[] args) {
		BellNumber bn = new BellNumber();
		
		for(int i = 0; i < MAX_ROW; i ++) {
			System.out.println(i + "Th Bell Number = " + bn.calculateNth(i));
		}
	}

	public static void bellRow(int prevRowLen, long prevRowBellNum, long[] prevRow, long[] curRow) {

		// First number is always the bell number of the previous row
		curRow[0] = prevRowBellNum;
		// this row will be one longer than the previous
		int rowLen = ++prevRowLen;
		
		// each following row value is the sum of the column to the left of our current column
		// plus the value in the same column of the previous row
		//
		/*
		 * 1
		 * 1 2
		 * 2 3 5
		 * 5 7 10 15
		 */

		for (int i = 1; i < rowLen; i++) {
			curRow[i] = prevRow[i -1] + curRow[i -1];
		}

	}
}
