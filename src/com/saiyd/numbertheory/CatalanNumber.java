package com.saiyd.numbertheory;

import java.math.BigInteger;

import com.saiyd.counting.Factorial;

public class CatalanNumber {
	/*
	 * Cn = (2n)! / ((n+1)!n!)
	 * 
	 * Cn counts the number of expressions containing n pairs of {X,Y} which are correctly matched
	 * 	 Ex: Dyck words, parenthesis balancing
	 * 
	 * Cn also counts the number of ways of associating n applications of a binary operator
	 * 
	 * Cn is also the number of full binary trees with n + 1 leaves
	 * 
	 * Cn is also the number of non-isomorphic ordered trees with n+1 vertices
	 * 
	 * Cn is the number of monotonic paths along the edges of a grid with NxN square cells which do not pass above the diagonal
	 *   This is equivalent to counting Dyck words where X means "move right" and Y means "move up"
	 *   
	 * Cn is the number of ways a convex poygon with n+2 sides can be cut into triangles by connecting vertices with straight lines
	 * 
	 * Cn is the number of ways to tile a stairstep shape of height n with n rectangles
	 * 
	 * http://en.wikipedia.org/wiki/Catalan_number
	 */
	public long calculate(long nThNumber) {
		Factorial fact = new Factorial();
		
		BigInteger twoNFact = fact.calculateBig(2 * nThNumber);
		BigInteger nPlusOneFact = fact.calculateBig(nThNumber + 1);
		BigInteger nFact = fact.calculateBig(nThNumber);
		
		return twoNFact.divide((nPlusOneFact.multiply(nFact))).longValue();
	}
	
	/*
	 * In how many ways can a regular n-gon be divided into n-2 triangles 
	 * if different orientations are counted separately?"
	 *  
	 * (Euler's polygon division problem). 
	 * 
	 * The solution is the Catalan numberC_(n-2)
	 */
	public long getTheNumberOfWaysARegularPolygonCanBeDividedIntoTriangles(long numOfPolygonSides) {
		if(numOfPolygonSides < 3)
			return 0;
		return calculate(numOfPolygonSides -2);
	}
	
	/*
	 * Ballot Problem
	 * 
	 * Suppose A and B are candidates for office and there are 2n voters, n voting for A and n for B. 
	 * In how many ways can the ballots be counted so that B is never ahead of A? The solution is a 
	 * Catalan number C_n.
	 * 
	 * A related problem also called "the" ballot problem is to let A receive a votes and B b votes 
	 * with a>=b. This version of the ballot problem then asks for the probability that A stays ahead 
	 * of  B as the votes are counted (Vardi 1991). The solution is (a-b+1)/(a+1), as first shown by 
	 * M. Bertrand (Hilton and Pedersen 1991). Another elegant solution was provided by André (1887) 
	 * using the so-called André's reflection method.
	 * 
	 * The problem can also be generalized (Hilton and Pedersen 1991). Furthermore, the TAK function 
	 * is connected with the ballot problem (Vardi 1991).
	 */
	
	/*
	 * Nonnegative Partial Sum
	 * DOWNLOAD Mathematica Notebook
	 * 
	 * Consider the number of sequences that can be formed from permutations of a set of elements such 
	 * that each partial sum is nonnegative. The number of sequences with nonnegative partial sums 
	 * which can be formed from the permutations of n 1s and n -1s (Bailey 1996, Brualdi 1997) is 
	 * given by the Catalan numbers C_n. 
	 * 
	 * For example, the C_3=5 permutations of (-1,-1,-1,1,1,1) having nonnegative partial sums are 
	 * (1,1,1,-1,-1,-1), (1,1,-1,1,-1,-1),  (1,1,-1,-1,1,-1), (1,-1,1,1,-1,-1), and (1, -1, 1, -1, 1, -1).
	 * 
	 * Similarly, the number of nonnegative partial sums of n 1s and k -1s (Bailey 1996) is given by
	 * 
	 * c_(nk)=((n+k)!(n-k+1))/(k!(n+1)!), 
	 */
	
	/*
	 * Catalan's Triangle
	 * Catalan's triangle is the number triangle
	 * 
 	 * 1       
 	 * 1 1      
 	 * 1 2 2    
 	 * 1 3 5 5   
 	 * 1 4 9 14 14 
 	 * 1 5 14 28 42 42  
 	 * 1 6 20 48 90 132 132 	
	 * (1)
	 * (OEIS A009766) with entries given by
	 * 
 	 * c_(nk)=((n+k)!(n-k+1))/(k!(n+1)!) 
 	 * 	
	 * (2)
	 * for 0<=k<=n. 
	 * Each element is equal to the one above plus the one to the left. 
	 * The sum of each row is equal to the last element of the next row and also equal to the 
	 * Catalan number C_n.
	 */
	public static void main(String[] args) {
		CatalanNumber cat = new CatalanNumber();
		
		for(int i=0; i < 22; i++) {
			System.out.println("i = " + i + " " + cat.calculate(i));
		}
	}
}
