package com.saiyd.numbertheory;

/*
 * The general strategy for finding gcd(m,n).
 *   Set m = original n
 *   Set n = original m MOD original n
 *   Try again
 */
public class EuclidGreatestCommonDivisor {
	public int calcViaRecursion(int m, int n) {
		if(m == 0 && n == 0)
			return 0;
		if(n == 0) {
			return m;
		} else {
			return calcViaRecursion(n, m % n);
		}
		
	}
	
	public int calcViaIteration(int m, int n) {
		if(m == 0 && n == 0)
			return 0;
		int a = m;
		int b = n;
		int temp = 0;
		while(b != 0) {
			temp = b;
			b = a % b;
			a = temp;
		}
		return a;
	}
	
	public static void main(String[] args) {
		EuclidGreatestCommonDivisor egcd = new EuclidGreatestCommonDivisor();
		System.out.println(egcd.calcViaRecursion(50, 20));
		System.out.println(egcd.calcViaRecursion(54, 23));
		System.out.println(egcd.calcViaRecursion(512400, 32600));
		System.out.println();
		System.out.println(egcd.calcViaIteration(50, 20));
		System.out.println(egcd.calcViaIteration(54, 23));
		System.out.println(egcd.calcViaIteration(512400, 32600));
	}
}
