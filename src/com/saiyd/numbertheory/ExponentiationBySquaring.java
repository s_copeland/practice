package com.saiyd.numbertheory;

/*
 * http://en.wikipedia.org/wiki/Exponentiation_by_squaring
 */
public class ExponentiationBySquaring {

	/*
	 * a^b mod n
	 * 
	 * we assume int's are 32 bits
	 */
	/*
	 * private static long expBySquaring(int a,int b, int n) { long c = 0; long
	 * d = 1;
	 * 
	 * for(int i = 31; i >= 0; i--) { c = 2 * c; d = (d * d) % n;
	 * 
	 * if( (b & (1 << i)) != 0) { c = c + 1; d = (d * a) % n; } }
	 * 
	 * return d; }
	 */

	static long expBySquaring(long base, long exponent) {
		long result = 1;

		while (exponent > 0) {
			if (exponent % 2 == 1) { // if b is odd
				result *= base;
			}
			exponent >>= 1;
			base *= base;
		}

		return result;
	}

	public static void main(String[] args) {
		System.out.println(expBySquaring(5, 20));

	}

}
