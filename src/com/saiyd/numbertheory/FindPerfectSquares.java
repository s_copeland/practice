package com.saiyd.numbertheory;

public class FindPerfectSquares {

	/*
	 * A perfect square or square number is a number
	 * that is the product of some integer with itself.
	 * 
	 * ex: 9 = 3 x 3 = 3^2 
	 * 
	 * square numbers are non-negative
	 * 
	 * a positive integer that has no perfect square divisors
	 * except 1 is called square-free
	 * 
	 * 
	 * Starting with 1, there are floor(sqrt(m)) square numbers
	 * up to and including m.
	 * 
	 * ex: floor(sqrt(100)) = 10
	 *   There are 10 square numbers up to including 100
	 */
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		for(int i=1; i < 101; i++) {
			int square = i * i;
			if(square <= 100)
				System.out.println(square);
		}
		
	}


	/*
	 * A square number is also the sum of 2 consecutive triangular numbers
	 */
}
