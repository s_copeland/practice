package com.saiyd.numbertheory;

/*
 * Modular exponentiation or the power mod
 * 
 * Exponentiation by squaring, binary exponentiation
 * 
 * Runtime: 
 * 
 * Modular exponentiation problems similar to the one described above are 
 * considered easy to solve, even when the numbers involved are enormous. 
 * On the other hand, computing the discrete logarithm - that is, the task 
 * of finding the exponent e if given b, c, and m - is believed to be difficult. 
 * This one way function behavior makes modular exponentiation a candidate 
 * for use in cryptographic algorithms.
 * 
 * 		Example: finding,      a^x mod n is easy
 * 
 * 					=== means modular congruence ( equivalent for some mod N)
 * 
 * 				 while finding x, where a^x === b, mod n
 * 				 is hard
 * 
 * 5^3 mod 7 = 125 mod 7 = 6
 * 
 * Used in the RSA public key encryption algorithm
 * 
 * Example of how to calculate 5^40 mod 7
 * 
 * 5^40 mod 7 = ?
 * 
 * 5^40 mod 7
 * -----------
 * 5^1 mod 7 = 5  
 * 5^2 mod 7 = 4  (we double the exponent at each step) 
 * ( we know that 5^4 mod 7 is equal to the answer of 5^2 mod 7 squared mod 7)
 * 5^4 mod 7 = 4^2 mod 7 = 2
 * 5^8 mod 7 = 2^2 mod 7 = 4
 * 5^16 mod 7 = 4^2 mod 7 = 2
 * 5^32 mod 7 = 2^2 mod 7 = 4
 * 
 * We stop at 5^32 because the next doubling of the exponent is greater than 40
 * We look to see what powers of 5 we can add together to get to our target of
 * 40. 32 + 8
 * 
 * (5^32 mod 7) * (5^8 mod 7) = 5^40 mod 7 = 
 * 5^40 mod 7 = (4 * 4) mod 7 = 2
 * 
 * The number of steps is the log base 2 of the exponent
 * 		log base 2 40  = 5.321
 * 
 * 
 * Cryptography uses computation mod n a lot, because calculating discrete
 * logarithms and square roots mod n can be hard problems. Modular arithmetic
 * is also eaiser to work with on computers, because it restricts the range
 * of all intermediate values and the result. For a k-bit modulus, n, the
 * intermediate results of any addition, subtraction, or multiplication will
 * not be more than 2k-bits long
 * 
 * 
 * Modular Congruence
 * 
 *  For a positive integer n, two integers a and b are said to be congruent modulo n
 *  if their difference a − b is an integer multiple of n (or n divides a − b) 
 */
public class ModularExponentiation {
	public long modularPower(long base, long exponent, int modulus) {
		
		long ret = 1;
		while(exponent > 0) {
			if(exponent % 2 == 1)
				ret = (ret * base) % modulus;
			exponent = exponent >> 1;
			base = (base * base) % modulus;
		}
		return ret;
	}
	
	public static void main(String[] args) {
		ModularExponentiation me = new ModularExponentiation();
		System.out.println(me.modularPower(5, 20, 7));
	}
}
