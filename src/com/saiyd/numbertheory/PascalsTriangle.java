package com.saiyd.numbertheory;

public class PascalsTriangle {
	public static final int MAX_ROW = 68; // this is the max when using longs before the calculate values wrap back around
	public static final long[][] TABLE;
	static {
		TABLE = new long[MAX_ROW][MAX_ROW];
		TABLE[0][0] = 1;
		TABLE[1][0] = 1;
		TABLE[1][1] = 1;
		
		int n = MAX_ROW;

		for (int i = 0; i < (n - 1); i++) {
			if(i > 0) {
				pascalRow(TABLE[i], TABLE[i+1], i+1);
			}
			
		}
		/*for(int i=0; i < (n-1); i++) {
			for (int j = 0; j < ROWS; j++) {
				System.out.print(TABLE[i][j] + " ");
			}
			
			System.out.println();
		}*/
	}

	public static void main(String[] args) {


	}

	public static void pascalRow(long[] previous, long[] current, int prevRowSize) {

		// First and last numbers in row are always 1
		int currentRowLen = prevRowSize + 1;
		current[0] = 1;
		current[prevRowSize] = 1;

		// The rest of the row can be
		// calculated based on previous row

		for (int i = 1; i < currentRowLen; i++) {
			current[i] = previous[i - 1] + previous[i];
		}

	}
}
