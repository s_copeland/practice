package com.saiyd.numbertheory;

import com.saiyd.counting.Combination;
import com.saiyd.counting.Factorial;

/*
 * In mathematics, particularly in combinatorics, 
 * a Stirling number of the second kind is the number of 
 * ways to partition a set of n objects into k non-empty 
 * subsets and is denoted by S(n,k) or \textstyle \lbrace{n\atop k}\rbrace.[1] 
 * Stirling numbers of the second kind occur in the field of 
 * mathematics called combinatorics and the study of partitions.

 Stirling numbers of the second kind is one of two kinds of 
 Stirling numbers, the other kind being called Stirling numbers 
 of the first kind. Mutually inverse (finite or infinite) triangular 
 matrices can be formed by arranging the Stirling numbers of 
 the first respectively second kind according to the parameters n, k.
 * 
 */
public class StirlingNumber2ndKind {
	/*
	 * {n k} = (1/k!) * Summation from j=0 to k of (-1)^(k-j) * (k choose j) *
	 * j^n
	 */
	public long calculate(int numOfIdenticalObj, int numOfBoxes) {
		Factorial fact = new Factorial();
		Combination comb = new Combination();

		long kFact = fact.calculate(numOfBoxes);
		long summationTotal = 0;
		int kMinusJ = 0;
		long negOneToKMinusJPower = 0;
		long kChooseJ = 0;
		long jToNPower = 0;

		for (int j = 0; j <= numOfBoxes; j++) {
			kMinusJ = numOfBoxes - j;
			negOneToKMinusJPower = (long) Math.pow(-1.0d, (double) kMinusJ);
			kChooseJ = comb.evaluateWithOutRepetition(numOfBoxes, j);
			jToNPower = (long) Math.pow(j, numOfIdenticalObj);

			summationTotal += negOneToKMinusJPower * kChooseJ * jToNPower;
		}

		return summationTotal / kFact;
	}

	public static void main(String[] args) {
		StirlingNumber2ndKind stirNum = new StirlingNumber2ndKind();

		// for(int i=0; i < 10; i++) {
		int i = 10;
		for (int j = 0; j <= i; j++) {
			System.out.println("Stirling number {" + i + " " + j + "} = "
					+ stirNum.calculate(i, j));
		}
		// }
	}
}
