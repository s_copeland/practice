package com.saiyd.numbertheory;

/**
 * A triangular number counts the number of objects that can form an equilateral triangle
 * 
 * The nth triangle number is the number of dots in a triangle with n dots on a side. 
 * 
 * It is the sum of the n natural numbers from 1 to n.
 * Ex: the 2nd triangle number is 3. 3 = 2 + 1
 * Ex: the 4th triangle number is 10. 10 = 4 + 3 + 2 + 1
 * 
 * Tn = (n+1) choose 2
 * 
 * The triangular number Tn solves the handshake problem of counting the number of handshakes if each 
 * person in a room full of n + 1 total people shakes hands once with each other person. 
 * In other words, the solution to the handshake problem of n people is Tn-1.
Triangle numbers are the additive analog of the factorials, which are the products of integers from 1 to n.
 * 
 * 
 * http://en.wikipedia.org/wiki/Triangular_number
 * 
 * @author Saiyd
 *
 */
public class TriangularNumber {
	/*
	 * http://en.wikipedia.org/wiki/Triangular_number
	 * 
	 * 
	 */
	public boolean isTriangular(long number) {
		long value = (8 * number) + 1;
		return isSquare(value);
	}
	
	/*
	 * Binomial coefficient (n+1) choose 2)
	 */
	public long calculateNth(int number) {
		int nPlusOne = number + 1;
		
		// look up the answer 
		return PascalsTriangle.TABLE[nPlusOne][2];
	}
	
	private boolean isSquare(long number) {
		long test = (long) Math.sqrt(number);
		return (test * test) == number;
	}
	
	public static void main(String[] args) {
		PascalsTriangle p = new PascalsTriangle();
		
		TriangularNumber triNum = new TriangularNumber();
		/*for(int i=0; i<56; i++) {
			boolean b = triNum.isTriangular(i);
			if(b) 
				System.out.println("i = " + i + " " + b);
		}*/
		
		for(int i=0; i<10; i++) {
			long nThTri = triNum.calculateNth(i);
			System.out.println("i = " + i + " " + nThTri);
		}
	}
}
