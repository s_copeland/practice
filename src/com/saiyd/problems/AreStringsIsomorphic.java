package com.saiyd.problems;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

public class AreStringsIsomorphic {

	/*
	 * We define isomorphism as the following. Two words
	 * are isomorphic is one word can be remapped to the 
	 * other. By remapping we mean replacing all occurences
	 * of one letter with another in the other word while the
	 * ordering of the letters remains unchanged. No two letters
	 * may map to the same letter in the other word, but a letter
	 * may map to itself.
	 * 
	 * Examples
	 * 
	 * foo -> app  = true
	 * 		f maps to a
	 * 		o maps to p
	 * 
	 * bar -> foo = false
	 * 		we can map both a and r to o
	 * 
	 * turtle -> tletur =  true
	 * 		t maps to t
	 * 		u maps to l
	 * 		r maps to e
	 * 		l maps to u
	 * 		e maps to r
	 * 		
	 * ab -> ca = true
	 * 		a maps to c
	 * 		b maps to a
	 */
	
	/* 
	 * THIS IS ALL WRONG
	 */
	
	
	/*
	public boolean areIsomorphic(String s1, String s2) {
		boolean ret = false;
		
		if(s1 == null | s2 == null)
			return ret;
		if(s1.length() != s2.length())
			return ret;
		
		BiMap<Character,Integer> s1IndexSumPerChar = getIndexSumPerChar(s1);
		BiMap<Character,Integer> s2IndexSumPerChar = getIndexSumPerChar(s2);
		
		// this shouldn't happen, but we check anyway
		if(s1IndexSumPerChar.size() != s2IndexSumPerChar.size())
			return ret;
		
		// look for matching index sums in both maps, if one differs then they are not isomorphic
		for(Integer indexSum : s1IndexSumPerChar.values()) {
			if(s2IndexSumPerChar.inverse().get(indexSum) == null)
				return false;
		}
		ret = true;
		return ret;
	}
	
	private BiMap<Character,Integer> getIndexSumPerChar(String input) {
		BiMap<Character,Integer> indexSumPerChar = HashBiMap.create();
		if(input != null) {
			for(int i = 0; i < input.length(); i++) {
				Integer existingSum = indexSumPerChar.get(input.charAt(i));
				if(existingSum == null)
					existingSum = 0;
				
				existingSum += i; // add the location of the character to all previous locations found
				indexSumPerChar.forcePut(input.charAt(i), existingSum);
			}
		}
		return indexSumPerChar;
	}
	
	public static void main(String[] args) {
		AreStringsIsomorphic asi = new AreStringsIsomorphic();
		String s1 = "foo";
		String s2 = "app";
		
		runTest(asi, s1, s2);
		
		s1 = "bar";
		s2 = "foo";
		runTest(asi, s1, s2);
		
		s1 = "turtle";
		s2 = "tletur";
		runTest(asi, s1, s2);
		
		s1 = "ab";
		s2 = "ca";
		runTest(asi, s1, s2);
		
		s1 = "0123443";
		s2 = "aenjccj";
		runTest(asi, s1, s2);
		
		s1 = "saiyd";
		s2 = "dyias";
		runTest(asi, s1, s2);
		
		s1 = "mmike";
		s2 = "right";
		runTest(asi, s1, s2);
		
		s1 = "axy";
		s2 = "boo";
		runTest(asi, s1, s2);
	}

	private static void runTest(AreStringsIsomorphic asi, String s1, String s2) {
		System.out.println("s1 = " + s1);
		System.out.println("s2 = " + s2);
		System.out.println("Are strings isomorphic = " + asi.areIsomorphic(s1, s2));
		System.out.println();
	}*/
}
