package com.saiyd.problems;

import java.security.SecureRandom;

/*
 * http://www.geeksforgeeks.org/write-an-efficient-method-to-check-if-a-number-is-multiple-of-3/
 */
public class CheckIfANumberIsAMultipleOf3 {
	public boolean isMultipleOf3(int n)
	{
	    int odd_count = 0;
	    int even_count = 0;
	 
	    /* Make no positive if +n is multiple of 3
	       then is -n. We are doing this to avoid
	       stack overflow in recursion*/
	    if(n < 0)   n = -n;
	    if(n == 0) return true;
	    if(n == 1) return false;
	 
	    while(n > 0)
	    {
	        /* If odd bit is set then
	           increment odd counter */
	        if((n & 1) == 1) 
	           odd_count++;
	        n = n>>1;
	 
	        /* If even bit is set then
	           increment even counter */
	        if((n & 1) == 1)
	            even_count++;
	        n = n>>1;
	    }
	 
	     return isMultipleOf3(Math.abs(odd_count - even_count));
	}
	
	public static void main(String[] args) {
		SecureRandom rand = new SecureRandom();
		CheckIfANumberIsAMultipleOf3 c = new CheckIfANumberIsAMultipleOf3();
		int size = 10;
		int range = 100;
		int cur = 0;

		for(int i = 0; i < size; i++) {
			cur = rand.nextInt(range);
			System.out.println(cur + " is a multiple of 3: " + c.isMultipleOf3(cur));
		}
	}
}
