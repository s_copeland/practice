package com.saiyd.problems;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.xml.datatype.Duration;

public class FindTheMedianInStream {
	private static final String ONE_MILLION_NUMBERS_TXT = "one-million-numbers.txt";
	private static final String PARTITION_1_TXT = "partition-1.txt";
	private static int[] bucket = new int[1000000];
	private static int countOfNumbers;
	
	public static void main(String[] args) throws IOException {
		long start = System.currentTimeMillis();
		//Duration dur = new Duration();
		
		BufferedReader isr = new BufferedReader(new InputStreamReader(new FileInputStream(ONE_MILLION_NUMBERS_TXT)), 1024 * 1024);
		// read the numbers one at a time
		int j = 0;
		char curNumber = 0;
		StringBuilder sb = new StringBuilder();
		int count = 0;
		while( (j = isr.read()) != -1) {
			curNumber = (char) j;
			if(curNumber == ',') {
				process(Integer.parseInt(sb.toString()));
				sb.delete(0, sb.length());
				//sb.trimToSize();
				count++;
				if(count % 100000000 == 0)
					System.out.println("Finished processing " + count);
			} else {
				sb.append(curNumber);
			}
		}
		
		//curNumber = (char) j;
		//System.out.println("appending " + j + " char = " + curNumber);
		if(sb.length() > 0)
			process(Integer.parseInt(sb.toString()));
		
		int medianIndex = countOfNumbers / 2;
		boolean isOdd = false;
		if(countOfNumbers > 2 && countOfNumbers % 2 != 0) {
			isOdd = true;
			medianIndex++; // we do this because of loss of precision in the division above
		}
		
		int curCountOfNumbers = 0;
		int indexOfLowerMidpoint = -1;
		// find the median
		for(int i =0; i < bucket.length; i++) {
			curCountOfNumbers += bucket[i];
			if(isOdd && curCountOfNumbers >= medianIndex) {
				System.out.println("\nThe median is " + i);
				break;
			} else if(!isOdd && curCountOfNumbers >= medianIndex && indexOfLowerMidpoint == -1) {
				indexOfLowerMidpoint = i;
			} else if(!isOdd && curCountOfNumbers >= medianIndex && indexOfLowerMidpoint != -1 && bucket[i] != 0) {
				double median = ((double) indexOfLowerMidpoint + (double) i) / 2;
				System.out.println("\nThe median is " + median);
				break;
			}
		}
		long end = System.currentTimeMillis();
		//dur.add(arg0)
		System.out.println("Duration " + (end - start));
	}
	
	private static void process(int num) {
		//System.out.print(num + " ");
		// increment the histogram counter
		bucket[num]++;
		countOfNumbers++;
	}
}
