package com.saiyd.problems;

public class FizzBuzz {
	public void fizzBuzz() {
		for(int i = 1; i <= 30; i++) {
			if(i % 3 == 0)
				System.out.print("Fizz");
			if(i % 5 == 0)
				System.out.print("Buzz");
			if(i % 3 != 0 && i % 5 != 0)
				System.out.print(i);
			
			System.out.println();
		}
	}
	public static void main(String[] args) {
		FizzBuzz fb = new FizzBuzz();
		fb.fizzBuzz();
	}
}
