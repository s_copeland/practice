package com.saiyd.problems;

import java.security.SecureRandom;

import com.saiyd.sorting.Util;

/*
 * http://en.wikipedia.org/wiki/Heap's_algorithm
 * 
 * Heap's algorithm is an algorithm used for generating all possible 
 * permutations of some given length. It was first proposed by B. R. 
 * Heap in 1963.[1] It generates each permutation from the previous one 
 * by choosing a pair of elements to interchange. In a 1977 review of 
 * permutation-generating algorithms, Robert Sedgewick concluded that 
 * it was at that time the most effective algorithm for generating 
 * permutations by computer.
 * 
 * http://www.cs.princeton.edu/~rs/talks/perms.pdf
 * https://gist.github.com/vo/351518
 */
public class HeapsAlgorithmForGeneratingPermutations {

	/*
	 * Not printed in order
	 */
	public void generateRecursive(int len, int[] input) {
		if(len == 1) {
			Util.print(input, false);
		} else {
			int endIndex = len -1;
			for(int i = 0; i < len; i++) {
				generate(endIndex, input);
				int j = 0;
				if(len % 2 == 1) { // if odd
					j = 0;
				} else {
					j = i;
				}
				
				// swap
				int temp = input[j];
				input[j] = input[endIndex];
				input[endIndex] = temp;
			}
		}
	}
	
	/*
	 * Not printed in order
	 */
	public void generate(int len, int[] input) {
		int[] idx = new int[input.length];
		
		int n = 0;
		for(n = 0; n < len; n++) {
			idx[n] = 0;
		}
		
		Util.print(input, false);
		
		for(n = 1; n < len;) {
			if(idx[n] < n) {
				int swap = n % 2 * idx[n];
				
				// swap
				int temp = input[swap];
				input[swap] = input[n];
				input[n] = temp;
				
				idx[n]++;
				n = 1;
				Util.print(input, false);
			} else {
				idx[n++] = 0;
			}
		}
	}
	
	public static void main(String[] args) {
		SecureRandom rand = new SecureRandom();
		
		int size = 3;
		int range = 100;
		int[] input = new int[size];
		int[] input2 = new int[size];

		for(int i = 0; i < input.length; i++) {
			input[i] = rand.nextInt(range);
			input2[i] = input[i];
		}
		
		HeapsAlgorithmForGeneratingPermutations heaps = new HeapsAlgorithmForGeneratingPermutations();
		long start = System.nanoTime();
		heaps.generate(input.length, input);
		long end = System.nanoTime();
		System.out.println("Duration: " + (end - start));
		
		System.out.println("---------");
		
		start = System.nanoTime();
		heaps.generateRecursive(input2.length, input2);
		end = System.nanoTime();
		System.out.println("Duration: " + (end - start));
	}
}
