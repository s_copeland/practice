package com.saiyd.problems;

import java.security.SecureRandom;
import java.util.Arrays;

import com.saiyd.counting.Combination;
import com.saiyd.sorting.MergeSort;
import com.saiyd.sorting.QuickSort;

/*
 * Can be found in n log n via modified merge sort
 * 
 * An inversion in an array is an instance where element i < j
 * array[i] > array[j]
 * 
 * In a sorted array there are zero inversions. In a reverse sorted
 * array (from highest to lowest). The number of inversions is
 * N choose 2
 * 
 * The count of inversions in a list can be used to determine how similar
 * two list are. Example compare a list of rankings between users.
 * 
 * The inversion count also indicates how far an array is from being sorted
 * 
 */
public class InversionsInAnArray <T extends Comparable<T>> {

	public int countInversions(T[] input) {
		int total = 0;
		total += mergeSort(input);

		return total;
	}

	private int mergeSort(T[] input) {
		// base case
		if(input.length <= 1)
			return 0;
		
		// divide the array into 2 pieces
		int midpoint = input.length / 2;
		T[] leftHalf = Arrays.copyOfRange(input, 0, midpoint);
		T[] rightHalf = Arrays.copyOfRange(input, midpoint, input.length);
		
		int subtotal = 0;
		// recurse, conquer
		subtotal += mergeSort(leftHalf);
		subtotal += mergeSort(rightHalf);
		
		subtotal += merge(leftHalf, rightHalf, input);
		
		return subtotal;
	}

	private int merge(T[] leftHalf, T[] rightHalf, T[] input) {
		int lIndex = 0;
		int rIndex = 0;
		int inputIndex = 0;
		int inversionCount = 0;
		
		for(; inputIndex < input.length; inputIndex++) {
			if(lIndex == leftHalf.length) {
				input[inputIndex] = rightHalf[rIndex];
				rIndex++;
			} else if(rIndex == rightHalf.length) {
				input[inputIndex] = leftHalf[lIndex];
				lIndex++;
			} else if(leftHalf[lIndex].compareTo(rightHalf[rIndex]) < 0) {
				input[inputIndex] = leftHalf[lIndex];
				lIndex++;
			} else {
				input[inputIndex] = rightHalf[rIndex];
				rIndex++;
				// we use leftHalf.length instead of (leftHalf.length - 1)
				// because we have already incremented rIndex.
				// we could switch the two lines and use (leftHalf.length - 1)
				inversionCount += (leftHalf.length) - lIndex;
			}
		}
		
		return inversionCount;
	}
	
	private void print(T[] input, boolean indent){
		if(indent)
			System.out.print("\t");
		for(int i = 0; i < input.length; i++) {
			System.out.print(input[i]);
			System.out.print(" ");
		}
		System.out.println();
	}
	
	public static void main(String[] args) {
		InversionsInAnArray<Integer> inversionCounter = new InversionsInAnArray<Integer>();
		SecureRandom rand = new SecureRandom();
		
		
		int size = 10;
		int range = 100;
		Integer[] input = new Integer[size];
		Integer[] input2 = new Integer[size];

		for(int i = 0; i < input.length; i++) {
			input[i] = rand.nextInt(range);
			input2[i] = input[i];
		}
		inversionCounter.print(input, false);
		long start = System.nanoTime();
		int count = inversionCounter.countInversions(input);
		long end = System.nanoTime();
		System.out.println("# of inversions = " + count);
		System.out.println("Duration: " + (end - start));
		Combination comb = new Combination();
		System.out.println("Max number of inversions = " + comb.evaluateWithOutRepetition(input.length, 2));
		inversionCounter.print(input, false);
		inversionCounter.print(input2, false);
		System.out.println("Verification of # of inversions = " + Inversions.count(input2));
		
	}
}
