package com.saiyd.problems;

import java.util.HashMap;
import java.util.Map;

public class IsStringAPermutationOfOther {
	public boolean isAPermutation(String src, String dst) {
		if(src == null || dst == null)
			return false;
		
		if(src.length() != dst.length())
			return false;
		
		Map<Character, Integer> charMap = new HashMap<>();
		
		// create a histogram for the src string
		for(int i = 0; i < src.length(); i++) {
			if(charMap.containsKey(src.charAt(i))) {
				int count = charMap.get(src.charAt(i));
				count++;
				charMap.put(src.charAt(i), count);
			} else {
				charMap.put(src.charAt(i), 1);
			}
		}
		
		// decrement the histogram using values from dst string
		for(int j = 0; j < dst.length(); j++) {
			if(charMap.containsKey(dst.charAt(j))) {
				int count = charMap.get(dst.charAt(j));
				count--;
				
				if(count < 0)
					return false;
				
				charMap.put(dst.charAt(j), count);
			} else {
				return false;
			}
		}
			
		return true;
	}
	
	public static void main(String[] args) {
		IsStringAPermutationOfOther isapo = new IsStringAPermutationOfOther();
		String src = "saiyd";
		String dst = "diyas";
		
		System.out.println("src = " + src);
		System.out.println("dst = " + dst);
		System.out.println(isapo.isAPermutation(src, dst));
	}
}
