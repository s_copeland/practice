package com.saiyd.problems;

public class ManhattanDistance {

	public static class Point {
		public int x;
		public int y;
	}
	
	public static class Point3D {
		public int x;
		public int y;
		public int z;
	}
	
	public int calc(Point a, Point b) {
		return Math.abs(a.x - b.x) + Math.abs(a.y - b.y);
	}
	
	public int calc(Point3D a, Point3D b) {
		return Math.abs(a.x - b.x) + Math.abs(a.y - b.y) + Math.abs(a.z - b.z);
	}
}
