package com.saiyd.problems;

public class MultiplyWithOnlyAdditon {

	/*
	 * Multiply two integers without using multiplication, division and bitwise
	 * operators, and no loops
	 * 
	 * Asked by Kapil
	 * 
	 * By making use of recursion, we can multiply two integers with the given
	 * constraints.
	 * 
	 * To multiply x and y, recursively add x y times.
	 * 
	 */
	/* function to multiply two numbers x and y */
	private static int multiply(int x, int y) {
		/* 0 multiplied with anything gives 0 */
		if (y == 0) {
			return 0;
		} else if (y > 0) { /* Add x one by one */
			return (x + multiply(x, y - 1));
		} else { // if (y < 0), the case where y is negative 
			return -multiply(x, -y);
		}
	}

	public static void main(String[] args) {
		System.out.println("5 * 3 = " + multiply(5, 3));
		System.out.println("5 * 3 = " + multiply(5, -3));
	}

}
