package com.saiyd.problems;

/*
 * http://googleresearch.blogspot.in/2006/06/extra-extra-read-all-about-it-nearly.html
 * 
 * The bug is in this line:
 *              int mid =(low + high) / 2;
 * 
 * In Programming Pearls Bentley says that the analogous line "sets m to the average of l and u, 
 * truncated down to the nearest integer." On the face of it, this assertion might appear correct, 
 * but it fails for large values of the int variables low and high. Specifically, it fails if 
 * the sum of low and high is greater than the maximum positive int value (2^31 - 1). The sum 
 * overflows to a negative value, and the value stays negative when divided by two. In C this 
 * causes an array index out of bounds with unpredictable results. In Java, it throws 
 * ArrayIndexOutOfBoundsException.
 */
public class NearlyAllBinarySearchesAreBroken {
	public static int binarySearch(int[] a, int key) {
		int low = 0;
		int high = a.length - 1;

		while (low <= high) {
			// The problem is here
			int mid = (low + high) / 2; // wrong way
			// This way is better
			mid = low + ((high - low) / 2); // right way
			
			int midVal = a[mid];

			if (midVal < key)
				low = mid + 1;
			else if (midVal > key)
				high = mid - 1;
			else
				return mid; // key found
		}
		return -(low + 1); // key not found.
	}
}
