package com.saiyd.problems;

import java.util.HashMap;
import java.util.Map;


public class OofNAreStringsIsomorphic {

	/*
	 * We define isomorphism as the following. Two words
	 * are isomorphic is one word can be remapped to the 
	 * other. By remapping we mean replacing all occurences
	 * of one letter with another in the other word while the
	 * ordering of the letters remains unchanged. No two letters
	 * may map to the same letter in the other word, but a letter
	 * may map to itself.
	 * 
	 * Examples
	 * 
	 * foo -> app  = true
	 * 		f maps to a
	 * 		o maps to p
	 * 
	 * bar -> foo = false
	 * 		we can map both a and r to o
	 * 
	 * turtle -> tletur =  true
	 * 		t maps to t
	 * 		u maps to l
	 * 		r maps to e
	 * 		l maps to u
	 * 		e maps to r
	 * 		
	 * ab -> ca = true
	 * 		a maps to c
	 * 		b maps to a
	 */
	public boolean areIsomorphic(String s1, String s2) {
		boolean ret = false;
		
		if(s1 == null | s2 == null)
			return ret;
		if(s1.length() != s2.length())
			return ret;
		
		return calculateIndexSumPerChar(s1, s2);

	}
	
	private boolean calculateIndexSumPerChar(String s1, String s2) {
		if(s1 == null | s2 == null)
			return false;
		
		Map<Character,Integer> indexSumPerChar = new HashMap();
		Map<Character,Integer> indexSumPerChar2 = new HashMap();
		
		for(int i = 0; i < s1.length(); i++) {
			Integer existingSum = indexSumPerChar.get(s1.charAt(i));
			Integer existingSum2 = indexSumPerChar2.get(s2.charAt(i));
			
			if(existingSum == null) {
				existingSum = i;
			} else {
				existingSum += i; // add the location of the character to all previous locations found
			}
			
			if(existingSum2 == null) {
				existingSum2 = i;
			} else {
				existingSum2 += i; // add the location of the character to all previous locations found
			}
			
			indexSumPerChar.put(s1.charAt(i), existingSum);
			indexSumPerChar2.put(s2.charAt(i), existingSum2);
			
			if(existingSum != existingSum2)
				return false;
		}

		return indexSumPerChar.size() == indexSumPerChar2.size();
	}
	
	public static void main(String[] args) {
		OofNAreStringsIsomorphic asi = new OofNAreStringsIsomorphic();
		String s1 = "foo";
		String s2 = "app";
		
		runTest(asi, s1, s2);
	
		s1 = "food";
		s2 = "appa";
		runTest(asi, s1, s2);
		
		s1 = "bar";
		s2 = "foo";
		runTest(asi, s1, s2);
		
		s1 = "turtle";
		s2 = "tletur";
		runTest(asi, s1, s2);
		
		s1 = "ab";
		s2 = "ca";
		runTest(asi, s1, s2);
		
		s1 = "0123443";
		s2 = "aenjccj";
		runTest(asi, s1, s2);
		
		s1 = "mmike";
		s2 = "right";
		runTest(asi, s1, s2);
		
		s1 = "axy";
		s2 = "boo";
		runTest(asi, s1, s2);
		
		s1 = "bar";
		s2 = "fof";
		runTest(asi, s1, s2);
	}

	private static void runTest(OofNAreStringsIsomorphic asi, String s1, String s2) {
		System.out.println("s1 = " + s1);
		System.out.println("s2 = " + s2);
		System.out.println("Are strings isomorphic = " + asi.areIsomorphic(s1, s2));
		System.out.println();
	}
}
