package com.saiyd.problems;

import java.security.SecureRandom;

import com.saiyd.sorting.MergeSort;
import com.saiyd.sorting.Util;

/*
 * http://www.geeksforgeeks.org/lexicographic-permutations-of-string/
 * 
 * Following are the steps to print the permutations lexicographic-ally
 * 
 * 1. Sort the given string in non-decreasing order and print it. 
 *    The first permutation is always the string sorted in non-decreasing order.
 * 
 * 2. Start generating next higher permutation. Do it until next higher 
 *    permutation is not possible. If we reach a permutation where all characters 
 *    are sorted in non-increasing order, then that permutation is the last permutation.
 * 
 */
public class PermutationInLexicographicOrder {

	/*
	 * Steps to generate the next higher permutation: 
	 * 
	 * 1. Take the previously printed permutation and find the rightmost character in it, which is
	 * smaller than its next character. Let us call this character as �first
	 * character�.
	 * 
	 * 2. Now find the ceiling of the �first character�. Ceiling is the smallest
	 * character on right of �first character�, which is greater than �first
	 * character�. Let us call the ceil character as �second character�.
	 * 
	 * 3. Swap the two characters found in above 2 steps.
	 * 
	 * 4. Sort the substring (in non-decreasing order) after the original index
	 * of �first character�.
	 */
	public int[] getNext(int[] prevPerm, int size) {
		// Find the rightmost character which is smaller than its next
		int i = 0;
		for (i = size - 2; i>= 0 && prevPerm[i] >= prevPerm[i + 1]; --i) {
		}

		/*
		 * if this doesn't occur, we've finished our permutations the array is
		 * reversed: (1, 2, 3, 4) => (4, 3, 2, 1)
		 */
		if (i == -1)
			return null;

		// slide up the array looking for a bigger number than what we found before
		//
		// Find the ceiling of prevPerm[i] to the right of prevPerm[i].
        // Ceiling of a character is the smallest character greater than it
		int j = 0;
		for (j = size -1; j >= 0 && prevPerm[j] <= prevPerm[i]; --j) {
		}

		// swap them
		int temp = prevPerm[i];
		prevPerm[i] = prevPerm[j];
		prevPerm[j] = temp;

		// now reverse the elements in between by swapping the ends
		i++;
		for (j = size -1; i < j; ++i, --j) {
			temp = prevPerm[i];
			prevPerm[i] = prevPerm[j];
			prevPerm[j] = temp;
		}

		return prevPerm;
	}

	public void generate(int[] input) {
		MergeSort mergeSort = new MergeSort();
		mergeSort.sortTopDownMerge(input);
		Util.print(input, false);
		
		int[] nextPerm = input;
		
		while( (nextPerm = getNext(nextPerm, nextPerm.length)) != null) {
			Util.print(input, false);
		}
	}

	public static void main(String[] args) {
		SecureRandom rand = new SecureRandom();

		int size = 4;
		int range = 100;
		int[] input = new int[size];
		int[] input2 = new int[size];

		for (int i = 0; i < input.length; i++) {
			input[i] = rand.nextInt(range);
			input2[i] = input[i];
		}

		PermutationInLexicographicOrder perm = new PermutationInLexicographicOrder();
		perm.generate(input);

	}
}
