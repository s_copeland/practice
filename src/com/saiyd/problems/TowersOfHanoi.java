package com.saiyd.problems;

public class TowersOfHanoi {
	private int numDisks;
	private int numPegs;

	
	public TowersOfHanoi(int pegs, int numberOfDisks) {
		numDisks = numberOfDisks;
		numPegs = pegs;
	}
	
	public void sovle() {
		move(numDisks, 0, numDisks -  1);
	}
	
	private void move(int numberOfDisks, int fromPeg, int toPeg) {
		if(numberOfDisks == 1) {
			System.out.println("Move a disk from " + fromPeg + " to " + toPeg);
		} else {
			int helperPeg = numPegs - fromPeg - toPeg;
			// move n - 1 disks from one peg to another via a helper
			move(numberOfDisks -1, fromPeg, helperPeg);
			
			System.out.println("Move a disk from " + fromPeg + " to " + toPeg);
			
			// move the last disk to the to peg
			move(numberOfDisks -1, helperPeg, toPeg);
		}
	}
	
	public static void main(String[] args) {
		TowersOfHanoi toh = new TowersOfHanoi(3, 3);
		toh.sovle();
	}

}
