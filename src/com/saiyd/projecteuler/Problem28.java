package com.saiyd.projecteuler;

import java.util.Arrays;

/*
 * https://projecteuler.net/archives
 * 
 * http://www.mathblog.dk/project-euler-26-find-the-value-of-d-1000-for-which-1d-contains-the-longest-recurring-cycle/
 */
public class Problem28 {
	public static void main(String[] args) {
		int sequenceLength = 0;
		int[] foundRemainders = null;
		
		for (int i = 1000; i > 1; i--) {
		    if (sequenceLength >= i) {
		    	System.out.println(Arrays.toString(foundRemainders));
		        break;
		    }
		 
		    foundRemainders = new int[i];
		    int value = 1;
		    int position = 0;
		 
		    while (foundRemainders[value] == 0 && value != 0) {
		        foundRemainders[value] = position;
		        value *= 10;
		        value %= i;
		        position++;
		    }
		 
		    if (position - foundRemainders[value] > sequenceLength) {
		        sequenceLength = position - foundRemainders[value];
		    }
		}
		
		System.out.println("Max sequence length = " + sequenceLength);
	}
}
