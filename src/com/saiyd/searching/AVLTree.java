package com.saiyd.searching;

import java.security.SecureRandom;
import java.util.Stack;

import com.saiyd.searching.BinaryTreeSearch.TreeNode;

/*
 * A height balanced binary search tree
 * 
 * 	        Average	    Worst case
 * Space	O(n)	    O(n)
 * Search	O(log n)	O(log n)
 * Insert	O(log n)	O(log n)
 * Delete	O(log n)	O(log n)
 * 
 *  In an AVL tree, the heights of the two child subtrees of any node differ by at most one; 
 *  if at any time they differ by more than one, rebalancing is done to restore this property.
 *  
 *  For lookup-intensive applications, AVL trees are faster than red-black trees because 
 *  they are more rigidly balanced. 
 */
public class AVLTree <T extends Comparable<T>> extends BinaryTreeSearch<T> {
	
	public TreeNode<T> rebalance(TreeNode<T> subTreeRoot, Stack<TreeNode<T>> path) {
		TreeNode<T> node = null;
		TreeNode<T> child = null;
		
		while(!path.empty()) {
			node = path.pop();
			// Ensure the AVL balance property
			int balanceFactor = balanceFactor(node);
			TreeNode<T> origNode = node;
			
			if(balanceFactor > 1) { // tree is left heavy
				child = node.left;
				if(balanceFactor(child) == -1) { // The Left Right Case
					child = rotateLeft(child); // // reduce to the Left Left Case
				}
				// Left Left Case
				node = rotateRight(node);
				// relink the rotated tree
				if(path.empty()) {
					subTreeRoot = node;
				} else if(origNode == path.peek().right) {
					path.peek().right = node;
				} else  {
					path.peek().left = node;
				}
			} else if(balanceFactor < -1) { // tree is right heavy
				child = node.right;
				if(balanceFactor(child) == 1) { // The Right Left Case
					child = rotateRight(child); // Reduce to Right Right Case
				}
				// Right Right Case
				node = rotateLeft(node);
				// relink the rotated tree
				if(path.empty()) {
					subTreeRoot = node;
				} else if(origNode == path.peek().right) {
					path.peek().right = node;
				} else  {
					path.peek().left = node;
				}
			}
			
			node.height = Math.max(height(node.left), height(node.right)) +1;
		}

		return subTreeRoot;
	}

	public TreeNode<T> insertIterative(T data) {
		if(data == null)
			return null; // don't insert null
		TreeNode<T> ret = null;
		if(root == null) {
			root = new TreeNode<T>(data);
			ret = root;
		} else {
			ret = doInsertIterative(root, data);
		}
		return ret;
	}
	
	private TreeNode<T> doInsertIterative(TreeNode<T> node, T data) {
		TreeNode<T> parentNode = null;
		TreeNode<T> curNode = node;
		Stack<TreeNode<T>> path = new Stack<TreeNode<T>>();
		
		// find where to insert the new node
		while(curNode != null) {
			parentNode = curNode;
			path.push(parentNode); // save the path to the new insertion point
			if(data.compareTo(curNode.data) == -1) {
				curNode = curNode.left;
			} else {
				curNode = curNode.right;
			}
		}
	
		// add the new node
		if(parentNode == null) {
			// the tree was empty
			root = new TreeNode<T>(data);
		} else if(data.compareTo(parentNode.data) == -1) {
			parentNode.left = new TreeNode<T>(data);
			curNode = parentNode.left;
		} else {
			parentNode.right = new TreeNode<T>(data);
			curNode = parentNode.right;
		}
		
		root = rebalance(root, path);
		return curNode;
	}
	private TreeNode<T> rotateRight(TreeNode<T> node) {
		TreeNode<T> left = node.left;
		TreeNode<T> leftNodeRightChild = left.right;
		
		left.right = node;
		node.left = leftNodeRightChild;
		// adjust heights
		node.height = Math.max(height(node.left), height(node.right)) + 1; 
		left.height = Math.max(height(left.left), height(left.right)) + 1; 
		return left;
	}
	private TreeNode<T> rotateLeft(TreeNode<T> node) {
		TreeNode<T> right = node.right;
		TreeNode<T> rightNodeLeftChild = right.left;
		
		right.left = node;
		node.right = rightNodeLeftChild;
		// adjust heights
		node.height = Math.max(height(node.left), height(node.right)) + 1; 
		right.height = Math.max(height(right.left), height(right.right)) + 1; 
		return right;
	}
	/*
	 * After inserting a node, it is necessary to check each of the node's 
	 * ancestors for consistency with the rules of AVL. The balance factor 
	 * is calculated as follows: 
	 * balanceFactor = height(left subtree) - height(right subtree). 
	 * 
	 * For each node checked, if the balance factor remains −1, 0, or +1 
	 * then no rotations are necessary.
	 */
	private int balanceFactor(TreeNode<T> node ) {
		int ret = height(node.left) - height(node.right);
		return ret;
	}
	
	private int height(TreeNode<T> node) {
		/*
		 * The height of none existing nodes is -1
		 * This is a hack to allow the equation 
		 *    height = max(node.left, node.right) + 1
		 * to evaluate to 0 in the case of leave nodes
		 */
		return node == null ? -1 : node.height;
	}
	
	public static void main(String[] args) {
		AVLTree<Integer> avl = new AVLTree<Integer>();
		
		SecureRandom rand = new SecureRandom();
		
		avl.insertIterative(1);
		avl.insertIterative(2);
		avl.insertIterative(3);
		avl.insertIterative(4);
		avl.insertIterative(5);
		avl.insertIterative(6);
		
		avl.printInOrderTraversalIterative();
	}
}
