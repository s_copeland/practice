package com.saiyd.searching;

import java.security.SecureRandom;

import com.saiyd.sorting.QuickSort;

/*
 * Divide and Conquer
 * 
 * Binary search divides the problem size approximately in half everytime
 * it executes the loop. The max number of times the collection of size N is cut in
 * half is log(n) if n is a power of 2, otherwise floor(log(n))
 * 
 * Best: O(1)
 * Average: O(log n)
 * Worst: O(log n)
 * 
 */
public class BinarySearch <T extends Comparable<T>> {
	/**
	 * The input is sorted
	 * 
	 * input is an indexed collection of elements. Each element
	 * A[i] has a key value ki that can be used to identify
	 * the element. The keys are totaly ordered, which means given two
	 * keys ki and kj, either ki < kj or ki = kj or kj < ki
	 * 
	 * @param input - a presorted array of type T
	 * @param query - the item to find
	 * @return
	 */
	public boolean search(T[] input, T query) {
		boolean ret = false;
		
		if(query != null) {
			int low = 0;
			int high = input.length - 1;
			
			while(low <= high) {
				int midpoint = (low + high) / 2;
				int comparisonValue = query.compareTo(input[midpoint]);
				
				if(comparisonValue < 0) {
					// query is less than midpoint value
					high = midpoint -1; // move the high end of the range to search down
				} else if(comparisonValue > 0) {
					// query is greater than midpoint value
					low = midpoint + 1; // move the low end of the range to search up
				} else {
					// found the item
					return true;
				}
			}
		}
		return ret;
	}
	
	public static void main(String[] args) {
		BinarySearch<Integer> bSearch = new BinarySearch<Integer>();
		
		SecureRandom rand = new SecureRandom();
		
		int size = 10;
		int range = 25;
		Integer[] input = new Integer[size];
		for(int i = 0; i < input.length; i++) {
			input[i] = rand.nextInt(range);
		}
		
		QuickSort<Integer> qSort = new QuickSort<Integer>(false);
		qSort.sort(input);
		
		for(int i = 0; i < input.length; i++) {
			System.out.print(input[i]);
			System.out.print(" ");
		}
		System.out.println();
		
		System.out.println(bSearch.search(input, 1));
		System.out.println(bSearch.search(input, 5));
		System.out.println(bSearch.search(input, 7));
		System.out.println(bSearch.search(input, 9));
		System.out.println(bSearch.search(input, 23));
	}
}
