package com.saiyd.searching;

import java.security.SecureRandom;
import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Queue;
import java.util.Stack;

import com.saiyd.graph2.VertexState;

/*
 * 	        Average	  Worst case
 * Space	O(n)	    O(n)
 * Search	O(log n)	O(n)
 * Insert	O(log n)	O(n)
 * Delete	O(log n)	O(n)
 * 
 * A node-based binary tree data structure where each node has a comparable key 
 * (and an associated value) and satisfies the restriction that the key in any 
 * node is larger than the keys in all nodes in that node's left subtree and 
 * smaller than the keys in all nodes in that node's right sub-tree.
 * 
 * 
 * 		The left subtree of a node contains only nodes with keys less than the node's key.
 * 		The right subtree of a node contains only nodes with keys greater than the node's key.
 * 		The left and right subtree each must also be a binary search tree.
 *		Each node can have up to two successor nodes.
 * 		There must be no duplicate nodes.
 * 		A unique path exists from the root to every other node.
 * 
 * 
 * Difference between binary tree and binary search tree
 * 
 * 	A binary tree is a tree where each node has up to two leaves. In a binary tree, a left 
 * 	child node and a right child node contain values which can be either greater, less, or equal to parent node.
 * 
 * 	In binary search tree, the left child contains nodes with values less than the parent node and where the 
 * 	right child only contains nodes with values greater than the parent node. There must be no duplicate nodes.
 * 
 * 
 * WHEN TO USE
 * 
 * 	The data set size is unknown, and the implementation must be able to handle any
 * 	possible size that fits in memory
 * 
 * 	The data set is highly dynamic, and there will be many insertions and deletions
 * 	during the data structures lifetime
 * 
 * 	The application requires traversing the data in ascending or descending
 * 	order
 * 
 * Advantages
 * 
 * 	Binary Search Tree is fast in insertion and deletion etc. when balanced.
 * 
 * 	Very efficient and its code is easier than other data structures.
 * 
 * 	Stores keys in the nodes in a way that searching, insertion and deletion can be done efficiently.
 * 
 * 	Implementation is very simple in Binary Search Trees.
 * 
 * 	Nodes in tree are dynamic in nature.
 * 
 * 
 * 
 * Disadvantages
 * 
 * 	The shape of the binary search tree totally depends on the order of insertions, and it can be degenerated.
 * 
 * 	When inserting or searching for an element in binary search tree, the key of each visited node has to be 
 * 	compared with the key of the element to be inserted or found, i.e., it takes a long time to search an 
 * 	element in a binary search tree.
 * 
 * 	The keys in the binary search tree may be long and the run time may increase.
 * 
 * 	After a long intermixed sequence of random insertion and deletion, the expected height of 
 * 	the tree approaches square root of the number of keys which grows much faster than \log n.
 */
public class BinaryTreeSearch <T extends Comparable<T>> {
	public static enum NodeState {
		NOT_EXPLORED, PARTIAL_EXPLORED, FULL_EXPLORED;
	}
	
	public static enum NodeColor {
		RED, BLACK;
	}
	
	public static class TreeNode <T extends Comparable<T>> {
		T data;
		TreeNode<T> parent; // not used in this binary search tree
		TreeNode<T> left;
		TreeNode<T> right;
		NodeState state = NodeState.NOT_EXPLORED;
		int subTreeSize; // the number of nodes in this subtree
		int height; // used by AVL trees
		NodeColor color; // used by red-black trees
		int priority; // used by treap
		
		public TreeNode(T data) {
			if(data == null)
				throw new IllegalArgumentException("data cannot be null");
			this.data = data;
		}
		
		public String toString() {
			return data.toString();
		}
	}
	
	public static <T extends Comparable<T>> boolean isBinarySearhTree(TreeNode<T> node, T maxData, T minData) {
		if(node == null)
			return true;
		if(node.data == null)
			return false;
		
		if(node.data.compareTo(maxData) >= 0 || node.data.compareTo(minData) <= 0)
			return false;
		/*
		 * Ensure all nodes in the left subtree are less than this node and greater than minData
		 * 
		 * Ensure all nodes in the right subtree are less than maxData and greater than this node
		 */
		return (isBinarySearhTree(node.left, node.data, minData) && isBinarySearhTree(node.right, maxData, node.data));
	}
	/*
	 * Example call
	 * 
	 * isBinarySearhTree(root, Integer.MAX_VALUE, Integer.MIN_VALUE)
	 */
	
	protected TreeNode<T> root;
	
	TreeNode<T> getRoot() {
		return root;
	}
	
	/*
	 * O(h) where h is the height of the tree
	 */
	protected TreeNode<T> findTreeMinimum(TreeNode<T> node) {
		TreeNode<T> ret = node;
		if(ret != null) {
			while(ret.left != null)
				ret = ret.left;
		}
		return ret;
	}
	
	/*
	 * O(h) where h is the height of the tree
	 */
	protected TreeNode<T> findTreeMaximum(TreeNode<T> node) {
		TreeNode<T> ret = node;
		if(ret != null) {
			while(ret.right != null) {
				ret = ret.right;
			}
		}
		return ret;
	}
	
	/*
	 * O(h) where h is the height of the tree
	 * 
	 * http://www.geeksforgeeks.org/inorder-successor-in-binary-search-tree/
	 */
	protected TreeNode<T> findSuccessor(TreeNode<T> node) {
		TreeNode<T> ret = null;
		if(node != null) {
			if(node.right != null) {
				ret = findTreeMinimum(node.right);
			} else {
				TreeNode<T> curNode = root;
				// start from the root and search down the
				// tree for the successor
				while(curNode != null) {
					if(node.data.compareTo(curNode.data) == -1) {
						ret = curNode;
						curNode = curNode.left; // go down the left
					} else if(node.data.compareTo(curNode.data) == 1) {
						curNode = curNode.right; // go down the right
					} else {
						break; // we found a node with the same value
					}
				}
			}
		}
		return ret;
	}
	
	/*
	 * O(h) where h is the height of the tree
	 * 
	 * http://www.geeksforgeeks.org/inorder-successor-in-binary-search-tree/
	 */
	protected TreeNode<T> findPredecessor(TreeNode<T> node) {
		TreeNode<T> ret = null;
		if(node != null) {
			if(node.left != null) {
				ret = findTreeMaximum(node.right);
			} else {
				TreeNode<T> curNode = root;
				// start from the root and search down the
				// tree for the predecessor
				while(curNode != null) {
					if(node.data.compareTo(curNode.data) == -1) {
						curNode = curNode.left; // go down the left
					} else if(node.data.compareTo(curNode.data) == 1) {
						ret = curNode;
						curNode = curNode.right; // go down the right
					} else {
						break; // we found a node with the same value
					}
				}
			}
		}
		return ret;
	}
	
	public boolean searchRecursive(T key) {
		TreeNode<T> node = searchRecursive(key, root);
		return (node != null);
	}
	
	private TreeNode<T> searchRecursive(T key, TreeNode<T> node) {
		if(node == null || (node.data != null && node.data.compareTo(key) == 0) ) {
			return node;
		} else if(key.compareTo(node.data) == -1) {
			return searchRecursive(key, node.left);
		} else {
			return searchRecursive(key, node.right);
		}
	}
	
	public boolean searchIterative(T key) {
		TreeNode<T> node = doSearchIterative(key);
		
		return (node != null);
	}
	
	private TreeNode<T> doSearchIterative(T key) {
		TreeNode<T> curNode = root;
		
		while(curNode != null) {
			if(curNode.data.compareTo(key) == 0) {
				return curNode;
			} else if(key.compareTo(curNode.data) == -1) {
				curNode = curNode.left;
			} else {
				curNode = curNode.right;
			}
		}
		return null;		
	}
	
	public void insertRecursive(T data) {
		if(data == null)
			return; // don't insert nulls
		doInsertRecursive(root, data);
	}

	private void doInsertRecursive(TreeNode<T> node, T data) {
		if(node == null) {
			node = new TreeNode<T>(data);
		} else if(data.compareTo(node.data) == -1) {
			doInsertRecursive(node.left, data);
		} else {
			doInsertRecursive(node.right, data);
		}
	}
	
	public TreeNode<T> insertIterative(T data) {
		if(data == null)
			return null; // don't insert null
		TreeNode<T> ret = null;
		if(root == null) {
			root = new TreeNode<T>(data);
			ret = root;
		} else {
			ret = doInsertIterative(root, data);
		}
		return ret;
	}

	private TreeNode<T> doInsertIterative(TreeNode<T> node, T data) {
		TreeNode<T> parentNode = null;
		TreeNode<T> curNode = node;
		
		// find where to insert the new node
		while(curNode != null) {
			parentNode = curNode;
			if(data.compareTo(curNode.data) == -1) {
				curNode = curNode.left;
			} else {
				curNode = curNode.right;
			}
		}
	
		// add the new node
		if(parentNode == null) {
			// the tree was empty
			root = new TreeNode<T>(data);
		} else if(data.compareTo(parentNode.data) == -1) {
			parentNode.left = new TreeNode<T>(data);
			curNode = parentNode.left;
		} else {
			parentNode.right = new TreeNode<T>(data);
			curNode = parentNode.right;
		}
		
		return curNode;
	}
	
	public void printInOrderTraversalRecursive() {
		if(root == null)
			return;
		
		doPrintInOrderTraversal(root.left);
		System.out.print(root.data.toString());
		System.out.print(" ");
		doPrintInOrderTraversal(root.right);
		System.out.println();
	}

	private void doPrintInOrderTraversal(TreeNode<T> node) {
		if(node == null)
			return;
		
		doPrintInOrderTraversal(node.left);
		System.out.print(node.data.toString());
		System.out.print(" ");
		doPrintInOrderTraversal(node.right);
	}
	
	public void printInOrderTraversalIterative() {
		doPrintInOrderTraversalIterative(root);
		System.out.println();
	}
	
	private void doPrintInOrderTraversalIterative(TreeNode<T> node) {
		if(node == null)
			return;
		
		Stack<TreeNode<T>> stack = new Stack<TreeNode<T>>();

		TreeNode<T> curNode = node;
		
		while(!stack.empty() || curNode != null) {
			if(curNode != null) {
				stack.push(curNode);
				curNode = curNode.left;
			} else {
				curNode = stack.pop();
		
				System.out.print(curNode.data.toString());
				System.out.print(" ");
					
				curNode = curNode.right;
			}
		}
	}
	
	public void printPostOrderTraversalRecursive() {
		doPrintPostOrderTraversalRecursive(root);
		System.out.println();
	}
	
	private void doPrintPostOrderTraversalRecursive(TreeNode<T> node) {
		if(node == null)
			return;
		
		doPrintPostOrderTraversalRecursive(node.left);
		doPrintPostOrderTraversalRecursive(node.right);
		System.out.print(node.data.toString());
		System.out.print(" ");
	}
	
	/*
	 * Pre-order traversal can be used to efficiently clone a tree
	 * 
	 * In order to build the children, you must have the parent (root) 
	 * built also. Pre-order is the only order that doesn't traverse 
	 * through a child before passing its parent, unlike in-order and 
	 * post-order which do.
	 */
	public void printPreOrderTraversalRecursive() {
		doPrintPreOrderTraversalRecursive(root);
		System.out.println();
	}
	
	private void doPrintPreOrderTraversalRecursive(TreeNode<T> node) {
		if(node == null)
			return;
		
		System.out.print(node.data.toString());
		System.out.print(" ");
		doPrintPreOrderTraversalRecursive(node.left);
		doPrintPreOrderTraversalRecursive(node.right);
	}
	
	public void printPreOrderTraversalIterative() {
		doPrintPreOrderTraversalIterative(root);
		System.out.println();
	}
	
	private void doPrintPreOrderTraversalIterative(TreeNode<T> node) {
		if(node == null)
			return;
		
		TreeNode<T> curNode = null;
		Stack<TreeNode<T>> stack = new Stack<TreeNode<T>>();
		stack.push(node);
		
		while(!stack.empty()) {
			curNode = stack.pop();
			System.out.print(curNode.data.toString());
			System.out.print(" ");
			
			// we push right then left, because we want to do left 1st
			if(curNode.right != null)
				stack.push(curNode.right);
			if(curNode.left != null)
				stack.push(curNode.left);
		}
	}
	
	public boolean delete(T data) {
		if(root == null)
			return false;

		return delete(null, root, data);
	}
	// http://www.mathcs.emory.edu/~cheung/Courses/171/Syllabus/9-BinTree/BST-delete.html
	// http://algs4.cs.princeton.edu/32bst/
	// hibbard deletion
	private boolean delete(TreeNode<T> parentNode, TreeNode<T> startNode, T data) {
		boolean ret = false;
		TreeNode<T> successorNode = null;
		TreeNode<T> curNode = startNode;
		
		// find the node to delete
		while(curNode != null) {
			if(data.compareTo(curNode.data) == 0) {
				boolean isLeftChildOfParent = true; // assume we are the left child
				if(parentNode != null && parentNode.right == curNode)
					isLeftChildOfParent = false;
				
				// if the target node has no children
				if(curNode.left == null && curNode.right == null) {
					// unlink
					if(parentNode != null) {
						if(isLeftChildOfParent) {
							parentNode.left = null;
						} else {
							parentNode.right = null;
						}
					} else {
						// this is the root
						root = null;
					}
					
					ret = true;
					break; // found
				}
				
				// if the target node has a single child
				if(curNode.left == null || curNode.right == null) {
					// select the single child
					TreeNode<T> singleChild = null;
					if(curNode.left != null) {
						singleChild = curNode.left;
					} else {
						singleChild = curNode.right;
					}
					
					// splice out the deleted node
					if(parentNode != null) {
						if(isLeftChildOfParent) {
							parentNode.left = singleChild;
						} else {
							parentNode.right = singleChild;
						}
					} else {
						// this is the root
						root = singleChild;
					}
					
					ret = true;
					break; // found
				}
				
				// if the target node has two children
				if(curNode.left != null && curNode.right != null) {
					successorNode = findSuccessor(curNode);
					
					// Note: if curNode is the root, then we swap it out instead of deleting
					
					// swap data
					curNode.data = successorNode.data;
					// we use curNode.right because we have chosen findSuccessor above
					ret = delete(curNode, curNode.right, successorNode.data);
					
					break; // found
				}

			} else {
				parentNode = curNode;
			}
			
			if(data.compareTo(curNode.data) == -1) {
				curNode = curNode.left;
			} else if(data.compareTo(curNode.data) == 1) {
				curNode = curNode.right;
			}
		}
		
		return ret;
	}
	
	// use pre order traversal to clone a tree, because the parent of children is created first before recursion
	
	/*
	 *  use breath first search to print the tree
	 *  AKA level order
	 */
	public void bfsPrint() {
		if(root == null)
			return;
		
		// ensure the vertex state are all set to NOT_EXPLORED
		//g.setResetAllVertexState();
				
		// color the start vertex partially explored
		root.state = NodeState.PARTIAL_EXPLORED;
		
		Queue<TreeNode<T>> queue = new ArrayDeque<TreeNode<T>>();
		queue.add(root);
		
		TreeNode<T> curNode = null;
		while(!queue.isEmpty()) {
			curNode = queue.remove();
			
			System.out.print(curNode.data.toString());
			System.out.print(" ");
			
			// check adjacent nodes
			if(curNode.left != null && curNode.left.state == NodeState.NOT_EXPLORED) {
				curNode.left.state = NodeState.PARTIAL_EXPLORED;
				queue.add(curNode.left);
			}
			if(curNode.right != null && curNode.right.state == NodeState.NOT_EXPLORED) {
				curNode.right.state = NodeState.PARTIAL_EXPLORED;
				queue.add(curNode.right);
			}
			
			// we explored all of the adjacent nodes, ie children
			curNode.state = NodeState.FULL_EXPLORED;
		}
	}
	
	public T getMax() {
		T ret = null;
		TreeNode<T> curNode = root;
		while(curNode != null) {
			ret = curNode.data;
			curNode = curNode.right;
		}
		return ret;
	}
	
	public T getMin() {
		T ret = null;
		TreeNode<T> curNode = root;
		while(curNode != null) {
			ret = curNode.data;
			curNode = curNode.left;
		}
		return ret;
	}
	
	public T findNextLarger(T data) {
		return null;
	}
	
	public T findNextSmaller(T data) {
		return null;
	}
	
	/*
	 * Range search. To implement the keys() method that returns the keys in a given range, we begin with a basic recursive BST traversal method, 
	 * known as inorder traversal. To illustrate the method, we consider the task of printing all the keys in a BST in order. To do so, 
	 * print all the keys in the left subtree (which are less than the key at the root by definition of BSTs), then print the key at the root, 
	 * then print all the keys in the right subtree, (which are greater than the key at the root by definition of BSTs).
	 */
	// do rank http://algs4.cs.princeton.edu/32bst/
	public static void main(String[] args) {
		BinaryTreeSearch<Integer> bst = new BinaryTreeSearch<Integer>();
		
		SecureRandom rand = new SecureRandom();
		
		int size = 10;
		int range = 100;

		for(int i = 0; i < size; i++) {
			bst.insertIterative(rand.nextInt(range));
		}
		
		System.out.println("In order traversal");
		bst.printInOrderTraversalRecursive();
		bst.printInOrderTraversalIterative();
		System.out.println("Max = " + bst.getMax());
		System.out.println("Min = " + bst.getMin());
		System.out.println("\nPost order traversal");
		bst.printPostOrderTraversalRecursive();
		System.out.println("\nPre order traversal");
		bst.printPreOrderTraversalRecursive();
		bst.printPreOrderTraversalIterative();
		System.out.println("\nlevel order traversal");
		bst.bfsPrint();
		System.out.println();
		System.out.println("----------------------------");
		
		bst = new BinaryTreeSearch<Integer>();
		bst.insertIterative(2);
		bst.insertIterative(1);
		bst.insertIterative(3);
		bst.insertIterative(4);
		
		System.out.println("BFS print");
		bst.bfsPrint();
		
		System.out.println("\nDeleting nodes");
		bst.printInOrderTraversalRecursive();
		bst.delete(2);
		bst.printInOrderTraversalIterative();
		bst.delete(4);
		bst.printInOrderTraversalIterative();
		
	}
	
	/*public String toString() {
		StringBuilder sb = new StringBuilder();
		int targetDepth = 1;
		int curDepth = 1;
		
		TreeNode<T> curNode = root;
		while(curNode != null) {
			if(data.compareTo(curNode.data) == -1) {
				if(curNode.left == null)
					curNode.left = new TreeNode<T>(data);
				curNode = curNode.left;
			} else {
				if(curNode.right == null)
					curNode.right = new TreeNode<T>(data);
				curNode = curNode.right;
			}
		}		
		return sb.toString();
	}*/
}
