package com.saiyd.searching;

import java.security.SecureRandom;
import java.util.LinkedList;

import com.saiyd.sorting.QuickSort;


/* http://en.wikipedia.org/wiki/Hash_table
 * 
 * The main advantage of hash tables over other table data structures is speed. 
 * This advantage is more apparent when the number of entries is large. Hash 
 * tables are particularly efficient when the maximum number of entries can be 
 * predicted in advance, so that the bucket array can be allocated once with 
 * the optimum size and never resized.
 * 
 * Best: O(1)
 * Average: O(1)
 * Worst: O(n)
 * 
 * Space	O(n)[1]	O(n)
 * Search	O(1)	O(n)
 * Insert	O(1)	O(n)
 * Delete	O(1)	O(n)
 * 
 * DIRECT-ADDRESS tables (not a hash table)
 * 
 * A simple technique that works well when the universe U of keys is reasonably
 * small. U = {0, 1, ... , m-1} where m is not too large. We shall assume no two
 * elements has he same key. To represent the dynamic set, we use an array, or
 * direct-address table in which each position corresponds to a key in the universe U.
 * 
 * Difficulty arises when the universe U is large, storing the table of size U may be
 * impractical or impossible. Furthermore the set of keys actually stored may be
 * small compared to U.
 * 
 * HASH FUNCTION
 * 
 * A basic requirement is that the function should provide a uniform distribution 
 * of hash values. A non-uniform distribution increases the number of collisions 
 * and the cost of resolving them. The function should be deterministic, h(x) should
 * be the same for each evaluation of the same input.
 * 
 * Perfect hash function
 * If all keys are known ahead of time, a perfect hash function can be used to 
 * create a perfect hash table that has no collisions. If minimal perfect hashing 
 * is used, every location in the hash table can be used as well.

 * Perfect hashing allows for constant time lookups in the worst case. This is in 
 * contrast to most chaining and open addressing methods, where the time for 
 * lookup is low on average, but may be very large (proportional to the number of entries) 
 * for some sets of keys.
 * 
 * 		DIVISION METHOD OF HASHING
 * 			In this method we map a key k into one of m slots by taking the remainder of
 * 			k divided by m.
 * 				hash(k) = k mod m
 * 
 * 			When using this method we usually avoid certain values of m. For example
 * 			m = 2.
 * 
 * 			A prime not too close to an exact power of 2 is often a good choice for m.
 * 
 * 		MULTIPLICATION METHOD
 * 			This method operates in two steps. First, we multiply the key k by a constant
 * 			A in the range 0 < A < 1 and extract the fractional part of kA. Then, we
 * 			multiply this value by m and take the floor of the result.
 * 				hash(k) = floor(m(k A mod 1)) , where "k A mod 1" means the fractional part of
 * 												kA, that is, kA - floor(kA)
 * 
 * 			The advantage of this method is that the value of m (number of slots) is not critical.
 * 
 * 		UNIVERSAL HASHING
 * 			If a malicious adversary chooses the keys to be hashed by some fixed hash function,
 * 			then he can choose n keys that all hash to the same slot, yielding an average
 * 			retrieval time of bigTheta(n).
 * 
 * 			The only effective way to improve this situation is to choose the hash function randomly
 * 			in a way that is independent of the keys.
 * 
 * 			Let H be a finite collection of hash functions that map a given universe U of keys
 * 			into the range {0, ... , m-1}. Such a collection is said to be universal if for each pair
 * 			of distinct keys k,l in U, the number of hash functions h in H for which
 * 			hash(k) = hash(l) is at most (abs(H)) / m
 * 
 * 		OPEN ADDRESSING
 * 			In open addressing, all elements are stored in the hash table itself. There are not lists
 * 			and no elements stored outside the table, as there is in chaining.
 * 
 * 			The hashtable can fill up; the load factor can never exceed 1.
 * 
 * 			The extra memory freed by not storing pointers provides the table with a 
 * 			larger number of slots for the same amount of memory.
 * 
 * 			To perform insertion we successively examine or probe the has table until
 * 			we find an empty slot in which to put the key. Instead of being
 * 			fixed in he order 0,1, ... , m-1 (which takes bigTheta(n) search time). The
 * 			sequence depends on the key being inserted. The hash function now takes an
 * 			additional parameter probe number.
 * 				hash(key, probeNumber)
 * 
 * 			It is required that for every key k, the probe sequence
 * 				hash(k,0) , hash(k,1), ... , hash(k, m-1) 
 * 			be a permutation of (0, 1, ... , m-1) , so that every hash-table position
 * 			is eventually considered as a slot for a new key.
 * 
 * 			Inserting an element into an open-address hash table with load factor alpha requires
 * 			at most 1 / (1 - alpha) probes on average, assuming uniform hashing
 * 
 * 				LINEAR PROBING
 * 					hash(k, i) = (hash'(k) + i) mod m
 * 					
 * 					Probes slots in a linear order and hen wraps around. Because the
 * 					initial probe determines the entire probe sequence, there are
 * 					only m distinct probe sequences.
 * 
 * 					Suffers from primary clustering. Long runs of occupied slots build
 * 					up, increasing the average search time.
 * 
 * 				QUADRATIC PROBING
 * 					hash(k,i) = (hash'(k) + a*i + b*i^2) mod m , where a and b are non zero constants
 * 
 * 					Later positions after the initial probe are offset by amounts that 
 * 					depend in a quadratic manner on the probe number i.
 * 
 * 					Works better than linear probing, but to make full use of the hash table
 * 					the values of a, b, and m are constrained. Also, if two keys have the same initial
 * 					probe position, then their probe sequences are the same. This property
 * 					leads to a milder form of clustering called secondary clustering. As in linear
 * 					probing, the initial probe determines the entire sequence, so only m distinct probe
 * 					sequences are used.
 * 
 * 				DOUBLE HASHING
 * 					This is one of the best methods available for open addressing because the permutations
 * 					produced have many of the characteristics of randomly chosen permutations.
 * 
 * 					hash(k,i) = (hash'(k) + i*hash"(k)) mod m
 * 
 * 					The later probe positions, after the initial probe, are offset by the amount hash"(k), mod m.
 * 					Thus the probe sequence here depends in two ways upon the key, since the initial probe
 * 					position, the offset, or both, may vary.
 * 
 * 					The value hash"(k) must be relatively prime to the hash table size m for the
 * 					entire hash table to be searched. A convenient way to ensure this is to let
 * 					m be a power of 2 and design hash"(k) so that it always produces an odd number. Another way
 * 					is to let m be prime and to design hash"(k) so that it always returns a positive integer
 * 					less than m.
 * 
 * 					bigTheta(m^2) probe sequences are used, this is an improvement over linear and quadratic
 * 					which have bigTheta(m) probe sequences.
 * 
 * 					Very close to the ideal scheme of uniform hashing
 * 
 * 		PERFECT HASHING
 * 			Hashing is most often used for its excellent expected performance, it can also be used to obtain
 * 			excellent worst-case performance when the set of keys is static. Once the keys are in the table
 * 			the set of keys never changes.
 * 
 * 			We call a hashing technique perfect hashing if the worst-case number of memory accesses required
 * 			to perform a search is O(1)
 * 
 * 			The basic idea is to use a two-level hashing scheme with universal hashing at each level. The 
 * 			first level is essentially the same as for hashing with chaining. Instead of making a list of the keys
 * 			hashing to slot j, we use a small secondary hash table with an associated hash function hashj().
 * 			By choosing hashj carefully we can guarantee that there are no collisions at the secondary level.
 * 
 * 			In order to guarantee that there are no collisions at the secondary level, we will need to let the size
 * 			mj of hash table Sj be the square of the number nj of keys hashing to slot j. 
 * LOAD FACTOR
 * 
 * A critical statistic for a hash table is called the load factor. This is 
 * simply the number of entries divided by the number of buckets, that is, 
 * n/k where n is the number of entries and k is the number of buckets.
 * 
 * If the load factor is kept reasonable, the hash table should perform well, 
 * provided the hashing is good. If the load factor grows too large, the hash 
 * table will become slow, or it may fail to work (depending on the method used). 
 * The expected constant time property of a hash table assumes that the load 
 * factor is kept below some bound. For a fixed number of buckets, the time 
 * for a lookup grows with the number of entries and so does not achieve the 
 * desired constant time.
 * 
 * Second to that, one can examine the variance of number of entries per 
 * bucket. For example, two tables both have 1000 entries and 1000 buckets; 
 * one has exactly one entry in each bucket, the other has all entries in 
 * the same bucket. Clearly the hashing is not working in the second one.
 * 
 * A low load factor is not especially beneficial. As the load factor 
 * approaches 0, the proportion of unused areas in the hash table increases, 
 * but there is not necessarily any reduction in search cost. 
 * This results in wasted memory
 * 
 * DRAW BACKS
 * 
 * Although operations on a hash table take constant time on average, the cost of 
 * a good hash function can be significantly higher than the inner loop of the 
 * lookup algorithm for a sequential list or search tree. Thus hash tables are not 
 * effective when the number of entries is very small. (However, in some cases the 
 * high cost of computing the hash function can be mitigated by saving the hash value 
 * together with the key.)
 * 
 * Hash tables in general exhibit poor locality of reference�that is, the data to 
 * be accessed is distributed seemingly at random in memory. Because hash tables 
 * cause access patterns that jump around, this can trigger microprocessor cache 
 * misses that cause long delays. Compact data structures such as arrays searched 
 * with linear search may be faster, if the table is relatively small and keys are 
 * compact. The optimal performance point varies from system to system.
 */
public class HashBasedSearch <T extends Comparable<T>> {
	private LinkedList<T>[] binArray;
	private int numberOfBins;
	private float loadFactor;
	
	public HashBasedSearch(int numberOfBins, T[] input) {
		binArray = (LinkedList<T>[]) new LinkedList[numberOfBins];
		this.numberOfBins = binArray.length;
		
		for(int i = 0; i < input.length; i++) {
			if(input[i] != null) {
				int bin = determineBin(input[i]);
				
				if(binArray[bin] == null) {
					binArray[bin] = new LinkedList<T>();
				}
				
				LinkedList<T> list = (LinkedList<T>) binArray[bin];
				list.addFirst(input[i]); // insert at the head to ensure O(1)
			}
		}
		
		// print the hash table
		for(int i = 0; i < binArray.length; i++) {
			System.out.print("bin " + i + " ");
			System.out.println(binArray[i]);
		}
		
		loadFactor = input.length / numberOfBins;
	}
	
	private int determineBin(T item) {
		int hash = item.hashCode();
		// determine the bin
		return hash % numberOfBins;
	}
	
	public float getLoadFactor() {
		return loadFactor;
	}

	public boolean search(T query) {
		boolean ret = false;
		
		int bin = determineBin(query);
		LinkedList<T> list = (LinkedList<T>) binArray[bin];
		if(list != null) {
			ret = list.contains(query);
		}
		return ret;
	}
	
	/*
	 * Deletion can be done in O(1) if the lists are
	 * doubly linked and the node to be deleted is
	 * passed are an argument
	 */
	
	public static void main(String[] args) {
		SecureRandom rand = new SecureRandom();
		
		int size = 20;
		int range = 25;
		Integer[] input = new Integer[size];
		for(int i = 0; i < input.length; i++) {
			input[i] = rand.nextInt(range);
		}
		
		HashBasedSearch<Integer> hashSearch = new HashBasedSearch<Integer>(10, input);
		
		System.out.println("load factor = " + hashSearch.getLoadFactor());
		System.out.println();
		
		System.out.println("Search for 1 = " + hashSearch.search(1));
		System.out.println("Search for 5 = " + hashSearch.search(5));
		System.out.println("Search for 7 = " + hashSearch.search(7));
		System.out.println("Search for 9 = " + hashSearch.search(9));
		System.out.println("Search for 23 = " + hashSearch.search(23));
	}
}
