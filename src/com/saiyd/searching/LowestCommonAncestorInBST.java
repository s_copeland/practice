package com.saiyd.searching;

import com.saiyd.searching.BinaryTreeSearch.TreeNode;


public class LowestCommonAncestorInBST <T extends Comparable<T>> {
	private TreeNode<T> root;
	
	public LowestCommonAncestorInBST(TreeNode<T> root) {
		this.root = root;
	}
	
	/*
	 * Time complexity of above solution is O(h) where h is height of tree. Also, the 
	 * above solution requires O(h) extra space in function call stack for recursive 
	 * function calls.
	 */
	public TreeNode<T> recursiveLCA(T a, T b) {
		TreeNode<T> nodeA = new TreeNode<T>(a);  
		TreeNode<T> nodeB = new TreeNode<T>(b);
		return doRecursiveLCA(root, nodeA, nodeB);
	}
	
	private TreeNode<T> doRecursiveLCA(TreeNode<T> curRoot, TreeNode<T> nodeA, TreeNode<T> nodeB) {
		if(curRoot == null || nodeA == null || nodeB == null)
			return null;
		
		if(curRoot.data == null || nodeA.data == null || nodeB.data == null)
			return null;
		
		if(curRoot.data.compareTo(nodeA.data) > 0 && curRoot.data.compareTo(nodeB.data) > 0)
			return doRecursiveLCA(curRoot.left, nodeA, nodeB);
		
		if(curRoot.data.compareTo(nodeA.data) < 0 && curRoot.data.compareTo(nodeB.data) < 0)
			return doRecursiveLCA(curRoot.right, nodeA, nodeB);
		
		return curRoot;
	}
	
	/*
	 * Runtime is O(h) with no extra space due to stack recursion
	 */
	public TreeNode<T> iterativeLCA(T a, T b) {
		if(root == null)
			return null;
		
		TreeNode<T> cur = root;
		
		while(cur != null) {
			if(cur.data.compareTo(a) > 0 && cur.data.compareTo(b) > 0) {
				cur = cur.left;
			} else if(cur.data.compareTo(a) < 0 && cur.data.compareTo(b) < 0) {
				cur = cur.right;
			} else {
				break;
			}
		}
		
		return cur;
	}
	
	public static void main(String[] args) {
		BinaryTreeSearch<Integer> bst = new BinaryTreeSearch<Integer>();
		bst.insertIterative(20);
		bst.insertIterative(8);
		bst.insertIterative(22);
		bst.insertIterative(4);
		bst.insertIterative(12);
		bst.insertIterative(10);
		bst.insertIterative(14);

		LowestCommonAncestorInBST<Integer> lcaib = new LowestCommonAncestorInBST<Integer>(bst.getRoot());
		int nodeA = 10;
		int nodeB = 14;
		System.out.println("The recursive LCA of " + nodeA + " " + nodeB + " is " + lcaib.recursiveLCA(nodeA, nodeB));
		System.out.println("The iterative LCA of " + nodeA + " " + nodeB + " is " + lcaib.iterativeLCA(nodeA, nodeB));
		
		nodeA = 14;
		nodeB = 8;
		System.out.println("The recursive LCA of " + nodeA + " " + nodeB + " is " + lcaib.recursiveLCA(nodeA, nodeB));
		System.out.println("The iterative LCA of " + nodeA + " " + nodeB + " is " + lcaib.iterativeLCA(nodeA, nodeB));
		
		nodeA = 10;
		nodeB = 22;
		System.out.println("The recursive LCA of " + nodeA + " " + nodeB + " is " + lcaib.recursiveLCA(nodeA, nodeB));
		System.out.println("The iterative LCA of " + nodeA + " " + nodeB + " is " + lcaib.iterativeLCA(nodeA, nodeB));
	}

}
