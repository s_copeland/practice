package com.saiyd.searching;

import com.saiyd.searching.BinaryTreeSearch.TreeNode;

/*
 * 			Average	    Worst case
 * Space	O(n)	    O(n)
 * Search	O(log n)	O(log n)
 * Insert	O(log n)	O(log n)
 * Delete	O(log n)	O(log n)
 * 
 * A self balancing tree to guarantee O(lg n) operations in the worst case
 * 
 * A binary search tree with one extra bit of storage per node that represents
 * the nodes color. RED or BLACK. By constraining the way nodes can be colored
 * on any path from the root to a leaf, red-black trees ensure that no such path
 * is more than twice as long as an other, so that the tree is approximately balanced.
 * 
 * A binary search tree is a red-black tree if it satisfies the following red-black
 * properties.
 * 		1. Every node is either red or black
 * 		2. The root is black
 * 		3. Every leaf (NIL) is black
 * 		4. If a node is red, then both its children are black
 * 		5. For each node, all paths from the node to descendant leaves
 * 		   contain the same number of black nodes
 * 
 * As a matter convenience in dealing with boundary conditions we use a single
 * sentinel to represent NIL. The color is black and the other fields can be
 * arbitrary values.
 * 
 * We call the number of black nodes on any path from, but not including, a node x
 * down to a leaf the black-height of the node, denoted by bh(x). The black height
 * of a red-black tree is the black height of its root.
 * 
 * Rotations are operation that preserves the binary search tree property
 * 		Left rotations "pivots" around the link from x to y. It makes y the new root 
 * 		of the subtree, with x as y's left child and y's left child as x's right child.
 */
public class RedBlackTree <T extends Comparable<T>> extends BinaryTreeSearch<T> {
	private final static class SentinelNode <T extends Comparable<T>> extends TreeNode<T> {

		public SentinelNode() {
			super(null);
			color = NodeColor.BLACK;
		}
		
	} 
	
	private SentinelNode<T> SENTINEL = new SentinelNode<T>();
	
	/*
	 * O(1) runtime
	 */
	private void rotateRight(TreeNode<T> node) {
		TreeNode<T> left = node.left;
		node.left = left.right;
		
		left.right.parent = node;
		left.parent = node.parent;
		if(node.parent == SENTINEL) {
			root = left;
		} else if(node == node.parent.right) {
			node.parent.right = left;
		} else {
			node.parent.left = left;
		}
			
		left.right = node;
		node.parent = left;
	}
	
	/*
	 * O(1) runtime
	 */
	private void rotateLeft(TreeNode<T> node) {
		TreeNode<T> right = node.right;
		node.right = right.left;
		
		right.left.parent = node;
		// link the new node up to the parent
		right.parent = node.parent; // link the rotated up right node to node's parent
		// link the parent down to the new rotated node
		if(node.parent == SENTINEL) {
			root = right;
		} else if(node == node.parent.left) { // if I'm my parent's left child
			node.parent.left = right;
		} else {
			node.parent.right = right;
		}
			
		right.left = node;
		node.parent = right;
	}
	
	/*
	 * O(lg n) runtime
	 * 
	 * Can break properties 2 and 4. This is why we need fixup.
	 * 
	 * Property 2 is violated if the node is the root
	 * Property 4 is violated if the node's parent is red
	 * 
	 * At most 2 rotations are performed during insert
	 */
	private TreeNode<T> doInsertIterative(TreeNode<T> node, T data) {
		TreeNode<T> parentNode = null;
		TreeNode<T> curNode = node;
		
		// find where to insert the new node
		while(curNode != null) {
			parentNode = curNode;
			if(data.compareTo(curNode.data) == -1) {
				curNode = curNode.left;
			} else {
				curNode = curNode.right;
			}
		}
	
		// add the new node
		if(parentNode == null) {
			// the tree was empty
			root = new TreeNode<T>(data);
			curNode = root;
		} else if(data.compareTo(parentNode.data) == -1) {
			parentNode.left = new TreeNode<T>(data);
			curNode = parentNode.left;
			// set the parent link
			curNode.parent = parentNode;
		} else {
			parentNode.right = new TreeNode<T>(data);
			curNode = parentNode.right;
			// set the parent link
			curNode.parent = parentNode;
		}
		
		// set the children to the sentinel
		curNode.left = SENTINEL;
		curNode.right = SENTINEL;
		curNode.color = NodeColor.RED;
		
		insertFixUp(curNode);
		return curNode;
	}

	/*
	 * The while loop maintains the following 3 part invariant.
	 * At the start of each iteration of the loop.
	 * 		1. curNode is red
	 * 		2. If curNode.parent is the root, then curNode.parent is black
	 * 		3. If there is a violation of the red-black properties, there is
	 * 			at most one violation and it is of either of property 2
	 * 			or property 4. If here is a violation of property 2, it occurs
	 * 			because curNode is the root and is red. If there is a violation
	 * 			of property 4, it occurs because both curNode and curNode.parent
	 * 			are red.
	 */
	private void insertFixUp(TreeNode<T> curNode) {
		while(curNode.parent.color == NodeColor.RED) {
			/*
			 * We know that curNode.parent.parent exists because of #2
			 * in the loop invariant.
			 * 
			 * Since we only enter a loop iteration only if curNode.parent is red,
			 * we know that curNode.parent cannot be the root
			 */
			if(curNode.parent == curNode.parent.parent.left) {
				TreeNode<T> uncle = curNode.parent.parent.right;
				if(uncle.color == NodeColor.RED) {
					curNode.parent.color = NodeColor.BLACK;
					uncle.color = NodeColor.BLACK;
					curNode.parent.parent.color = NodeColor.RED;
					// move up two levels
					curNode = curNode.parent.parent;
				} else {
					if(curNode == curNode.parent.right) {
						curNode = curNode.parent;
						rotateLeft(curNode);
					}
					
					curNode.parent.color = NodeColor.BLACK;
					curNode.parent.parent.color = NodeColor.RED;
					rotateRight(curNode.parent.parent);
				}
			} else {
				TreeNode<T> uncle = curNode.parent.parent.left;
				if(uncle.color == NodeColor.RED) {
					curNode.parent.color = NodeColor.BLACK;
					uncle.color = NodeColor.BLACK;
					curNode.parent.parent.color = NodeColor.RED;
					// move up two levels
					curNode = curNode.parent.parent;
				} else {
					if(curNode == curNode.parent.left) {
						curNode = curNode.parent;
						rotateRight(curNode);
					}
					
					curNode.parent.color = NodeColor.BLACK;
					curNode.parent.parent.color = NodeColor.RED;
					rotateLeft(curNode.parent.parent);
				}
			}
		}
		
		root.color = NodeColor.BLACK;
	}
	
	/*
	 * Deletion causes at most 3 rotations
	 */
	/*public boolean delete(T data) {
		if(root == null)
			return false;

		return delete(null, root, data);
	}*/
}
