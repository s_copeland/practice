package com.saiyd.searching;

import java.security.SecureRandom;

import com.saiyd.sorting.CountingSort;

/*
 * Input: A set name A of n distinct numbers and a number i, with 1 <= i <= n
 * Output: The element x in the set A that is larger than exactly i - 1 other elements of A
 * 
 * The iTh order statistic of a set of n elements is the iTh smallest element.
 * 		Ex: 
 * 			The minimum of a set of elements is the first order statistic (i = 1)
 * 			The maximum of a set of elements is the nTh order statistic (i = n)
 * 			A median is the halfway point of a set
 * 				When n is odd, the median is unique, occuring at i = (n + 1)/2
 * 				When n is even, there are 2 medians, occuring at i = n/2 and i = n/2 + 1
 */
public class SelectionProblem {
	/*
	 * Finding the min or max can be found in n - 1 comparisons or less
	 * 
	 * Every element except the winner (min or max) must lose at least one
	 * match. Hence, n - 1 comparisons are necessary to determine the minimum
	 */
	public int selectMin(int[] input) {
		int ret = input[0];
		if(input.length > 1) {
			for(int i = 1; i < input.length; i++) {
				if(ret > input[i]) 
					ret = input[i];
			}
		}
		return ret;
	}
	
	public int selectMax(int[] input) {
		int ret = input[0];
		if(input.length > 1) {
			for(int i = 1; i < input.length; i++) {
				if(ret < input[i]) 
					ret = input[i];
			}
		}
		return ret;
	}
	
	private int min;
	private int max;
	public int getMin() {
		return min;
	}

	public int getMax() {
		return max;
	}

	/*
	 * Find the min and max at the same time
	 * 
	 * At most 3 * floor(n/2) comparisons are sufficent to
	 * find the minimum and maximum elements
	 * 		3 comparisons for every 2 elements
	 * 			vs
	 * 		2 comparisons per element in a normal algorithm that checks each element against
	 * 		the current min and max
	 */
	public void selectMinAndMax(int[] input) {
		int len = input.length;
		int index = 1;
		if(len % 2 == 0) {
			// even
			if(input[0] < input[1]) {
				min = input[0];
				max = input[1];
			} else {
				min = input[1];
				max = input[0];
			}
			index = 2;
		} else {
			// odd
			min = input[0];
			max = input[0];
		}
		
		// compare pairs
		int curMin = 0;
		int curMax = 0;
		for(; (index + 1) < input.length; index += 2) {
			// Find the min and max of this pair
			if(input[index] < input[index + 1]) {
				curMin = input[index];
				curMax = input[index + 1];
			} else {
				curMin = input[index + 1];
				curMax = input[index];
			}
			
			// compare with the global min and max
			if(curMin < min)
				min = curMin;
			if(curMax > max)
				max = curMax;
		}
	}
	
	/*
	 * Find the iTh smallest element of the input array
	 */
	public int findKthSmallest(int[] input, int kThOrderStatistic) {
		if(input == null)
			throw new IllegalArgumentException("input cannot be null");
		if(input.length < 1)
			throw new IllegalArgumentException("There must be at least one element in the input");
		if(kThOrderStatistic  < 1)
			throw new IllegalArgumentException("kThOrderStatistic must be greater than 0");
		if(kThOrderStatistic  > input.length)
			throw new IllegalArgumentException("kThOrderStatistic must at most " + input.length);
		return randomizedSelect(input, 0, input.length -1, kThOrderStatistic);
	}
	
	private SecureRandom sr = new SecureRandom();
	
	private int randomizedSelect(int[] input, int start, int end, int kThStat) {
		if(start == end)
			return input[start];

		int pivot = randomizedPartition(input, start, end);
		int k = pivot - start + 1;
		if(kThStat == k) {
			return input[pivot];
		} else if(kThStat < k) {
			return randomizedSelect(input, start, pivot - 1, kThStat);
		} else {
			return randomizedSelect(input, pivot + 1, end, kThStat - k);
		}
	}
	
	private int randomizedPartition(int[] input, int start, int end) {
		// chose random pivot
		int pivotOffset = sr.nextInt(end - start); // choose a number between 0 and partition size, we use this
		int pivot = start + pivotOffset;
		
		// swap
		swap(input, end, pivot);
		return partition(input, start, end);
	}

	private void swap(int[] input, int x, int y) {
		int temp = input[y];
		input[y] = input[x];
		input[x] = temp;
	}
	
	private int partition(int[] input, int start, int end) {
		int x = input[end];
		int i = start -1;
		for(int j = start; j < end; j++) {
			if(input[j] <= x) {
				/*
				 * This version of partition is different from the one in
				 * quicksort. We set i to start -1, so we must increment
				 * before we swap to avoid i = -1 in the initial case
				 */
				i++;
				swap(input, j, i);
			}
		}
		swap(input, i + 1, end);
		return i + 1;
	}
	
	private static void print(int[] input, boolean indent){
		if(indent)
			System.out.print("\t");
		for(int i = 0; i < input.length; i++) {
			System.out.print(input[i]);
			System.out.print(" ");
		}
		System.out.println();
	}
	
	public static void main(String[] args) {
		SelectionProblem sp = new SelectionProblem();
		SecureRandom rand = new SecureRandom();
		
		int size = 11;
		int range = 100;
		int[] input = new int[size];
		int[] input2 = new int[size];
		for(int i = 0; i < input.length; i++) {
			input[i] = rand.nextInt(range);
			input2[i] = input[i];
		}
		
		CountingSort cs = new CountingSort();
		cs.sort(input2, range);
		
		print(input, false);
		print(input2, false);
		
		System.out.println("Find min and max separately");
		System.out.println("Max: " + sp.selectMax(input));
		System.out.println("Min: " + sp.selectMin(input));
		
		System.out.println("Find min and max at the same time");
		sp.selectMinAndMax(input);
		System.out.println("Max: " + sp.getMax());
		System.out.println("Min: " + sp.getMin());
		
		System.out.println("Find kTh order statistic");
		System.out.println("1st: " + sp.findKthSmallest(input, 1));
		System.out.println("2nd: " + sp.findKthSmallest(input, 2));
		System.out.println("median: " + sp.findKthSmallest(input, input.length / 2));
		System.out.println("last: " + sp.findKthSmallest(input, input.length));
	}
}
