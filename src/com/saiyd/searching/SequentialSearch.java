package com.saiyd.searching;

/*
 * This is best used if N is relatively small or you won't be performing the search over again often
 * 
 * Best: O(1) - when the item begin queried is the first element
 * Average: O(n)
 * Worst: O(n) - when you search for an item that is not in the collection
 */
public class SequentialSearch <T extends Comparable<T>> {

	public boolean search(T[] input, T query) {
		boolean ret = false;
		
		for(int i = 0; i < input.length; i++) {
			/*
			 * Checking for null in each element slows performance, if a sentinel value 
			 * was used instead of null, then the if statement would only need to test
			 * the value for equality
			 * 
			 * A sentinel value always returns false when compared to any other item
			 */
			if(input[i] != null && input[i].compareTo(query) == 0) {
				ret = true;
				break;
			}
		}
		
		return ret;
	}
	
	/*
	 * Variation to improve performance
	 * 
	 * Move to front on success
	 * 		This strategy is suitable if there is an increased likelihood that an item wil be searched
	 * 		for again. When item T is found at location i, move the elements from A[0,i-1] to A[1,i]
	 * 		and move T to A[0]. Ex: MRU paging algorithms
	 * 
	 * Move up on success
	 * 		This strategy is suitable if there is an increased likelihood that an item will be searched
	 * 		for again; and there is a desire to avoid the cost of moving lots of elements. When T is found
	 * 		at location i, swap element A[i-1] with A[i] (except when at the front). This way more frequently
	 * 		searched items bubble to the front
	 * 
	 * Move to the end on success
	 * 		If a single element is unlikely to be searched for multiple times, then moving it to the end
	 * 		when it is found will improve the performance of future searches. This can be the opposite
	 * 		of either of the above approaches
	 */
}
