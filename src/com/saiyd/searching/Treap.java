package com.saiyd.searching;

import java.security.SecureRandom;

import com.saiyd.searching.BinaryTreeSearch.NodeColor;
import com.saiyd.searching.BinaryTreeSearch.TreeNode;

/*
 *          Average	    Worst case
 * Space	O(n)	    O(n)
 * Search	O(log n)	amortized O(log n)
 * Insert	O(log n)	amortized O(log n)
 * Delete	O(log n)	amortized O(log n)
 * 
 * The treap and the randomized binary search tree are two closely related 
 * forms of binary search tree data structures that maintain a dynamic set 
 * of ordered keys and allow binary searches among the keys
 * 
 * In addition to being a binary search tree, the nodes in a Treap also obey the heap property.
 * 
 * The important point about the priority values in a Treap is that they are unique and assigned at random. 
 * 
 * It is a Cartesian tree[3] in which each key is given a (randomly chosen) 
 * numeric priority. As with any binary search tree, the inorder traversal 
 * order of the nodes is the same as the sorted order of the keys. The structure 
 * of the tree is determined by the requirement that it be heap-ordered: that is, 
 * the priority number for any non-leaf node must be greater than or equal to 
 * the priority of its children. Thus, as with Cartesian trees more generally, 
 * the root node is the maximum-priority node, and its left and right subtrees 
 * are formed in the same manner from the subsequences of the sorted order to 
 * the left and right of that node.
 * 
 * The problem with random binary search trees is, of course, that they are not 
 * dynamic. They don't support the add(x) or remove(x)
 * operations needed to implement the SSet interface.
 */
public class Treap <T extends Comparable<T>> extends BinaryTreeSearch<T> {
	private SecureRandom sr = new SecureRandom();
	/*
	 * O(1) runtime
	 */
	private void rotateRight(TreeNode<T> node) {
		TreeNode<T> left = node.left;
		node.left = left.right;
		
		left.right.parent = node;
		left.parent = node.parent;
		if(node.parent == null) {
			root = left;
		} else if(node == node.parent.right) {
			node.parent.right = left;
		} else {
			node.parent.left = left;
		}
			
		left.right = node;
		node.parent = left;
	}
	
	/*
	 * O(1) runtime
	 */
	private void rotateLeft(TreeNode<T> node) {
		TreeNode<T> right = node.right;
		node.right = right.left;
		
		right.left.parent = node;
		// link the new node up to the parent
		right.parent = node.parent; // link the rotated up right node to node's parent
		// link the parent down to the new rotated node
		if(node.parent == null) {
			root = right;
		} else if(node == node.parent.left) { // if I'm my parent's left child
			node.parent.left = right;
		} else {
			node.parent.right = right;
		}
			
		right.left = node;
		node.parent = right;
	}
	
	/*
	 * O(lg n) runtime
	 * 
	 * Can break properties 2 and 4. This is why we need fixup.
	 * 
	 * Property 2 is violated if the node is the root
	 * Property 4 is violated if the node's parent is red
	 * 
	 * At most 2 rotations are performed during insert
	 */
	private TreeNode<T> doInsertIterative(TreeNode<T> node, T data) {
		TreeNode<T> parentNode = null;
		TreeNode<T> curNode = node;
		
		// find where to insert the new node
		while(curNode != null) {
			parentNode = curNode;
			if(data.compareTo(curNode.data) == -1) {
				curNode = curNode.left;
			} else {
				curNode = curNode.right;
			}
		}
	
		// add the new node
		if(parentNode == null) {
			// the tree was empty
			root = new TreeNode<T>(data);
			curNode = root;
		} else if(data.compareTo(parentNode.data) == -1) {
			parentNode.left = new TreeNode<T>(data);
			curNode = parentNode.left;
			// set the parent link
			curNode.parent = parentNode;
		} else {
			parentNode.right = new TreeNode<T>(data);
			curNode = parentNode.right;
			// set the parent link
			curNode.parent = parentNode;
		}
		
		// set the priority
		curNode.priority = sr.nextInt();
		
		// set the children to the sentinel
		curNode.left = null;
		curNode.right = null;
		curNode.color = NodeColor.RED;
		
		bubbleUp(curNode); // maintain he heap property
		return curNode;
	}

	private void bubbleUp(TreeNode<T> curNode) {
		while(curNode.parent != null && curNode.parent.priority > curNode.priority) {
			if(curNode.parent.right == curNode) {
				rotateLeft(curNode.parent);
			} else {
				rotateRight(curNode.parent);
			}
		}
		
		if(curNode.parent == null)
			root = curNode;
	}
	
	public boolean delete(T data) {
		if(root == null)
			return false;

		return delete(null, root, data);
	}

	private boolean delete(TreeNode<T> parentNode, TreeNode<T> startNode, T data) {
		boolean ret = false;
		TreeNode<T> successorNode = null;
		TreeNode<T> curNode = startNode;
		
		// find the node to delete
		while(curNode != null) {
			if(data.compareTo(curNode.data) == 0) {
				trickleDown(curNode);
				splice(curNode);
			} else {
				parentNode = curNode;
			}
			
			if(data.compareTo(curNode.data) == -1) {
				curNode = curNode.left;
			} else if(data.compareTo(curNode.data) == 1) {
				curNode = curNode.right;
			}
		}
		
		return ret;
	}

	private void trickleDown(TreeNode<T> curNode) {
		while(curNode.left != null || curNode.right != null) {
			if(curNode.left == null) {
				rotateLeft(curNode);
			} else if(curNode.right == null) {
				rotateRight(curNode);
			} else if(curNode.left.priority < curNode.right.priority) {
				rotateRight(curNode);
			} else {
				rotateLeft(curNode);
			}
			
			if(root == curNode) {
				root = curNode.parent;
			}
		}
	}
	
	private void splice(com.saiyd.searching.BinaryTreeSearch.TreeNode<T> curNode) {
		TreeNode<T> s;
		TreeNode<T> parentNode;;
		
		if(curNode.left != null) {
			s = curNode.left;
		} else {
			s = curNode.right;
		}
		
		if(curNode == root) {
			root = s;
			parentNode = null;
		} else {
			parentNode = curNode.parent;
			if(parentNode.left == curNode) {
				parentNode.left = s;
			} else {
				parentNode.right = s;
			}
		}
		
		if(s != null) {
			s.parent = parentNode;
		}
	}
}
