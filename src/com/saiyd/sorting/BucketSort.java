package com.saiyd.sorting;

import java.io.StreamTokenizer;
import java.security.SecureRandom;
import java.util.Date;
import java.util.Vector;

/*
 * Use bucket sort if the following two properties hold
 * 		Uniform distribution - The input data must be uniformly distributed or a given range.
 * 			Based on this distribution, n buckets are created to evenly partition the input range
 * 
 * 		Ordered hash function - The buckets must be ordered. If i < j, then elements inserted into
 * 			bucket bi are lexicographically smaller than elements in bucket bj
 * 
 * Bucket sort works as follows:

    Set up an array of initially empty "buckets".
    Scatter: Go over the original array, putting each object in its bucket.
    Sort each non-empty bucket.
    Gather: Visit the buckets in order and put all elements back into the original array.
 * 
 * Notes: The insert into the bucket should take O(1), this can be accomplished with a linked list or vector
 * Best: O(n)
 * Average: O(n)
 * Worst: O(n^2)  ?
 * 
 * Bucket sort is not appropriate for sorting arbitrary  strings. Why?
 * 
 * 
 * Generic bucket sort[edit]
 * The most common variant of bucket sort operates on a list of n numeric inputs 
 * between zero and some maximum value M and divides the value range into n buckets 
 * each of size M/n. If each bucket is sorted using insertion sort, the sort can be 
 * shown to run in expected linear time (where the average is taken over all possible 
 * inputs).[2] However, the performance of this sort degrades with clustering; if 
 * many values occur close together, they will all fall into a single bucket and be 
 * sorted slowly.
 */
public class BucketSort {

	public void sort(float[] input, int numBuckets) {
		Vector[] buckets = new Vector[numBuckets];
		
		for(int i=0; i < input.length; i++) {
			int bucketIndex = (int) (numBuckets * input[i]);
			if(buckets[bucketIndex] == null) {
				buckets[bucketIndex] = new Vector<Float>();
			}
			
			buckets[bucketIndex].add(input[i]);
		}
		
		// sort each bucket
		for(int i=0; i < numBuckets; i++) {
			if(buckets[i] != null)
				sort(buckets[i]);
		}
		
		// concatenate all buckets
		for(int i=0; i < numBuckets; i++) {
			if(buckets[i] != null) {
				for(int j=0; j < buckets[i].size(); j++) {
					System.out.print( buckets[i].get(j));
					System.out.print(" ");
				}
			}
		}
		System.out.println();
	}
	
	public Vector<Float> sort(Vector<Float> input) {
		// insert each element back into the same array using insert
		for(int i = 0; i < input.size(); i++) {
			insert(input, i, input.get(i));
		}
		
		return input;
	}

	/**
	 * 
	 * @param input
	 * @param position
	 * @param value
	 */
	private void insert(Vector<Float> input, int position, Float value) {
		/*
		 * compare value to all proceeding positions
		 * if value is smaller then slide the proceeding value
		 * up and then move down to the next proceeding value
		 * until we find one that is not greater
		 * 
		 * then place ourselves here
		 */
		int index = position -1;
		while(index >= 0 && input.get(index).compareTo(value) == 1) {
			input.set(index + 1, input.get(index));
			index = index -1;
			
			//print(input, true);
		}
		input.set(index + 1, value);
		
		//print(input, false);
	}
	
	private void print(Vector<Float> input, boolean indent){
		if(indent)
			System.out.print("\t");
		for(int i = 0; i < input.size(); i++) {
			System.out.print(input.get(i));
			System.out.print(" ");
		}
		System.out.println();
	}
	
	public static void main(String[] args) {
		SecureRandom rand = new SecureRandom();
		
		int size = 10;
		float[] input = new float[size];
		for(int i = 0; i < input.length; i++) {
			input[i] = rand.nextFloat();
			System.out.print(input[i]);
			System.out.print(" ");
		}
		System.out.println();


		System.err.println("Sorting started");

		BucketSort bs = new BucketSort(); // calling constructor sorts

		bs.sort(input, 5);
	}
}
