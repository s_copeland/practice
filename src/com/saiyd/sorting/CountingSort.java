package com.saiyd.sorting;

import java.security.SecureRandom;

/*
 * Its running time is linear in the number of items and the difference between 
 * the maximum and minimum key values, so it is only suitable for direct use 
 * in situations where the variation in keys is not significantly greater than 
 * the number of items. 
 * 
 * What if the elements are in range from 1 to n^2? 
 * We can�t use counting sort because counting sort will take O(n^2) 
 * which is worse than comparison based sorting algorithms. Can we 
 * sort such an array in linear time?
 * Radix Sort is the answer. The idea of Radix Sort is to do digit 
 * by digit sort starting from least significant digit to most 
 * significant digit. Radix sort uses counting sort as a subroutine to sort.
 * 
 * This is a non comparison sort
 * 
 * This works because we know something about the elements in advance.
 * It is best used for sorting integers from a fixed range [0,k).
 * It can also be used whenever a total ordering of k elements can be determined
 * and a unique integer 0 <= i <= k can be assigned for those k elements
 * 
 * For example, if sorting a set of fractions of the form 1/p (where p is an 
 * integer) whose maximum denominator p is k, then each fraction 1/p can be assigned
 * the unique value k -p
 * 
 * AKA Histogram Sort
 * 
 * Best: O(n)
 * Average: O(n)
 * Worst: O(n)
 */
public class CountingSort {

	public int[] sort(int[] input, int numBuckets) {
		// create buckets to count instances of each type
		int[] buckets = new int[numBuckets];
		
		print(input, false);
		print(buckets, true);
		
		// scan the array and count # of instances of each type
		for(int i = 0; i < input.length; i++) {
			buckets[input[i]]++;
		}
		
		print(input, false);
		print(buckets, true);
		
		int index = 0;
		
		for(int i = 0; i < buckets.length; i++) {
			while(buckets[i] > 0) {
				input[index++] = i;
				buckets[i]--;
			}
		}
		
		print(input, false);
		print(buckets, true);
		return input;
	}
	
	/*
	 * the algorithm loops over the items, computing a histogram of the number of times each 
	 * key occurs within the input collection. It then performs a prefix sum computation 
	 * (a second loop, over the range of possible keys) to determine, for each key, the 
	 * starting position in the output array of the items having that key. Finally, it 
	 * loops over the items again, moving each item into its sorted position in the output array
	 */
	public int[] sortStable(int[] input, int numBuckets) {
		 int[] output = new int[input.length];
		 
		// create buckets to count instances of each type
		int[] buckets = new int[numBuckets];
		
		System.out.println("Step 0");
		print(input, false);
		print(buckets, true);
		
		/*
		 * 1) Use a bucket array to store the count of each unique object.
		 * Input:     2 1 2 4 2 3 3 3 0 2
		 * Index:     0  1  2  3  4
		 * Count:     1  1  4  3  1
		 */
		// scan the array and count # of instances of each type
		for(int i = 0; i < input.length; i++) {
			buckets[input[i]]++;
		}
		System.out.println("Step 1");
		print(input, false);
		print(buckets, true);
		
		/*
		 * 2) Modify the bucket array such that each element at each index 
		 *   stores the starting index of that bucket. 
		 * Input:     2 1 2 4 2 3 3 3 0 2
		 * Index:     0  1  2  3  4
		 * Count:       1  1  4  3  1
		 * Start Pos: 0  1  2  6  9    (Notice the gap between each pos is equal to count)
		 * 
		 * The lowest class of number will always start at zero, so we
		 * need to determine where the rest start.
		 * 
		 * You can go forward from the low end to the high end or reverse.
		 * There reverse direction allows you to ommit a temp
		 * variable for the running sum
		 * 
		 * We use the reverse here and handle the initial case
		 * outside the loop
		 * 
		 * The starting position of the highest class of number is
		 *    startPos = array.len - numOfItemsInClass
		 * 
		 * Once we know this we work out way backward to zero
		 */
		int lastIndex = buckets.length -1;
		buckets[lastIndex] = input.length - buckets[lastIndex];
		// calculate the starting index for each key
		for(int i = lastIndex -1; i > -1; i--) {
			// 
			buckets[i] = buckets[i + 1] - buckets[i];
		}
		System.out.println("Step 2");
		print(input, false);
		print(buckets, true);
		System.out.println("----------");
		/*
		 * 3) Output each object from the input sequence followed by 
		 *   increasing the start pos by 1, so duplicate items don't
		 *   overwrite each other.
		 *   
		 *   Input:     2 1 2 4 2 3 3 3 0 2
		 *   Index:     0  1  2  3  4
		 *   Start Pos: 0  1  2  6  9  
		 *    
		 *   For the first item, the start position of 2 is index 2.
		 *   Put data 2 at index 2 in output. Increase bucket start pos by 1 to place 
		 *   next data 2 at an index 1 larger than before.
		 */
		for(int i = 0; i < input.length; i++) {
			output[buckets[input[i]]] = input[i];
			buckets[input[i]] += 1;
			print(output, false);
			print(buckets, true);
			System.out.println("----------");
		}
		
		print(output, false);
		print(buckets, true);
		
		return output;
	}
	
	/*
	 * same version, but for any BASE X number
	 */
	public int[] sortStable(int[] input, int numBuckets, int exponent, int base) {
		 int[] output = new int[input.length];
		 
		// create buckets to count instances of each type
		int[] buckets = new int[numBuckets];
		
		System.out.println("Step 0");
		print(input, false);
		print(buckets, true);
		
		// scan the array and count # of instances of each type
		for(int i = 0; i < input.length; i++) {
			// for base 10 this would be the one's, ten's, hundreths's place digit
			int nThPlaceValueForBase = (input[i] / exponent) % base;
			buckets[nThPlaceValueForBase]++;
		}
		System.out.println("Step 1");
		print(input, false);
		print(buckets, true);
		
		int lastIndex = buckets.length -1;
		buckets[lastIndex] = input.length - buckets[lastIndex];
		// calculate the starting index for each key
		for(int i = lastIndex -1; i > -1; i--) {
			// 
			buckets[i] = buckets[i + 1] - buckets[i];
		}
		System.out.println("Step 2");
		print(input, false);
		print(buckets, true);
		System.out.println("----------");

		for(int i = 0; i < input.length; i++) {
			int nThPlaceValueForBase = (input[i] / exponent) % base;
			output[buckets[nThPlaceValueForBase]] = input[i];
			buckets[nThPlaceValueForBase] += 1;
			print(output, false);
			print(buckets, true);
			System.out.println("----------");
		}
		
		print(output, false);
		print(buckets, true);
		
		return output;
	}
	
	private void print(int[] input, boolean indent){
		if(indent)
			System.out.print("\t");
		for(int i = 0; i < input.length; i++) {
			System.out.print(input[i]);
			System.out.print(" ");
		}
		System.out.println();
	}
	
	public static void main(String[] args) {
		CountingSort countSort = new CountingSort();
		SecureRandom rand = new SecureRandom();
		
		int size = 10;
		int range = 5;
		int[] input = new int[size];
		int[] output;
		for(int i = 0; i < input.length; i++) {
			input[i] = rand.nextInt(range);
		}
		/*input[0] = 2;
		input[1] = 1;
		input[2] = 2;
		input[3] = 4;
		input[4] = 2;
		input[5] = 3;
		input[6] = 3;
		input[7] = 3;
		input[8] = 0;
		input[9] = 2;*/
		
		output = countSort.sortStable(input, range);
		countSort.print(output, false);
	}
}
