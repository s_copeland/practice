package com.saiyd.sorting;

import java.security.SecureRandom;
import java.util.Arrays;

import com.saiyd.datastructures.BinaryHeap;

/*
 * comparison sort
 * 
 * unstable sort
 */
public class HeapSort <T extends Comparable<T>> {

	public void sort(T[] array, int count, BinaryHeap.Type type) {
		int end = count -1;
		
		BinaryHeap<T> binaryHeap = new BinaryHeap<T>(type);
		binaryHeap.buildHeap(array, end);
		
		while(end > 0) {
			/*
			 * swap the root max/min value of the heap with
			 * the last element of the heap
			 */
			T temp = array[0];
			array[0] = array[end];
			array[end] = temp;
			
			/*
			 * decrease the size of the heap by one,
			 * so  that the previous max/min value will
			 * stay in its proper place
			 */
			end = end - 1;
			binaryHeap.heapify(array, end, 0);
		}
	}
	
	public static void main(String[] args) {
		HeapSort<Integer> hs = new HeapSort<Integer>();
		
		SecureRandom sr = new SecureRandom();
		
		Integer[] array = new Integer[10];
		for(int i = 0; i < array.length; i++) {
			array[i] = sr.nextInt(20);
		}
		
		System.out.println(Arrays.toString(array));
		
		hs.sort(array, array.length, BinaryHeap.Type.MAX);
		
		System.out.println(Arrays.toString(array));
	}
}
