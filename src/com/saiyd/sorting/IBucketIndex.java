package com.saiyd.sorting;

public interface IBucketIndex <T> {
	public T getType();
	public int getBucketIndex();
}