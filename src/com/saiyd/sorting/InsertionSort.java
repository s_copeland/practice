package com.saiyd.sorting;

/**
 * comparison sort
 * 
 * stable sort
 * 
 * Use this sort when you have a small number of elements to sort
 * or the elements in the intial collection are already "nearly sorted"
 * 
 * Best: O(n) - items already  sorted
 * Average: O(n^2)
 * Worst: O(n^2)
 * 
 * 
 * Already sorted collections offer the best performance, while collections
 * sorted in reverse order offer the worst performance
 * 
 * @author Saiyd
 *
 * @param <T>
 */
public class InsertionSort <T extends Comparable<T>> {

	public T[] sort(T[] input) {
		// insert each element back into the same array using insert
		for(int i = 0; i < input.length; i++) {
			insert(input, i, input[i]);
		}
		
		return input;
	}

	/**
	 * 
	 * @param input
	 * @param position
	 * @param value
	 */
	private void insert(T[] input, int position, T value) {
		/*
		 * compare value to all proceeding positions
		 * if value is smaller then slide the proceeding value
		 * up and then move down to the next proceeding value
		 * until we find one that is not greater
		 * 
		 * then place ourselves here
		 */
		int index = position -1;
		while(index >= 0 && input[index].compareTo(value) == 1) {
			input[index + 1] = input[index];
			index = index -1;
			
			print(input, true);
		}
		input[index + 1] = value;
		
		print(input, false);
	}
	
	private void print(T[] input, boolean indent){
		if(indent)
			System.out.print("\t");
		for(int i = 0; i < input.length; i++) {
			System.out.print(input[i]);
			System.out.print(" ");
		}
		System.out.println();
	}
	
	public static void main(String[] args) {
		InsertionSort<Integer> insertSort = new InsertionSort<Integer>();
		
		Integer[] input = new Integer[10];
		input[0] = 1;
		input[1] = 4;
		input[2] = 8;
		input[3] = 9;
		input[4] = 11;
		input[5] = 15;
		input[6] = 7;
		input[7] = 12;
		input[8] = 13;
		input[9] = 6;
		
		insertSort.sort(input);
	}
}
