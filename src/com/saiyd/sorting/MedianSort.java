package com.saiyd.sorting;

/**
 * Dividing and Conquer
 * 
 * Best: O(n log n)
 * Average: O(n log n)
 * Worst: O(n^2)
 * 
 * @author Saiyd
 *
 */
public class MedianSort <T extends Comparable<T>> {

	public T[] sort(T[] input) {
		medianSort(input, 0, input.length -1);
		return input;
	}
	
	public void medianSort(T[] input, int leftIndex, int rightIndex) {
		// if the sub array to be sorted has 1 or fewer elements
		if(rightIndex <= leftIndex)
			return;
		
		int mid = (rightIndex - leftIndex + 1) / 2;
		int median = selectKth(input, mid + 1, leftIndex, rightIndex);
		
		medianSort(input, leftIndex, leftIndex + mid -1);
		medianSort(input, leftIndex + mid + 1, rightIndex);
	}

	/*
	 * Average-case linear time recursive algorithm to find position of Kth
	 * element in the input array, which is modified as this computation proceeds
	 * 
	 */
	private int selectKth(T[] input, int i, int leftIndex, int rightIndex) {
		//int index = 
		return 0;
	}
}
