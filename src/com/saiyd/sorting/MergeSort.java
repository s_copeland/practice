package com.saiyd.sorting;

import java.security.SecureRandom;

/*
 * Worst case performance	O(n log n)
 * Best case performance	
 * O(n log n) typical,
 * 
 * O(n) natural variant
 * Average case performance	O(n log n)
 * Worst case space complexity	O(n) auxiliary
 */
public class MergeSort {
	private int[] numbers;
	private int[] helper;
	private int count;
	
	public void sortTopDownMerge(int[] input) {
		this.numbers = input;
		count = input.length;
		this.helper = new int[count];
		mergeSort(0, count -1);
	}

	private void mergeSort(int low, int high) {
		// if low is smaller than high, then the array is sorted
		if(low < high) {
			// get the index of the element which is in the middle
			int middle = low + (high - low) / 2;
			// sort the left side of the array
			mergeSort(low, middle);
			// sort the right side of the array
			mergeSort(middle + 1, high);
			// combine both sides
			merge(low, middle, high);
		}
	}

	private void merge(int low, int middle, int high) {
		// copy both parts into the helper array
		for(int i = low; i <= high; i++) {
			helper[i] = numbers[i];
		}
		
		int i = low;
		int j = middle + 1;
		int k = low;
		
		// copy the smallest values from either the left or the right side back
		// to the original array
		while(i <= middle && j <= high) {
			if(helper[i] <= helper[j]) {
				numbers[k] = helper[i];
				i++;
			} else {
				numbers[k] = helper[j];
				j++;
			}
			k++;
		}
		
		// copy  the rest of the left side of the array into the target array
		while(i <= middle) {
			numbers[k] = helper[i];
			k++;
			i++;
		}
	}
	
	public void sortBottomUpMerge(int[] input) {
		this.helper = new int[input.length];
		int width = 0;
		// each 1 element sub array is already sorted
		
		// Make successively longer sorted sub arrays of length 2, 4, 8, 16... until the whole array is sorted
		for(width = 1; width < input.length; width = 2 * width) {
			int i = 0;
			
			// array is full of sub arrays of length width... ?
			for(i = 0; i < input.length; i = i + 2 * width) {
				/*
				 * merge two sub arrays: input[i:i+width-1] and input[i+width:i+2*width-1] to helper[]
				 * or copy input[i:length-1] to helper[] ( if(i+width >= n)
				 */
				bottomUpMerge(input, i, Math.min(i + width,  input.length), Math.min(i + 2*width, input.length));
				//Util.print(helper, true);
				//Util.print(input, true);
			}
			
			/*
			 * Now work array helper[] is full of runs of length 2*width
			 * copy helper[] to input[] for next iteration
			 * a more efficient implementation would swap the roles of input and helper
			 */
			System.arraycopy(helper, 0, input, 0, helper.length);
			// now input[] is full of runs of length 2*width
		}
		//Util.print(helper, true);
		//Util.print(input, true);
	}

	private void bottomUpMerge(int[] input, int iLeft, int iRight, int iEnd) {
		int i0 = iLeft;
		int i1 = iRight;
		int j = 0;
		
		// while there are elements in the left or the right lists
		for(j = iLeft; j < iEnd; j++) {
			// if left list head exists and is <= existing right list head
			if(i0 <iRight && (i1 >= iEnd || input[i0] <= input[i1])) {
				helper[j] = input[i0];
				i0 = i0 +1;
			} else {
				helper[j] = input[i1];
				i1 = i1 + 1;
			}
		}
	}

	public static void main(String[] args) {
		SecureRandom rand = new SecureRandom();
		
		int size = 10;
		int range = 100;
		int[] input = new int[size];
		int[] input2 = new int[size];
		for(int i = 0; i < input.length; i++) {
			input[i] = rand.nextInt(range);
			input2[i] = input[i];
		}
		
		MergeSort ms = new MergeSort();
		Util.print(input, false);
		ms.sortTopDownMerge(input);
		Util.print(input, false);
		
		ms = new MergeSort();
		Util.print(input2, false);
		ms.sortBottomUpMerge(input2);
		Util.print(input2, false);
	}
}
