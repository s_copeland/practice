package com.saiyd.sorting;

import java.security.SecureRandom;

/*
 * 
 * Best: O(n log n)
 * Average: O(n log n)
 * Worst: O(n^2)
 * 
 */
public class QuickSort <T extends Comparable<T>> {
	private boolean debug;
	public QuickSort(boolean debug) {
		this.debug = debug;
	}
	public T[] sort(T[] input) {
		quickSort(input, 0, input.length -1, false);
		return input;
	}
	
	public T[] sortRandomized(T[] input) {
		quickSort(input, 0, input.length -1, true);
		return input;
	}

	private void quickSort(T[] input, int left, int right, boolean randomized) {
		if(left < right) {
			int partitionIndex = 0;
			if(randomized) {
				partitionIndex = randomizedPartition(input, left, right);
			} else {
				partitionIndex = partition(input, left, right);
			}
			
			quickSort(input, left, partitionIndex -1, randomized);
			quickSort(input, partitionIndex +1, right, randomized);
		}
	}

	private SecureRandom sr = new SecureRandom();
	private int randomizedPartition(T[] input, int left, int right) {
		// chose random pivot
		int pivotOffset = sr.nextInt(right - left); // choose a number between 0 and partition size, we use this
		int pivot = left + pivotOffset;
		
		return commonPartition(input, left, right, pivot);
	}
	
	private int partition(T[] input, int left, int right) {
		int pivot = selectPivot(left, right);
		
		return commonPartition(input, left, right, pivot);
	}

	private int commonPartition(T[] input, int left, int right, int pivot) {
		// swap
		T temp = input[pivot];
		input[pivot] = input[right];
		input[right] = temp;
		
		int store = left;
		for(int i = left; i < right; i++) {
			// if i is less than or equal to
			if(input[i].compareTo(input[right]) < 1) {
				/*
				 * This version of partition is different from the one in
				 * SelectionProblem. We set i to store to left (not left -1), so we must increment
				 * AFTER we swap otherwise we would be swapping he wrong value
				 * in the range of values greater than the pivot
				 */
				temp = input[i];
				input[i] = input[store];
				input[store] = temp;
				
				store++;
			}
		}
		
		temp = input[store];
		input[store] = input[right];
		input[right] = temp;
		
		if(debug)
			print(input, false);
		return store;
	}

	private int selectPivot(int left, int right) {
		return (left + right) / 2; // pick the mid point
	}
	
	private void print(T[] input, boolean indent){
		if(indent)
			System.out.print("\t");
		for(int i = 0; i < input.length; i++) {
			System.out.print(input[i]);
			System.out.print(" ");
		}
		System.out.println();
	}
	
	public static void main(String[] args) {
		QuickSort<Integer> qSort = new QuickSort<Integer>(false);
		SecureRandom rand = new SecureRandom();
		
		
		int size = 10;
		int range = 100;
		Integer[] input = new Integer[size];
		Integer[] input2 = new Integer[size];

		for(int i = 0; i < input.length; i++) {
			input[i] = rand.nextInt(range);
			input2[i] = input[i];
		}
		long start = System.nanoTime();
		qSort.sort(input);
		qSort.print(input, false);
		long end = System.nanoTime();
		System.out.println("Duration: " + (end - start));
		
		start = System.nanoTime();
		qSort.sort(input2);
		qSort.print(input2, false);
		end = System.nanoTime();
		System.out.println("Duration: " + (end - start));
	}
}
