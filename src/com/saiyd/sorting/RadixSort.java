package com.saiyd.sorting;

import java.security.SecureRandom;

/* 
 * Worst case performance	O(kN)
 * Worst case space complexity	O(k + N)
 * 	 where n is the number of keys, and k is the average key length
 * 
 * An LSD radix sort operates in O(nk) time, where n is the number of 
 * keys, and k is the average key length. This kind of performance for 
 * variable-length keys can be achieved by grouping all of the keys 
 * that have the same length together and separately performing an 
 * LSD radix sort on each group of keys for each length, from shortest 
 * to longest, in order to avoid processing the whole list of keys on 
 * every sorting pass.
 * 
 * D rounds of stable counting sort, where D is the number of digits
 * 
 * Definition
 *  Each key is first figuratively dropped into one level of buckets corresponding 
 *  to the value of the rightmost digit. Each bucket preserves the original order 
 *  of the keys as the keys are dropped into the bucket. There is a one-to-one 
 *  correspondence between the number of buckets and the number of values that 
 *  can be represented by the rightmost digit. Then, the process repeats with the 
 *  next neighbouring more significant digit until there are no more digits to process. In other words:
 * 
 *  Take the least significant digit (or group of bits, both being examples of radices) of each key.
 *  Group the keys based on that digit, but otherwise keep the original order of keys. (This is what makes the LSD radix sort a stable sort.)
 *  Repeat the grouping process with each more significant digit.
 *  The sort in step 2 is usually done using bucket sort or counting sort, which are efficient in this case since there are usually only a small number of digits.
 *  
 *  
 * In other words, we can sort an array of integers with range from 1 to nc if the numbers are represented in base n (or every digit takes \log_2(n) bits).
 * 
 * Postman's sort[edit]
 * The Postman's sort is a variant of bucket sort that takes advantage of a 
 * hierarchical structure of elements, typically described by a set of attributes. 
 * This is the algorithm used by letter-sorting machines in post offices: mail 
 * is sorted first between domestic and international; then by state, province 
 * or territory; then by destination post office; then by routes, etc. Since keys 
 * are not compared against each other, sorting time is O(cn), where c depends on 
 * the size of the key and number of buckets. This is similar to a radix sort that 
 * works "top down," or "most significant digit first."[4]
 * 
 */
public class RadixSort {
	private static final int BASE_10 = 10;

	public int[] sort(int[] input) {
		
		int max = findMax(input);
		CountingSort countSort = new CountingSort();
		
		// we assume the numbers are base 10
		for(int exponent = 1; (max / exponent) > 0; exponent *= BASE_10) {
			input = countSort.sortStable(input, max, exponent, BASE_10);
		}
		return input;
	}

	private int findMax(int[] input) {
		int max = input[0];
		for(int i=0; i < input.length; i++) {
			if(input[i] > max)
				max = input[i];
		}
		return max;
	}
	
	private void print(int[] input, boolean indent){
		if(indent)
			System.out.print("\t");
		for(int i = 0; i < input.length; i++) {
			System.out.print(input[i]);
			System.out.print(" ");
		}
		System.out.println();
	}
	
	public static void main(String[] args) {
		RadixSort radixSort = new RadixSort();
		SecureRandom rand = new SecureRandom();
		
		int size = 10;
		int range = 999;
		//int[] input = new int[size];
		int[] input = new int[8];
		int[] output;
		/*for(int i = 0; i < input.length; i++) {
			input[i] = rand.nextInt(range);
		}*/
		input[0] = 170;
		input[1] = 45;
		input[2] = 75;
		input[3] = 90;
		input[4] = 802;
		input[5] = 2;
		input[6] = 24;
		input[7] = 66;
		
		output = radixSort.sort(input);
		radixSort.print(output, false);
	}
}
