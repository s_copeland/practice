package com.saiyd.sorting;

import java.security.SecureRandom;

/*
 * An in-place comparison sort
 * 
 * O(n^2)
 */
public class SelectionSort <T extends Comparable<T>>{
	public T[] sort(T[] input) {
		int curMinIndex = 0 ;
		int i = 0;
		for(; i < input.length -1; i++) {
			curMinIndex = i; // assume the min is the first element
			
			for(int j = i + 1; j < input.length; j++) {
				// if this element is less then it is the new minimum
				if(input[j].compareTo(input[curMinIndex]) < 0) {
					// record the new minimum's index
					curMinIndex = j;
				}
			}
			
			if(curMinIndex != i) {
				T temp = input[i];
				input[i] = input[curMinIndex];
				input[curMinIndex] = temp;
			}
			
			print(input, false);
		}
		
		// I'm not sure if I need this

		return input;
	}
	
	private void print(T[] input, boolean indent){
		if(indent)
			System.out.print("\t");
		for(int i = 0; i < input.length; i++) {
			System.out.print(input[i]);
			System.out.print(" ");
		}
		System.out.println();
	}
	
	public static void main(String[] args) {
		SelectionSort<Integer> selSort = new SelectionSort<Integer>();
		SecureRandom rand = new SecureRandom();
		
		int size = 10;
		int range = 100;
		Integer[] input = new Integer[size];
		for(int i = 0; i < input.length; i++) {
			input[i] = rand.nextInt(range);
		}
		
		selSort.sort(input);
		selSort.print(input, false);
	}
}
