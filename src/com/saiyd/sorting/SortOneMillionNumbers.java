package com.saiyd.sorting;

/*
 * http://www.geeksforgeeks.org/tournament-tree-and-binary-heap/
 */
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.security.SecureRandom;

public class SortOneMillionNumbers {

	private static final String ONE_MILLION_NUMBERS_TXT = "one-million-numbers.txt";

	public static void main(String[] args) throws IOException {
		//createFile();
		//partitionFile();
		
		/*PartitionedTextFileNode ptn = new PartitionedTextFileNode("work/partition-1.txt");
		for(int i=0; i < 10; i++) {
			System.out.println(ptn.getCurrentValue());
			ptn.loadNext();
		}*/
	}

	private static void partitionFile() throws IOException {
		InputStreamReader isr = new InputStreamReader(new FileInputStream(ONE_MILLION_NUMBERS_TXT));
		String outputFilePrefix = "work/partition-";
		int partitionCount = 1;
		String outputFileSuffix = ".txt";
		PrintWriter pw = getNextPrintWriter(outputFilePrefix, partitionCount, outputFileSuffix);
		
		char delimeter = ',';
		char curChar = 0;
		int delimeterCount = 0;
		
		while((curChar = (char) isr.read()) != -1) {
			if(curChar == delimeter)
				delimeterCount++;
			
			if(delimeterCount == 1000000) {
				pw.flush();
				pw.close();
				partitionCount++;
				pw = getNextPrintWriter(outputFilePrefix, partitionCount, outputFileSuffix);
				delimeterCount = 0;
			} else {
				pw.write(curChar);
			}
		}
		
		pw.flush();
		pw.close();
	}
	
	private static PrintWriter getNextPrintWriter(String prefix, int partitionCount, String suffix) throws FileNotFoundException {
		return new PrintWriter(new FileOutputStream(prefix + partitionCount + suffix));
	}
	
	private static void createFile() throws FileNotFoundException {
		SecureRandom rand = new SecureRandom();
		PrintWriter pw = new PrintWriter(new BufferedOutputStream(new FileOutputStream(ONE_MILLION_NUMBERS_TXT), 1024 * 1024));
		int size = 1000000000;
		int range = 1000000;
		for(int i = 0; i < size; i++) {
			pw.write(Integer.toString(rand.nextInt(range)));
			pw.write(",");
		}
		
		pw.flush();
		pw.close();
	}
	
	static class PartitionedTextFileNode {
		private InputStreamReader reader;
		private int currentValue = Integer.MAX_VALUE;
		
		public PartitionedTextFileNode(String path) throws FileNotFoundException {
			reader = new InputStreamReader(new FileInputStream(path));
			// set the initial value
			loadNext();
		}
		
		public int getCurrentValue() {
			return currentValue;
		}

		public void loadNext() {
			StringBuilder number = new StringBuilder();
			try {
				char curChar = (char) reader.read();
				while(curChar != -1) {
					if(curChar == ',') {
						break;
					} else {
						number.append(Character.valueOf(curChar));
					}
					curChar = (char) reader.read();
				}
				// try to parse a number
				currentValue = Integer.parseInt(number.toString());
			} catch(NumberFormatException | IOException e) {
				currentValue = Integer.MAX_VALUE;
			}
		}
	}
}
