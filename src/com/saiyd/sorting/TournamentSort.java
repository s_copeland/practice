package com.saiyd.sorting;

/*
 * Worst case performance	O(n log n)
 * Average case performance	O(n log n)
 * 
 * Tournament sort is a sorting algorithm. It improves upon the naive 
 * selection sort by using a priority queue to find the next element 
 * in the sort. In the naive selection sort, it takes O(n) operations 
 * to select the next element of n elements; in a tournament sort, it 
 * takes O(log n) operations (after building the initial tournament in 
 * O(n)). Tournament sort is a variation of heapsort.
 * 
 * Tournament tree is a form of min (max) heap which is a complete binary 
 * tree. Every external node represents a player and internal node represents 
 * winner. In a tournament tree every internal node contains winner and 
 * every leaf node contains one player.
 * 
 * There will be N � 1 internal nodes in a binary tree with N leaf 
 * (external) nodes. For details see this post (put n = 2 in equation given in the post)
 * 
 * The largest/smallest key must have been in lgN(5) comparisons is N = 32
 * 
 * #########################
 * http://xlinux.nist.gov/dads//HTML/tournamentSort.html
 * Note: Heapsort is sometimes called tournament sort. Consider the tree of 
 * matches in a single-elimination sports tournament, where players' abilities 
 * are a total order. The tournament tree has the heap property. When a winner 
 * is found (the least element is chosen), the winner is removed and the 
 * appropriate matches replayed.
 * 
 * In Using Tournament Trees to Sort, Center for Advanced Technology in 
 * Telecommunications, Polytechnic University, Brooklyn, New York, C.A.T.T 
 * Technical Report 86-13, Alexander Stepanov and Aaron Kershenbaum extend 
 * the observation that a tournament can find the winner in N comparisons 
 * and the 2nd in log N more comparisons.
 * 
 * ##########
 * 
 * Begin by running a standard tournament. In the first round compare 3, and 54,
 * 23 and 7, etc. Then compare the "winners", or larger numbers. Continue comparing
 * until only one number remains. That number is the largest. It is 54.
 * To find the second largest, the only candidates are 3, 23, and 9
 * 
 *            ------54-------
 *           /               \
 *          /                 \ 
 *         54                  9
 *       /    \               / \
 *      /      \             /   \
 *     /        \           /     \
 *   54          23        9       4
 *  /  \       /   \      / \     / \
 * 3    54    23    7    9   2   1   4
 * 
 * 
 * Run tournament to find the largest of the candidates for 2nd largest overall.
 * Note that we compare 3 and 23 first due to the order of
 * elimination in the previous tournament. 23 is the 2nd largest;
 * candidates for third are 3, 7, and 9
 * 
 *      23
 *     /  \
 *    23   9
 *  /   \
 * 3    23
 * 
 * 
 * 9 is the third largest, 2, 4, 5 are candiates for fourth
 * 
 *      9
 *     / \
 *    7   9
 *  /  \
 * 3    7
 * 
 * 7 is fourth largest, 4 and 3 are candidates for fifth
 * 
 *      7
 *     / \
 *    4   7
 *  /  \
 * 3    4
 * 
 * 4 is the fifth largest, 2, 3, 1 are candidates for sixth
 * 
 *   4
 *  / \
 * 3   4
 * 
 * 
 * 3 is the sixth largest, 1 and 2 are candidates for seventh largest,
 * but the comparison is already done, and 2 is seventh, 1 is eighth
 * 
 *      3
 *     / \
 *    2   3
 *  /  \
 * 2    1
 */
public class TournamentSort {

}
