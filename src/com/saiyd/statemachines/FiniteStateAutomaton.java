package com.saiyd.statemachines;

public class FiniteStateAutomaton {
	private boolean[] acceptingStates = {false, false, false, true };
	
	public final char SYMBOL_A = 'A';
	private final int SYMBOL_A_INDEX = 1;
	public final char SYMBOL_B = 'B';
	private final int SYMBOL_B_INDEX = 2;
	public final char SYMBOL_C = 'C';
	private final int SYMBOL_C_INDEX = 3;
	
	private int[][] transitionTable = {
		// Start=0, A=1, B=2, C=3 , input symbol	
		{ 0, 1, 0, 0 }, // Start state
		{ 0, 1, 2, 0 }, // A state
		{ 0, 1, 0, 3 }, // B state
		{ 0, 1, 0, 0 }  // C state
	};
	
	private int curState;
	
	public boolean matches(String input) {
		// re initialize the state
		curState = 0;
		
		for(int i = 0; i < input.length(); i++) {
			transition(input.charAt(i));
		}
		return acceptingStates[curState];
	}
	
	public int contains(String input) {
		int count = 0;
		// re initialize the state
		curState = 0;
		
		for(int i = 0; i < input.length(); i++) {
			if(transition(input.charAt(i)))
				count++;
		}
		return count;
	}

	private boolean transition(char curSymbol) {
		boolean accepted = false;
		switch(curSymbol) {
		case SYMBOL_A:
			curState = transitionTable[curState][SYMBOL_A_INDEX];
			break;
		case SYMBOL_B:
			curState = transitionTable[curState][SYMBOL_B_INDEX];
			break;
		case SYMBOL_C:
			curState = transitionTable[curState][SYMBOL_C_INDEX];
			break;
		default:
			throw new IllegalArgumentException("Unknown symbol encountered, " + curSymbol);
		}
		return acceptingStates[curState];
	}
	
	public static void main(String[] args) {
		FiniteStateAutomaton fsa = new FiniteStateAutomaton();
		String input = "ABC";
		test(fsa, input);
		
		input = "";
		test(fsa, input);
		
		input = "A";
		test(fsa, input);
		
		input = "B";
		test(fsa, input);
		
		input = "C";
		test(fsa, input);
		
		input = "AB";
		test(fsa, input);
		
		input = "BA";
		test(fsa, input);
		
		input = "CBA";
		test(fsa, input);
		
		input = "ABCC";
		test(fsa, input);
		
		input = "ABCC";
		testContains(fsa, input);
		
		input = "AABCCABCABCAABBCCCAA";
		testContains(fsa, input);
	}

	private static void test(FiniteStateAutomaton fsa, String input) {
		System.out.println("'" + input + "' matches = " + fsa.matches(input));
	}
	
	private static void testContains(FiniteStateAutomaton fsa, String input) {
		System.out.println("'" + input + "' contains = " + fsa.contains(input));
	}
}
