package com.saiyd.statemachines;

import java.util.Stack;

/* 
 * A push down automaton that accepts string with the same number of 
 * 'a' characters followed a single 'c' followed by the same number of 'b' characters.
 * 
 * Example string
 * 
 * aacbb
 */
public class PushDownAutomaton2 {
	private boolean[] acceptingStates = {false, false, false, true };
	
	public final char SYMBOL_$ = '$';  // start symbol
	private final int SYMBOL_$_INDEX = 0;
	public final char SYMBOL_A = 'a';
	private final int SYMBOL_A_INDEX = 1;
	public final char SYMBOL_C = 'c';
	private final int SYMBOL_C_INDEX = 2;	
	public final char SYMBOL_B = 'b';
	private final int SYMBOL_B_INDEX = 3;	
	
	private int[][] transitionTable = {
			// $, A, C, B
			{ 0, 1, 0, 0}, // Start state
			{ 0, 1, 2, 0}, // State 1
			{ 0, 2, 0, 3},  // State 2
			{ 0, 0, 0, 3}  // Final state
	};
	
	private final int STACK_NO_OP = 0;
	private final int STACK_PUSH = 1;
	private final int STACK_POP = 2;
	private int[] stackOperationTable = {
			// operation for $, a, c, b
			0, 1, 0, 2
	};
	
	private Stack<Character> symbolStack = new Stack<>();
	private int curState;
	
	public PushDownAutomaton2() {
		symbolStack.push(SYMBOL_$);
	}
	
	public boolean matches(String input) {
		// re initialize the state
		curState = 0;
		
		for(int i = 0; i < input.length(); i++) {
			transition(input.charAt(i));
		}
		//System.out.println(symbolStack.size());
		return acceptingStates[curState] && symbolStack.size() == 1;
	}
	
	private boolean transition(char curSymbol) {
		//int stackTopSymbolIndex = getSymbolIndex(symbolStack.peek());
		
		switch(curSymbol) {
		case SYMBOL_A:
			curState = transitionTable[curState][SYMBOL_A_INDEX];
			break;
		case SYMBOL_B:
			curState = transitionTable[curState][SYMBOL_B_INDEX];
			break;
		case SYMBOL_C:
			curState = transitionTable[curState][SYMBOL_C_INDEX];
			break;
		default:
			throw new IllegalArgumentException("Unknown symbol encountered, " + curSymbol);
		}
		
		doStackOperation(curState, curSymbol);
		
		return acceptingStates[curState] && symbolStack.size() == 1;
	}
	
	private int getSymbolIndex(char character) {
		switch(character) {
		case SYMBOL_A:
			return SYMBOL_A_INDEX;
		case SYMBOL_B:
			return SYMBOL_B_INDEX;
		case SYMBOL_C:
			return SYMBOL_C_INDEX;
		case SYMBOL_$:
			return SYMBOL_$_INDEX;
		default:
			throw new IllegalArgumentException("Unknown character encountered, " + character);
		}
	}
	
	private void doStackOperation(int symbolIndex, char symbol) {
		int op = stackOperationTable[symbolIndex];
		
		switch(op) {
		case STACK_NO_OP:
			break;
		case STACK_PUSH:
			symbolStack.push(symbol);
			break;
		case STACK_POP:
			symbolStack.pop();
			break;
		}
	}
	
	public static void main(String[] args) {
		PushDownAutomaton2 pda = new PushDownAutomaton2();
		
		String input = "acb";
		test(pda, input);
		
		input = "aacbb";
		test(pda, input);
		
		input = "a";
		test(pda, input);
		
		input = "aab";
		test(pda, input);
		
		input = "abb";
		test(pda, input);
	}

	private static void test(PushDownAutomaton2 pda, String input) {
		System.out.println("'" + input + "' matches = " + pda.matches(input));
	}

}
