package com.saiyd.stringmatching;

import java.util.ArrayList;
import java.util.List;

/*
 * Runtime: Theta(m +n)  where n is the length of the text and n is the length of the pattern
 * 
 * Searches for occurrences of a "word" W within a main "text string" S by employing the 
 * observation that when a mismatch occurs, the word itself embodies sufficient information 
 * to determine where the next match could begin, thus bypassing re-examination of previously 
 * matched characters.
 * 
 * The KMP algorithm does not have the horrendous worst-case performance of the straightforward 
 * algorithm. KMP spends a little time precomputing a table (on the order of the size of W[], O(n)), 
 * and then it uses that table to do an efficient search of the string in O(k). Where k is the length
 * of the text to search.
 */
public class KnuthMorrisPratt {
	/*
	 * +public boolean match(char[] text, char[] pattern) { boolean ret = false;
	 * // let n be the size of the text, m the // size of the pattern, and F[] -
	 * the // "failure function"
	 * 
	 * int[] F = createFailureFunction(pattern);
	 * 
	 * int i = 0; // the initial state of the automaton is // the empty string
	 * 
	 * int j = 0; // the first character of the text
	 * 
	 * for( ; i < text.length ; ) { if(j == text.length) break; // we reached
	 * the end of the text
	 * 
	 * // if the current character of the text "expands" the // current match
	 * if(text[j] == pattern[i]) { i++; // change the state of the automaton
	 * j++; // get the next character from the text if(i == pattern.length) {//
	 * match found ret = true; } }
	 * 
	 * // if the current state is not zero (we have not // reached the empty
	 * string yet) we try to // "expand" the next best (largest) match else if(i
	 * > 0) i = F[i];
	 * 
	 * // if we reached the empty string and failed to // "expand" even it; we
	 * go to the next // character from the text, the state of the // automaton
	 * remains zero else j++; }
	 * 
	 * return ret; }
	 */

	public List<Integer> match(char[] text, char[] pattern) {
		ArrayList<Integer> ret = new ArrayList<>();
		int n = text.length;
		int m = pattern.length;
		int[] F = createNFA2(pattern);
		int q = 0;
		for (int i = 0; i < n && q < m; i++) {
			while (q > 0 && pattern[q] != text[i]) {
				q = F[q - 1];
			}
			if (pattern[q] == text[i])
				q++;
			if (q == m) {
				ret.add(n - m + 1);
				System.out.println("Pattern occurs with shift " + (i - m + 1));
				q = F[q - 1];
			}
		}
		return ret;
	}

    public int search(char[] text, char[] pattern) {
        int M = pattern.length;
        int N = text.length;
        int[] F = createNFA(pattern);
        int i, j;
        for (i = 0, j = 0; i < N && j < M; i++) {
            while (j >= 0 && text[i] != pattern[j])
                j = F[j];
            j++;
        }
        if (j == M) {
        	System.out.println("Match found at " + (i - M));
        	return i - M;
        }
        return N;
    }
    
	/*
	 * private int[] createFailureFunction(char[] pattern) { // Pay attention!
	 * // the prefix under index i in the table above is // is the string from
	 * pattern[0] to pattern[i - 1] // inclusive, so the last character of the
	 * string under // index i is pattern[i - 1]
	 * 
	 * // let m be the length of the pattern int[] F = new int[pattern.length +
	 * 1];
	 * 
	 * F[0] = F[1] = 0; // always true int j = 0; for (int i = 2; i <=
	 * pattern.length; i++) { // j is the index of the largest next partial
	 * match // (the largest suffix/prefix) of the string under // index i - 1 j
	 * = F[i - 1]; for (;;) { // check to see if the last character of string i
	 * - // - pattern[i - 1] "expands" the current "candidate" // best partial
	 * match - the prefix under index j if (pattern[j] == pattern[i - 1]) { F[i]
	 * = j + 1; break; } // if we cannot "expand" even the empty string if (j ==
	 * 0) { F[i] = 0; break; } // else go to the next best "candidate" partial
	 * match j = F[j]; } } return F; }
	 */

	/*
	 * private int[] createPrefixFunction(char[] pattern) { int[] F = new
	 * int[pattern.length]; F[0] = F[1] = 0; // always true int k = 0;
	 * 
	 * for(int q = 2; q < pattern.length; q++) { while(k > 0 && pattern[k+1] !=
	 * pattern[q]) { k = F[k]; } if(pattern[k+1] == pattern[q]) k = k + 1; F[q]
	 * = k; } return F; }
	 */

	/* The returned array encapsulates knowledge about how the pattern matches
	 * against shifts of itself.
	 * 
	 * The goal of the table is to allow the algorithm not to match any character 
	 * of S more than once. The key observation about the nature of a linear search 
	 * that allows this to happen is that in having checked some segment of the 
	 * main string against an initial segment of the pattern, we know exactly at 
	 * which places a new potential match which could continue to the current 
	 * position could begin prior to the current position. In other words, we 
	 * "pre-search" the pattern itself and compile a list of all possible fallback 
	 * positions that bypass a maximum of hopeless characters while not sacrificing 
	 * any potential matches in doing so.
	 * 
	 * The index of next[] is the length of the string in the text we have matched so far
	 * 		index 0 of next[] is for a string of length 0
	 * 		index 1 of next[] is for a string of length 1
	 * 		...
	 * 
	 * Each next[i] is the length of the longest prefix of P that is a proper suffix of
	 * the pattern of length i
	 * 		If i = 0, then the pattern is empty string
	 * 		If i = 1, then the pattern is the first char
	 * 		If i = 2, then teh pattern is the first two chars
	 */
	private int[] createNFA(char[] pattern) {
		int[] next = new int[pattern.length];
		int j = -1;
		for (int i = 0; i < pattern.length; i++) {
			if (i == 0)
				next[i] = -1;
			else if (pattern[i] != pattern[j])
				next[i] = j;
			else
				next[i] = next[j];
			while (j >= 0 && pattern[i] != pattern[j]) {
				j = next[j];
			}
			j++;
		}

		for (int i = 0; i < pattern.length; i++)
			System.out.println("next[" + i + "] = " + next[i]);
		return next; 
	}
	
	private int[] createNFA2(char[] pattern) {
		int[] next = new int[pattern.length];
		//next[0] = 0;
		int k = 0;
		
		for(int i = 1; i < pattern.length; i++) {
			while(k > 0 && pattern[k] != pattern[i])
				k = next[k - 1];
			
			if(pattern[k] == pattern[i])
				k = k + 1;
			
			next[i] = k;
		}

		for (int i = 0; i < pattern.length; i++)
			System.out.println("next[" + i + "] = " + next[i]);
		return next; 
	}

	public static void main(String[] args) {
		KnuthMorrisPratt kmp = new KnuthMorrisPratt();
		// String pattern = "abb";
		// kmp.createFailureFunction(pattern.toCharArray());
		// kmp.createPrefixFunction(pattern.toCharArray());
		// kmp.createNFA(pattern.toCharArray());

		String text = "";
		String pattern = "";
		// test(n, text, pattern);

		text = "";
		pattern = "a";
		// test(n, text, pattern);

		text = "a";
		pattern = "";
		// test(n, text, pattern);

		text = "abbabbcaab";
		pattern = "abbcab";
		test(kmp, text, pattern);
	}

	private static void test(KnuthMorrisPratt n, String text, String pattern) {
		n.search(text.toCharArray(), pattern.toCharArray());
		n.match(text.toCharArray(), pattern.toCharArray());

		/*
		 * boolean found = n.match(text.toCharArray(), pattern.toCharArray());
		 * System.out.println("The pattern '" + pattern + "' returned found = "
		 * + found); System.out.println();
		 */
	}
}
