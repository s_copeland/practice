package com.saiyd.stringmatching;

import java.util.ArrayList;
import java.util.List;

/*
 * The "naive" approach is easy to understand and implement but it can be too slow in some cases. 
 * If the length of the text is n and the length of the pattern m, in the worst case it may take 
 * as much as (n * m) iterations to complete the task.
 */
public class Naive {
	public List<Integer> match(char[] text, char[] pattern) {
		List<Integer> matchStartingIndex = new ArrayList<>();
		
		int n = text.length;
		int m = pattern.length;
		
		for(int s = 0; s <= (n - m); s++) {
			if(compare(text, s, pattern)) {
				matchStartingIndex.add(s);
			}
		}
		return matchStartingIndex;
	}
	
	private boolean compare(char[] text, int offest, char[] pattern) {
		if(text.length == 0 || pattern.length == 0)
			return false;
		int count = 0;
		for(int i = 0; i < pattern.length;  i++) {
			if(text[offest + i] != pattern[i]) {
				return false;
			} else {
				count++;
			}
		}
		return count == pattern.length ? true : false;
	}
	
	public static void main(String[] args) {
		Naive n = new Naive();
		String text = "";
		String pattern = "";
		test(n, text, pattern);
		
		text = "";
		pattern = "a";
		test(n, text, pattern);
		
		text = "a";
		pattern = "";
		test(n, text, pattern);
		
		text = "abbabbaab";
		pattern = "ab";
		test(n, text, pattern);
	}

	private static void test(Naive n, String text, String pattern) {
		List<Integer> l = n.match(text.toCharArray(), pattern.toCharArray());
		System.out.println("The pattern '" + pattern + "' starts at the following indices in the text.");
		System.out.println(l);
	}

}
