package com.saiyd.stringmatching;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/*
 *  Uses hashing to find any one of a set of pattern strings in a text. For text of length 
 *  n and p patterns of combined length m, its average and best case running time is O(n+m) 
 *  in space O(p), but its worst-case time is O(nm).
 *  
 *  
 * This is actually the "naive" approach augmented with a powerful programming technique 
 * - the hash function.
 *
 * Every string s[] of length m can be seen as a number H written in a positional numeral 
 * system in base B (B >= size of the alphabet used in the string):
 *
 * H = s[0] * B(m - 1) + s[1] * B(m - 2) + … + s[m - 2] * B1 + s[m - 1] * B0
 *
 * If we calculate the number H (the hash value) for the pattern and the same number for 
 * every substring of length m of the text than the inner loop of the "naive" method will 
 * disappear - instead of comparing two strings character by character we will have just 
 * to compare two integers.
 *
 * A problem arises when m and B are big enough and the number H becomes too large to fit 
 * into the standard integer types. To overcome this, instead of the number H itself we 
 * use its remainder when divided by some other number M. To get the remainder we do not 
 * have to calculate H. Applying the basic rules of modular arithmetic to the above expression:
 *
 * A + B = C => (A % M + B % M) % M = C % M
 * A * B = C => ((A % M) * (B % M)) % M = C % M
 *
 * We get:
 *
 * H % M = (((s[0] % M) * (B(m - 1) % M)) % M + ((s[1] % M) * (B(m - 2) % M)) % M +…
 * …+ ((s[m - 2] % M) * (B1 % M)) % M + ((s[m - 1] % M) * (B0 % M)) % M) % M
 *
 * The drawback of using remainders is that it may turn out that two different strings map 
 * to the same number (it is called a collision). This is less likely to happen if M is 
 * sufficiently large and B and M are prime numbers. Still this does not allow us to 
 * entirely skip the inner loop of the "naive" method. However, its usage is significantly 
 * limited. We have to compare the "candidate" substring of the text with the pattern 
 * character by character only when their hash values are equal.
 * 
 * 
 * Unfortunately, there are still cases when we will have to run the entire inner loop of 
 * the "naive" method for every starting position in the text -- for example, when searching 
 * for the pattern "aaa" in the string "aaaaaaaaaaaaaaaaaaaaaaaaa" -- so in the worst case 
 * we will still need (n * m) iterations. How do we overcome this?
 */
public class RabinKarp {

	public List<Integer> match(char[] text, char[] pattern, int radix, int mod) {
		List<Integer> matchStartingIndex = new ArrayList<>();

		if (text.length == 0 || pattern.length == 0)
			return matchStartingIndex;

		int n = text.length;
		int m = pattern.length;
		long h = (int) Math.pow(radix, m - 1);
		long p = 0;
		long t = 0;

		// pre processing
		// Calculate the hash value of pattern and first window of text
		for (int i = 0; i < m; i++) {
			p = (radix * p) + pattern[i] % mod;
			t = (radix * t) + text[i] % mod;
		}

		// Slide the pattern over text one by one
		for (int s = 0; s <= (n - m); s++) {
			// Check the hash values of current window of text and pattern
			// If the hash values match then only check for characters on by one
			if (p == t) {
				if (compare(text, s, pattern))
					matchStartingIndex.add(s);
			}

			// Calculate hash value for next window of text: Remove leading
			// digit,
			// add trailing digit
			if (s < (n - m)) {
				t = (radix * (t - (text[s] * h)) + text[s + m]) % mod;
			}
		}
		return matchStartingIndex;
	}

	private boolean compare(char[] text, int offest, char[] pattern) {
		if (text.length == 0 || pattern.length == 0)
			return false;
		int count = 0;
		for (int i = 0; i < pattern.length; i++) {
			if (text[offest + i] != pattern[i]) {
				return false;
			} else {
				count++;
			}
		}
		return count == pattern.length ? true : false;
	}

	// a random 31-bit prime
	private static long longRandomPrime() {
		BigInteger prime = BigInteger.probablePrime(31, new Random());
		return prime.longValue();
	}

	public static void main(String[] args) {
		RabinKarp n = new RabinKarp();
		String text = "";
		String pattern = "";
		// test(n, text, pattern);

		text = "";
		pattern = "a";
		// test(n, text, pattern);

		text = "a";
		pattern = "";
		// test(n, text, pattern);

		text = "abbabbaab";
		pattern = "ab";
		test(n, text, pattern);
	}

	private static void test(RabinKarp n, String text, String pattern) {
		List<Integer> l = n.match(text.toCharArray(), pattern.toCharArray(),
				26, (int) longRandomPrime());
		System.out.println("The pattern '" + pattern
				+ "' starts at the following indices in the text.");
		System.out.println(l);
	}

}
