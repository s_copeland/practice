package com.saiyd.stringmatching;

public class Suffix {

	public boolean isSuffix(char[] text, char[] suffix) {
		boolean ret = false;
		
		if(suffix.length == 0 || text.length == 0)
			return false;
		
		if(suffix.length > text.length)
			return false;
		
		int count = 0;
		for(int i = 1; i <= suffix.length; i++) {
			if(suffix[suffix.length - i] == text[text.length - i]) {
				count++;
			} else {
				break;
			}
		}
		
		if(count == suffix.length)
			ret = true;
		return ret;
	}
	
	public static void main(String[] args) {
		Suffix s = new Suffix();
		String text = "";
		String suffix = "";

		runTest(s, text, suffix);
		
		text = "a";
		suffix = "";
		runTest(s, text, suffix);
		
		text = "";
		suffix = "a";
		runTest(s, text, suffix);
		
		text = "a";
		suffix = "a";
		runTest(s, text, suffix);

		text = "ab";
		suffix = "a";
		runTest(s, text, suffix);
		
		text = "ab";
		suffix = "ab";
		runTest(s, text, suffix);
		
		text = "ab";
		suffix = "b";
		runTest(s, text, suffix);
		
		text = "waterloo";
		suffix = "ter";
		runTest(s, text, suffix);
		
		text = "waterloo";
		suffix = "water";
		runTest(s, text, suffix);
	}
	
	private static void runTest(Suffix s, String text, String suffix) {
		System.out.println("text = '" + text + "'");
		System.out.println("suffix = '" + suffix + "'");
		System.out.println("isSuffix = " + s.isSuffix(text.toCharArray(), suffix.toCharArray()));
		System.out.println();
	}

}
